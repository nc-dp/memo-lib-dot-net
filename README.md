# MeMo-lib #
The main purpose of this library is to handle the XML serialization and deserialization of MeMo messages.
It also provides support for handling MeMo messages contained in zip archives.

## Message serialization/dezerialization

#### Serialization
##### Building the Message object
To construct the Message object, you can use the provided builder classes which are placed in the 
Dk.Digst.Digital.Post.Memolib.Builder namespace.

Here is an example of using a builder:

```csharp
MessageBuilder.NewBuilder()
    .MessageHeader(MessageHeader().Build())
    .MessageBody(MessageBody().Build());
```

##### Handling File content
The MeMo message can contain attached files (pdf, text, html, etc.) and they are contained in the
content field of the File class.
It is possible to include a reference to a file in the file system which will be included in 
the message when the message is serialized, to avoid having the file in memory while building.

Example of providing a reference to a file, which will be Base64 encoded by the MeMo lib during 
serialization of the message:

```csharp
FileContentBuilder.NewBuilder().Location("/filesystem/document.pdf");
```

Example of providing the Base64 encoded content:

```csharp
FileContentBuilder.NewBuilder().Base64Content("ABCJBEV");
```

##### Writing the MeMo XML message to a Stream
The message can be written to a Stream by using the MeMoStreamWriter.

```csharp
var stream = new MemoryStream();
var writer = MeMoWriterFactory.CreateWriter(stream);
writer.Write(message);
stream.Flush();
stream.Position = 0;
```

#### Deserialization
A IMeMoParser can be used to deserialize a MeMo message. XML schema validation can be enabled when using the
MeMoParserFactory to instantiate a parser.

```csharp
Message message;
using (Stream stream = MeMoTestDataProvider.StreamMeMoMessageFullExample())
{
    using (IMeMoParser parser = MeMoParserFactory.CreateParser(stream, true))
    {
        message = parser.Parse();
    }
}
```

##### Streaming using Visitors
A MeMoParser can also be used to deserialize specific elements of a MeMo message without reading the entire message.
XML Schema validation can be enabled if needed when using the MeMoParserFactory to instantiate the parser. 

The implementation is based on the Visitor pattern, allowing one or more custom visitors to process only the needed elements. 
The following example shows how to instantiate the parser and read the xml stream:

```csharp
Stream stream = File.OpenRead(path);;
IMeMoParser parser = MeMoParserFactory.CreateParser(stream);

/* traverse the xml stream */
parser.Traverse(new List<IMeMoStreamVisitor> { visitor1, visitor2, visitor3 });
```

#### Visitors
A visitor must implement the interface IMeMoStreamVisitor. The simplest way to create a visitor is to use the MeMoStreamVisitorFactory. 
At the moment, two types of visitors can be created by the factory: Processor and Consumer.

##### Processor
The most simple to use is a processor which implements the interface IMeMoStreamProcessorVisitor and can be used to parse certain MeMo classes
within the Message. The factory also provides a method so that the parsed MeMo class can be filtered by the parent element. This could be relevant
if e.g. only Files related to a TechnicalDocument should be returned.

Example of creating and using a processor: 
```csharp
IMeMoStreamProcessorVisitor<File> visitor = MeMoStreamVisitorFactory.CreateProcessor<File>(typeof(TechnicalDocument));
parser.Traverse(new List<IMeMoStreamVisitor> {visitor});
/* Get files in technical document */
List<File> files = visitor.GetResult();
```
If we know beforehand, that the processor will only return one element, another option is to call GetSingleResult() instead:
```csharp
IMeMoStreamProcessorVisitor<MessageHeader> visitor = MeMoStreamVisitorFactory.CreateProcessor<MessageHeader>();
parser.traverse(visitor2);
Optional<MessageHeader> header = visitor.GetSingleResult();
```

##### Consumer
The MeMoStreamVisitorFactory can also create a consumer, which can be used if more control is needed. The visitor must be defined
by a Consumer delegator/lambda expression which takes a MeMoStreamCursor as parameter. The MeMoStreamCursor provides 
methods to either:
- parse the whole MeMo class including child MeMo classes.
- only parse the elements (simple fields) of the MeMo class.
- traverse the child MeMo classes.

It should be noted that the state of the underlying xml stream will be affected by these methods since the XmlReader cursor will move
forward. This means that parsing the whole MeMo class would prevent subsequent traversal of the child MeMo classes.

An example of a visitor, which is only applied to Actions. It parses the elements and collects the startDateTime element.
```csharp
List<DateTime> actionStartDateTimes = new List<DateTime>();

IMeMoStreamVisitor visitor = MeMoStreamVisitorFactory.CreateConsumer<Action>(
  c => {
    MeMoClassElements elements = c.ParseMeMoClassElements();
    actionStartDateTimes.Add(elements.Get(nameof(Action.startDateTime)));
  });
```
An example of a visitor, which parses the elements an subsequently traverses the child MeMo classes applying other visitors:
```csharp
IMeMoStreamVisitor visitor2 = MeMoStreamVisitorFactory.CreateProcessor<Action>();

List<DateTime> actionStartDateTimes = new List<DateTime>();

IMeMoStreamVisitor visitor = MeMoStreamVisitorFactory.CreateConsumer<Action>(
  c => {
    MeMoClassElements elements = c.ParseMeMoClassElements();
    actionStartDateTimes.Add(elements.Get(nameof(Action.startDateTime)));
    c.TraverseChildren(new List<IMeMoStreamVisitor>{visitor2});
  });
```
As the consumer does not return a result, it is expected to operate via side-effects.
The side effect could be updating the state of an object or adding the result to a collection.

### MeMo Container serialization/deserialization

A MeMo container is an archive containing a list of MeMo messages describing the 
contents of the container. As of now, the supported archive format is Zip. 

#### Serialization
Example of writing two MeMo messages as entries to the MeMo container:
```csharp

Stream stream = File.Create(_path);
MeMoContainerOutputStream meMoContainer = new MeMoContainerOutputStream(stream);
MeMoContainerWriter containerWriter = new MeMoContainerWriter(meMoContainer);

/* write first entry */
containerWriter.WriteEntry("memo_1.xml", message1);

/* write second entry */
containerWriter.WriteEntry("memo_2.xml", message2);
```

#### Deserialization
Example of reading a MeMo container:
```csharp
/* create reader */
ZipInputStream stream = new ZipInputStream(File.OpenRead(path));
IIterableMeMoContainer iterator = new ZipEntryIterator(stream);
MeMoContainerReader reader = new MeMoContainerReader(iterator);

/* read all entries */
List<Message> result = new List<Message>();
while (reader.HasEntry())
    result.Add(reader.ReadEntry(validate).Get());
```