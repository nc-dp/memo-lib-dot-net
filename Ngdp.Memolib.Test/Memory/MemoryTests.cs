﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Dk.Digst.Digital.Post.Memolib.Builder;
using Dk.Digst.Digital.Post.Memolib.Factory;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Parser;
using Dk.Digst.Digital.Post.Memolib.Test.Util;
using Dk.Digst.Digital.Post.Memolib.Util;
using Dk.Digst.Digital.Post.Memolib.Visitor;
using Dk.Digst.Digital.Post.Memolib.Writer;
using NUnit.Framework;
using File = Dk.Digst.Digital.Post.Memolib.Model.File;

namespace Dk.Digst.Digital.Post.Memolib.Test.Memory
{
    [TestFixture, SingleThreaded]
    public class MemoryTests
    {
        // If running on Linux, PrivateMemorySize64 will always be 0 in .NET core 2.0
        // These tests are therefore ignored on linux
        // See https://github.com/dotnet/runtime/issues/23284

        private static long GetCurrentlyUsedMemoryInMb()
        {
            Process.GetCurrentProcess().Refresh();
            return Process.GetCurrentProcess().PrivateMemorySize64 / 1024 / 1024;
        }

        private static bool IsNotRunningWindows()
        {
            return !new List<PlatformID>
            {
                PlatformID.Win32NT,
                PlatformID.Win32S,
                PlatformID.Win32Windows,
                PlatformID.WinCE
            }.Contains(Environment.OSVersion.Platform);
        }


        private static Message BuildLargerThan100MbMessage(bool external)
        {
            MessageBuilder builder =
                MessageBuilder.NewBuilder().MessageHeader(MessageHeaderBuilder.NewBuilder().Build());

            List<File> files = new List<File>();
            for (int i = 0; i < 100; i++)
                if (external)
                    files.Add(
                        FileBuilder.NewBuilder().Content(
                            FileContentBuilder.NewBuilder().Location(
                                MeMoTestDataProvider.OneMbFileLocation()).Build()).Build());
                else

                    files.Add(
                        FileBuilder.NewBuilder().Content(
                            FileContentBuilder.NewBuilder().Base64Content(
                                    new StreamReader(System.IO.File.OpenRead(MeMoTestDataProvider.OneMbFileLocation()))
                                        .ReadToEnd())
                                .Build()).Build());
            return builder.MessageBody(
                MessageBodyBuilder.NewBuilder().MainDocument(
                    MainDocumentBuilder.NewBuilder().File(files).Build()).Build()).Build();
        }

        [SetUp]
        public void Setup()
        {
            if (IsNotRunningWindows()) Assert.Ignore();
        }

        [Test]
        public void ParserCanParseWholeMemo()
        {
            // Given
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources/Files/Xml/largememo.xml");

            Message message = BuildLargerThan100MbMessage(true);
            using (Stream stream = System.IO.File.Create(
                       Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources/Files/Xml/largememo.xml")))
            {
                using (IMeMoStreamWriter writer = MeMoWriterFactory.CreateWriter(stream))
                {
                    writer.Write(message);
                }
            }

            long startMemory = GetCurrentlyUsedMemoryInMb();

            // Then
            using (Stream stream = System.IO.File.OpenRead(path))
            {
                using (IMeMoParser parser = MeMoParserFactory.CreateParser(stream))
                {
                    Message parsedMessage = parser.Parse();
                    Assert.IsNotNull(parsedMessage);
                    Assert.Greater(GetCurrentlyUsedMemoryInMb() - startMemory, 100);
                }
            }
        }

        [Test]
        public void VisitorCanParseFilesWithoutMemoryGrowingLarge()
        {
            // Given
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources/Files/Xml/largememo.xml");

            Message message = BuildLargerThan100MbMessage(true);
            using (Stream stream = System.IO.File.Create(
                       Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources/Files/Xml/largememo.xml")))
            {
                using (IMeMoStreamWriter writer = MeMoWriterFactory.CreateWriter(stream))
                {
                    writer.Write(message);
                }
            }

            GC.Collect();

            long startMemory = GetCurrentlyUsedMemoryInMb();

            // When
            long maxMemory = 0;
            using (Stream stream = System.IO.File.OpenRead(path))
            {
                using (IMeMoParser parser = MeMoParserFactory.CreateParser(stream))
                {
                    parser.Traverse(
                        new List<IMeMoStreamVisitor>
                        {
                            MeMoStreamVisitorFactory.CreateConsumer<File>(
                                c =>
                                {
                                    File file = c.ParseMeMoClass<File>();
                                    Assert.IsNotNull(file);
                                    maxMemory = Math.Max(maxMemory, GetCurrentlyUsedMemoryInMb());
                                    GC.Collect();
                                })
                        });
                }
            }

            // Then
            Assert.Less(maxMemory - startMemory, 15);
        }

        [Test]
        public void WriterCanWriteWholeMemoWithExternalContent()
        {
            // Given
            long startMemory = GetCurrentlyUsedMemoryInMb();

            // When
            Message message = BuildLargerThan100MbMessage(true);

            // Then
            Assert.Less(GetCurrentlyUsedMemoryInMb() - startMemory, 5);

            // When
            MemoryTestFileContentResolver resolver = new MemoryTestFileContentResolver();
            using (Stream stream = System.IO.File.Create(
                       Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources/Files/Xml/largememo.xml")))
            {
                using (IMeMoStreamWriter writer = MeMoWriterFactory.CreateWriter(
                           stream,
                           new FileContentLoader(
                               new List<IFileContentResolver>
                               {
                                   resolver
                               })))
                {
                    writer.Write(message);
                }
            }

            // Then
            Assert.Less(resolver.MaxUsedMemoryInMb - startMemory, 15);
            Assert.Less(GetCurrentlyUsedMemoryInMb() - startMemory, 15);
        }

        [Test]
        public void WriterCanWriteWholeMemoWithInternalContent()
        {
            // Given
            long startMemory = GetCurrentlyUsedMemoryInMb();

            // When
            Message message = BuildLargerThan100MbMessage(false);

            // Then
            Assert.Greater(GetCurrentlyUsedMemoryInMb() - startMemory, 100);

            // And
            using (Stream stream = System.IO.File.Create(
                       Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources/Files/Xml/largememo.xml")))
            {
                using (IMeMoStreamWriter writer = MeMoWriterFactory.CreateWriter(stream))
                {
                    writer.Write(message);
                    Assert.Greater(GetCurrentlyUsedMemoryInMb() - startMemory, 100);
                }
            }
        }
    }

    public class MemoryTestFileContentResolver : IFileContentResolver
    {
        public long MaxUsedMemoryInMb;

        public Optional<Stream> Resolve(string location)
        {
            Process.GetCurrentProcess().Refresh();
            MaxUsedMemoryInMb = Math.Max(
                Process.GetCurrentProcess().PrivateMemorySize64 / 1024 / 1024,
                MaxUsedMemoryInMb);
            return System.IO.File.Exists(location)
                ? Optional<Stream>.Of(System.IO.File.OpenRead(location))
                : Optional<Stream>.Empty();
        }
    }
}