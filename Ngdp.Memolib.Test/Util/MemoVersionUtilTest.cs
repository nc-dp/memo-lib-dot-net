﻿using System;
using System.IO;
using Dk.Digst.Digital.Post.Memolib.Exception;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Util;
using NUnit.Framework;
using File = System.IO.File;

namespace Dk.Digst.Digital.Post.Memolib.Test.Util
{
    [TestFixture]
    public class MemoVersionUtilTest
    {
        [TestCase(
            "Resources/Files/Xml/MeMo_Minimum_Example.xml", MemoVersion.MemoVersion11, TestName = "MeMo version 1.1")]
        [TestCase(
            "Resources/Files/Xml/memo_version_1_2/MeMo_Minimum_Example_V1.2.xml", MemoVersion.MemoVersion12,
            TestName = "MeMo version 1.2")]
        public void ReadMemoVersion(string path, MemoVersion expectedVersion)
        {
            // Given
            var stream = File.OpenRead(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path));

            // And
            var result = MemoVersionUtil.ReadMemoVersion(stream);

            // Then
            Assert.AreEqual(expectedVersion, result);
        }

        [Test]
        public void MemoVersionRegexCapturesVersion_1_1()
        {
            // Given
            var xml = @"<?xml version=""1.0"" encoding=""UTF-8""?>
<!--********************************************************************************************************* 
MeMo XML example,Minimum. This example illustrates a MeMo example with only the required elements
The example data is not correct business content 

Date: 20191014 CDS 

Modifications 
20191014: CDS: Changed from RDF/XML to XML 
20200304: CDS: Updted acc to Model v 0.9
20200311: CDS: DateTime format updated
***********************************************************************************************************-->
<memo:Message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:memo=""https://DigitalPost.dk/MeMo-1"" memoVersion=""1.1"" memoSchVersion=""1.1.0"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema#"">";

            // And
            var match = MemoVersionUtil.MEMO_VERSION_REGEX.Match(xml);

            // Then
            Assert.True(match.Success);
            Assert.AreEqual("1.1", match.Groups[1].Value);
        }

        [Test]
        public void MemoVersionRegexCatyresVersion_1_2()
        {
            // Given
            var xml = @"<?xml version=""1.0"" encoding=""UTF-8""?>
<memo:Message xmlns:memo=""https://DigitalPost.dk/MeMo-1"" memoVersion=""1.2"">";

            // And
            var match = MemoVersionUtil.MEMO_VERSION_REGEX.Match(xml);

            // Then
            Assert.True(match.Success);
            Assert.AreEqual("1.2", match.Groups[1].Value);
        }

        [TestCase(
            "Resources/Files/Xml/MeMo_Minimum_Example_Invalid_Version.xml", TestName = "MeMo version 1.0")]
        [TestCase(
            "Resources/Files/Xml/MeMo_Minimum_Example_No_Version.xml", TestName = "No MeMo Version")]
        public void ReadMemoVersionInvalid(string path)
        {
            // Given
            var stream = File.OpenRead(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path));

            // And
            var result = MemoVersionUtil.ReadMemoVersion(stream);

            // Then
            Assert.AreEqual(MemoVersion.MeMoVersionUnknown, result);
        }
    }
}