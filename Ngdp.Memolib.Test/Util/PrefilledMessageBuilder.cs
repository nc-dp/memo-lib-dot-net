﻿using System;
using System.Collections.Generic;
using Dk.Digst.Digital.Post.Memolib.Builder;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Test.Util
{
    /// <summary>
    ///     This class provides builders for the MeMo model classes and should only be used by automated
    ///     tests. The builders have been pre-filled with test data, but can be customized afterwards.
    /// </summary>
    public static class PrefilledMessageBuilder
    {
        public static MessageBuilder Message()
        {
            return MessageBuilder.NewBuilder()
                .MemoVersion(MemoVersion.MemoVersion11)
                .MemoSchVersion(MemoSchVersion.Name)
                .MessageHeader(MessageHeader().Build())
                .MessageBody(MessageBody().Build());
        }

        public static MessageHeaderBuilder MessageHeader()
        {
            return MessageHeaderBuilder.NewBuilder()
                .MessageType(memoMessageType.DIGITALPOST)
                .MessageUUID(Guid.NewGuid())
                .MessageID("someMessageID")
                .MessageCode("someMessageCode")
                .Label("someLabel")
                .Notification("someNotification")
                .Reply(false)
                .ReplyByDateTime(DateTime.Parse("2100-02-03T12:00:00").ToUniversalTime())
                .DoNotDeliverUntilDate(DateTime.Parse("2100-10-10"))
                .Mandatory(false)
                .LegalNotification(false)
                .PostType("somePostType")
                .Sender(Sender().Build())
                .Recipient(Recipient().Build())
                .ReplyData(
                    new List<ReplyData>
                    {
                        ReplyData().Build()
                    })
                .ForwardData(ForwardData().Build())
                .ContentData(ContentData().Build());
        }

        public static ForwardDataBuilder ForwardData()
        {
            return ForwardDataBuilder.NewBuilder()
                .MessageUUID(Guid.NewGuid())
                .OriginalSender("someOriginalSender")
                .OriginalContentResponsible("someOriginalContentResponsible")
                .OriginalMessageDateTime(DateTime.Parse("2100-02-03T12:00:00").ToUniversalTime())
                .Comment("someComment");
        }

        public static SenderBuilder Sender()
        {
            return SenderBuilder.NewBuilder()
                .SenderId("someSenderID")
                .IdType("someSenderIDtype")
                .IdTypeLabel("someSenderIdTypeLabel")
                .Label("someSenderLabel")
                .AttentionData(AttentionData(true).Build())
                .ContactPoint(ContactPoint().Build());
        }

        public static RecipientBuilder Recipient()
        {
            return RecipientBuilder.NewBuilder()
                .RecipientID("someRecipientID")
                .IdType("someRecipientIdType")
                .IdTypeLabel("someRecipientIdTypeLabel")
                .Label("someRecipientLabel")
                .AttentionData(AttentionData(false).Build())
                .ContactPoint(ContactPoint().Build());
        }

        public static AttentionDataBuilder AttentionData(bool usingAddress)
        {
            AttentionPerson testAttentionPerson =
                AttentionPersonBuilder.NewBuilder()
                    .PersonID("somePersonId")
                    .Label("someAttentionPersonLabel")
                    .Build();

            ProductionUnit testProductionUnit =
                ProductionUnitBuilder.NewBuilder()
                    .ProductionUnitNumber(666)
                    .ProductionUnitName("someProductionUnitName")
                    .Build();

            GlobalLocationNumber testGlobalLocationNumber =
                GlobalLocationNumberBuilder.NewBuilder()
                    .GlobalLocationNumber(9999999999999L)
                    .Location("someLocation")
                    .Build();

            EMail testEmail =
                EmailBuilder.NewBuilder()
                    .EmailAddress("someEmailAddress")
                    .RelatedAgent("someRelatedAgent")
                    .Build();

            SEnumber testSEnumber =
                SeNumberBuilder.NewBuilder().SeNumber("911").CompanyName("someCompanyName").Build();

            Telephone testTelephone =
                TelephoneBuilder.NewBuilder()
                    .TelephoneNumber("88776655")
                    .RelatedAgent("someRelatedAgent")
                    .Build();

            EID testRiDdate =
                EidBuilder.NewBuilder()
                    .Eid("someRIDnumber")
                    .Label("someCompanyName")
                    .Build();

            ContentResponsible testContentResponsible =
                ContentResponsibleBuilder.NewBuilder()
                    .ContentResponsibleID("someContentResponsibleID")
                    .Label("someContentResponsibleLabel")
                    .Build();

            GeneratingSystem testGeneratingSystem =
                GeneratingSystemBuilder.NewBuilder()
                    .GeneratingSystemID("someGeneratingSystemID")
                    .Label("someGeneratingSystemLabel")
                    .Build();

            SORdata sorData = SorDataBuilder.NewBuilder().SorIdentifier("somecode").EntryName("someName").Build();

            Address testAddress = Address().Build();

            AttentionDataBuilder attentionDataBuilder =
                AttentionDataBuilder.NewBuilder()
                    .AttentionPerson(testAttentionPerson)
                    .ProductionUnit(testProductionUnit)
                    .GlobalLocationNumber(testGlobalLocationNumber)
                    .EMail(testEmail)
                    .SEnumber(testSEnumber)
                    .Telephone(testTelephone)
                    .Eid(testRiDdate)
                    .SorData(sorData)
                    .ContentResponsible(testContentResponsible)
                    .GeneratingSystem(testGeneratingSystem);
            if (usingAddress)
                return attentionDataBuilder.Address(testAddress);
            UnstructuredAddress testUnstructuredAddress =
                UnstructuredAddressBuilder.NewBuilder().Unstructured("someUnstructuredAddress").Build();
            return attentionDataBuilder.UnstructuredAddress(testUnstructuredAddress);
        }

        private static AddressPoint AddressPoint()
        {
            return AddressPointBuilder.NewBuilder()
                .GeographicEastingMeasure("someGeographicEastMeasure")
                .GeographicNorthingMeasure("someGeographicNorthingMeasure")
                .GeographicHeightMeasure("someGeographicHeightMeasure")
                .Build();
        }

        private static AddressBuilder Address()
        {
            return AddressBuilder.NewBuilder()
                .Id(Guid.NewGuid().ToString())
                .AddressLabel("someAddressLabel")
                .HouseNumber("somehouseNumber")
                .Door("someDoor")
                .Floor("someFloor")
                .Co("someCO")
                .ZipCode("9999")
                .City("someCity")
                .Country("someCountry")
                .AddressPoint(AddressPoint());
        }

        public static ReservationBuilder Reservation()
        {
            return ReservationBuilder.NewBuilder()
                .Description("someReservationDescription")
                .ReservationUuid(Guid.NewGuid())
                .ReservationAbstract("someReservationAbstract")
                .Location("someLocation")
                .StartDateTime(DateTime.Parse("2100-06-03T12:00:00"))
                .EndDateTime(DateTime.Parse("2100-04-03T12:00:00"))
                .OrganizerMail("someOrganizerMail")
                .OrganizerName("someOrganizerName");
        }

        public static EntryPointBuilder EntryPoint()
        {
            return EntryPointBuilder.NewBuilder().Url("someURL");
        }

        public static ContactInfoBuilder ContactInfo()
        {
            return ContactInfoBuilder.NewBuilder()
                .Label("someContactInfoLabel")
                .Value("someContactInfoValue");
        }

        public static ContactPointBuilder ContactPoint()
        {
            return ContactPointBuilder.NewBuilder()
                .ContactGroup("someContactGroup")
                .ContactPointID("someContactPointID")
                .Label("someContactPointLabel")
                .ContactInfo(new List<ContactInfo> {ContactInfo().Build()});
        }

        public static ReplyDataBuilder ReplyData()
        {
            return ReplyDataBuilder.NewBuilder()
                .MessageID("someReplyDataMessageId")
                .MessageUUID(Guid.NewGuid())
                .ReplyUUID(Guid.NewGuid())
                .SenderID("someReplyDataSenderID")
                .RecipientID("someReplyDataRecipientID")
                .CaseID("someReplyDataCaseID")
                .ContactPointID("someReplyDataContactPoint")
                .GeneratingSystemID("someReplyDataGeneratingSystemId")
                .Comment("someReplyDataComment")
                .AddAdditionalReplyData(AdditionalReplyDataBuilder.NewBuilder().Label("key1").Value("someReplyData1").Build())
                .AddAdditionalReplyData(AdditionalReplyDataBuilder.NewBuilder().Label("key2").Value("someReplyData2").Build())
                .AddAdditionalReplyData(AdditionalReplyDataBuilder.NewBuilder().Label("key3").Value("someReplyData3").Build())
                .AddAdditionalReplyData(AdditionalReplyDataBuilder.NewBuilder().Label("key4").Value("someReplyData4").Build());
        }

        public static MessageBodyBuilder MessageBody()
        {
            return MessageBodyBuilder.NewBuilder()
                .CreatedDateTime(DateTime.Parse("2100-05-03T12:00:00").ToUniversalTime())
                .MainDocument(MainDocument().Build())
                .AddAdditionalDocument(AdditionalDocument().Build())
                .AddTechnicalDocument(TechnicalDocument().Build());
        }

        public static MainDocumentBuilder MainDocument()
        {
            return MainDocumentBuilder.NewBuilder()
                .DocumentID("someMainDocumentID")
                .Label("someDigitalDocumentLabel")
                .AddFile(File().Build())
                .AddFile(File().Build())
                .AddAction(Action(true).Build());
        }

        public static AdditionalDocumentBuilder AdditionalDocument()
        {
            return AdditionalDocumentBuilder.NewBuilder()
                .DocumentId("someAdditionalDocumentID")
                .Label("someDigitalDocumentLabel")
                .AddFile(File().Build())
                .AddFile(File().Build())
                .AddAction(Action(true).Build());
        }

        public static TechnicalDocumentBuilder TechnicalDocument()
        {
            return TechnicalDocumentBuilder.NewBuilder()
                .DocumentId("someTechnicalDocumentID")
                .Label("someDigitalDocumentLabel")
                .AddFile(File().Build())
                .AddFile(File().Build());
        }

        public static FileBuilder File()
        {
            return FileBuilder.NewBuilder()
                .EncodingFormat("application/pdf")
                .Filename("model.Pdf")
                .Language("da")
                .Content(FileContent().Build());
        }

        public static FileContentBuilder FileContent()
        {
            return FileContentBuilder.NewBuilder().Base64Content("JBEV");
        }

        public static ActionBuilder Action(bool usingReservation)
        {
            ActionBuilder actionBuilder =
                ActionBuilder.NewBuilder()
                    .Label("someActionLabel")
                    .ActionCode("someActionCode")
                    .StartDateTime(DateTime.Parse("2110-05-03T12:00:00"))
                    .EndDateTime(DateTime.Parse("2120-05-03T12:00:00"));
            return usingReservation
                ? actionBuilder.Reservation(Reservation().Build())
                : actionBuilder.EntryPoint(EntryPoint().Build());
        }

        public static ContentDataBuilder ContentData()
        {
            return ContentDataBuilder.NewBuilder()
                .CprData(CprDataBuilder.NewBuilder().CprNumber("12424578").Name("Hans Hansen").Build())
                .CvrData(CvrDataBuilder.NewBuilder().CvrNumber("98575759").CompanyName("Sjov og løjer ApS").Build())
                .MotorVehicle(MotorVehicleBuilder.NewBuilder().LicenseNumber("YT58472")
                    .ChassisNumber("a92734lad8222AAA").Build())
                .PropertyNumber(PropertyNumberBuilder.NewBuilder().PropertyNumber("AB75").Build())
                .CaseID(CaseIDBuilder.NewBuilder().CaseID("75633838373").CaseSystem("ESDH").Build())
                .KleData(KleDataBuilder.NewBuilder().SubjectKey("Some subject").Version("2").ActivityFacet("some facet")
                    .Label("some label").Build())
                .FormData(FormDataBuilder.NewBuilder().TaskKey("some task").Version("7").ActivityFacet("some facet")
                    .Label("some label").Build())
                .ProductionUnit(ProductionUnitBuilder.NewBuilder().ProductionUnitNumber(272644)
                    .ProductionUnitName("some unit name").Build())
                .Education(
                    EducationBuilder.NewBuilder().EducationCode("635EAS").EducationName("some education").Build())
                .Address(Address().Build())
                .AddAdditionalContentData(AdditionalContentData().Build())
                .AddAdditionalContentData(AdditionalContentData().Build())
                .AddAdditionalContentData(AdditionalContentData().Build());
        }

        public static AdditionalContentDataBuilder AdditionalContentData()
        {
            return new AdditionalContentDataBuilder()
                .ContentDataType("content data type")
                .ContentDataName("content data name")
                .ContentDataValue("content data value");
        }

        public static MessageHeaderBuilder MessageHeaderNoReplyUUID()
        {
            return MessageHeaderBuilder.NewBuilder()
                .MessageType(memoMessageType.DIGITALPOST)
                .MessageUUID(Guid.NewGuid())
                .MessageID("someMessageID")
                .MessageCode("someMessageCode")
                .Label("someLabel")
                .Mandatory(false)
                .LegalNotification(false)
                .Sender(Sender().Build())
                .Recipient(Recipient().Build())
                .ReplyData(new List<ReplyData> {ReplyDataNoReplyUUID().Build()});
        }

        public static ReplyDataBuilder ReplyDataNoReplyUUID()
        {
            return ReplyDataBuilder.NewBuilder()
                .MessageID("someReplyDataMessageId")
                .MessageUUID(Guid.NewGuid())
                .SenderID("someReplyDataSenderID")
                .RecipientID("someReplyDataRecipientID")
                .AddAdditionalReplyData(AdditionalReplyDataBuilder.NewBuilder().Label("key1").Value("someReplyData1").Build())
                .AddAdditionalReplyData(AdditionalReplyDataBuilder.NewBuilder().Label("key4").Value("someReplyData4").Build());
        }
    }
}