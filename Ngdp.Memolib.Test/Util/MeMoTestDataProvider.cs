﻿using System;
using System.IO;

namespace Dk.Digst.Digital.Post.Memolib.Test.Util
{
    public static class MeMoTestDataProvider
    {
        public static Stream StreamLzmaCompressed100Memo()
        {
            return File.OpenRead(Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                "Resources/Files/Lzma/memo-lib-Affaldssorteringe.tar.lzma"));
        }

        public static Stream StreamMeMoContainerWithTwoMessages()
        {
            return File.OpenRead(Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                "Resources/Files/Zip/memo_archive_2_messages.zip"));
        }

        public static Stream StreamMeMoContainerWithOneInvalidMessage()
        {
            return File.OpenRead(Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                "Resources/Files/Zip/memo_archive_1_invalid_message.zip"));
        }

        public static Stream StreamMeMoContainerWithZeroMessages()
        {
            return File.OpenRead(Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                "Resources/Files/Zip/memo_archive_0_messages.zip"));
        }

        public static Stream StreamMeMoMessageMinimumExample()
        {
            return File.OpenRead(Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                "Resources/Files/Xml/MeMo_Minimum_Example.xml"));
        }

        public static Stream StreamMeMoMessageFullExample()
        {
            return File.OpenRead(Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                "Resources/Files/Xml/MeMo_Full_Example.xml"));
        }

        public static Stream StreamMeMoMessageFullExample12()
        {
            return File.OpenRead(Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                "Resources/Files/Xml/memo_version_1_2/MeMo_Full_Example_V1.2.xml"));
        }
       
        public static Stream StreamMeMoMessageFullExampleV2()
        {
            return File.OpenRead(Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                "Resources/Files/Xml/MeMo_Full_Example_V2.xml"));
        }

        public static Stream StreamInvalidMeMoMessage()
        {
            return File.OpenRead(Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                "Resources/Files/Xml/MeMo_Full_Example_Invalid.xml"));
        }

        public static string OneMbFileLocation()
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources/Files/Xml/1mbfile.xml");
        }

        public static string ModelPdf()
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources/Files/Pdf/model.pdf");
        }

        public static string NotatPdf()
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources/Files/Pdf/notat.pdf");
        }

        public static Stream TestMemoJ()
        {
            return File.OpenRead(Path.Combine(Environment.CurrentDirectory,
                "Resources/Files/Xml/validation/MeMo_Invalid_Missing_Reply_Data_V1_1.xml"));
        }

        public static Stream FullExample12()
        {
            return File.OpenRead(Path.Combine(Environment.CurrentDirectory,
                "Resources/Files/Xml/MeMo_Full_Example_V1.2.xml"));
        }

        public static Stream InvalidIdAndTypes()
        {
            return File.OpenRead(Path.Combine(Environment.CurrentDirectory,
                "Resources/Files/Xml/validation/MeMo_Invalid_Id_Type.xml"));
        }
        public static Stream InvalidCvr()
        {
            return File.OpenRead(Path.Combine(Environment.CurrentDirectory,
                "Resources/Files/Xml/validation/MeMo_Invalid_Cvr_Number.xml"));
        }
        public static Stream InvalidCpr()
        {
            return File.OpenRead(Path.Combine(Environment.CurrentDirectory,
                "Resources/Files/Xml/validation/MeMo_Invalid_Cpr_Number.xml"));
        }
        public static Stream InvalidRoot()
        {
            return File.OpenRead(Path.Combine(Environment.CurrentDirectory,
                "Resources/Files/Xml/validation/MeMo_Invalid_Root_Element.xml"));
        }
        public static Stream MissingNs()
        {
            return File.OpenRead(Path.Combine(Environment.CurrentDirectory,
                "Resources/Files/Xml/validation/MeMo_Invalid_Missing_Memo_Ns.xml"));
        }
        public static Stream MissingBody()
        {
            return File.OpenRead(Path.Combine(Environment.CurrentDirectory,
                "Resources/Files/Xml/validation/MeMo_Invalid_Empty_body.xml"));
        }
        public static Stream multipleErrors()
        {
            return File.OpenRead(Path.Combine(Environment.CurrentDirectory,
                "Resources/Files/Xml/validation/MeMo_Invalid_Multiple_Errors.xml"));
        }
        public static Stream ValidMeMo11()
        {
            return File.OpenRead(Path.Combine(Environment.CurrentDirectory,
                "Resources/Files/Xml/MeMo_Full_Example.xml"));
        }
        public static Stream ValidNemsms()
        {
            return File.OpenRead(Path.Combine(Environment.CurrentDirectory,
                "Resources/Files/Xml/validation/MeMo_Valid_Nemsms.xml"));
        }
    }
}