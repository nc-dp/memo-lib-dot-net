﻿using System.Collections.Generic;
using System.Text;
using Dk.Digst.Digital.Post.Memolib.Container;
using Dk.Digst.Digital.Post.Memolib.Lzma;
using Dk.Digst.Digital.Post.Memolib.Model;
using ICSharpCode.SharpZipLib.Tar;
using SharpCompress.Compressors.LZMA;
using File = System.IO.File;

namespace Dk.Digst.Digital.Post.Memolib.Test.Util
{
    public static class TarReaderUtil
    {
        public static List<Message> ReadAllMessagesFromTar(
            string path, bool validate = false, MemoVersion memoVersion = MemoVersion.MemoVersion11)
        {
            LZMAInputStream lzma = new LZMAInputStream(File.OpenRead(path));
            TarInputStream stream = new TarInputStream(lzma, Encoding.UTF8);
            IIterableMeMoContainer iterator = new ArchiveEntryIterator(stream);
            MeMoContainerReader reader = new MeMoContainerReader(iterator);

            List<Message> result = new List<Message>();
            while (reader.HasEntry())
                result.Add(reader.ReadEntry(validate, memoVersion).Get());
            return result;
        }
    }
}