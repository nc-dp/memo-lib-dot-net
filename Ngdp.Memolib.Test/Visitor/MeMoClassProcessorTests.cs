﻿using System.Collections.Generic;
using System.IO;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Parser;
using Dk.Digst.Digital.Post.Memolib.Test.Util;
using Dk.Digst.Digital.Post.Memolib.Visitor;
using NUnit.Framework;
using File = Dk.Digst.Digital.Post.Memolib.Model.File;

namespace Dk.Digst.Digital.Post.Memolib.Test.Visitor
{
    [TestFixture]
    public class MeMoClassProcessorTests
    {
        [SetUp]
        public void Setup()
        {
            _stream = MeMoTestDataProvider.StreamMeMoMessageFullExample();
            _parser = MeMoParserFactory.CreateParser(_stream);
        }

        [TearDown]
        public void Cleanup()
        {
            _parser.Dispose();
            _stream?.Dispose();
        }

        private IMeMoParser _parser;

        private Stream _stream;

        [Test]
        public void ConsumerCanBeLimitedOnParentClass()
        {
            // Given
            IMeMoStreamProcessorVisitor<File> visitor =
                MeMoStreamVisitorFactory.CreateProcessor<File>(typeof(TechnicalDocument));

            // When
            _parser.Traverse(new List<IMeMoStreamVisitor> {visitor});

            // Then
            Assert.AreEqual(1, visitor.GetResult().Count);
            Assert.AreEqual("text/xml", visitor.GetResult()[0].encodingFormat);
            Assert.AreEqual("TekniskDokument.xml", visitor.GetResult()[0].filename);
            Assert.AreEqual("da", visitor.GetResult()[0].language);
            Assert.AreEqual("VGhpcyBpcyBhIHRlc3Q=", visitor.GetResult()[0].content);
        }

        [Test]
        public void ProcessorCanVisitAMeMoClass()
        {
            // Given
            IMeMoStreamProcessorVisitor<File> visitor =
                MeMoStreamVisitorFactory.CreateProcessor<File>();

            // When
            _parser.Traverse(new List<IMeMoStreamVisitor> {visitor});

            // Then
            Assert.AreEqual(6, visitor.GetResult().Count);
            Assert.AreEqual("application/pdf", visitor.GetResult()[4].encodingFormat);
            Assert.AreEqual("vejledning.pdf", visitor.GetResult()[4].filename);
            Assert.AreEqual("da", visitor.GetResult()[4].language);
            Assert.AreEqual("VGhpcyBpcyBhIHRlc3Q=", visitor.GetResult()[4].content);
        }
    }
}