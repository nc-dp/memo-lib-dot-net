﻿using System.Collections.Generic;
using System.IO;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Parser;
using Dk.Digst.Digital.Post.Memolib.Test.Util;
using Dk.Digst.Digital.Post.Memolib.Visitor;
using NUnit.Framework;
using File = Dk.Digst.Digital.Post.Memolib.Model.File;

namespace Dk.Digst.Digital.Post.Memolib.Test.Visitor
{
    [TestFixture]
    public class MeMoClassConsumerTests
    {
        [SetUp]
        public void Setup()
        {
            _stream = MeMoTestDataProvider.StreamMeMoMessageFullExample();
            _parser = MeMoParserFactory.CreateParser(_stream);
        }

        [TearDown]
        public void Cleanup()
        {
            _parser.Dispose();
            _stream?.Dispose();
        }

        private IMeMoParser _parser;

        private Stream _stream;

        [Test]
        public void ConsumerCanBeLimitedOnParentClass()
        {
            // Given
            List<MeMoClassElements> parsedElements = new List<MeMoClassElements>();
            IMeMoStreamVisitor visitor =
                MeMoStreamVisitorFactory.CreateConsumer<File>(typeof(TechnicalDocument),
                    c => parsedElements.Add(c.ParseMeMoClassElements()));

            // When
            _parser.Traverse(new List<IMeMoStreamVisitor> {visitor});

            // Then
            Assert.AreEqual(1, parsedElements.Count);
            Assert.AreEqual("text/xml", parsedElements[0].Get("encodingFormat"));
            Assert.AreEqual("TekniskDokument.xml", parsedElements[0].Get("filename"));
            Assert.AreEqual("da", parsedElements[0].Get("language"));
            Assert.AreEqual("VGhpcyBpcyBhIHRlc3Q=", parsedElements[0].Get("content"));
        }

        [Test]
        public void ConsumerCanVisitAMeMoClass()
        {
            // Given
            List<MeMoClassElements> parsedElements = new List<MeMoClassElements>();
            IMeMoStreamVisitor visitor =
                MeMoStreamVisitorFactory.CreateConsumer<File>(
                    c => parsedElements.Add(c.ParseMeMoClassElements()));

            // When
            _parser.Traverse(new List<IMeMoStreamVisitor> {visitor});

            // Then
            Assert.AreEqual(6, parsedElements.Count);
            Assert.AreEqual("application/pdf", parsedElements[4].Get("encodingFormat"));
            Assert.AreEqual("vejledning.pdf", parsedElements[4].Get("filename"));
            Assert.AreEqual("da", parsedElements[4].Get("language"));
            Assert.AreEqual("VGhpcyBpcyBhIHRlc3Q=", parsedElements[4].Get("content"));
        }
    }
}