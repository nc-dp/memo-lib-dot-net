﻿using System;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Visitor;
using NUnit.Framework;
using Action = Dk.Digst.Digital.Post.Memolib.Model.Action;

namespace Dk.Digst.Digital.Post.Memolib.Test.Visitor
{
    [TestFixture]
    public class MeMoStreamVisitorFactoryTests
    {
        [Test]
        public void ConsumerMustHaveDelegatorNoParent()
        {
            Assert.Throws<ArgumentNullException>(() => MeMoStreamVisitorFactory.CreateConsumer<Action>(
                null));
        }

        [Test]
        public void ConsumerMustHaveDelegatorWithParent()
        {
            Assert.Throws<ArgumentNullException>(() => MeMoStreamVisitorFactory.CreateConsumer<Action>(
                typeof(Message), null));
        }

        [Test]
        public void FactoryCanCreateMeMoClassConsumer()
        {
            // When
            IMeMoStreamVisitor consumer = MeMoStreamVisitorFactory.CreateConsumer<Action>(
                c => { });

            // Then
            Assert.IsNotNull(consumer);
        }

        [Test]
        public void FactoryCanCreateMeMoClassConsumerWithParentClass()
        {
            // When
            IMeMoStreamVisitor consumer = MeMoStreamVisitorFactory.CreateConsumer<Action>(typeof(MessageHeader),
                c => { });

            // Then
            Assert.IsNotNull(consumer);
        }

        [Test]
        public void FactoryCanCreateMeMoClassProcessor()
        {
            // When
            IMeMoStreamVisitor consumer = MeMoStreamVisitorFactory.CreateProcessor<Action>();

            // Then
            Assert.IsNotNull(consumer);
        }

        [Test]
        public void FactoryCanCreateMeMoClassProcessorWithParentClass()
        {
            // When
            IMeMoStreamVisitor consumer = MeMoStreamVisitorFactory.CreateConsumer<Action>(typeof(MessageHeader),
                c => { });

            // Then
            Assert.IsNotNull(consumer);
        }
    }
}