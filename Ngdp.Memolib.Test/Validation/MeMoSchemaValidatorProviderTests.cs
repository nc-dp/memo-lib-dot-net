﻿using System.IO;
using System.Xml.Schema;
using Dk.Digst.Digital.Post.Memolib.Test.Util;
using Dk.Digst.Digital.Post.Memolib.Validation;
using NUnit.Framework;

namespace Dk.Digst.Digital.Post.Memolib.Test.Validation
{
    [TestFixture]
    public class MeMoSchemaValidatorProviderTests
    {
        [Test] 
        public void ValidMessageCanBeValidated()
        {
            using (Stream stream = MeMoTestDataProvider.StreamMeMoMessageFullExample())
            {
                Assert.DoesNotThrow(() => MeMoSchemaValidatorProvider.Validate(stream));
            }
            using (Stream stream = MeMoTestDataProvider.StreamMeMoMessageFullExample())
            {
                Assert.IsTrue(MeMoSchemaValidatorProvider.IsValid(stream));
            }
        }

        [Test]
        public void InvalidMessageFailsValidation()
        {
            using (Stream stream = MeMoTestDataProvider.StreamInvalidMeMoMessage())
            {
                Assert.Throws<XmlSchemaValidationException>(() => MeMoSchemaValidatorProvider.Validate(stream));
            }
            using (Stream stream = MeMoTestDataProvider.StreamInvalidMeMoMessage())
            {
                Assert.IsFalse(MeMoSchemaValidatorProvider.IsValid(stream));
            }
        }
    }
}