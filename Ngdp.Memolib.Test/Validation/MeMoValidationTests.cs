﻿using System;
using System.IO;
using System.Linq;
using Dk.Digst.Digital.Post.Memolib.Exception;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Parser;
using Dk.Digst.Digital.Post.Memolib.Test.Util;
using Dk.Digst.Digital.Post.Memolib.Validation;
using NUnit.Framework;

namespace Dk.Digst.Digital.Post.Memolib.Test
{
    [TestFixture]
    public class MeMoValidationTest
    {
        private const string XML_VALIDATOR_DIR = "Resources/Files/Xml/validation/";
        private const string XML_DIR = "Resources/Files/Xml";

        [Test]
        [TestCase("MeMo_Invalid_Post_Type.xml", ValidationErrorCode.POST_TYPE_INVALID, new[] { "POST" })]
        [TestCase(
            "MeMo_Invalid_Nemsms_Notification.xml", ValidationErrorCode.EMPTY_NOTIFICATION_NOT_ALLOWED, new string[0])]
        [TestCase("MeMo_Invalid_Reply_Data.xml", ValidationErrorCode.REPLY_DATA_MESSAGE_UUID_NOT_FOUND, new string[0])]
        [TestCase(
            "MeMo_Invalid_Missing_Reply_Data.xml", ValidationErrorCode.REPLY_DATA_MESSAGE_UUID_NOT_FOUND,
            new string[0])]
        public void ValidateMeMoInvalidHeader(string fileName, ValidationErrorCode errorCode, string[] args)
        {
            // Given
            var memo = new FileInfo(Path.Combine(Environment.CurrentDirectory, XML_VALIDATOR_DIR, fileName));
            using (var fileStream = new FileStream(memo.FullName, FileMode.Open))
            {
                // And
                var parser = MeMoParserFactory.CreateParser(fileStream, false);
                var message = parser.Parse();

                // And
                var validator = MeMoValidatorFactory.CreateMeMoValidator(message);

                // When
                var meMoValidatorErrors = validator.Validate();

                // Then
                Assert.That(meMoValidatorErrors, Has.Count.EqualTo(1));
                Assert.That(
                    meMoValidatorErrors, Has.Some.Matches<MeMoValidatorError>(
                        e =>
                            e.ErrorCode == errorCode &&
                            e.Args.SequenceEqual(args)));
            }
        }

        [Test]
        public void ValidateMeMo1_1ValidationDoesNotValidateInvalidReplyData()
        {
            // Given
            var memo = new FileInfo(
                Path.Combine(
                    Environment.CurrentDirectory, XML_VALIDATOR_DIR, "MeMo_Invalid_Missing_Reply_Data_V1.1.xml"));
            using (var fileStream = new FileStream(memo.FullName, FileMode.Open))
            {
                // And
                var parser = MeMoParserFactory.CreateParser(fileStream, false);
                var message = parser.Parse();

                // And
                var validator = MeMoValidatorFactory.CreateMeMoValidator(message);

                // When
                var meMoValidatorErrors = validator.Validate();

                // Then
                Assert.That(meMoValidatorErrors, Is.Empty);
            }
        }

        [Test]
        public void ValidateMeMoInvalidIDTypes()
        {
            // Given
            var memo = new FileInfo(
                Path.Combine(Environment.CurrentDirectory, XML_VALIDATOR_DIR, "MeMo_Invalid_Id_Type.xml"));
            using (var fileStream = new FileStream(memo.FullName, FileMode.Open))
            {
                // And
                var parser = MeMoParserFactory.CreateParser(fileStream, false);
                ;
                var message = parser.Parse();

                // And
                var validator = MeMoValidatorFactory.CreateMeMoValidator(message);

                // When
                var meMoValidatorErrors = validator.Validate();

                // Then
                Assert.That(meMoValidatorErrors, Has.Count.EqualTo(3));

                // And
                Assert.That(
                    meMoValidatorErrors, Has.Some.Matches<MeMoValidatorError>(
                        e =>
                            e.ErrorCode == ValidationErrorCode.ID_TYPE_INVALID &&
                            e.Args.SequenceEqual(new[] { "recipient", "CLR" })));
                Assert.That(
                    meMoValidatorErrors, Has.Some.Matches<MeMoValidatorError>(
                        e =>
                            e.ErrorCode == ValidationErrorCode.ID_TYPE_INVALID &&
                            e.Args.SequenceEqual(new[] { "sender", "CKR" })));
                Assert.That(
                    meMoValidatorErrors, Has.Some.Matches<MeMoValidatorError>(
                        e =>
                            e.ErrorCode == ValidationErrorCode.ID_TYPE_INVALID &&
                            e.Args.SequenceEqual(new[] { "sender.representative", "SSDASR" })));
            }
        }

        [Test]
        public void ValidateMeMoInvalidCvrs()
        {
            // Given
            var memo = new FileInfo(
                Path.Combine(Environment.CurrentDirectory, XML_VALIDATOR_DIR, "MeMo_Invalid_Cvr_Number.xml"));
            using (var fileStream = new FileStream(memo.FullName, FileMode.Open))
            {
                // And
                var parser = MeMoParserFactory.CreateParser(fileStream, false);
                ;
                var message = parser.Parse();

                // And
                var validator = MeMoValidatorFactory.CreateMeMoValidator(message);

                // When
                var meMoValidatorErrors = validator.Validate();

                // Then
                Assert.That(meMoValidatorErrors, Has.Count.EqualTo(3));

                // And
                Assert.That(
                    meMoValidatorErrors, Has.Some.Matches<MeMoValidatorError>(
                        e =>
                            e.ErrorCode == ValidationErrorCode.RECIPIENT_CVR_INVALID &&
                            e.Args.SequenceEqual(new[] { "2211771212" })));
                Assert.That(
                    meMoValidatorErrors, Has.Some.Matches<MeMoValidatorError>(
                        e =>
                            e.ErrorCode == ValidationErrorCode.REPRESENTATIVE_CVR_INVALID &&
                            e.Args.SequenceEqual(new[] { "123-4678" })));
                Assert.That(
                    meMoValidatorErrors, Has.Some.Matches<MeMoValidatorError>(
                        e =>
                            e.ErrorCode == ValidationErrorCode.SENDER_CVR_INVALID &&
                            e.Args.SequenceEqual(new[] { "1234567" })));
            }
        }

        [Test]
        public void ValidateMeMoInvalidCprs()
        {
            // Given
            var memo = new FileInfo(
                Path.Combine(Environment.CurrentDirectory, XML_VALIDATOR_DIR, "MeMo_Invalid_Cpr_Number.xml"));
            using (var fileStream = new FileStream(memo.FullName, FileMode.Open))
            {
                // And
                var parser = MeMoParserFactory.CreateParser(fileStream, false);
                ;
                var message = parser.Parse();

                // And
                var validator = MeMoValidatorFactory.CreateMeMoValidator(message);

                // When
                var meMoValidatorErrors = validator.Validate();

                // Then
                Assert.That(meMoValidatorErrors, Has.Count.EqualTo(3));

                // And
                Assert.That(
                    meMoValidatorErrors, Has.Some.Matches<MeMoValidatorError>(
                        e =>
                            e.ErrorCode == ValidationErrorCode.RECIPIENT_CPR_INVALID &&
                            e.Args.SequenceEqual(new[] { "12345678901" })));
                Assert.That(
                    meMoValidatorErrors, Has.Some.Matches<MeMoValidatorError>(
                        e =>
                            e.ErrorCode == ValidationErrorCode.REPRESENTATIVE_CPR_INVALID &&
                            e.Args.SequenceEqual(new[] { "123-4678" })));
                Assert.That(
                    meMoValidatorErrors, Has.Some.Matches<MeMoValidatorError>(
                        e =>
                            e.ErrorCode == ValidationErrorCode.SENDER_CPR_INVALID &&
                            e.Args.SequenceEqual(new[] { "12345678" })));
            }
        }

        [Test]
        public void MeMoWithInvalidRootFailsHard()
        {
            // Given
            var memo = new FileInfo(
                Path.Combine(Environment.CurrentDirectory, XML_VALIDATOR_DIR, "MeMo_Invalid_Root_Element.xml"));
            using (var fileStream = new FileStream(memo.FullName, FileMode.Open))
            {
                // When & Then
                var parser = MeMoParserFactory.CreateParser(fileStream, false);
                ;
                Assert.Throws<MeMoParseException>(() => parser.Parse());
            }
        }

        [Test]
        public void MeMoWithMissingXmlnsMemoFailsHard()
        {
            // Given
            var memo = new FileInfo(
                Path.Combine(Environment.CurrentDirectory, XML_VALIDATOR_DIR, "MeMo_Invalid_Missing_Memo_Ns.xml"));
            using (var fileStream = new FileStream(memo.FullName, FileMode.Open))
            {
                // When & Then
                var parser = MeMoParserFactory.CreateParser(fileStream, false);
                ;
                Assert.Throws<MeMoParseException>(() => parser.Parse());
            }
        }

        [Test]
        public void ValidateMeMoMissingBody()
        {
            // Given
            var memo = new FileInfo(
                Path.Combine(Environment.CurrentDirectory, XML_VALIDATOR_DIR, "MeMo_Invalid_Empty_Body.xml"));
            using (var fileStream = new FileStream(memo.FullName, FileMode.Open))
            {
                // And
                var parser = MeMoParserFactory.CreateParser(fileStream, false);
                ;
                var message = parser.Parse();

                // And
                var validator = MeMoValidatorFactory.CreateMeMoValidator(message);

                // When
                var meMoValidatorErrors = validator.Validate();

                // Then
                Assert.That(meMoValidatorErrors, Has.Count.EqualTo(1));
                Assert.That(
                    meMoValidatorErrors, Has.Some.Matches<MeMoValidatorError>(
                        e =>
                            e.ErrorCode == ValidationErrorCode.MESSAGE_BODY_NOT_FOUND &&
                            e.Args.Length == 0));
            }
        }

        [Test]
        public void MeMoWithMultipleErrorsReturnsAListOfAllErrors()
        {
            // Given
            var memo = new FileInfo(
                Path.Combine(Environment.CurrentDirectory, XML_VALIDATOR_DIR, "MeMo_Invalid_Multiple_Errors.xml"));
            using (var fileStream = new FileStream(memo.FullName, FileMode.Open))
            {
                // And
                var parser = MeMoParserFactory.CreateParser(fileStream, false);
                ;
                var message = parser.Parse();

                // And
                var validator = MeMoValidatorFactory.CreateMeMoValidator(message);

                // When
                var meMoValidatorErrors = validator.Validate();

                // Then
                Assert.That(meMoValidatorErrors, Has.Count.EqualTo(6));

                // And
                Assert.That(
                    meMoValidatorErrors, Has.Some.Matches<MeMoValidatorError>(
                        e =>
                            e.ErrorCode == ValidationErrorCode.ID_TYPE_INVALID &&
                            e.Args.SequenceEqual(new[] { "sender.representative", "SSDASR" })));
                Assert.That(
                    meMoValidatorErrors, Has.Some.Matches<MeMoValidatorError>(
                        e =>
                            e.ErrorCode == ValidationErrorCode.MESSAGE_BODY_NOT_FOUND &&
                            e.Args.Length == 0));
                Assert.That(
                    meMoValidatorErrors, Has.Some.Matches<MeMoValidatorError>(
                        e =>
                            e.ErrorCode == ValidationErrorCode.POST_TYPE_INVALID &&
                            e.Args.SequenceEqual(new[] { "POST" })));
                Assert.That(
                    meMoValidatorErrors, Has.Some.Matches<MeMoValidatorError>(
                        e =>
                            e.ErrorCode == ValidationErrorCode.RECIPIENT_CVR_INVALID &&
                            e.Args.SequenceEqual(new[] { "2211771212" })));
                Assert.That(
                    meMoValidatorErrors, Has.Some.Matches<MeMoValidatorError>(
                        e =>
                            e.ErrorCode == ValidationErrorCode.REPLY_DATA_MESSAGE_UUID_NOT_FOUND &&
                            e.Args.Length == 0));
                Assert.That(
                    meMoValidatorErrors, Has.Some.Matches<MeMoValidatorError>(
                        e =>
                            e.ErrorCode == ValidationErrorCode.SENDER_CPR_INVALID &&
                            e.Args.SequenceEqual(new[] { "12345678" })));
            }
        }

        [Test]
        public void ValidateAValidMeMoV1_2()
        {
            // Given
            var memo = new FileInfo(Path.Combine(Environment.CurrentDirectory, XML_DIR, "MeMo_Full_Example_V1.2.xml"));
            using (var fileStream = new FileStream(memo.FullName, FileMode.Open))
            {
                // And
                var parser = MeMoParserFactory.CreateParser(fileStream, false);
                ;
                var message = parser.Parse();

                // And
                var validator = MeMoValidatorFactory.CreateMeMoValidator(message);

                // When
                var meMoValidatorErrors = validator.Validate();

                // Then
                Assert.That(meMoValidatorErrors, Is.Empty);
            }
        }

        [Test]
        public void ValidateAValidMeMoV1_1()
        {
            // Given
            var memo = new FileInfo(Path.Combine(Environment.CurrentDirectory, XML_DIR, "MeMo_Full_Example.xml"));
            using (var fileStream = new FileStream(memo.FullName, FileMode.Open))
            {
                // And
                var parser = MeMoParserFactory.CreateParser(fileStream, false);
                var message = parser.Parse();

                // And
                var validator = MeMoValidatorFactory.CreateMeMoValidator(message);

                // When
                var meMoValidatorErrors = validator.Validate();

                // Then
                Assert.That(meMoValidatorErrors, Is.Empty);
            }
        }

        [Test]
        public void MeMoValidatorConstructorWithJustMeMoVersionWorks()
        {
            // Given
            var memo = new FileInfo(Path.Combine(Environment.CurrentDirectory, XML_DIR, "MeMo_Full_Example.xml"));
            using (var fileStream = new FileStream(memo.FullName, FileMode.Open))
            {
                // And
                var parser = MeMoParserFactory.CreateParser(fileStream, false);
                var message = parser.Parse();

                // And
                var validator = MeMoValidatorFactory.CreateMeMoValidator(MemoVersion.MemoVersion11);

                // When
                var meMoValidatorErrors = validator.Validate(message);

                // Then
                Assert.That(meMoValidatorErrors, Is.Empty);
            }
        }

        [Test]
        public void MeMoValidatorConstructorWithJustMeMoVersionThrowsExceptionIfMessageIsNotProvided()
        {
            // Given
            var memo = new FileInfo(Path.Combine(Environment.CurrentDirectory, XML_DIR, "MeMo_Full_Example.xml"));
            using (var fileStream = new FileStream(memo.FullName, FileMode.Open))
            {
                // And
                var parser = MeMoParserFactory.CreateParser(fileStream, false);
                parser.Parse();

                // And
                var validator = MeMoValidatorFactory.CreateMeMoValidator(MemoVersion.MemoVersion11);

                // When & Then
                Assert.Throws<MeMoValidationException>(() => validator.Validate());
            }
        }

        [Test]
        public void ValidateAValidNemsms()
        {
            // Given
            var memo = new FileInfo(
                Path.Combine(Environment.CurrentDirectory, XML_VALIDATOR_DIR, "MeMo_Valid_Nemsms.xml"));
            using (var fileStream = new FileStream(memo.FullName, FileMode.Open))
            {
                // And
                var parser = MeMoParserFactory.CreateParser(fileStream, false);
                ;
                var message = parser.Parse();

                // And
                var validator = MeMoValidatorFactory.CreateMeMoValidator(message);

                // When
                var meMoValidatorErrors = validator.Validate();

                // Then
                Assert.That(meMoValidatorErrors, Is.Empty);
            }
        }
    }
}