﻿using System;
using System.Collections.Generic;
using System.IO;
using Dk.Digst.Digital.Post.Memolib.Container;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Test.Util;
using NUnit.Framework;
using File = System.IO.File;

namespace Dk.Digst.Digital.Post.Memolib.Test.Container
{
    [TestFixture, SingleThreaded]
    public class MeMoContainerWriterTests
    {
        public void InitializeTest(string path)
        {
            _stream = File.Create(path);
            _meMoContainer = new MeMoContainerOutputStream(_stream);
            _containerWriter = new MeMoContainerWriter(_meMoContainer);
        }

        public void Dispose()
        {
            _containerWriter?.Dispose();
            _stream.Dispose();
        }

        private MeMoContainerWriter _containerWriter;

        private Stream _stream;

        private MeMoContainerOutputStream _meMoContainer;

        [Test]
        public void MeMoContainerCanBeWrittenCorrectlyVersion_1_1()
        {
            var path = Path.Combine(
                AppDomain.CurrentDomain.BaseDirectory,
                "write_test_target_1_1.tar.lzma");
            InitializeTest(path);

            // When: Two messages are written to a MeMoContainer stream
            Message message1 = PrefilledMessageBuilder.Message().MemoVersion(MemoVersion.MemoVersion11)
                .MemoSchVersion(MemoSchVersion.Name).Build();
            Message message2 = PrefilledMessageBuilder.Message().MemoVersion(MemoVersion.MemoVersion11)
                .MemoSchVersion(MemoSchVersion.Name).Build();

            _containerWriter.WriteEntry("memo_1.xml", message1);
            _containerWriter.WriteEntry("memo_2.xml", message2);

            // And: The message is written to a file
            _containerWriter.Dispose();
            _stream.Dispose();

            // And: The file is read
            List<Message> messages = TarReaderUtil.ReadAllMessagesFromTar(
                Path.Combine(path), memoVersion: MemoVersion.MemoVersion11);

            // Then: The read messages are equal to the written
            Assert.AreEqual(2, messages.Count);

            ObjectsComparer.Comparer<Message> comparer = new ObjectsComparer.Comparer<Message>();
            Assert.IsEmpty(comparer.CalculateDifferences(message1, messages[0]));
            Assert.IsEmpty(comparer.CalculateDifferences(message2, messages[1]));

            Dispose();
        }

        [Test]
        public void MeMoContainerCanBeWrittenCorrectlyVersion_1_2()
        {
            var path = Path.Combine(
                AppDomain.CurrentDomain.BaseDirectory,
                "write_test_target_1_2.tar.lzma");
            InitializeTest(path);

            // When: Two messages are written to a MeMoContainer stream
            Message message1 = PrefilledMessageBuilder.Message().MemoVersion(MemoVersion.MemoVersion12).Build();
            message1.memoSchVersion = null;
            Message message2 = PrefilledMessageBuilder.Message().MemoVersion(MemoVersion.MemoVersion12).Build();
            message2.memoSchVersion = null;

            _containerWriter.WriteEntry("memo_1.xml", message1);
            _containerWriter.WriteEntry("memo_2.xml", message2);

            // And: The message is written to a file
            _containerWriter.Dispose();
            _stream.Dispose();

            // And: The file is read
            List<Message> messages = TarReaderUtil.ReadAllMessagesFromTar(
                Path.Combine(path), memoVersion: MemoVersion.MemoVersion12);

            // Then: The read messages are equal to the written
            Assert.AreEqual(2, messages.Count);

            ObjectsComparer.Comparer<Message> comparer = new ObjectsComparer.Comparer<Message>();
            Assert.IsEmpty(comparer.CalculateDifferences(message1, messages[0]));
            Assert.IsEmpty(comparer.CalculateDifferences(message2, messages[1]));

            Dispose();
        }
    }
}