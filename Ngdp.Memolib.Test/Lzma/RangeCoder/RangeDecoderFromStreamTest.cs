﻿using Dk.Digst.Digital.Post.Memolib.Lzma;
using Dk.Digst.Digital.Post.Memolib.Lzma.RangeCoder;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Test.Lzma.RangeCoder
{
    [TestFixture]
    public class RangeDecoderFromStreamTest
    {
        [Test]
        public void ConstructorThrowsException()
        {
            Assert.Throws(typeof(CorruptedInputException), () => new RangeDecoderFromStream(new MemoryStream(Encoding.UTF8.GetBytes("AAAAAAAAAAAAAAAAAAAAAAAA"))));
        }

        [Test]
        public void InstanceIsCreatedCorrectly()
        {
            byte[] buf = new byte[] { 0, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65 };
            MemoryStream ms = new MemoryStream(buf);

            RangeDecoderFromStream actualRangeDecoderFromStream = new RangeDecoderFromStream(ms);
            Assert.False(actualRangeDecoderFromStream.Finished);
            Assert.AreEqual(0xFFFFFFFF, actualRangeDecoderFromStream.range);
        }

        [Test]
        public void IsFinished()
        {
            byte[] buf = new byte[] { 0, 0, 0, 0, 0, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65 };
            MemoryStream ms = new MemoryStream(buf);

            Assert.True((new RangeDecoderFromStream(ms)).Finished);
        }

        [Test]
        public void Normalized()
        {
            byte[] buf = new byte[] { 0, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65 };
            MemoryStream ms = new MemoryStream(buf);

            RangeDecoderFromStream rangeDecoderFromStream = new RangeDecoderFromStream(ms);
            rangeDecoderFromStream.Normalize();
            Assert.False(rangeDecoderFromStream.Finished);
            Assert.AreEqual(0xFFFFFFFF, rangeDecoderFromStream.range);
        }
    }
}
