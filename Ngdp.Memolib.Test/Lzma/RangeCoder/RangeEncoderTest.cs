﻿using Dk.Digst.Digital.Post.Memolib.Lzma.RangeCoder;
using NUnit.Framework;
using System;

namespace Dk.Digst.Digital.Post.Memolib.Test.Lzma.RangeCoder
{
    [TestFixture]
    public class RangeEncoderTest
    {
        [Test]
        public virtual void TestGetBitPrice()
        {
            Assert.AreEqual(1, RangeEncoder.GetBitPrice(1, 1));
            Assert.AreEqual(128, RangeEncoder.GetBitPrice(1, 0));
            Assert.Throws(typeof(IndexOutOfRangeException), () => RangeEncoder.GetBitPrice(-16777216, 1));
        }

        [Test]
        public virtual void TestGetBitTreePrice()
        {
            Assert.AreEqual(129, RangeEncoder.GetBitTreePrice(new short[] { 1, 1, 1, 1 }, 1));
            Assert.Throws(typeof(IndexOutOfRangeException), () => RangeEncoder.GetBitTreePrice(new short[] { 1, -1, 1, 1 }, 1));
            Assert.Throws(typeof(IndexOutOfRangeException), () => RangeEncoder.GetBitTreePrice(new short[] { }, 1));
        }

        [Test]
        public virtual void TestGetReverseBitTreePrice()
        {
            Assert.AreEqual(129, RangeEncoder.GetReverseBitTreePrice(new short[] { 1, 1, 1, 1 }, 1));
            Assert.Throws(typeof(IndexOutOfRangeException), () => RangeEncoder.GetReverseBitTreePrice(new short[] { 1, -1, 1, 1 }, 1));
        }

        [Test]
        public virtual void TestGetDirectBitsPrice()
        {
            Assert.AreEqual(48, RangeEncoder.GetDirectBitsPrice(3));
        }
    }
}
