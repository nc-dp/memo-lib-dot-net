﻿using Dk.Digst.Digital.Post.Memolib.Lzma.Lz;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Test.Lzma.Lz
{
    [TestFixture]
    public class BT4Test
    {
        [Test]
        public void BT4MemoryUsageIsCalculatedCorrectly()
        {
            Assert.AreEqual(530, BT4.GetMemoryUsage(3));
        }
    }
}
