﻿using Dk.Digst.Digital.Post.Memolib.Lzma.Lz;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Test.Lzma.Lz
{
    [TestFixture]
    public class Hash234Test
    {
        [Test]
        public void Hash234MemoryUsageIsCalculatedCorrectly()
        {
            Assert.AreEqual(520, Hash234.GetMemoryUsage(3));
            Assert.AreEqual(4194568, Hash234.GetMemoryUsage(0));
        }
    }
}
