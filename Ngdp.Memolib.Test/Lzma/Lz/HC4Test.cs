﻿using Dk.Digst.Digital.Post.Memolib.Lzma;
using Dk.Digst.Digital.Post.Memolib.Lzma.Lz;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Test.Lzma.Lz
{
    [TestFixture]
    public class HC4Test
    {
        private ArrayCache arrayCache;

        [SetUp]
        public void SetUp()
        {
            byte [] buf = Encoding.UTF8.GetBytes("AAAAAAAAAAAAAAAAAAAAAAAA");
            arrayCache = new ArrayCache();
            arrayCache.PutArray(buf);
        }

        [Test]
        public void HC4MemoryUsageIsCalculatedCorrectly()
        {
            Assert.AreEqual(530, HC4.GetMemoryUsage(3));
        }

        [Test]
        [TestCase(0, 3, 1, 3, 3, 2)]
        [TestCase(536870912, 3, 1, 3, 3, 2)]
        public void HC4ObjectIsCreated(int dictSize, int beforeSizeMin, int readAheadMax, int niceLen, int matchLenMax, int depthLimit)
        { 
            new HC4(dictSize, beforeSizeMin, readAheadMax, niceLen, matchLenMax, depthLimit, arrayCache);
        }

        [Test]
        public void ExceptionIsThrownDuringCreatingHC4Object()
        {
            Assert.Throws(typeof(OverflowException), () => new HC4(3, 3, 1, 0, 3, 2, arrayCache));
        }

        [Test]
        public void GetMatchesReturnsCountZero()
        {
            HC4 hc4 = new HC4(3, 3, 1, 3, 3, 2, arrayCache);
            Assert.AreEqual(0, hc4.GetMatches().count);
            Assert.IsTrue(hc4.IsStarted());
        }

        [Test]
        public void GetMatchesThrowsException()
        {
            Assert.Throws(typeof(IndexOutOfRangeException), () => (new HC4(3, 3, 1, 3, 0, 2, arrayCache)).GetMatches());
        }

        [Test]
        public void SkipTest()
        {
            HC4 hc4 = new HC4(3, 3, 1, 3, 3, 2, arrayCache);
            hc4.Skip(3);
            Assert.IsTrue(hc4.IsStarted());
            Assert.AreEqual(0, hc4.GetMatches().count);
        }
    }
}
