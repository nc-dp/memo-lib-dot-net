﻿using Dk.Digst.Digital.Post.Memolib.Lzma;
using Dk.Digst.Digital.Post.Memolib.Lzma.Lz;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Test.Lzma.Lz
{
    [TestFixture]
    public class LZDecoderTest
    {
        [Test]
        public void LZDecoderObjectIsCreated()
        {
            byte[] presetDict = Encoding.UTF8.GetBytes("AAAAAAAAAAAAAAAAAAAAAAAA");
            LZDecoder actualLzDecoder = new LZDecoder(3, presetDict, new ArrayCache());
            Assert.AreEqual(3, actualLzDecoder.Pos);
            Assert.False(actualLzDecoder.HasSpace());
            Assert.False(actualLzDecoder.HasPending());
        }


        [Test]
        public void LZDecoderObjectIsCreatedWithNullPresetDict()
        {
            LZDecoder actualLzDecoder = new LZDecoder(3, null, new ArrayCache());
            Assert.AreEqual(0, actualLzDecoder.Pos);
            Assert.False(actualLzDecoder.HasSpace());
            Assert.False(actualLzDecoder.HasPending());
        }
    }
}
