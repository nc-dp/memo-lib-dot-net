﻿using Dk.Digst.Digital.Post.Memolib.Lzma;
using Dk.Digst.Digital.Post.Memolib.Lzma.Lz;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Test.Lzma.Lz
{
    [TestFixture]
    public class LZEncoderTest
    {
        [Test]
        public virtual void ArrayIsNormalizedCorrectly()
        {
            // Arrange
            int[] positions = new int[] { 1, 1, 1, 1 };
            int[] expected = new int[] { 0, 0, 0, 1 };

            // Act
            LZEncoder.Normalize(positions, 3, 1);

            // Assert
            Assert.IsTrue(Enumerable.SequenceEqual(expected, positions));
        }

        [Test]
        public virtual void ArrayIsNormalizedCorrectly2()
        {
            // Arrange
            int[] positions = new int[] { 3, 1, 1, 1 };
            int[] expected = new int[] { 2, 0, 0, 1 };

            // Act
            LZEncoder.Normalize(positions, 3, 1);

            // Assert
            Assert.IsTrue(Enumerable.SequenceEqual(expected, positions));
        }

        [Test]
        public virtual void NarmalizeThrowsException()
        {
            Assert.Throws(typeof(System.IndexOutOfRangeException), () => LZEncoder.Normalize(new int[] { }, 3, 1));
        }

        [Test]
        public virtual void LZEncoderMemoryUsageIsCalculatedCorrectly()
        {
            Assert.Throws(typeof(System.ArgumentException), () => LZEncoder.GetMemoryUsage(3, 3, 3, 3, 1));
            Assert.AreEqual(796, LZEncoder.GetMemoryUsage(1, 1, 1, 1, 4));
            Assert.AreEqual(796, LZEncoder.GetMemoryUsage(1, 1, 1, 1, LZEncoder.MF_BT4));
        }

        [Test]
        [TestCase(3, 3, 3, 3, 3, 1, 2, typeof(System.ArgumentException))]
        [TestCase(int.MinValue, 1, 1, 1, 1, 4, 1, typeof(OverflowException))]
        public virtual void GetInstanceThrowsException(int dictSize, int extraSizeBefore, int extraSizeAfter, int niceLen, int matchLenMax, int mf, int depthLimit, Type exceptionType)
        {
            Assert.Throws(exceptionType, () => LZEncoder.GetInstance(dictSize, extraSizeBefore, extraSizeAfter, niceLen, matchLenMax, mf, depthLimit, new ArrayCache()));
        }

        [Test]
        public virtual void InstnaceWithHC4CreatedCorrectly()
        {
            LZEncoder actualInstance = LZEncoder.GetInstance(1, 1, 1, 1, 1, 4, 1, new ArrayCache());
            int actualResultInt = ((HC4)actualInstance).niceLen;
            Matches matches = actualInstance.GetMatches();
            int[] intArray = matches.len;
            Assert.AreEqual(1, actualResultInt);
            Assert.AreEqual(0, actualInstance.GetPos());
            Assert.AreEqual(262148, ((HC4)actualInstance).buf.Length);
            Assert.AreEqual(262148, ((HC4)actualInstance).bufSize);
            Assert.AreEqual(1, ((HC4)actualInstance).matchLenMax);
            Assert.AreEqual(0, intArray.Length);
            Assert.AreEqual(0, matches.dist.Length);
            Assert.AreEqual(0, matches.count);
        }

        [Test]
        public virtual void InstnaceWithBT4CreatedCorrectly()
        {
            LZEncoder actualInstance = LZEncoder.GetInstance(1, 1, 1, 1, 1, LZEncoder.MF_BT4, 1, new ArrayCache());
            Assert.AreEqual(-1, actualInstance.GetPos());
            Assert.AreEqual(1, ((BT4)actualInstance).niceLen);
            Assert.AreEqual(1, ((BT4)actualInstance).matchLenMax);
            Assert.AreEqual(262148, ((BT4)actualInstance).bufSize);
            Assert.AreEqual(262148, ((BT4)actualInstance).buf.Length);
        }

        [Test]
        [TestCase(0, 1, 1, 1, 1, 4, 1)]
        [TestCase(0, 1, 1, 1, 1, LZEncoder.MF_BT4, 1)]
        public virtual void GetInstanceNoException(int dictSize, int extraSizeBefore, int extraSizeAfter, int niceLen, int matchLenMax, int mf, int depthLimit)
        {
            LZEncoder.GetInstance(dictSize, extraSizeBefore, extraSizeAfter, niceLen, matchLenMax, mf, depthLimit, new ArrayCache());
        }
    }
}
