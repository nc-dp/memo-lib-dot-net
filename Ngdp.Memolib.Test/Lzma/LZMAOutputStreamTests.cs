﻿using Dk.Digst.Digital.Post.Memolib.Lzma;
using Dk.Digst.Digital.Post.Memolib.Test.Util;
using NUnit.Framework;
using SharpCompress.Compressors.LZMA;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Test.Lzma
{
    [TestFixture]
    public class LZMAOutputStreamTests
    {
        [Test]
        public void StreamSuccessfullyCompressed()
        {
            // Arrange
            byte[] expected =  {93, 0, 0, 0, 8, 255, 255, 255, 255, 255, 255, 255, 255, 0, 0, 65, 254, 247, 255, 255, 224, 0, 128, 0};
            MemoryStream stream = new MemoryStream();
            LZMAOptions options = new LZMAOptions(3);
            options.DictSize = 128 * 1024 * 1024;

            // Act
            LZMAOutputStream lzmaStream = new LZMAOutputStream(stream, options, -1);
            lzmaStream.Write(256);
            lzmaStream.Close();
            byte[] actual = stream.ToArray();

            // Assert
            Assert.AreEqual(expected.Length, actual.Length);
            for (int i = 0; i < expected.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i]);
            }
        }

        [Test]
        public void CompressAndDecompressStream()
        {
            // Arrange
            MemoryStream stream = new MemoryStream();
            MemoryStream actual = new MemoryStream();
            LZMAOptions options = new LZMAOptions(3);
            options.DictSize = 128 * 1024 * 1024;

            // Act
            LZMAOutputStream lzmaStream = new LZMAOutputStream(stream, options, -1);
            File.OpenRead(MeMoTestDataProvider.OneMbFileLocation()).CopyTo(lzmaStream);
            lzmaStream.Finish();
            stream.Position = 0;
            LZMAInputStream lzma = new LZMAInputStream(stream);

            int r = lzma.Read();
            while (r != -1)
            {
                actual.WriteByte((byte)r);

                r = lzma.Read();
            }
            actual.Position = 0;

            // Assert
            Assert.IsTrue(StreamEquals(actual, File.OpenRead(MeMoTestDataProvider.OneMbFileLocation())));
        }

        private bool StreamEquals(Stream a, Stream b)
        {
            if (a == b)
            {
                return true;
            }

            if (a == null || b == null)
            {
                throw new ArgumentNullException(a == null ? "a" : "b");
            }

            if (a.Length != b.Length)
            {
                return false;
            }

            for (int i = 0; i < a.Length; i++)
            {
                int aByte = a.ReadByte();
                int bByte = b.ReadByte();
                if (aByte.CompareTo(bByte) != 0)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
