﻿using Dk.Digst.Digital.Post.Memolib.Lzma;
using Dk.Digst.Digital.Post.Memolib.Lzma.Lz;
using Dk.Digst.Digital.Post.Memolib.Lzma.Lzma;
using Dk.Digst.Digital.Post.Memolib.Lzma.RangeCoder;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Test.Lzma.Lzma
{
    [TestFixture]
    public class LZMADecoderTest
    {
        [Test]
        public void LZMADecoderObjectCreated()
        {
            byte[] presetDict = Encoding.UTF8.GetBytes("AAAAAAAAAAAAAAAAAAAAAAAA");
            LZMADecoder actualLzmaDecoder = new LZMADecoder(new LZDecoder(3, presetDict, new ArrayCache()), null, 3, 0, 2);
            Assert.AreEqual(16, actualLzmaDecoder.distAlign.Length);
            Assert.AreEqual(4, actualLzmaDecoder.reps.Length);
            Assert.AreEqual(4, actualLzmaDecoder.distSlots.Length);
            Assert.AreEqual(12, actualLzmaDecoder.isMatch.Length);
            Assert.AreEqual(3, actualLzmaDecoder.posMask);
            Assert.AreEqual(12, actualLzmaDecoder.isRep1.Length);
            Assert.AreEqual(12, actualLzmaDecoder.isRep2.Length);
            Assert.AreEqual(12, actualLzmaDecoder.isRep0Long.Length);
            Assert.AreEqual(12, actualLzmaDecoder.isRep0.Length);
            Assert.AreEqual(12, actualLzmaDecoder.isRep.Length);
            Assert.AreEqual(10, actualLzmaDecoder.distSpecial.Length);
            Assert.AreEqual(0, actualLzmaDecoder.state.Get());
        }

        [Test]
        public void ResetTest()
        {
            byte[] presetDict = Encoding.UTF8.GetBytes("AAAAAAAAAAAAAAAAAAAAAAAA");
            LZMADecoder lzmaDecoder = new LZMADecoder(new LZDecoder(3, presetDict, new ArrayCache()), null, 3, 0, 2);
            lzmaDecoder.Reset();
            Assert.AreEqual(0, lzmaDecoder.state.Get());
        }


        [Test]
        public void EndMarkerDetected()
        {
            byte[] presetDict = Encoding.UTF8.GetBytes("AAAAAAAAAAAAAAAAAAAAAAAA");
            Assert.False((new LZMADecoder(new LZDecoder(3, presetDict, new ArrayCache()), null, 3, 0, 2)).EndMarkerDetected());
        }


        [Test]
        public void DecodeTest()
        {
            byte[] presetDict = Encoding.UTF8.GetBytes("AAAAAAAAAAAAAAAAAAAAAAAA");
            LZDecoder lz = new LZDecoder(3, presetDict, new ArrayCache());
            byte[] buf = new byte[] { 0, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41 };
            MemoryStream ms = new MemoryStream(buf);

            (new LZMADecoder(lz, new RangeDecoderFromStream(ms), 3, 0, 2)).Decode();
        }
    }
}
