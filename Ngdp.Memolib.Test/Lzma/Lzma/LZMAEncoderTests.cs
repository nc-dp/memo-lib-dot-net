﻿using Dk.Digst.Digital.Post.Memolib.Lzma.Lzma;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Test.Lzma.Lzma
{

    [TestFixture]
    public class LZMAEncoderTests
    {
        [Test]
        [TestCase(-2147483648, 62)]
        [TestCase(-123456, 63)]
        [TestCase(-1, 63)]
        [TestCase(0, 0)]
        [TestCase(1, 1)]
        [TestCase(123456, 33)]
        [TestCase(2147483647, 61)]
        public void DistanceCalculatedCorrectly(int dist, int expected)
        { 
            var actual = LZMAEncoder.GetDistSlot(dist);

            Assert.AreEqual(expected, actual);
        }
    }
}
