﻿using System.IO;
using Dk.Digst.Digital.Post.Memolib.Test.Util;
using Dk.Digst.Digital.Post.Memolib.Util;
using Dk.Digst.Digital.Post.Memolib.Writer;
using NUnit.Framework;

namespace Dk.Digst.Digital.Post.Memolib.Test.Writer
{
    [TestFixture]
    public class DefaultFileContentResolverTests
    {
        [Test]
        public void InvalidFilepathReturnsOptionalOfEmpty()
        {
            // Given
            DefaultFileContentResolver resolver = new DefaultFileContentResolver();

            // When
            Optional<Stream> stream = resolver.Resolve("https://host.domain");

            // Then
            Assert.True(stream.IsEmpty());
        }

        [Test]
        public void ValidFilePathCanBeResolved()
        {
            // Given
            string filePath = MeMoTestDataProvider.ModelPdf();
            DefaultFileContentResolver resolver = new DefaultFileContentResolver();

            // When
            Optional<Stream> stream = resolver.Resolve(filePath);

            // Then
            Assert.That(stream.IsPresent);
            Assert.That(stream.Get().Length > 0);

            // Cleanup
            stream.Get().Dispose();
        }
    }
}