﻿using System;
using System.IO;
using System.Linq;
using Dk.Digst.Digital.Post.Memolib.Factory;
using Dk.Digst.Digital.Post.Memolib.Writer;
using NUnit.Framework;

namespace Dk.Digst.Digital.Post.Memolib.Test.Writer
{
    [TestFixture]
    public class MeMoWriterFactoryTests
    {
        [Test]
        public void FactoryCanCreateAWriter()
        {
            // When
            Stream stream = new MemoryStream();
            IMeMoStreamWriter writer = MeMoWriterFactory.CreateWriter(stream);

            // Then
            Assert.IsNotNull(writer);
        }

        [Test]
        public void FactoryCanCreateWithFileContentLoader()
        {
            // Given
            Stream stream = new MemoryStream();
            FileContentLoader loader = new FileContentLoader();

            // When
            IMeMoStreamWriter writer = MeMoWriterFactory.CreateWriter(stream, loader);

            // Then
            Assert.AreEqual(loader, writer.FileContentLoader);
            Assert.AreEqual(typeof(DefaultFileContentResolver),
                writer.FileContentLoader.Resolvers.Single().GetType());
        }

        [Test]
        public void FactoryThrowsAnExceptionForNoStream()
        {
            // Then
            Assert.Throws<ArgumentNullException>(() => MeMoWriterFactory.CreateWriter(null));
        }
    }
}