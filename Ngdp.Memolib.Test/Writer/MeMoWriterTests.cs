﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using Dk.Digst.Digital.Post.Memolib.Builder;
using Dk.Digst.Digital.Post.Memolib.Factory;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Parser;
using Dk.Digst.Digital.Post.Memolib.Test.Util;
using Dk.Digst.Digital.Post.Memolib.Validation;
using Dk.Digst.Digital.Post.Memolib.Writer;
using Microsoft.XmlDiffPatch;
using NUnit.Framework;
using ObjectsComparer;
using File = System.IO.File;

namespace Dk.Digst.Digital.Post.Memolib.Test.Writer
{
    [TestFixture]
    public class MeMoWriterTests
    {
        [SetUp]
        public void InitializeTest()
        {
            _output = new MemoryStream();
            _meMoStreamWriter = MeMoWriterFactory.CreateWriter(_output);
        }

        [TearDown]
        public void Dispose()
        {
            _meMoStreamWriter.Dispose();
            _output?.Dispose();
        }

        private Stream _output;

        private IMeMoStreamWriter _meMoStreamWriter;


        [TestCase(
            "Resources/Files/Xml/MeMo_Full_Example.xml",
            TestName = "MeMo Full Example")]
        [TestCase(
            "Resources/Files/Xml/MeMo_Full_Example_V2.xml",
            TestName = "MeMo Full Example V2")]
        [TestCase(
            "Resources/Files/Xml/MeMo_Minimum_Example.xml",
            TestName = "MeMo Minimum Example")]
        [TestCase(
            "Resources/Files/Xml/MeMo_Scenarie1_Eksempel.xml",
            TestName = "MeMo Scenarie 1")]
        [TestCase(
            "Resources/Files/Xml/MeMo_Scenarie2_Eksempel_1af2.xml",
            TestName = "MeMo Scenarie 2 1af2")]
        [TestCase(
            "Resources/Files/Xml/MeMo_Scenarie2_Eksempel_2af2.xml",
            TestName = "MeMo Scenarie 2 2af2")]
        [TestCase(
            "Resources/Files/Xml/MeMo_Scenarie3_Eksempel.xml",
            TestName = "MeMo Scenarie 3")]
        [TestCase(
            "Resources/Files/Xml/MeMo_Scenarie4_Eksempel_1af3.xml",
            TestName = "MeMo Scenarie 4 1af3")]
        [TestCase(
            "Resources/Files/Xml/MeMo_Scenarie4_Eksempel_2af3.xml",
            TestName = "MeMo Scenarie 4 2af3")]
        [TestCase(
            "Resources/Files/Xml/MeMo_Scenarie4_Eksempel_3af3.xml",
            TestName = "MeMo Scenarie 4 3af3")]
        [TestCase(
            "Resources/Files/Xml/MeMo_Scenarie5_Eksempel.xml",
            TestName = "MeMo Scenarie 5")]
        [TestCase(
            "Resources/Files/Xml/MeMo_Scenarie6_Eksempel.xml",
            TestName = "MeMo Scenarie 6")]
        [TestCase(
            "Resources/Files/Xml/memo_version_1_2/MeMo_Full_Example_V1.2.xml",
            TestName = "MeMo Full Example V1.2")]
        [TestCase(
            "Resources/Files/Xml/memo_version_1_2/MeMo_Minimum_Example_V1.2_Default.xml",
            TestName = "MeMo Minimum Example V1.2")]
        public void MeMoExamplesCanBeParsedAndWrittenWithoutLosingContent(
            string path)
        {
            // Given
            Message message = MeMoParserFactory.CreateParser(
                File.OpenRead(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path)), false).Parse();

            // When
            _meMoStreamWriter.Write(message);
            _output.Flush();
            _output.Position = 0;

            // Then
            string serializedMessage = new StreamReader(_output).ReadToEnd();


            XDocument res = new XDocument();

            XmlWriter writer = res.CreateWriter();

            // ReSharper disable once BitwiseOperatorOnEnumWithoutFlags
            XmlDiff diff = new XmlDiff(
                XmlDiffOptions.IgnoreComments | XmlDiffOptions.IgnoreNamespaces | XmlDiffOptions.IgnoreChildOrder);
            bool equal = diff.Compare(
                XElement.Parse(new StreamReader(File.OpenRead(path)).ReadToEnd()).CreateReader(),
                XElement.Parse(serializedMessage).CreateReader(),
                writer);
            writer.Flush();
            writer.Close();

            if (!equal)
                throw new AssertionException("Test failed due to XML strings not being equal. Errors: " + res);
            Assert.IsTrue(equal);
        }

        [Test]
        public void ExternalContentCanBeIncluded()
        {
            // Given
            Message message = PrefilledMessageBuilder.Message().Build();

            // And
            IFileContent content = new ExternalFileContent(MeMoTestDataProvider.ModelPdf(), false);
            message.MessageBody.MainDocument.File = new[]
            {
                FileBuilder.NewBuilder().Content(content).Language("da").Filename("abc")
                    .EncodingFormat("application/xml").Build()
            };

            // When
            _meMoStreamWriter.Write(message);
            _output.Flush();
            _output.Position = 0;

            // Then
            MeMoSchemaValidatorProvider.Validate(_output);
        }

        [Test]
        public void FullMessageCanBeWrittenAndParsed()
        {
            // Given
            Message message = PrefilledMessageBuilder.Message().Build();
            message.memoSchVersion = "1.1.0";
            message.memoVersion = new decimal(1.1);

            // When
            _meMoStreamWriter.Write(message);
            _output.Flush();
            _output.Position = 0;

            // Then
            Message result = MeMoParserFactory.CreateParser(_output, true).Parse();

            Comparer<Message> comparer = new Comparer<Message>();
            Assert.IsEmpty(comparer.CalculateDifferences(message, result));
        }

        [Test]
        public void MessageWithNoReplyUUIDCanBeWrittenAndParsed()
        {
            // Given
            Message message = MessageBuilder.NewBuilder()
                .MessageHeader(PrefilledMessageBuilder.MessageHeaderNoReplyUUID().Build())
                .MessageBody(PrefilledMessageBuilder.MessageBody().Build())
                .Build();
            message.memoSchVersion = "1.1.0";
            message.memoVersion = new decimal(1.1);

            // When
            _meMoStreamWriter.Write(message);
            _output.Flush();
            _output.Position = 0;

            // Then
            Message result = MeMoParserFactory.CreateParser(_output, true).Parse();
            Assert.AreEqual(null, result.MessageHeader.ReplyData[0].replyUUID);

            Comparer<Message> comparer = new Comparer<Message>();
            Assert.IsEmpty(comparer.CalculateDifferences(message, result));
        }

        [Test]
        public void DateTimeIsFormattedAs24HoursClock()
        {
            // Given
            Message message = PrefilledMessageBuilder.Message().Build();
            message.memoSchVersion = "1.1.0";
            message.MessageHeader.replyByDateTime = DateTime.Parse("2100-02-03T16:00:01").ToUniversalTime();
            message.memoVersion = new decimal(1.1);

            // When
            _meMoStreamWriter.Write(message);
            _output.Flush();
            _output.Position = 0;

            // Then
            Message result = MeMoParserFactory.CreateParser(_output, true).Parse();
            Assert.AreEqual(result.MessageHeader.replyByDateTime, message.MessageHeader.replyByDateTime);
        }

        [Test]
        public void ValidMemoCanBeGenerated()
        {
            // Given
            Message message = PrefilledMessageBuilder.Message().Build();

            // When
            _meMoStreamWriter.Write(message);
            _output.Flush();
            _output.Position = 0;

            // Then
            MeMoSchemaValidatorProvider.Validate(_output);
        }

        [Test]
        public void ValidMemoWithNoBody()
        {
            // Given
            Message message = PrefilledMessageBuilder.Message().Build();
            message.MessageBody = null;

            // When
            _meMoStreamWriter.Write(message);
            _output.Flush();
            _output.Position = 0;

            // Then
            MeMoSchemaValidatorProvider.Validate(_output);
        }

        [Test]
        public void WriterClosesFilestream()
        {
            // Arrange
            Message message = PrefilledMessageBuilder.Message().Build();
            _meMoStreamWriter.Write(message);

            // When
            _meMoStreamWriter.Dispose();

            // Then
            Assert.IsTrue(_meMoStreamWriter.IsClosed());
            Assert.IsTrue(_output.CanWrite);
        }
    }
}