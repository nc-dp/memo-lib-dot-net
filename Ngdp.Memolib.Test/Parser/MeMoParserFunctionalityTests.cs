﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Schema;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Parser;
using Dk.Digst.Digital.Post.Memolib.Test.Util;
using Dk.Digst.Digital.Post.Memolib.Visitor;
using NUnit.Framework;
using File = Dk.Digst.Digital.Post.Memolib.Model.File;

namespace Dk.Digst.Digital.Post.Memolib.Test.Parser
{
    [TestFixture]
    public class MeMoParserFunctionalityTests
    {
        [SetUp]
        public void Setup()
        {
            _stream = MeMoTestDataProvider.StreamMeMoMessageFullExample();
            _parser = MeMoParserFactory.CreateParser(_stream);
        }

        [TearDown]
        public void Cleanup()
        {
            _parser.Dispose();
            _stream?.Dispose();
        }

        private IMeMoParser _parser;

        private Stream _stream;

        [Test]
        public void CallingDisposeDoesNotDisposeUnderlyingStream()
        {
            _parser.Dispose();
            Assert.IsTrue(_parser.IsDisposed());
            Assert.DoesNotThrow(() => _stream.CopyTo(new MemoryStream()));
        }

        [Test]
        public void DisposingTwiceDoesNothing()
        {
            _parser.Dispose();
            Assert.DoesNotThrow(() => _parser.Dispose());
        }

        [TestCase(MemoVersion.MemoVersion11, TestName = "Error is thrown if invalid, MeMo version 1.1")]
        [TestCase(MemoVersion.MemoVersion12, TestName = "Error is thrown if invalid, MeMo version 1.2")]
        public void IfValidationIsEnabledErrorIsThrownIfInvalid(MemoVersion memoVersion)
        {
            // Given
            Stream stream = MeMoTestDataProvider.StreamInvalidMeMoMessage();
            IMeMoParser parser = MeMoParserFactory.CreateParser(stream, true, memoVersion);

            // Then
            Assert.Throws<XmlSchemaValidationException>(() => parser.Traverse(new List<IMeMoStreamVisitor>()));
        }

        [Test]
        public void ParserCanParseMeMo()
        {
            // Given
            _parser.Parse();

            // Then
            Assert.IsTrue(_parser.IsDisposed());
        }


        [Test]
        public void ParserCanParseMeMoClassElementWithAttachedFile()
        {
            // Given
            Stream stream = MeMoTestDataProvider.StreamMeMoMessageFullExample();
            IMeMoParser parser = MeMoParserFactory.CreateParser(stream,true);
            IMeMoStreamVisitor visitor = MeMoStreamVisitorFactory.CreateConsumer<IMeMoClass>(
                c => c.ParseMeMoClassElements());

            // Then
            Assert.DoesNotThrow(() => parser.Traverse(new List<IMeMoStreamVisitor> {visitor}));
        }

        [Test]
        public void ParserCanTraverseMeMo()
        {
            // Given
            _parser.Traverse(new List<IMeMoStreamVisitor>());

            // Expect
            Assert.IsTrue(_parser.IsDisposed());
        }

        [Test]
        public void ParserVisitsAllMeMoClasses()
        {
            // Data
            SortedSet<string> allMeMoElements = new SortedSet<string>
            {
                "Action",
                "AdditionalContentData",
                "AdditionalDocument",
                "AdditionalReplyData",
                "Address",
                "AddressPoint",
                "AttentionData",
                "AttentionPerson",
                "CaseID",
                "ContactInfo",
                "ContactPoint",
                "ContentData",
                "ContentResponsible",
                "CPRdata",
                "CVRdata",
                "Education",
                "EID",
                "EMail",
                "EntryPoint",
                "File",
                "FORMdata",
                "ForwardData",
                "GeneratingSystem",
                "GlobalLocationNumber",
                "KLEdata",
                "MainDocument",
                "Message",
                "MessageBody",
                "MessageHeader",
                "MotorVehicle",
                "ProductionUnit",
                "PropertyNumber",
                "Recipient",
                "ReplyData",
                "Reservation",
                "SEnumber",
                "Sender",
                "SORdata",
                "TechnicalDocument",
                "Telephone",
                "UnstructuredAddress"
            };

            // Given
            SortedSet<string> visitedClasses = new SortedSet<string>();
            IMeMoStreamVisitor visitor = MeMoStreamVisitorFactory.CreateConsumer<IMeMoClass>(
                c => visitedClasses.Add(c.GetXmlElementName()));

            // When
            _parser.Traverse(new List<IMeMoStreamVisitor> {visitor});

            // Then
            Assert.AreEqual(allMeMoElements, visitedClasses);
        }

        [Test]
        public void VisitorCanGetFieldsOfCertainClass()
        {
            // Given
            List<string> additionalDocumentIds = new List<string>();
            IMeMoStreamVisitor visitor = MeMoStreamVisitorFactory.CreateConsumer<AdditionalDocument>(
                c =>
                {
                    MeMoClassElements elements = c.ParseMeMoClassElements();
                    additionalDocumentIds.Add(elements.Get(nameof(AdditionalDocument.additionalDocumentID)));
                });

            // When
            _parser.Traverse(new List<IMeMoStreamVisitor> {visitor});

            // Then
            Assert.AreEqual(new List<string> {"789", "678"}, additionalDocumentIds);
        }

        [Test]
        public void VisitorCanParseCertainElement()
        {
            // Given
            List<File> files = new List<File>();
            IMeMoStreamVisitor visitor = MeMoStreamVisitorFactory.CreateConsumer<File>(
                c => files.Add(c.ParseMeMoClass<File>()));

            // When
            _parser.Traverse(new List<IMeMoStreamVisitor> {visitor});

            // Then
            Assert.AreEqual(6, files.Count);
            Assert.AreEqual(new List<string>
            {
                "Pladsanvisning.pdf",
                "Pladsanvisning.txt",
                "Pladsanvisning.pdf",
                "Praktiske oplysninger.doc",
                "vejledning.pdf",
                "TekniskDokument.xml"
            }, files.Select(f => f.filename));
        }

        [Test]
        public void VisitorCanTraverseChildrenOfMeMoClass()
        {
            // Given
            List<string> filenames = new List<string>();
            IMeMoStreamVisitor fileVisitor = MeMoStreamVisitorFactory.CreateConsumer<File>(
                c =>
                {
                    MeMoClassElements elements = c.ParseMeMoClassElements();
                    filenames.Add(elements.Get(nameof(File.filename)));
                });

            List<string> additionalDocumentIds = new List<string>();
            IMeMoStreamVisitor additionalDocumentVisitor
                = MeMoStreamVisitorFactory.CreateConsumer<AdditionalDocument>(
                    c =>
                    {
                        MeMoClassElements elements = c.ParseMeMoClassElements();
                        additionalDocumentIds.Add(elements.Get(nameof(AdditionalDocument.additionalDocumentID)));
                        c.TraverseChildren(new List<IMeMoStreamVisitor> {fileVisitor});
                    });

            // When
            _parser.Traverse(new List<IMeMoStreamVisitor> {additionalDocumentVisitor});

            // Then
            Assert.AreEqual(new List<string> {"789", "678"}, additionalDocumentIds);
            Assert.AreEqual(new List<string> {"Pladsanvisning.pdf", "Praktiske oplysninger.doc", "vejledning.pdf"},
                filenames);
        }
    }
}