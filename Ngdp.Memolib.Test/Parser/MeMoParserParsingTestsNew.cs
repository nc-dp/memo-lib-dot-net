﻿using System;
using System.Collections.Generic;
using System.IO;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Parser;
using Dk.Digst.Digital.Post.Memolib.Test.Util;
using Dk.Digst.Digital.Post.Memolib.Visitor;
using NUnit.Framework;
using Action = Dk.Digst.Digital.Post.Memolib.Model.Action;

namespace Dk.Digst.Digital.Post.Memolib.Test.Parser
{
    [TestFixture]
    public class MeMoParserParsingTestsNew
    {
        [SetUp]
        public void Setup()
        {
            using (Stream stream = MeMoTestDataProvider.StreamMeMoMessageFullExample12())
            {
                using (IMeMoParser parser = MeMoParserFactory.CreateParser(stream, true))
                {
                    _message = parser.Parse();
                }
            }
        }

        private Message _message;

        [Test]
        public
            void NewMemoHeaderIsParsedCorrectly()
        {
            Representative representative = _message.MessageHeader.Sender.Representative;

            Assert.AreEqual(1.2m, _message.memoVersion);
            Assert.AreEqual("12345678", representative.representativeID);
            Assert.AreEqual(null, _message.memoSchVersion);
        }
    }
}