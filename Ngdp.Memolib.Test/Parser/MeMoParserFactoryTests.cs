﻿using System.IO;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Parser;
using NUnit.Framework;

namespace Dk.Digst.Digital.Post.Memolib.Test.Parser
{
    [TestFixture]
    public class MeMoParserFactoryTests
    {
        [TestCase(MemoVersion.MemoVersion11, TestName = "Non-validating MeMo version 1.1")]
        [TestCase(MemoVersion.MemoVersion12, TestName = "Non-validatingMeMo version 1.2")]
        public void NonValidatingParserCanBeCreated(MemoVersion memoVersion)
        {
            IMeMoParser parser = MeMoParserFactory.CreateParser(new MemoryStream(), false, memoVersion);
            Assert.IsNotNull(parser);
        }

        [TestCase(MemoVersion.MemoVersion11, TestName = "Validating MeMo version 1.1")]
        [TestCase(MemoVersion.MemoVersion12, TestName = "Validating MeMo version 1.2")]
        public void ValidatingParserCanBeCreated(MemoVersion memoVersion)
        {
            IMeMoParser parser = MeMoParserFactory.CreateParser(new MemoryStream(), true, memoVersion);
            Assert.IsNotNull(parser);
        }
    }
}