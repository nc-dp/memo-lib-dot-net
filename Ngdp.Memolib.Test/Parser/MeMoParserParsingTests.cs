﻿using System;
using System.IO;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Parser;
using Dk.Digst.Digital.Post.Memolib.Test.Util;
using NUnit.Framework;
using Action = Dk.Digst.Digital.Post.Memolib.Model.Action;

namespace Dk.Digst.Digital.Post.Memolib.Test.Parser
{
    [TestFixture]
    public class MeMoParserParsingTests
    {
        [SetUp]
        public void Setup()
        {
            using (Stream stream = MeMoTestDataProvider.StreamMeMoMessageFullExample())
            {
                using (IMeMoParser parser = MeMoParserFactory.CreateParser(stream, true))
                {
                    _message = parser.Parse();
                }
            }
        }

        private Message _message;


        private static T GetItem<T>(Action action, ItemsChoiceType choice)
        {
            return (T) action.Items[Array.IndexOf(action.ItemsElementName, choice)];
        }

        private void AssertContent(IFileContent fileContent, string expectedContent)
        {
            string actual = new StreamReader(fileContent.StreamContent()).ReadToEnd();
            Assert.AreEqual(expectedContent, actual);
        }

        [Test]
        public
            void ContentDataIsParsedCorrectly()
        {
            // When
            ContentData contentData = _message.MessageHeader.ContentData;

            // Then
            Assert.AreEqual("2512169996", contentData.CPRdata.cprNumber);
            Assert.AreEqual("Emilie Hansen", contentData.CPRdata.name);
            Assert.AreEqual("12345678", contentData.CVRdata.cvrNumber);
            Assert.AreEqual("[Virksomhed]", contentData.CVRdata.companyName);
            Assert.AreEqual("AB12345", contentData.MotorVehicle.licenseNumber);
            Assert.AreEqual("WFR18ZZ67W094959", contentData.MotorVehicle.chassisNumber);
            Assert.AreEqual("ABC1234", contentData.PropertyNumber.propertyNumber);
            Assert.AreEqual("SAG-12345", contentData.CaseID.caseID);
            Assert.AreEqual("Sagssystem 1234", contentData.CaseID.caseSystem);
            Assert.AreEqual("Maj 2018", contentData.KLEdata.version);
            Assert.AreEqual("00.00.00", contentData.KLEdata.subjectKey);
            Assert.AreEqual("[Tekst]", contentData.KLEdata.activityFacet);
            Assert.AreEqual("[KLE tekst]", contentData.KLEdata.label);
            Assert.AreEqual("00.00.00.00", contentData.FORMdata.taskKey);
            Assert.AreEqual("Opgavenøglen v2.12", contentData.FORMdata.version);
            Assert.AreEqual("[Tekst]", contentData.FORMdata.activityFacet);
            Assert.AreEqual("[FORM tekst]", contentData.FORMdata.label);
            Assert.AreEqual(1234567890, contentData.ProductionUnit.productionUnitNumber);
            Assert.AreEqual("[Produktionsenhed]", contentData.ProductionUnit.productionUnitName);
            Assert.AreEqual("123ABC", contentData.Education.educationCode);
            Assert.AreEqual("[Uddannelse navn]", contentData.Education.educationName);

            // And
            Address address = contentData.Address;
            Assert.AreEqual("da1c15bb-f74d-4a26-8617-4fbb5ac4f063", address.id);
            Assert.AreEqual("Søen", address.addressLabel);
            Assert.AreEqual("45", address.houseNumber);
            Assert.AreEqual("tv", address.door);
            Assert.AreEqual("0", address.floor);
            Assert.AreEqual("AB1", address.co);
            Assert.AreEqual("5000", address.zipCode);
            Assert.AreEqual("Odense", address.city);
            Assert.AreEqual("DK", address.country);

            AdditionalContentData additionalContentData1 = contentData.AdditionalContentData[0];
            Assert.AreEqual("Liste A", additionalContentData1.contentDataType);
            Assert.AreEqual("Navn 1", additionalContentData1.contentDataName);
            Assert.AreEqual("Værdi 1", additionalContentData1.contentDataValue);

            AdditionalContentData additionalContentData2 = contentData.AdditionalContentData[1];
            Assert.AreEqual("Liste A", additionalContentData2.contentDataType);
            Assert.AreEqual("Navn 2", additionalContentData2.contentDataName);
            Assert.AreEqual("Værdi 2", additionalContentData2.contentDataValue);
        }


        [Test]
        public void ForwardDataIsParsedCorrectly()
        {
            // When
            ForwardData forwardData = _message.MessageHeader.ForwardData;

            // Then
            Assert.AreEqual("8c2ea15d-61fb-4ba9-9366-42f8b194c114", forwardData.messageUUID);
            Assert.AreEqual("Kommunen", forwardData.originalSender);
            Assert.AreEqual("Børnehaven, Tusindfryd", forwardData.originalContentResponsible);
            Assert.AreEqual("kommentar til modtageren", forwardData.comment);
            Assert.AreEqual(DateTime.Parse("2018-09-30T12:00:00Z").ToUniversalTime(), forwardData.originalMessageDateTime);
            Assert.AreEqual("1234.1234", forwardData.contactPointID);
        }

        [Test]
        public void MessageBodyIsParsedCorrectly()
        {
            // When
            MessageBody messageBody = _message.MessageBody;

            // Then
            Assert.AreEqual(DateTime.Parse("2018-05-03T12:00:00Z").ToUniversalTime(), messageBody.createdDateTime);

            MainDocument mainDocument = messageBody.MainDocument;

            Assert.AreEqual("456", mainDocument.mainDocumentID);
            Assert.AreEqual("Tilbud om børnehaveplads", mainDocument.label);

            Assert.AreEqual("application/pdf", mainDocument.File[0].encodingFormat);
            Assert.AreEqual("Pladsanvisning.pdf", mainDocument.File[0].filename);
            Assert.AreEqual("da", mainDocument.File[0].language);
            AssertContent(mainDocument.File[0].GetContent(), "VGhpcyBpcyBhIHRlc3Q=");


            Assert.AreEqual("text/plain", mainDocument.File[1].encodingFormat);
            Assert.AreEqual("Pladsanvisning.txt", mainDocument.File[1].filename);
            Assert.AreEqual("da", mainDocument.File[1].language);
            Assert.AreEqual("VGhpcyBpcyBhIHRlc3Q=", mainDocument.File[1].content);


            Assert.AreEqual("Spørgeskema", mainDocument.Action[0].label);
            Assert.AreEqual(DateTime.Parse("2018-11-09T12:00:00Z").ToUniversalTime(),
                GetItem<DateTime>(mainDocument.Action[0], ItemsChoiceType.startDateTime));
            Assert.AreEqual(DateTime.Parse("2018-12-09T12:00:00Z").ToUniversalTime(),
                GetItem<DateTime>(mainDocument.Action[0], ItemsChoiceType.endDateTime));
            Assert.AreEqual(
                "https://www.tusindfryd.dk/spørgeskema.html",
                GetItem<EntryPoint>(mainDocument.Action[0], ItemsChoiceType.EntryPoint).url);

            Assert.AreEqual("Opret aftale i kalender", mainDocument.Action[1].label);
            Assert.AreEqual("AFTALE", mainDocument.Action[1].actionCode);

            Reservation reservation = GetItem<Reservation>(mainDocument.Action[1], ItemsChoiceType.Reservation);

            Assert.AreEqual("Opstart Tusindfryd", reservation.description);
            Assert.AreEqual("8c2ea15d-61fb-4ba9-9366-42f8b194d241", reservation.reservationUUID);
            Assert.AreEqual("Opstart", reservation.@abstract);
            Assert.AreEqual("Gl. Landevej 61, 9000 aalborg, Rød stue", reservation.location);
            Assert.AreEqual(DateTime.Parse("2018-11-10T09:00:00Z").ToUniversalTime(), reservation.startDateTime);
            Assert.AreEqual(DateTime.Parse("2018-11-10T12:00:00Z").ToUniversalTime(), reservation.endDateTime);
            Assert.AreEqual("info@tusindfryd.dk", reservation.organizerMail);
            Assert.AreEqual("Jette Hansen", reservation.organizerName);


            Assert.AreEqual("789", messageBody.AdditionalDocument[0].additionalDocumentID);
            Assert.AreEqual("Tilbud om børnehaveplads", messageBody.AdditionalDocument[0].label);


            Assert.AreEqual("application/pdf", messageBody.AdditionalDocument[0].File[0].encodingFormat);
            Assert.AreEqual("Pladsanvisning.pdf", messageBody.AdditionalDocument[0].File[0].filename);
            Assert.AreEqual("da", messageBody.AdditionalDocument[0].File[0].language);
            AssertContent(messageBody.AdditionalDocument[0].File[0].GetContent(), "VGhpcyBpcyBhIHRlc3Q=");


            Assert.AreEqual("application/msword", messageBody.AdditionalDocument[0].File[1].encodingFormat);
            Assert.AreEqual("Praktiske oplysninger.doc", messageBody.AdditionalDocument[0].File[1].filename);
            Assert.AreEqual("da", messageBody.AdditionalDocument[0].File[1].language);
            AssertContent(messageBody.AdditionalDocument[0].File[1].GetContent(), "VGhpcyBpcyBhIHRlc3Q=");


            Assert.AreEqual("Tusindfryd hjemmeside", messageBody.AdditionalDocument[0].Action[0].label);
            Assert.AreEqual("INFORMATION", messageBody.AdditionalDocument[0].Action[0].actionCode);
            Assert.AreEqual(DateTime.Parse("2018-11-10T09:00:00Z").ToUniversalTime(),
                GetItem<DateTime>(messageBody.AdditionalDocument[0].Action[0], ItemsChoiceType.startDateTime));
            Assert.AreEqual(DateTime.Parse("2018-11-09T12:00:00Z").ToUniversalTime(),
                GetItem<DateTime>(messageBody.AdditionalDocument[0].Action[0], ItemsChoiceType.endDateTime));
            Assert.AreEqual(
                "https://www.tusindfryd.dk",
                GetItem<EntryPoint>(messageBody.AdditionalDocument[0].Action[0], ItemsChoiceType.EntryPoint).url);

            Assert.AreEqual("Opret aftale i kalender", messageBody.AdditionalDocument[0].Action[1].label);
            Assert.AreEqual("AFTALE", messageBody.AdditionalDocument[0].Action[1].actionCode);
            Assert.AreEqual(DateTime.Parse("2018-11-10T09:00:00Z").ToUniversalTime(),
                GetItem<DateTime>(messageBody.AdditionalDocument[0].Action[0], ItemsChoiceType.startDateTime));
            Assert.AreEqual(DateTime.Parse("2018-11-09T12:00:00Z").ToUniversalTime(),
                GetItem<DateTime>(messageBody.AdditionalDocument[0].Action[0], ItemsChoiceType.endDateTime));

            Reservation reservation2 =
                GetItem<Reservation>(messageBody.AdditionalDocument[0].Action[1], ItemsChoiceType.Reservation);


            Assert.AreEqual("Introduktion til nye forældre", reservation2.description);
            Assert.AreEqual("8c2ea15d-61fb-4ba9-9366-42f8b194e845", reservation2.reservationUUID);
            Assert.AreEqual("Invitation", reservation2.@abstract);
            Assert.AreEqual("Gl. Landevej 61, 9000 Aalborg", reservation2.location);
            Assert.AreEqual(DateTime.Parse("2018-11-10T19:00:00Z").ToUniversalTime(), reservation2.startDateTime);
            Assert.AreEqual(DateTime.Parse("2018-11-10T20:30:00Z").ToUniversalTime(), reservation2.endDateTime);
            Assert.AreEqual("info@tusindfryd.dk", reservation2.organizerMail);
            Assert.AreEqual("Jette Hansen", reservation2.organizerName);


            Assert.AreEqual("222555", messageBody.TechnicalDocument[0].technicalDocumentID);
            Assert.AreEqual("Teknisk dokument", messageBody.TechnicalDocument[0].label);

            Assert.AreEqual("text/xml", messageBody.TechnicalDocument[0].File[0].encodingFormat);
            Assert.AreEqual("TekniskDokument.xml", messageBody.TechnicalDocument[0].File[0].filename);
            Assert.AreEqual("da", messageBody.TechnicalDocument[0].File[0].language);
            AssertContent(messageBody.TechnicalDocument[0].File[0].GetContent(), "VGhpcyBpcyBhIHRlc3Q=");
        }

        [Test]
        public void MessageheaderIsParsedCorrectly()
        {
            // When
            MessageHeader messageHeader = _message.MessageHeader;

            // Then

            Assert.AreEqual(memoMessageType.DIGITALPOST, messageHeader.messageType);
            Assert.AreEqual("8c2ea15d-61fb-4ba9-9366-42f8b194c114", messageHeader.messageUUID);
            Assert.AreEqual("MSG-12345", messageHeader.messageID);
            Assert.AreEqual("Pladsanvisning", messageHeader.messageCode);
            Assert.AreEqual("Besked fra Børneforvaltningen", messageHeader.label);
            Assert.AreEqual("Du har fået digitalpost fra Kommunen vedr. din ansøgning om børnehaveplads.", messageHeader.notification);
            Assert.AreEqual("Du har fået digitalpost fra Kommunen vedr. din ansøgning om børnehaveplads til Emilie Hansen", messageHeader.additionalNotification);
            Assert.IsTrue(messageHeader.reply);
            Assert.AreEqual(DateTime.Parse("2018-09-30T12:00:00Z").ToUniversalTime(), messageHeader.replyByDateTime);
            Assert.AreEqual(DateTime.Parse("2018-09-15"), messageHeader.doNotDeliverUntilDate);
            Assert.AreEqual("VIRKSOMHEDSPOST", messageHeader.postType);
        }

        [Test]
        public void RecipientIsParsedCorrectly()
        {
            // When
            Recipient recipient = _message.MessageHeader.Recipient;

            // Then
            Assert.AreEqual("2211771212", recipient.recipientID);
            Assert.AreEqual("CPR", recipient.idType);
            Assert.AreEqual("eID", recipient.idTypeLabel);
            Assert.AreEqual("Mette Hansen", recipient.label);

            AttentionData attentionData = recipient.AttentionData;
            Assert.AreEqual("2211771212", attentionData.AttentionPerson.personID);
            Assert.AreEqual("Mette Hansen", attentionData.AttentionPerson.label);

            Assert.AreEqual(1234567890, attentionData.ProductionUnit.productionUnitNumber);
            Assert.AreEqual("[Produktionsenhed]", attentionData.ProductionUnit.productionUnitName);

            Assert.AreEqual(5798000012345L, attentionData.GlobalLocationNumber.globalLocationNumber);
            Assert.AreEqual("[Navn på lokation]", attentionData.GlobalLocationNumber.location);

            Assert.AreEqual("m.hansen@gmail.com", attentionData.EMail.emailAddress);
            Assert.AreEqual("Mette Hansen", attentionData.EMail.relatedAgent);

            Assert.AreEqual("12345678", attentionData.SEnumber.seNumber);
            Assert.AreEqual("[Virksomhed]", attentionData.SEnumber.companyName);

            Assert.AreEqual("12345678", attentionData.Telephone.telephoneNumber);
            Assert.AreEqual("Mette Hansen", attentionData.Telephone.relatedAgent);

            Assert.AreEqual("12345678_1234567890", attentionData.EID.eID);
            Assert.AreEqual("[Virksomhed]", attentionData.EID.label);

            Assert.AreEqual("468031000016004", attentionData.SORdata.sorIdentifier);
            Assert.AreEqual("[SOR tekst]", attentionData.SORdata.entryName);

            Assert.AreEqual("22334455", attentionData.ContentResponsible.contentResponsibleID);
            Assert.AreEqual("[Ansvarlig]", attentionData.ContentResponsible.label);

            Assert.AreEqual("Bakketoppen 6, 9000 Aalborg", attentionData.UnstructuredAddress.unstructured);

            ContactPoint contactPoint = recipient.ContactPoint;

            Assert.AreEqual("22.33.44.55", contactPoint.contactGroup);
            Assert.AreEqual("241d39f6-998e-4929-b198-ccacbbf4b330", contactPoint.contactPointID);
            Assert.AreEqual("Kommunen, Pladsanvisningen", contactPoint.label);

            Assert.AreEqual("Barnets CPR nummer", contactPoint.ContactInfo[0].label);
            Assert.AreEqual("2512169996", contactPoint.ContactInfo[0].value);
            Assert.AreEqual("Barnets navn", contactPoint.ContactInfo[1].label);
            Assert.AreEqual("Emilie Hansen", contactPoint.ContactInfo[1].value);
        }


        [Test]
        public void ReplyDataIsParsedCorrectly()
        {
            // When
            ReplyData[] replyData = _message.MessageHeader.ReplyData;

            // Then
            ReplyData replyData1 = replyData[0];
            Assert.AreEqual("MSG-12344", replyData1.messageID);
            Assert.AreEqual("8c2ea15d-61fb-4ba9-9366-42f8b194c114", replyData1.messageUUID);
            Assert.AreEqual(null, replyData1.replyUUID);
            Assert.AreEqual("12345678", replyData1.senderID);
            Assert.AreEqual("1234567890", replyData1.recipientID);
            Assert.AreEqual("SAG-456", replyData1.caseID);
            Assert.AreEqual("1234567890", replyData1.contactPointID);
            Assert.AreEqual("ABC-123", replyData1.generatingSystemID);
            Assert.AreEqual("Tilbud om børnehaveplads til Emilie Hansen", replyData1.comment);
            Assert.AreEqual("Intern note", replyData1.AdditionalReplyData[0].label);
            Assert.AreEqual(2, replyData1.AdditionalReplyData.Length);
            Assert.AreEqual("Intern note", replyData1.AdditionalReplyData[0].label);
            Assert.AreEqual("tekst", replyData1.AdditionalReplyData[0].value);
            Assert.AreEqual("Intern reference", replyData1.AdditionalReplyData[1].label);
            Assert.AreEqual("tekst", replyData1.AdditionalReplyData[1].value);

            ReplyData replyData2 = replyData[1];
            Assert.AreEqual("MSG-12345", replyData2.messageID);
            Assert.AreEqual("8c2ea15d-61fb-4ba9-9366-42f8b194c657", replyData2.messageUUID);
            Assert.AreEqual("8c2ea15d-61fb-4ba9-9366-42f8b194c114", replyData2.replyUUID);
            Assert.AreEqual("1234567890", replyData2.senderID);
            Assert.AreEqual("12345678", replyData2.recipientID);
            Assert.AreEqual("SAG-4567", replyData2.caseID);
            Assert.AreEqual("12345678", replyData2.contactPointID);
            Assert.AreEqual("ABC-1234", replyData2.generatingSystemID);
            Assert.AreEqual("tekst", replyData2.comment);
            Assert.AreEqual(4, replyData2.AdditionalReplyData.Length);
            Assert.AreEqual("Intern note", replyData2.AdditionalReplyData[0].label);
            Assert.AreEqual("tekst", replyData2.AdditionalReplyData[0].value);
            Assert.AreEqual("Intern reference", replyData2.AdditionalReplyData[1].label);
            Assert.AreEqual("tekst", replyData2.AdditionalReplyData[1].value);
            Assert.AreEqual("Vedr", replyData2.AdditionalReplyData[2].label);
            Assert.AreEqual("tekst", replyData2.AdditionalReplyData[2].value);
            Assert.AreEqual("Att", replyData2.AdditionalReplyData[3].label);
            Assert.AreEqual("tekst", replyData2.AdditionalReplyData[3].value);
        }

        [Test]
        public void SenderIsParsedCorrectly()
        {
            // When
            Sender sender = _message.MessageHeader.Sender;

            // Then

            Assert.AreEqual("12345678", sender.senderID);
            Assert.AreEqual("CVR", sender.idType);
            Assert.AreEqual("eID", sender.idTypeLabel);
            Assert.AreEqual("Kommunen", sender.label);

            AttentionData attentionData = sender.AttentionData;
            Assert.AreEqual("9000001234", attentionData.AttentionPerson.personID);
            Assert.AreEqual("Hans Hansen", attentionData.AttentionPerson.label);

            Assert.AreEqual(1234567890, attentionData.ProductionUnit.productionUnitNumber);
            Assert.AreEqual("Produktionsenhed A", attentionData.ProductionUnit.productionUnitName);

            Assert.AreEqual(5798000012345L, attentionData.GlobalLocationNumber.globalLocationNumber);
            Assert.AreEqual("Kommune A", attentionData.GlobalLocationNumber.location);

            Assert.AreEqual("info@tusindfryd.dk", attentionData.EMail.emailAddress);
            Assert.AreEqual("Hans Hansen", attentionData.EMail.relatedAgent);

            Assert.AreEqual("12345678", attentionData.SEnumber.seNumber);
            Assert.AreEqual("Kommune A", attentionData.SEnumber.companyName);

            Assert.AreEqual("12345678", attentionData.Telephone.telephoneNumber);
            Assert.AreEqual("Hans Hansen", attentionData.Telephone.relatedAgent);

            Assert.AreEqual("CVR:12345678-RID:1234567890123", attentionData.EID.eID);
            Assert.AreEqual("Kommune A", attentionData.EID.label);

            Assert.AreEqual("22334455", attentionData.ContentResponsible.contentResponsibleID);
            Assert.AreEqual("Børnehaven, Tusindfryd", attentionData.ContentResponsible.label);

            Assert.AreEqual("Sys-1234", attentionData.GeneratingSystem.generatingSystemID);
            Assert.AreEqual("KommunaltPostSystem", attentionData.GeneratingSystem.label);

            Address address = attentionData.Address;
            Assert.AreEqual("8c2ea15d-61fb-4ba9-9366-42f8b194c852", address.id);
            Assert.AreEqual("Gaden", address.addressLabel);
            Assert.AreEqual("7A", address.houseNumber);
            Assert.AreEqual("th", address.door);
            Assert.AreEqual("3", address.floor);
            Assert.AreEqual("C/O", address.co);
            Assert.AreEqual("9000", address.zipCode);
            Assert.AreEqual("Aalborg", address.city);
            Assert.AreEqual("DK", address.country);

            Assert.AreEqual("557501.23", address.AddressPoint.geographicEastingMeasure);
            Assert.AreEqual("6336248.89", address.AddressPoint.geographicNorthingMeasure);
            Assert.AreEqual("0.0", address.AddressPoint.geographicHeightMeasure);
        }

        [Test]
        public void StructuredAddressIsParsedCorrectlyFromRecipient()
        {
            // Given
            using (Stream stream = MeMoTestDataProvider.StreamMeMoMessageFullExampleV2())
            {
                using (IMeMoParser parser = MeMoParserFactory.CreateParser(stream, true))
                {
                    _message = parser.Parse();
                }
            }

            Address address = _message.MessageHeader.Recipient.AttentionData.Address;

            // Then

            Assert.AreEqual("8c2ea15d-61fb-4ba9-9366-42f8b194c852", address.id);
            Assert.AreEqual("Gaden", address.addressLabel);
            Assert.AreEqual("7A", address.houseNumber);
            Assert.AreEqual("th", address.door);
            Assert.AreEqual("3", address.floor);
            Assert.AreEqual("C/O", address.co);
            Assert.AreEqual("9000", address.zipCode);
            Assert.AreEqual("Aalborg", address.city);
            Assert.AreEqual("DK", address.country);


            Assert.AreEqual("557501.23",
                address.AddressPoint.geographicEastingMeasure);
            Assert.AreEqual("6336248.89",
                address.AddressPoint.geographicNorthingMeasure);
            Assert.AreEqual("0.0",
                address.AddressPoint.geographicHeightMeasure);
        }


        [Test]
        public void UnstructuredAddressIsParsedCorrectlyFromSender()
        {
            // Given
            using (Stream stream = MeMoTestDataProvider.StreamMeMoMessageFullExampleV2())
            {
                using (IMeMoParser parser = MeMoParserFactory.CreateParser(stream))
                {
                    _message = parser.Parse();
                }
            }

            // Then
            UnstructuredAddress address = _message.MessageHeader.Sender.AttentionData.UnstructuredAddress;
            Assert.AreEqual("[Ustruktureret adresseangivlese]", address.unstructured);
            
        }
    }
}