﻿using System;
using System.Collections.Generic;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Parser;
using NUnit.Framework;

namespace Dk.Digst.Digital.Post.Memolib.Test.Parser
{
    [TestFixture]
    public class MeMoXmlStreamLocationTests
    {
        [Test]
        public void ChildElementCanBeAdded()
        {
            // Given
            MeMoXmlStreamLocation location = new MeMoXmlStreamLocation(
                new List<Type> {typeof(Message), typeof(MessageHeader)});

            // When
            location.UpdateStack(3, typeof(Sender));

            // Then
            Assert.AreEqual(typeof(Sender), location.GetCurrentPosition());
            Assert.AreEqual(typeof(MessageHeader), location.GetParentPosition().Get());
            Assert.AreEqual(3, location.GetDepth());
        }

        [Test]
        public void DepthCanNotBeSetToZero()
        {
            // Given
            MeMoXmlStreamLocation location = new MeMoXmlStreamLocation(
                new List<Type> {typeof(Message), typeof(MessageHeader)});

            // Then
            Assert.Throws<ArgumentException>(() => location.UpdateStack(0, typeof(Sender)));
        }

        [Test]
        public void EmptyLocationCanBeCreated()
        {
            // When
            MeMoXmlStreamLocation location = new MeMoXmlStreamLocation();

            // Then
            Assert.IsTrue(location.GetParentPosition().IsEmpty());
            Assert.AreEqual(0, location.GetDepth());
        }

        [Test]
        public void GettingCurrentPositionOfEmptyLocationThrowsException()
        {
            // Given
            MeMoXmlStreamLocation location = new MeMoXmlStreamLocation();

            // Then
            Assert.Throws<InvalidOperationException>(() => location.GetCurrentPosition());
        }

        [Test]
        public void LocationWithOneElementHasNoParent()
        {
            // Given
            MeMoXmlStreamLocation location = new MeMoXmlStreamLocation(
                new List<Type> {typeof(Message)});

            // Then
            Assert.IsTrue(location.GetParentPosition().IsEmpty());
        }

        [Test]
        public void MeMoTypeAndDepthCanBeRetrieved()
        {
            // Given
            MeMoXmlStreamLocation location = new MeMoXmlStreamLocation(
                new List<Type> {typeof(Message), typeof(MessageHeader), typeof(Sender)});

            // Then
            Assert.AreEqual(typeof(Sender), location.GetCurrentPosition());
            Assert.AreEqual(typeof(MessageHeader), location.GetParentPosition().Get());
            Assert.AreEqual(3, location.GetDepth());
        }

        [Test]
        public void ParentElementCanBeAdded()
        {
            // Given
            MeMoXmlStreamLocation location = new MeMoXmlStreamLocation(
                new List<Type> {typeof(Message), typeof(MessageHeader), typeof(Sender)});

            // When
            location.UpdateStack(2, typeof(MessageBody));

            // Then
            Assert.AreEqual(typeof(MessageBody), location.GetCurrentPosition());
            Assert.AreEqual(typeof(Message), location.GetParentPosition().Get());
            Assert.AreEqual(2, location.GetDepth());
        }

        [Test]
        public void PositionCanBeCopied()
        {
            // Given
            MeMoXmlStreamLocation location = new MeMoXmlStreamLocation(
                new List<Type> {typeof(Message), typeof(MessageHeader)});
            MeMoXmlStreamLocation copy = new MeMoXmlStreamLocation(location);

            // When
            copy.UpdateStack(3, typeof(Sender));

            // Then
            Assert.AreEqual(typeof(MessageHeader), location.GetCurrentPosition());
            Assert.AreEqual(typeof(Message), location.GetParentPosition().Get());
            Assert.AreEqual(2, location.GetDepth());

            // And
            Assert.AreEqual(typeof(Sender), copy.GetCurrentPosition());
            Assert.AreEqual(typeof(MessageHeader), copy.GetParentPosition().Get());
            Assert.AreEqual(3, copy.GetDepth());
        }

        [Test]
        public void PositionCanBeUpdatedMultipleTimes()
        {
            // Given
            MeMoXmlStreamLocation location = new MeMoXmlStreamLocation(
                new List<Type> {typeof(Message), typeof(MessageHeader)});

            // When
            location.UpdateStack(3, typeof(Sender));

            // Then
            Assert.AreEqual(typeof(Sender), location.GetCurrentPosition());
            Assert.AreEqual(typeof(MessageHeader), location.GetParentPosition().Get());
            Assert.AreEqual(3, location.GetDepth());

            // When
            location.UpdateStack(4, typeof(ContactPoint));

            // Then
            Assert.AreEqual(typeof(ContactPoint), location.GetCurrentPosition());
            Assert.AreEqual(typeof(Sender), location.GetParentPosition().Get());
            Assert.AreEqual(4, location.GetDepth());
        }

        [Test]
        public void SiblingElementCanBeAdded()
        {
            // Given
            MeMoXmlStreamLocation location = new MeMoXmlStreamLocation(
                new List<Type> {typeof(Message), typeof(MessageHeader)});

            // When
            location.UpdateStack(2, typeof(MessageBody));

            // Then
            Assert.AreEqual(typeof(MessageBody), location.GetCurrentPosition());
            Assert.AreEqual(typeof(Message), location.GetParentPosition().Get());
            Assert.AreEqual(2, location.GetDepth());
        }
    }
}