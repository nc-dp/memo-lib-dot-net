﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Parser;
using Dk.Digst.Digital.Post.Memolib.Test.Util;
using Dk.Digst.Digital.Post.Memolib.Visitor;
using NUnit.Framework;

namespace Dk.Digst.Digital.Post.Memolib.Test.Parser
{
    [TestFixture]
    public class MeMoXmlStreamCursorTestsNew
    {
        [SetUp]
        public void Setup()
        {
            _stream = MeMoTestDataProvider.StreamMeMoMessageFullExample12();
            _parser = new MeMoXmlStreamParser(_stream, false, MemoVersion.MemoVersion12);
            _cursor = new MeMoXmlStreamCursor(nameof(MessageHeader), new MeMoXmlStreamLocation(
                new List<Type> {typeof(Message), typeof(MessageHeader)}), _parser);
            _reader = (XmlReader) typeof(MeMoXmlStreamParser)
                .GetField("_reader", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(_parser);
            MoveToHeader(_reader);
        }

        [TearDown]
        public void Cleanup()
        {
            _parser.Dispose();
            _stream?.Dispose();
        }

        private MeMoXmlStreamParser _parser;

        private Stream _stream;

        private MeMoXmlStreamCursor _cursor;

        private XmlReader _reader;

        private void MoveToHeader(XmlReader reader)
        {
            while (!reader.EOF && reader.LocalName != nameof(MessageHeader))
                reader.Read();
        }

        [Test]
        public void ClassCanNotBeParsedAfterElementsAre()
        {
            // Given
            _cursor.ParseMeMoClassElements();

            // Then
            Assert.Throws<InvalidOperationException>(() => _cursor.ParseMeMoClass<MessageHeader>());
        }

        [Test]
        public void CursorCanAcceptVisitor()
        {
            // Given
            MessageHeader messageHeader = null;
            IMeMoStreamVisitor visitor = MeMoStreamVisitorFactory.CreateConsumer<MessageHeader>(
                c => messageHeader = c.ParseMeMoClass<MessageHeader>());

            // Expect
            Assert.IsNull(messageHeader);

            // When
            _cursor.Accept(visitor);

            // Then
            Assert.IsNotNull(messageHeader);
            Assert.AreEqual(memoMessageType.DIGITALPOST, messageHeader.messageType);
            Assert.AreEqual("8c2ea15d-61fb-4ba9-9366-42f8b194c114", messageHeader.messageUUID);
            Assert.AreEqual("MSG-12345", messageHeader.messageID);
            Assert.AreEqual("Besked fra Børneforvaltningen", messageHeader.messageCode);
            Assert.AreEqual("Pladsanvisning", messageHeader.label);
        }

        [Test]
        public void CursorCanParseElements()
        {
            // When
            MeMoClassElements elements = _cursor.ParseMeMoClassElements();

            // Then
            Assert.AreEqual("DIGITALPOST", elements.Get(nameof(MessageHeader.messageType)));
            Assert.AreEqual("8c2ea15d-61fb-4ba9-9366-42f8b194c114", elements.Get(nameof(MessageHeader.messageUUID)));
            Assert.AreEqual("MSG-12345", elements.Get(nameof(MessageHeader.messageID)));
            Assert.AreEqual("Besked fra Børneforvaltningen", elements.Get(nameof(MessageHeader.messageCode)));
            Assert.AreEqual("Pladsanvisning", elements.Get(nameof(MessageHeader.label)));
            Assert.AreEqual("true", elements.Get(nameof(MessageHeader.reply)));
            Assert.AreEqual("2018-09-30T12:00:00Z", elements.Get(nameof(MessageHeader.replyByDateTime)));
            Assert.AreEqual("2018-09-15", elements.Get(nameof(MessageHeader.doNotDeliverUntilDate)));
            Assert.AreEqual("false", elements.Get(nameof(MessageHeader.mandatory)));
            Assert.AreEqual("false", elements.Get(nameof(MessageHeader.legalNotification)));
            Assert.AreEqual("MYNDIGHEDSPOST", elements.Get(nameof(MessageHeader.postType)));
        }

        [Test]
        public void CursorCanPassMeMoClass()
        {
            // When
            MessageHeader messageHeader = _cursor.ParseMeMoClass<MessageHeader>();

            // Then
            Assert.AreEqual(memoMessageType.DIGITALPOST, messageHeader.messageType);
            Assert.AreEqual("8c2ea15d-61fb-4ba9-9366-42f8b194c114", messageHeader.messageUUID);
            Assert.AreEqual("MSG-12345", messageHeader.messageID);
            Assert.AreEqual("Besked fra Børneforvaltningen", messageHeader.messageCode);
            Assert.AreEqual("Pladsanvisning", messageHeader.label);
            Assert.AreEqual(true, messageHeader.reply);
            Assert.AreEqual(DateTime.Parse("2018-09-30T12:00:00Z").ToUniversalTime(),
                messageHeader.replyByDateTime);
            Assert.AreEqual(DateTime.Parse("2018-09-15"), messageHeader.doNotDeliverUntilDate);
            Assert.AreEqual(false, messageHeader.mandatory);
            Assert.AreEqual(false, messageHeader.legalNotification);
            Assert.AreEqual("MYNDIGHEDSPOST", messageHeader.postType);
        }

        [Test]
        public void CursorCanReturnMeMoTypeOfPosition()
        {
            Assert.AreEqual(typeof(MessageHeader), _cursor.GetCurrentPosition());
        }

        [Test]
        public void CursorCanReturnParentMeMoTypeOfPosition()
        {
            Assert.AreEqual(typeof(Message), _cursor.GetParentPosition().Get());
        }

        [Test]
        public void CursorCanReturnXmlElementNameOfPosition()
        {
            Assert.AreEqual(nameof(MessageHeader), _cursor.GetXmlElementName());
        }

        [Test]
        public void CursorCanTraverseChildMeMoClassesUsingParser()
        {
            // When
            _reader.Read(); // Move past MessageHeader tag
            _cursor.TraverseChildren(new List<IMeMoStreamVisitor>());

            // Then
            Assert.AreEqual(nameof(MessageBody), _reader.LocalName);
        }

        [Test]
        public void CursorReturnsOptionalEmptyIfNoParent()
        {
            // Given
            _cursor = new MeMoXmlStreamCursor(
                "File", new MeMoXmlStreamLocation(
                    new List<Type>
                    {
                        typeof(Message)
                    }), new MeMoXmlStreamParser(new MemoryStream(), false, MemoVersion.MemoVersion11));

            // Expect
            Assert.IsTrue(_cursor.GetParentPosition().IsEmpty());
        }

        [Test]
        public void ElementsCanNotBeParsedAfterClassIs()
        {
            // Given
            _cursor.ParseMeMoClass<MessageHeader>();

            // Then
            Assert.Throws<InvalidOperationException>(() => _cursor.ParseMeMoClassElements());
        }

        [Test]
        public void ParsingMeMoClassTwiceReturnsSameResult()
        {
            // When
            _cursor.ParseMeMoClass<MessageHeader>();
            MessageHeader messageHeader = _cursor.ParseMeMoClass<MessageHeader>();

            // Then
            Assert.AreEqual(memoMessageType.DIGITALPOST, messageHeader.messageType);
            Assert.AreEqual("8c2ea15d-61fb-4ba9-9366-42f8b194c114", messageHeader.messageUUID);
            Assert.AreEqual("MSG-12345", messageHeader.messageID);
            Assert.AreEqual("Besked fra Børneforvaltningen", messageHeader.messageCode);
            Assert.AreEqual("Pladsanvisning", messageHeader.label);
            Assert.AreEqual(true, messageHeader.reply);
            Assert.AreEqual(DateTime.Parse("2018-09-30T12:00:00Z").ToUniversalTime(),
                messageHeader.replyByDateTime);
            Assert.AreEqual(DateTime.Parse("2018-09-15"), messageHeader.doNotDeliverUntilDate);
            Assert.AreEqual(false, messageHeader.mandatory);
            Assert.AreEqual(false, messageHeader.legalNotification);
            Assert.AreEqual("MYNDIGHEDSPOST", messageHeader.postType);
        }

        [Test]
        public void ParsingTwiceReturnsTheSame()
        {
            // When
            _cursor.ParseMeMoClassElements();
            MeMoClassElements elements = _cursor.ParseMeMoClassElements();

            // Then
            Assert.AreEqual("DIGITALPOST", elements.Get(nameof(MessageHeader.messageType)));
            Assert.AreEqual("8c2ea15d-61fb-4ba9-9366-42f8b194c114", elements.Get(nameof(MessageHeader.messageUUID)));
            Assert.AreEqual("MSG-12345", elements.Get(nameof(MessageHeader.messageID)));
            Assert.AreEqual("Besked fra Børneforvaltningen", elements.Get(nameof(MessageHeader.messageCode)));
            Assert.AreEqual("Pladsanvisning", elements.Get(nameof(MessageHeader.label)));
            Assert.AreEqual("true", elements.Get(nameof(MessageHeader.reply)));
            Assert.AreEqual("2018-09-30T12:00:00Z", elements.Get(nameof(MessageHeader.replyByDateTime)));
            Assert.AreEqual("2018-09-15", elements.Get(nameof(MessageHeader.doNotDeliverUntilDate)));
            Assert.AreEqual("false", elements.Get(nameof(MessageHeader.mandatory)));
            Assert.AreEqual("false", elements.Get(nameof(MessageHeader.legalNotification)));
            Assert.AreEqual("MYNDIGHEDSPOST", elements.Get(nameof(MessageHeader.postType)));
        }
    }
}