dotnet publish Ngdp.Memolib/Dk.Digst.Digital.Post.Memolib.csproj --framework $1 --output ../release
cd ../release
zip -r dot-net-$2-memolib-$VERSION.zip $FILENAME
curl -X POST "https://$BITBUCKET_USERNAME:$BITBUCKET_APP_PASSWORD@api.bitbucket.org/2.0/repositories/$BITBUCKET_REPO_OWNER/${BITBUCKET_REPO_NAME}/downloads" --form files=@"dot-net-$2-memolib-$VERSION.zip" -v
cd ../build