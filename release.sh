set -eux
version=$(<version.txt)
echo "Current version: $version"
a=( ${version//./ } )
echo ${a[2]}

# Increment version numbers as requested.

while getopts ":mpM" opt; do
 case $opt in
  m) 
	((a[1]=a[1]+1))
	a[2]=0
	;;
  p) 
	((a[2]=a[2]+1)) 
	;;
  M) 
	((a[0]=a[0]+1))
	a[1]=0
	a[2]=0
	;;
  \?) 
	echo "Usage: cmd [-M] [-m] [-p]"
 esac
done

echo "${a[0]}.${a[1]}.${a[2]}" > version.txt

# Commit new version to git
version=$(<version.txt)
echo "New version: $version"
apt-get update -y
apt-get install -y git
git add version.txt
export VERSION=$(<version.txt)
git tag -a $VERSION -m 'Release version $VERSION'
git commit -m 'Updated version'
git push origin $BITBUCKET_BRANCH  --tags

# Release .dll for .NET standard 2.0
apt-get update -y
apt-get install -y zip
export BITBUCKET_REPO_OWNER=nc-dp
export BITBUCKET_REPO_NAME=memo-lib-dot-net
export FILENAME=Dk.Digst.Digital.Post.Memolib.dll
bash release-dll.sh netstandard2.0 standard

# Publish nuget
dotnet pack -p:PackageVersion=$VERSION
cd Ngdp.Memolib/bin/Release
dotnet nuget push Dk.Digst.Digital.Post.Memolib.$VERSION.nupkg -k $NUGET_API_KEY -s https://api.nuget.org/v3/index.json