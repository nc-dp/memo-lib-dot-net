﻿using System;
using System.Collections.Generic;

namespace Dk.Digst.Digital.Post.Memolib.Model
{
    public static class MeMoMessageClassDictionary
    {
        private static readonly Dictionary<string, Type> MeMoClassDictionary = new Dictionary<string, Type>
        {
            {nameof(Action), typeof(Action)},
            {nameof(AdditionalContentData), typeof(AdditionalContentData)},
            {nameof(AdditionalDocument), typeof(AdditionalDocument)},
            {nameof(AdditionalReplyData), typeof(AdditionalReplyData)},
            {nameof(Address), typeof(Address)},
            {nameof(AddressPoint), typeof(AddressPoint)},
            {nameof(AttentionData), typeof(AttentionData)},
            {nameof(AttentionPerson), typeof(AttentionPerson)},
            {nameof(CaseID), typeof(CaseID)},
            {nameof(ContactInfo), typeof(ContactInfo)},
            {nameof(ContactPoint), typeof(ContactPoint)},
            {nameof(ContentData), typeof(ContentData)},
            {nameof(ContentResponsible), typeof(ContentResponsible)},
            {nameof(CPRdata), typeof(CPRdata)},
            {nameof(CVRdata), typeof(CVRdata)},
            {nameof(Education), typeof(Education)},
            {nameof(EID), typeof(EID)},
            {nameof(EMail), typeof(EMail)},
            {nameof(EntryPoint), typeof(EntryPoint)},
            {nameof(File), typeof(File)},
            {nameof(FORMdata), typeof(FORMdata)},
            {nameof(ForwardData), typeof(ForwardData)},
            {nameof(GeneratingSystem), typeof(GeneratingSystem)},
            {nameof(GlobalLocationNumber), typeof(GlobalLocationNumber)},
            {nameof(KLEdata), typeof(KLEdata)},
            {nameof(MainDocument), typeof(MainDocument)},
            {nameof(Message), typeof(Message)},
            {nameof(MessageBody), typeof(MessageBody)},
            {nameof(MessageHeader), typeof(MessageHeader)},
            {nameof(MotorVehicle), typeof(MotorVehicle)},
            {nameof(ProductionUnit), typeof(ProductionUnit)},
            {nameof(PropertyNumber), typeof(PropertyNumber)},
            {nameof(Recipient), typeof(Recipient)},
            {nameof(ReplyData), typeof(ReplyData)},
            {nameof(Representative), typeof(Representative)},
            {nameof(Reservation), typeof(Reservation)},
            {nameof(Sender), typeof(Sender)},
            {nameof(SEnumber), typeof(SEnumber)},
            {nameof(SORdata), typeof(SORdata)},
            {nameof(TechnicalDocument), typeof(TechnicalDocument)},
            {nameof(Telephone), typeof(Telephone)},
            {nameof(UnstructuredAddress), typeof(UnstructuredAddress)}
        };

        public static bool IsMeMoClass(string name)
        {
            return MeMoClassDictionary.ContainsKey(name);
        }

        public static Type GetMemoType(string name)
        {
            return MeMoClassDictionary[name];
        }
    }
}