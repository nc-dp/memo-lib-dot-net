﻿using System.IO;

namespace Dk.Digst.Digital.Post.Memolib.Model
{
    public interface IFileContent
    {
        bool IsBase64Encoded { get; }

        string Location { get; }

        bool CanBeStreamed { get; }

        Stream StreamContent();
    }
}