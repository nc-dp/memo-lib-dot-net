﻿using Dk.Digst.Digital.Post.Memolib.Properties;

namespace Dk.Digst.Digital.Post.Memolib.Model
{
    public static class SchemaResources
    {
        public static string MeMoCoreSchema = Resources.MeMo_core;

        public static string MeMoDmvSchema = Resources.MeMo_dmv;

        public static string MeMoFprSchema = Resources.MeMo_form;

        public static string MeMoGlnSchema = Resources.MeMo_gln;

        public static string MeMoGrdSchema = Resources.MeMo_grd;

        public static string MeMoKleSchema = Resources.MeMo_kle;

        public static string MeMoDatatypesSchema = Resources.MeMo_dataTypes;

        public static string MeMoSorSchema = Resources.MeMo_sor;

        public static string MeMoUddSchema = Resources.MeMo_udd;
    }
}