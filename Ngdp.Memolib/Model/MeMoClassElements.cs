﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace Dk.Digst.Digital.Post.Memolib.Model
{
    public class MeMoClassElements
    {
        private readonly Dictionary<string, string> _elements = new Dictionary<string, string>();

        /// <summary>
        ///     Adds an element
        /// </summary>
        /// <param name="elementName">the name of the element</param>
        /// <param name="elementText">the value of the element</param>
        public void Add(string elementName, string elementText)
        {
            _elements.Add(elementName, elementText);
        }

        /// <summary>
        ///     Returns the text value of element if present
        /// </summary>
        /// <param name="elementName">the name of the element</param>
        /// <returns>the element text value</returns>
        public string Get(string elementName)
        {
            return !_elements.ContainsKey(elementName) ? null : _elements[elementName];
        }

        /// <summary>
        ///     Returns an element value if present.
        /// </summary>
        /// <typeparam name="T">memo class of element to return</typeparam>
        /// <param name="elementName">the name of the element</param>
        /// <returns>memo class</returns>
        public T Get<T>(string elementName) where T : IMeMoClass
        {
            if (!_elements.ContainsKey(elementName)) return default(T);
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            return (T) serializer.Deserialize(new MemoryStream(Encoding.UTF8.GetBytes(_elements[elementName])));
        }
    }
}