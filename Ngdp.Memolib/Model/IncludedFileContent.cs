﻿using System.IO;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Model
{
    public class IncludedFileContent : IFileContent
    {
        public byte[] Content { get; set; }


        public IncludedFileContent()
        {
        }

        public IncludedFileContent(string content)
        {
            Content = Encoding.UTF8.GetBytes(content);
            IsBase64Encoded = true;
        }

        public IncludedFileContent(byte[] content)
        {
            Content = content;
            IsBase64Encoded = true;
        }

        public IncludedFileContent(string content, bool base64Encoded)
        {
            Content = Encoding.UTF8.GetBytes(content);
            IsBase64Encoded = base64Encoded;
        }

        public string Location { get; } = null;

        public bool IsBase64Encoded { get; set; }

        public bool CanBeStreamed { get; } = true;

        public Stream StreamContent()
        {
            return new MemoryStream(Content);
        }
    }
}