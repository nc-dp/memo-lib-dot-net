﻿using System.Collections.Generic;

namespace Dk.Digst.Digital.Post.Memolib.Model
{
    public static class Namespaces
    {
        public static string Memo = "https://DigitalPost.dk/MeMo-1";

        public static string Grd = "https://data.gov.dk/model/core/";

        public static string Gln =
            "https://www.gs1.dk/gs1-standarder/identifikation/gln-global-location-number/";

        public static string Rid =
            "https://www.nets.eu/dk-da/løsninger/nemid/" +
            "nemid-tjenesteudbyder/supplerende-tjenester/pid-rid-cpr-tjenester";

        public static string Dmv = "https://motorregister.skat.dk/";

        public static string Kle = "http://kle-online.dk/";

        public static string Form = "http://www.form-online.dk/";

        public static string Udd =
            "https://www.dst.dk/da/TilSalg/Forskningsservice/Dokumentation/hoejkvalitetsvariable/elevregister-2/udd#";

        public static string Sor = "https://services.nsi.dk/en/Services/SOR";

        public static Dictionary<string, string> GetNamespaces()
        {
            return new Dictionary<string, string>
            {
                {"memo", Memo},
                {"grd", Grd},
                {"gln", Gln},
                {"rid", Rid},
                {"udd", Udd},
                {"form", Form},
                {"dmv", Dmv},
                {"kle", Kle},
                {"sor", Sor}
            };
        }
    }
}