﻿using System;
using System.IO;

namespace Dk.Digst.Digital.Post.Memolib.Model
{
    public class ExternalFileContent : IFileContent
    {
        public ExternalFileContent(string location, bool base64Encoded)
        {
            Location = location;
            IsBase64Encoded = base64Encoded;
        }

        public ExternalFileContent()
        {
        }

        public bool IsBase64Encoded { get; set; }

        public string Location { get; set; }

        public bool CanBeStreamed { get; } = false;

        public Stream StreamContent()
        {
            throw new NotSupportedException("External file content can not be streamed.");
        }
    }
}