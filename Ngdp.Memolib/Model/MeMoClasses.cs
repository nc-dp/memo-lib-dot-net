﻿using System.Diagnostics.CodeAnalysis;
using System.IO;
using Dk.Digst.Digital.Post.Memolib.Builder;

namespace Dk.Digst.Digital.Post.Memolib.Model
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public partial class File : IMeMoClass
    {
        private IFileContent contentField;

        public byte[] content
        {
            get
            {
                MemoryStream tmp = new MemoryStream();
                contentField.StreamContent().CopyTo(tmp);
                return tmp.ToArray();
            }
            set => contentField = FileContentBuilder.NewBuilder().Base64Content(
                System.Convert.ToBase64String(value)).Build();
        }

        public IFileContent GetContent()
        {
            return contentField;
        }

        public void SetContent(IFileContent value)
        {
            contentField = value;
        }
    }

    public partial class EMail : IMeMoClass
    {
    }

    public partial class Action : IMeMoClass
    {
    }

    public partial class EntryPoint : IMeMoClass
    {
    }

    public partial class Reservation : IMeMoClass
    {
    }

    public partial class CaseID : IMeMoClass
    {
    }

    public partial class Sender : IMeMoClass
    {
    }

    public partial class AttentionData : IMeMoClass

    {
    }

    public partial class AttentionPerson : IMeMoClass
    {
    }

    public partial class ProductionUnit : IMeMoClass
    {
    }

    public partial class GlobalLocationNumber : IMeMoClass

    {
    }

    public partial class SEnumber : IMeMoClass

    {
    }

    public partial class Telephone : IMeMoClass

    {
    }

    public partial class ContentResponsible : IMeMoClass
    {
    }

    public partial class GeneratingSystem : IMeMoClass
    {
    }

    public partial class SORdata : IMeMoClass
    {
    }

    public partial class Address : IMeMoClass
    {
    }

    public partial class AddressPoint : IMeMoClass
    {
    }

    public partial class UnstructuredAddress : IMeMoClass
    {
    }

    public partial class ContactPoint : IMeMoClass
    {
    }

    public partial class ContactInfo : IMeMoClass
    {
    }

    public partial class Representative : IMeMoClass
    {
    }

    public partial class Message : IMeMoClass
    {
    }

    public partial class MessageHeader : IMeMoClass
    {
    }

    public partial class Recipient : IMeMoClass
    {
    }

    public partial class ContentData : IMeMoClass
    {
    }

    public partial class CPRdata : IMeMoClass
    {
    }

    public partial class CVRdata : IMeMoClass
    {
    }

    public partial class MotorVehicle : IMeMoClass
    {
    }

    public partial class PropertyNumber : IMeMoClass
    {
    }

    public partial class KLEdata : IMeMoClass
    {
    }

    public partial class FORMdata : IMeMoClass
    {
    }

    public partial class Education : IMeMoClass
    {
    }

    public partial class AdditionalContentData : IMeMoClass
    {
    }

    public partial class ForwardData : IMeMoClass
    {
    }

    public partial class ReplyData : IMeMoClass
    {
    }

    public partial class MessageBody : IMeMoClass
    {
    }

    public partial class MainDocument : IMeMoClass
    {
    }

    public partial class AdditionalDocument : IMeMoClass
    {
    }

    public partial class TechnicalDocument : IMeMoClass
    {
    }

    public partial class EID : IMeMoClass
    {
    }

    public partial class AdditionalReplyData : IMeMoClass
    {
    }
}