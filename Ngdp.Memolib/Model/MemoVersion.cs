﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Dk.Digst.Digital.Post.Memolib.Model
{
    public enum MemoVersion
    {
        MeMoVersionUnknown,
        MemoVersion11,
        MemoVersion12,
    }

    public static class MemoVersionMetadata
    {
        private static readonly Dictionary<MemoVersion, decimal> VersionToDecimal = new Dictionary<MemoVersion, decimal>
        {
            {MemoVersion.MemoVersion11, 1.1M},
            {MemoVersion.MemoVersion12, 1.2M}
        };

        private static readonly Dictionary<decimal, MemoVersion> DecimalToVersion = new Dictionary<decimal, MemoVersion>
        {
            {1.1M, MemoVersion.MemoVersion11},
            {1.2M, MemoVersion.MemoVersion12}
        };

        public static decimal GetDecimalValue(MemoVersion memoVersion)
        {
            return VersionToDecimal.TryGetValue(memoVersion, out var value) ? value : -1;
        }

        public static MemoVersion FromDecimal(decimal memoVersion)
        {
            return DecimalToVersion.TryGetValue(memoVersion, out var value) ? value : MemoVersion.MeMoVersionUnknown;
        }

        public static MemoVersion FromString(string memoVersion)
        {
            var decimalVersion = decimal.Parse(memoVersion, CultureInfo.InvariantCulture);

            return FromDecimal(decimalVersion);
        }
    }
}