﻿using Dk.Digst.Digital.Post.Memolib.Model;
using System.Collections.Generic;
using Dk.Digst.Digital.Post.Memolib.Validation.rules;

namespace Dk.Digst.Digital.Post.Memolib.Validation
{
    public class MeMoValidator
    {
        private readonly IList<MeMoValidationRule> validationRules;
        private readonly Message message;

        public MeMoValidator(IList<MeMoValidationRule> validationRules, Message message)
        {
            this.validationRules = validationRules;
            this.message = message;
        }

        public HashSet<MeMoValidatorError> Validate()
        {
            if (message == null)
            {
                throw new MeMoValidationException("Message is null. Can't validate.");
            }
            return Validate(message);
        }

        public HashSet<MeMoValidatorError> Validate(Message message)
        {
            var meMoValidatorErrors = new HashSet<MeMoValidatorError>();

            foreach (var rule in validationRules)
            {
                rule.Validate(message, meMoValidatorErrors);
            }

            return meMoValidatorErrors;
        }
    }
}