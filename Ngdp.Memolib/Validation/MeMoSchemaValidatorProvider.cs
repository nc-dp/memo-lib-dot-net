﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Parser;
using Dk.Digst.Digital.Post.Memolib.Util;
using Dk.Digst.Digital.Post.Memolib.Visitor;

namespace Dk.Digst.Digital.Post.Memolib.Validation
{
    public static class MeMoSchemaValidatorProvider
    {
        /// <summary>
        ///     Creates a schemaset with all MeMo schemas
        /// </summary>
        /// <returns>the schema set</returns>
        public static XmlSchemaSet CreateMemoXmlSchemaSet(MemoVersion memoVersion)
        {
            XmlSchemaSet schemaSet = new XmlSchemaSet();

            Type schemaResources;

            switch (memoVersion)
            {
                case MemoVersion.MemoVersion12:
                    schemaResources = typeof(SchemaResources);
                    break;
                default:
                    schemaResources = typeof(SchemaResourcesOld);
                    break;
            }

            foreach (string schema in ReflectionUtil.GetFieldValues<string>(schemaResources))
            {
                AddSchema(schema, schemaSet);
            }

            return schemaSet;
        }

        private static void AddSchema(string xsd, XmlSchemaSet schemaSet)
        {
            StringReader stringReader = new StringReader(xsd);
            XmlTextReader reader = new XmlTextReader(stringReader);
            XmlSchema schema = XmlSchema.Read(reader, ValidationCallback);
            schemaSet.Add(schema);
        }

        /// <summary>
        ///     Validates the given xml in accordance to the MeMo schemas
        /// </summary>
        /// <param name="xml">the xml to validate</param>
        public static void Validate(Stream xml)
        {
            IMeMoParser parser = MeMoParserFactory.CreateParser(xml, true);
            parser.Traverse(new List<IMeMoStreamVisitor>());
        }

        /// <summary>
        ///     Validates the given xml in accordance to the MeMo schemas
        /// </summary>
        /// <param name="xml">the xml to validate</param>
        /// <returns>bool markin if the xml is a valid MeMo</returns>
        public static bool IsValid(Stream xml)
        {
            try
            {
                Validate(xml);
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        private static void ValidationCallback(object sender, ValidationEventArgs args)
        {
            switch (args.Severity)
            {
                case XmlSeverityType.Warning:
                    Console.Write("WARNING: ");
                    break;
                case XmlSeverityType.Error:
                    Console.Write("ERROR: ");
                    break;
            }
        }
    }
}