﻿using System;

namespace Dk.Digst.Digital.Post.Memolib.Validation
{
    public class MeMoValidatorError
    {
        public ValidationErrorCode ErrorCode { get; set; }
        public string[] Args { get; set; }

        public MeMoValidatorError(ValidationErrorCode errorCode, string[] args)
        {
            ErrorCode = errorCode;
            Args = args;
        }

        public MeMoValidatorError(ValidationErrorCode errorCode)
        {
            ErrorCode = errorCode;
            Args = new string[0];
        }

        public MeMoValidatorError(ValidationErrorCode errorCode, string arg)
        {
            ErrorCode = errorCode;
            Args = new string[] { arg };
        }
    }
}