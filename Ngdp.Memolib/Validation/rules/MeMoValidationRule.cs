﻿using System.Collections.Generic;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Validation.rules
{
    public interface MeMoValidationRule
    {
        void Validate(Message message, HashSet<MeMoValidatorError> meMoValidatorErrors);
    }
}