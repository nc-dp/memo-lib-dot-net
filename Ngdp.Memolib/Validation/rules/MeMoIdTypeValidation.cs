﻿using System.Collections.Generic;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Validation.rules
{
    public class MeMoIdTypeValidation : MeMoValidationRule
    {
        private static readonly List<string> validIdTypes = new List<string> { "CPR", "CVR" };

        public void Validate(Message message, HashSet<MeMoValidatorError> meMoValidatorErrors)
        {
            if (message.MessageHeader.Sender != null)
            {
                ValidateSenderIdType(message.MessageHeader.Sender, meMoValidatorErrors);
            }
            if (message.MessageHeader.Recipient != null)
            {
                ValidateRecipientIdType(message.MessageHeader.Recipient, meMoValidatorErrors);
            }
        }

        private static void ValidateIdType(string idType, string meMoClass, HashSet<MeMoValidatorError> meMoValidatorErrors)
        {
            if (!validIdTypes.Contains(idType.ToUpper()))
            {
                meMoValidatorErrors.Add(
                    new MeMoValidatorError(
                        ValidationErrorCode.ID_TYPE_INVALID,
                        new string[] { meMoClass, idType }));
            }
        }

        private void ValidateSenderIdType(Sender sender, HashSet<MeMoValidatorError> meMoValidatorErrors)
        {
            ValidateIdType(sender.idType, "sender", meMoValidatorErrors);

            if (sender.Representative != null)
            {
                ValidateIdType(
                    sender.Representative.idType,
                    "sender.representative",
                    meMoValidatorErrors);
            }
        }

        private void ValidateRecipientIdType(Recipient recipient, HashSet<MeMoValidatorError> meMoValidatorErrors)
        {
            ValidateIdType(recipient.idType, "recipient", meMoValidatorErrors);
        }
    }
}