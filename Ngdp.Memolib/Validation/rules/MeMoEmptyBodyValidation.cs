﻿using System.Collections.Generic;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Validation.rules
{
    public class MeMoEmptyBodyValidation : MeMoValidationRule
    {
        public void Validate(Message message, HashSet<MeMoValidatorError> meMoValidatorErrors)
        {
            if (IsMessageTypeDigitalPost(message.MessageHeader.messageType)
                && IsMessageBodyNullOrEmpty(message.MessageBody))
            {
                meMoValidatorErrors.Add(
                    new MeMoValidatorError(ValidationErrorCode.MESSAGE_BODY_NOT_FOUND));
            }
        }

        private bool IsMessageBodyNullOrEmpty(MessageBody messageBody)
        {
            return messageBody == null || HasNoDocuments(messageBody);
        }

        private bool IsMessageTypeDigitalPost(memoMessageType messageType)
        {
            return messageType == memoMessageType.DIGITALPOST;
        }

        private bool HasNoDocuments(MessageBody messageBody)
        {
            bool isAdditionalDocumentsEmpty =
                messageBody.AdditionalDocument == null
                || messageBody.AdditionalDocument.Length == 0;
            bool isTechnicalDocumentsEmpty =
                messageBody.TechnicalDocument == null || messageBody.TechnicalDocument.Length == 0;

            return (messageBody.MainDocument == null
                    && isAdditionalDocumentsEmpty
                    && isTechnicalDocumentsEmpty);
        }
    }
}