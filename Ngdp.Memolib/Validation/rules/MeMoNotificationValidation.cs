﻿using System.Collections.Generic;
using Dk.Digst.Digital.Post.Memolib.Model;
using System.Linq;

namespace Dk.Digst.Digital.Post.Memolib.Validation.rules
{
    public class MeMoNotificationValidation : MeMoValidationRule
    {
        public void Validate(Message message, HashSet<MeMoValidatorError> meMoValidatorErrors)
        {
            if (!IsMessageTypeNemSms(message.MessageHeader.messageType))
            {
                return;
            }

            if (string.IsNullOrWhiteSpace(message.MessageHeader.notification))
            {
                meMoValidatorErrors.Add(
                    new MeMoValidatorError(ValidationErrorCode.EMPTY_NOTIFICATION_NOT_ALLOWED));
            }
        }

        private bool IsMessageTypeNemSms(memoMessageType messageType)
        {
            return messageType == memoMessageType.NEMSMS;
        }
    }
}