﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Validation.rules
{
    public static class MeMoValidationRuleFactory
    {
        public static IList<MeMoValidationRule> CreateRules(MemoVersion meMoVersion)
        {
            if (meMoVersion.Equals(MemoVersion.MemoVersion12))
            {
                return CreateNewMeMoRules();
            }
            return CreateRules();
        }

        /// <summary>
        /// This utility creates rules used by MeMo validator for MeMo header validation.
        /// </summary>
        /// <returns>Unmodifiable List of rules.</returns>
        private static IList<MeMoValidationRule> CreateRules()
        {
            var meMoMeMoValidationRules = new List<MeMoValidationRule>
            {
                new MeMoNotificationValidation(),
                new MeMoIdTypeValidation(),
                new MeMoPostTypeValidation(),
                new MeMoCprValidation(),
                new MeMoCvrValidation(),
                new MeMoEmptyBodyValidation()
            };

            return new ReadOnlyCollection<MeMoValidationRule>(meMoMeMoValidationRules);
        }

        private static IList<MeMoValidationRule> CreateNewMeMoRules()
        {
            var meMoMeMoValidationRules = new List<MeMoValidationRule>
            {
                new MeMoNotificationValidation(),
                new MeMoIdTypeValidation(),
                new MeMoReplyDataValidation(),
                new MeMoPostTypeValidation(),
                new MeMoCprValidation(),
                new MeMoCvrValidation(),
                new MeMoEmptyBodyValidation()
            };

            return new ReadOnlyCollection<MeMoValidationRule>(meMoMeMoValidationRules);
        }
    }
}