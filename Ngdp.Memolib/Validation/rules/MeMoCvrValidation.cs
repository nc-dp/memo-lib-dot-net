﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Validation.rules
{
    public class MeMoCvrValidation : MeMoValidationRule
    {
        private const string CVR = "CVR";

        public void Validate(Message message, HashSet<MeMoValidatorError> meMoValidatorErrors)
        {
            if (message.MessageHeader.Sender != null)
            {
                ValidateSenderCvr(message.MessageHeader.Sender, meMoValidatorErrors);
            }
            if (message.MessageHeader.Recipient != null)
            {
                ValidateRecipientCvr(message.MessageHeader.Recipient, meMoValidatorErrors);
            }
        }

        private bool IsCvr(string idType)
        {
            return string.Equals(CVR, idType, System.StringComparison.OrdinalIgnoreCase);
        }

        private static void ValidateCvr(
            string cvr, ValidationErrorCode errorCode, HashSet<MeMoValidatorError> meMoValidatorErrors)
        {
            if (!Regex.IsMatch(cvr, @"^\d{8}$"))
            {
                meMoValidatorErrors.Add(new MeMoValidatorError(errorCode, cvr));
            }
        }

        private void ValidateSenderCvr(Sender sender, HashSet<MeMoValidatorError> meMoValidatorErrors)
        {
            if (IsCvr(sender.idType))
            {
                ValidateCvr(
                    sender.senderID, ValidationErrorCode.SENDER_CVR_INVALID, meMoValidatorErrors);
            }

            if (sender.Representative != null && IsCvr(sender.Representative.idType))
            {
                ValidateCvr(
                    sender.Representative.representativeID,
                    ValidationErrorCode.REPRESENTATIVE_CVR_INVALID,
                    meMoValidatorErrors);
            }
        }

        private void ValidateRecipientCvr(
            Recipient recipient, HashSet<MeMoValidatorError> meMoValidatorErrors)
        {
            if (IsCvr(recipient.idType))
            {
                ValidateCvr(
                    recipient.recipientID,
                    ValidationErrorCode.RECIPIENT_CVR_INVALID,
                    meMoValidatorErrors);
            }
        }
    }
}