﻿using System.Collections.Generic;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Validation.rules
{
    public class MeMoPostTypeValidation : MeMoValidationRule
    {
        private readonly List<string> validPostType = new List<string> { "MYNDIGHEDSPOST", "VIRKSOMHEDSPOST" };

        public void Validate(Message message, HashSet<MeMoValidatorError> meMoValidatorErrors)
        {
            if (!PostTypeExists(message.MessageHeader))
            {
                return;
            }

            if (!validPostType.Contains(message.MessageHeader.postType))
            {
                meMoValidatorErrors.Add(
                    new MeMoValidatorError(
                        ValidationErrorCode.POST_TYPE_INVALID,
                        message.MessageHeader.postType));
            }
        }

        private bool PostTypeExists(MessageHeader messageHeader)
        {
            return messageHeader.postType != null;
        }
    }
}