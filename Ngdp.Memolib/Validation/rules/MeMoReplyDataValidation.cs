﻿using System.Collections.Generic;
using System.Linq;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Validation.rules
{
    public class MeMoReplyDataValidation : MeMoValidationRule
    {
        public void Validate(Message message, HashSet<MeMoValidatorError> meMoValidatorErrors)
        {
            if (!HasReplyAndReplyIsTrue(message.MessageHeader.reply))
            {
                return;
            }

            ValidateReplyData(message.MessageHeader.ReplyData, meMoValidatorErrors);
        }

        private bool HasReplyAndReplyIsTrue(bool reply)
        {
            return reply;
        }

        private void ValidateReplyData(
            ReplyData[] replyDataList, HashSet<MeMoValidatorError> meMoValidatorErrors)
        {
            if (replyDataList == null || !replyDataList.Any())
            {
                meMoValidatorErrors.Add(
                    new MeMoValidatorError(
                        ValidationErrorCode.REPLY_DATA_MESSAGE_UUID_NOT_FOUND));
                return;
            }

            foreach (var replyData in replyDataList)
            {
                if (string.IsNullOrEmpty(replyData.messageUUID))
                {
                    meMoValidatorErrors.Add(
                        new MeMoValidatorError(
                            ValidationErrorCode.REPLY_DATA_MESSAGE_UUID_NOT_FOUND));
                }
            }
        }
    }
}