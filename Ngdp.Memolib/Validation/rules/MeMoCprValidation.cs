﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Validation.rules
{
    public class MeMoCprValidation : MeMoValidationRule
    {
        private const string CPR = "CPR";

        public void Validate(Message message, HashSet<MeMoValidatorError> meMoValidatorErrors)
        {
            if (message.MessageHeader.Sender != null)
            {
                ValidateSenderCpr(message.MessageHeader.Sender, meMoValidatorErrors);
            }
            if (message.MessageHeader.Recipient != null)
            {
                ValidateRecipientCpr(message.MessageHeader.Recipient, meMoValidatorErrors);
            }
        }

        private bool IsCpr(string idType)
        {
            return string.Equals(CPR, idType, System.StringComparison.OrdinalIgnoreCase);
        }

        private static void ValidateCpr(
            string cpr, ValidationErrorCode errorCode, HashSet<MeMoValidatorError> meMoValidatorErrors)
        {
            if (!Regex.IsMatch(cpr, @"^\d{6}-?\d{4}$"))
            {
                meMoValidatorErrors.Add(new MeMoValidatorError(errorCode, cpr));
            }
        }

        private void ValidateSenderCpr(Sender sender, HashSet<MeMoValidatorError> meMoValidatorErrors)
        {
            if (IsCpr(sender.idType))
            {
                ValidateCpr(
                    sender.senderID, ValidationErrorCode.SENDER_CPR_INVALID, meMoValidatorErrors);
            }

            if (sender.Representative != null && IsCpr(sender.Representative.idType))
            {
                ValidateCpr(
                    sender.Representative.representativeID,
                    ValidationErrorCode.REPRESENTATIVE_CPR_INVALID,
                    meMoValidatorErrors);
            }
        }

            private void ValidateRecipientCpr(
            Recipient recipient, HashSet<MeMoValidatorError> meMoValidatorErrors)
        {
            if (IsCpr(recipient.idType))
            {
                ValidateCpr(
                    recipient.recipientID,
                    ValidationErrorCode.RECIPIENT_CPR_INVALID,
                    meMoValidatorErrors);
            }
        }
    }
}