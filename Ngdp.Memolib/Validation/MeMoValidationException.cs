﻿namespace Dk.Digst.Digital.Post.Memolib.Validation
{
    public class MeMoValidationException : System.Exception
    {
        public MeMoValidationException(string message) : base(message)
        {
        }

        public MeMoValidationException(System.Exception e) : base(e.Message, e)
        {
        }
    }
}