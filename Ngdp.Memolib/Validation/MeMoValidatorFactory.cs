﻿using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Validation.rules;

namespace Dk.Digst.Digital.Post.Memolib.Validation
{
    public static class MeMoValidatorFactory
    {
        /// <summary>
        /// Create a Message MeMo validator specified by the MeMo version
        /// </summary>
        /// <param name="meMoVersion">the target version of the MeMo to use for validation</param>
        /// <returns>Memo validator</returns>
        public static MeMoValidator CreateMeMoValidator(MemoVersion memoVersion)
        {
            return new MeMoValidator(
                MeMoValidationRuleFactory.CreateRules(memoVersion),
                null);
        }

        /// <summary>
        /// Create a Message MeMo validator with a Message
        /// </summary>
        /// <param name="message">the Message that can be validated</param>
        /// <returns>Memo validator</returns>
        public static MeMoValidator CreateMeMoValidator(Message message)
        {
            return new MeMoValidator(
                MeMoValidationRuleFactory.CreateRules(MemoVersionMetadata.FromDecimal(message.memoVersion)),
                message);
        }
    }
}