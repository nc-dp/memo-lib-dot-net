﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma
{
    /// <summary>
    /// Caches large arrays for reuse (base class and a dummy cache implementation).
    /// <para>
    /// When compressing or decompressing many (very) small files in a row, the
    /// time spent in construction of new compressor or decompressor objects
    /// can be longer than the time spent in actual compression or decompression.
    /// A large part of this initialization overhead comes from allocation and
    /// garbage collection of large arrays.
    /// </para>
    /// <para>
    /// The {@code ArrayCache} API provides a way to cache large array allocations
    /// for reuse. It can give a major performance improvement when compressing or
    /// decompressing many tiny files. If you are only (de)compressing one or two
    /// files or the files a very big, array caching won't improve anything,
    /// although it won't make anything slower either.
    /// </para>
    /// <para>
    /// <b>Important: The users of ArrayCache don't return the allocated arrays
    /// back to the cache in all situations.</b>
    /// This a reason why it's called a cache instead of a pool.
    /// If it is important to be able to return every array back to a cache,
    /// <seealso cref="ResettableArrayCache"/> can be useful.
    /// </para>
    /// <para>
    /// In compressors (OutputStreams) the arrays are returned to the cache
    /// when a call to {@code finish()} or {@code close()} returns
    /// successfully (no exceptions are thrown).
    /// </para>
    /// <para>
    /// In decompressors (InputStreams) the arrays are returned to the cache when
    /// the decompression is successfully finished ({@code read} returns {@code -1})
    /// or {@code close()} or {@code close(boolean)} is called. This is true even
    /// if closing throws an exception.
    /// </para>
    /// <para>
    /// Raw decompressors don't support {@code close(boolean)}. With raw
    /// decompressors, if one wants to put the arrays back to the cache without
    /// closing the underlying {@code InputStream}, one can wrap the
    /// {@code InputStream} into <seealso cref="CloseIgnoringInputStream"/> when creating
    /// the decompressor instance. Then one can use {@code close()}.
    /// </para>
    /// <para>
    /// Different cache implementations can be extended from this base class.
    /// All cache implementations must be thread safe.
    /// </para>
    /// <para>
    /// This class also works as a dummy cache that simply calls {@code new}
    /// to allocate new arrays and doesn't try to cache anything. A statically
    /// allocated dummy cache is available via <seealso cref="getDummyCache()"/>.
    /// </para>
    /// <para>
    /// If no {@code ArrayCache} is specified when constructing a compressor or
    /// decompressor, the default {@code ArrayCache} implementation is used.
    /// See <seealso cref="getDefaultCache()"/> and <seealso cref="setDefaultCache(ArrayCache)"/>.
    /// </para>
    /// <para>
    /// This is a class instead of an interface because it's possible that in the
    /// future we may want to cache other array types too. New methods can be
    /// added to this class without breaking existing cache implementations.
    /// 
    /// @since 1.7
    /// 
    /// </para>
    /// </summary>
    /// <seealso cref= BasicArrayCache </seealso>
    public class ArrayCache
    {
        /// <summary>
        /// Global dummy cache instance that is returned by {@code getDummyCache()}.
        /// </summary>
        private static readonly ArrayCache dummyCache = new ArrayCache();

        /// <summary>
        /// Global default {@code ArrayCache} that is used when no other cache has
        /// been specified.
        /// </summary>
        private static volatile ArrayCache defaultCache = dummyCache;

        /// <summary>
        /// Returns a statically-allocated {@code ArrayCache} instance.
        /// It can be shared by all code that needs a dummy cache.
        /// </summary>
        public static ArrayCache DummyCache
        {
            get
            {
                return dummyCache;
            }
        }

        /// <summary>
        /// Gets the default {@code ArrayCache} instance.
        /// This is a global cache that is used when the application
        /// specifies nothing else. The default is a dummy cache
        /// (see <seealso cref="getDummyCache()"/>).
        /// </summary>
        public static ArrayCache DefaultCache
        {
            get
            {
                // It's volatile so no need for synchronization.
                return defaultCache;
            }
            set
            {
                if (value == null)
                {
                    throw new System.NullReferenceException();
                }

                // It's volatile so no need for synchronization.
                defaultCache = value;
            }
        }


        /// <summary>
        /// Creates a new {@code ArrayCache} that does no caching
        /// (a dummy cache). If you need a dummy cache, you may want to call
        /// <seealso cref="getDummyCache()"/> instead.
        /// </summary>
        public ArrayCache()
        {
        }

        /// <summary>
        /// Allocates a new byte array.
        /// <para>
        /// This implementation simply returns {@code new byte[size]}.
        /// 
        /// </para>
        /// </summary>
        /// <param name="size">            the minimum size of the array to allocate;
        ///                          an implementation may return an array that
        ///                          is larger than the given {@code size}
        /// </param>
        /// <param name="fillWithZeros">   if true, the caller expects that the first
        ///                          {@code size} elements in the array are zero;
        ///                          if false, the array contents can be anything,
        ///                          which speeds things up when reusing a cached
        ///                          array </param>
        public virtual byte[] GetByteArray(int size, bool fillWithZeros)
        {
            return new byte[size];
        }

        /// <summary>
        /// Puts the given byte array to the cache. The caller must no longer
        /// use the array.
        /// <para>
        /// This implementation does nothing.
        /// </para>
        /// </summary>
        public virtual void PutArray(byte[] array)
        {
        }

        /// <summary>
        /// Allocates a new int array.
        /// <para>
        /// This implementation simply returns {@code new int[size]}.
        /// 
        /// </para>
        /// </summary>
        /// <param name="size">            the minimum size of the array to allocate;
        ///                          an implementation may return an array that
        ///                          is larger than the given {@code size}
        /// </param>
        /// <param name="fillWithZeros">   if true, the caller expects that the first
        ///                          {@code size} elements in the array are zero;
        ///                          if false, the array contents can be anything,
        ///                          which speeds things up when reusing a cached
        ///                          array </param>
        public virtual int[] GetIntArray(int size, bool fillWithZeros)
        {
            return new int[size];
        }

        /// <summary>
        /// Puts the given int array to the cache. The caller must no longer
        /// use the array.
        /// <para>
        /// This implementation does nothing.
        /// </para>
        /// </summary>
        public virtual void PutArray(int[] array)
        {
        }
    }
}
