﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma
{
    internal class UnsupportedOptionsException : System.Exception
    {
        public UnsupportedOptionsException(string message)
            : base(message)
        {

        }
    }
}
