﻿using Dk.Digst.Digital.Post.Memolib.Lzma.Lz;
using Dk.Digst.Digital.Post.Memolib.Lzma.Lzma;
using Dk.Digst.Digital.Post.Memolib.Lzma.RangeCoder;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma
{
    public class LZMAOutputStream : FinishableOutputStream
    {
        private Stream outputStream;

        private readonly ArrayCache arrayCache;

        private LZEncoder lz;
        private readonly RangeEncoderToStream rc;
        private LZMAEncoder lzma;

        private readonly int props;
        private readonly bool useEndMarker;
        private readonly long expectedUncompressedSize;
        private long currentUncompressedSize = 0;

        private bool finished = false;
        private IOException exception = null;

        private readonly byte[] tempBuf = new byte[1];

        private LZMAOutputStream(Stream outputStream, LZMAOptions options, bool useHeader, bool useEndMarker, long expectedUncompressedSize, ArrayCache arrayCache)
        {
            if (outputStream == null)
            {
                throw new System.NullReferenceException();
            }

            // -1 indicates unknown and >= 0 are for known sizes.
            if (expectedUncompressedSize < -1)
            {
                throw new System.ArgumentException("Invalid expected input size (less than -1)");
            }

            this.useEndMarker = useEndMarker;
            this.expectedUncompressedSize = expectedUncompressedSize;

            this.arrayCache = arrayCache;

            this.outputStream = outputStream;
            rc = new RangeEncoderToStream(outputStream);

            int dictSize = options.DictSize;
            lzma = LZMAEncoder.GetInstance(rc, options.Lc, options.Lp, options.Pb, options.Mode, dictSize, 0, options.NiceLen, options.MatchFinder, options.DepthLimit, arrayCache);

            lz = lzma.LZEncoder;

            sbyte[] presetDict = options.PresetDict;
            if (presetDict != null && presetDict.Length > 0)
            {
                if (useHeader)
                {
                    throw new UnsupportedOptionsException("Preset dictionary cannot be used in .lzma files (try a raw LZMA stream instead)");
                }

                lz.SetPresetDict(dictSize, presetDict);
            }

            props = (options.Pb * 5 + options.Lp) * 9 + options.Lc;

            if (useHeader)
            {
                // Props byte stores lc, lp, and pb.
                outputStream.WriteByte((byte)props);

                // Dictionary size is stored as a 32-bit unsigned little endian
                // integer.
                for (int i = 0; i < 4; ++i)
                {
                    outputStream.WriteByte((byte)(dictSize & 0xFF));
                    dictSize = (int)((uint)dictSize >> 8);
                }

                // Uncompressed size is stored as a 64-bit unsigned little endian
                // integer. The max value (-1 in two's complement) indicates
                // unknown size.
                for (int i = 0; i < 8; ++i)
                {
                    outputStream.WriteByte((byte)((int)((long)((ulong)expectedUncompressedSize >> (8 * i))) & 0xFF));
                }
            }
        }

        /// <summary>
        /// Creates a new compressor for the legacy .lzma file format.
        /// <para>
        /// If the uncompressed size of the input data is known, it will be stored
        /// in the .lzma header and no end of stream marker will be used. Otherwise
        /// the header will indicate unknown uncompressed size and the end of stream
        /// marker will be used.
        /// </para>
        /// <para>
        /// Note that a preset dictionary cannot be used in .lzma files but
        /// it can be used for raw LZMA streams.
        /// 
        /// </para>
        /// </summary>
        /// <param name="outputStream">         output stream to which the compressed data
        ///                          will be written
        /// </param>
        /// <param name="options">     LZMA compression options;
        /// </param>
        /// <param name="inputSize">   uncompressed size of the data to be compressed;
        ///                          use <code>-1</code> when unknown
        /// </param>
        /// <exception cref="IOException"> may be thrown from <code>out</code> </exception>
        public LZMAOutputStream(Stream outputStream, LZMAOptions options, long inputSize) : this(outputStream, options, inputSize, ArrayCache.DefaultCache)
        {
        }

        /// <summary>
        /// Creates a new compressor for the legacy .lzma file format.
        /// <para>
        /// This is identical to
        /// <code>LZMAOutputStream(OutputStream, LZMA2Options, long)</code>
        /// except that this also takes the <code>arrayCache</code> argument.
        /// 
        /// </para>
        /// </summary>
        /// <param name="outputStream">         output stream to which the compressed data
        ///                          will be written
        /// </param>
        /// <param name="options">     LZMA compression options; 
        /// </param>
        /// <param name="inputSize">   uncompressed size of the data to be compressed;
        ///                          use <code>-1</code> when unknown
        /// </param>
        /// <param name="arrayCache">  cache to be used for allocating large arrays
        /// </param>
        /// <exception cref="IOException"> may be thrown from <code>out</code>
        /// </exception>
        public LZMAOutputStream(Stream outputStream, LZMAOptions options, long inputSize, ArrayCache arrayCache) : this(outputStream, options, true, inputSize == -1, inputSize, arrayCache)
        {
        }

        /// <summary>
        /// Creates a new compressor for raw LZMA (also known as LZMA1) stream.
        /// <para>
        /// Raw LZMA streams can be encoded with or without end of stream marker.
        /// When decompressing the stream, one must know if the end marker was used
        /// and tell it to the decompressor. If the end marker wasn't used, the
        /// decompressor will also need to know the uncompressed size.
        /// 
        /// </para>
        /// </summary>
        /// <param name="outputStream">         output stream to which the compressed data
        ///                          will be written
        /// </param>
        /// <param name="options">     LZMA compression options;
        /// </param>
        /// <param name="useEndMarker">
        ///                          if end of stream marker should be written
        /// </param>
        /// <exception cref="IOException"> may be thrown from <code>out</code> </exception>
        public LZMAOutputStream(Stream outputStream, LZMAOptions options, bool useEndMarker) : this(outputStream, options, useEndMarker, ArrayCache.DefaultCache)
        {
        }

        /// <summary>
        /// Creates a new compressor for raw LZMA (also known as LZMA1) stream.
        /// <para>
        /// This is identical to
        /// <code>LZMAOutputStream(OutputStream, LZMA2Options, boolean)</code>
        /// except that this also takes the <code>arrayCache</code> argument.
        /// 
        /// </para>
        /// </summary>
        /// <param name="outputStream">         output stream to which the compressed data
        ///                          will be written
        /// </param>
        /// <param name="options">     LZMA compression options;
        /// </param>
        /// <param name="useEndMarker">
        ///                          if end of stream marker should be written
        /// </param>
        /// <param name="arrayCache">  cache to be used for allocating large arrays
        /// </param>
        /// <exception cref="IOException"> may be thrown from <code>out</code>
        /// </exception>
        public LZMAOutputStream(Stream outputStream, LZMAOptions options, bool useEndMarker, ArrayCache arrayCache) : this(outputStream, options, false, useEndMarker, -1, arrayCache)
        {
        }

        /// <summary>
        /// Returns the LZMA lc/lp/pb properties encoded into a single byte.
        /// This might be useful when handling file formats other than .lzma
        /// that use the same encoding for the LZMA properties as .lzma does.
        /// </summary>
        public virtual int Props
        {
            get
            {
                return props;
            }
        }

        /// <summary>
        /// Gets the amount of uncompressed data written to the stream.
        /// This is useful when creating raw LZMA streams without
        /// the end of stream marker.
        /// </summary>
        public virtual long UncompressedSize
        {
            get
            {
                return currentUncompressedSize;
            }
        }

        public override bool CanRead => outputStream.CanRead;

        public override bool CanSeek => outputStream.CanSeek;

        public override bool CanWrite => outputStream.CanWrite;

        public override long Length => outputStream.Length;

        public override long Position
        {
            get
            {
                return outputStream.Position;
            }
            set
            {
                outputStream.Position = value;
            }
        }

        public virtual void Write(int b)
        {
            tempBuf[0] = (byte)b;
            Write(tempBuf, 0, 1);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            if (offset < 0 || count < 0 || offset + count < 0 || offset + count > buffer.Length)
            {
                throw new System.IndexOutOfRangeException();
            }

            if (exception != null)
            {
                throw exception;
            }

            if (finished)
            {
                throw new IOException("Stream finished or closed");
            }

            if (expectedUncompressedSize != -1 && expectedUncompressedSize - currentUncompressedSize < count)
            {
                throw new IOException("Expected uncompressed input size (" + expectedUncompressedSize + " bytes) was exceeded");
            }

            currentUncompressedSize += count;

            try
            {
                while (count > 0)
                {
                    int used = lz.FillWindow(buffer, offset, count);
                    offset += used;
                    count -= used;
                    lzma.EncodeForLZMA1();
                }
            }
            catch (IOException e)
            {
                exception = e;
                throw e;
            }
        }

        public override void Flush()
        {
        }

        /// <summary>
        /// Finishes the stream without closing the underlying OutputStream.
        /// </summary>
        public override void Finish()
        {
            if (!finished)
            {
                if (exception != null)
                {
                    throw exception;
                }

                try
                {
                    if (expectedUncompressedSize != -1 && expectedUncompressedSize != currentUncompressedSize)
                    {
                        throw new IOException($"Expected uncompressed size ({expectedUncompressedSize}) doesn't equal the number of bytes written to the stream ({currentUncompressedSize})");
                    }

                    lz.SetFinishing();
                    lzma.EncodeForLZMA1();

                    if (useEndMarker)
                    {
                        lzma.EncodeLZMA1EndMarker();
                    }

                    rc.Finish();
                }
                catch (IOException e)
                {
                    exception = e;
                    throw e;
                }

                finished = true;

                lzma.PutArraysToCache(arrayCache);
                lzma = null;
                lz = null;
            }
        }

        /// <summary>
        /// Finishes the stream and closes the underlying OutputStream.
        /// </summary>
        public override void Close()
        {
            if (outputStream != null)
            {
                try
                {
                    Finish();
                }
                catch (IOException e) { }

                try
                {
                    outputStream.Close();
                }
                catch (IOException e)
                {
                    if (exception == null)
                        exception = e;
                }

                outputStream = null;
            }

            if (exception != null)
                throw exception;
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return outputStream.Read(buffer, offset, count);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return outputStream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            outputStream.SetLength(value);
        }
    }
}
