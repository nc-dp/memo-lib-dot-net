﻿using Dk.Digst.Digital.Post.Memolib.Lzma.Lz;
using Dk.Digst.Digital.Post.Memolib.Lzma.Lzma;
using Dk.Digst.Digital.Post.Memolib.Lzma.RangeCoder;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma
{
    /// <summary>
    /// Decompresses .lzma files and raw LZMA streams (no .lzma header).
    /// <para>
    /// <b>IMPORTANT:</b> In contrast to other classes in this package, this class
    /// reads data from its input stream one byte at a time. If the input stream
    /// is for example <seealso cref="System.IO.FileStream_Input"/>, wrapping it into
    /// <seealso cref="java.io.BufferedInputStream"/> tends to improve performance a lot.
    /// This is not automatically done by this class because there may be use
    /// cases where it is desired that this class won't read any bytes past
    /// the end of the LZMA stream.
    /// </para>
    /// </summary>
    public class LZMAInputStream : Stream
    {
        /// <summary>
        /// Largest dictionary size supported by this implementation.
        /// <para>
        /// LZMA allows dictionaries up to one byte less than 4 GiB. This
        /// implementation supports only 16 bytes less than 2 GiB. This
        /// limitation is due to Java using signed 32-bit integers for array
        /// indexing. The limitation shouldn't matter much in practice since so
        /// huge dictionaries are not normally used.
        /// </para>
        /// </summary>
        public static readonly int DICT_SIZE_MAX = int.MaxValue & ~15;

        private Stream inputStream;
        private ArrayCache arrayCache;
        private LZDecoder lz;
        private RangeDecoderFromStream rc;
        private LZMADecoder lzma;

        private bool endReached = false;
        private bool relaxedEndCondition = false;

        private readonly byte[] tempBuf = new byte[1];

        /// <summary>
        /// Number of uncompressed bytes left to be decompressed, or -1 if
        /// the end marker is used.
        /// </summary>
        private long remainingSize;

        private IOException exception = null;

        public override bool CanRead => inputStream.CanRead;

        public override bool CanSeek => inputStream.CanSeek;

        public override bool CanWrite => false;

        public override long Length => inputStream.Length;

        public override long Position { get => inputStream.Position; set => inputStream.Position = value; }

        /// <summary>
        /// Gets approximate decompressor memory requirements as kibibytes for
        /// the given dictionary size and LZMA properties byte (lc, lp, and pb).
        /// </summary>
        /// <param name="dictSize">    LZMA dictionary size as bytes, should be
        ///                          in the range [<code>0</code>,
        ///                          <code>DICT_SIZE_MAX</code>]
        /// </param>
        /// <param name="propsByte">   LZMA properties byte that encodes the values
        ///                          of lc, lp, and pb
        /// </param>
        /// <returns>      approximate memory requirements as kibibytes (KiB)
        /// </returns>
        /// <exception cref="UnsupportedOptionsException">
        ///                          if <code>dictSize</code> is outside
        ///                          the range [<code>0</code>,
        ///                          <code>DICT_SIZE_MAX</code>]
        /// </exception>
        /// <exception cref="CorruptedInputException">
        ///                          if <code>propsByte</code> is invalid </exception>
        public static int GetMemoryUsage(int dictSize, byte propsByte)
        {
            if (dictSize < 0 || dictSize > DICT_SIZE_MAX)
            {
                throw new UnsupportedOptionsException("LZMA dictionary is too big for this implementation");
            }

            int props = propsByte & 0xFF;
            if (props > (4 * 5 + 4) * 9 + 8)
            {
                throw new CorruptedInputException("Invalid LZMA properties byte");
            }

            props %= 9 * 5;
            int lp = props / 9;
            int lc = props - lp * 9;

            return GetMemoryUsage(dictSize, lc, lp);
        }

        /// <summary>
        /// Gets approximate decompressor memory requirements as kibibytes for
        /// the given dictionary size, lc, and lp. Note that pb isn't needed.
        /// </summary>
        /// <param name="dictSize">    LZMA dictionary size as bytes, must be
        ///                          in the range [<code>0</code>,
        ///                          <code>DICT_SIZE_MAX</code>]
        /// </param>
        /// <param name="lc">          number of literal context bits, must be
        ///                          in the range [0, 8]
        /// </param>
        /// <param name="lp">          number of literal position bits, must be
        ///                          in the range [0, 4]
        /// </param>
        /// <returns>      approximate memory requirements as kibibytes (KiB) </returns>
        public static int GetMemoryUsage(int dictSize, int lc, int lp)
        {
            if (lc < 0 || lc > 8 || lp < 0 || lp > 4)
            {
                throw new System.ArgumentException("Invalid lc or lp");
            }

            // Probability variables have the type "short". There are
            // 0x300 (768) probability variables in each literal subcoder.
            // The number of literal subcoders is 2^(lc + lp).
            //
            // Roughly 10 KiB for the base state + LZ decoder's dictionary buffer
            // + sizeof(short) * number probability variables per literal subcoder
            //   * number of literal subcoders
            return 10 + GetDictSize(dictSize) / 1024 + ((2 * 0x300) << (lc + lp)) / 1024;
        }

        private static int GetDictSize(int dictSize)
        {
            if (dictSize < 0 || dictSize > DICT_SIZE_MAX)
            {
                throw new System.ArgumentException("LZMA dictionary is too big for this implementation");
            }

            // For performance reasons, use a 4 KiB dictionary if something
            // smaller was requested. It's a rare situation and the performance
            // difference isn't huge, and it starts to matter mostly when the
            // dictionary is just a few bytes. But we need to handle the special
            // case of dictSize == 0 anyway, which is an allowed value but in
            // practice means one-byte dictionary.
            //
            // Note that using a dictionary bigger than specified in the headers
            // can hide errors if there is a reference to data beyond the original
            // dictionary size but is still within 4 KiB.
            if (dictSize < 4096)
            {
                dictSize = 4096;
            }

            // Round dictionary size upward to a multiple of 16. This way LZMA
            // can use LZDecoder.getPos() for calculating LZMA's posMask.
            return (dictSize + 15) & ~15;
        }

        /// <summary>
        /// Creates a new .lzma file format decompressor without
        /// a memory usage limit.
        /// </summary>
        /// <param name="inputStream">          input stream from which .lzma data is read;
        ///                          it might be a good idea to wrap it in
        ///                          <code>BufferedInputStream</code>, see the
        ///                          note at the top of this page
        /// </param>
        /// <exception cref="CorruptedInputException">
        ///                          file is corrupt or perhaps not in
        ///                          the .lzma format at all
        /// </exception>
        /// <exception cref="UnsupportedOptionsException">
        ///                          dictionary size or uncompressed size is too
        ///                          big for this implementation
        /// </exception>
        /// <exception cref="EOFException">
        ///                          file is truncated or perhaps not in
        ///                          the .lzma format at all
        /// </exception>
        /// <exception cref="IOException"> may be thrown by <code>in</code> </exception>
        public LZMAInputStream(Stream inputStream) : this(inputStream, -1)
        {
        }

        /// <summary>
        /// Creates a new .lzma file format decompressor without
        /// a memory usage limit.
        /// <para>
        /// This is identical to <code>LZMAInputStream(InputStream)</code>
        /// except that this also takes the <code>arrayCache</code> argument.
        /// 
        /// </para>
        /// </summary>
        /// <param name="inputStream">          input stream from which .lzma data is read;
        ///                          it might be a good idea to wrap it in
        ///                          <code>BufferedInputStream</code>, see the
        ///                          note at the top of this page
        /// 
        /// </param>
        /// <param name="arrayCache">  cache to be used for allocating large arrays
        /// </param>
        /// <exception cref="CorruptedInputException">
        ///                          file is corrupt or perhaps not in
        ///                          the .lzma format at all
        /// </exception>
        /// <exception cref="UnsupportedOptionsException">
        ///                          dictionary size or uncompressed size is too
        ///                          big for this implementation
        /// </exception>
        /// <exception cref="EOFException">
        ///                          file is truncated or perhaps not in
        ///                          the .lzma format at all
        /// </exception>
        /// <exception cref="IOException"> may be thrown by <code>in</code>
        /// 
        /// @since 1.7 </exception>
        public LZMAInputStream(Stream inputStream, ArrayCache arrayCache) : this(inputStream, -1, arrayCache)
        {
        }

        /// <summary>
        /// Creates a new .lzma file format decompressor with an optional
        /// memory usage limit.
        /// </summary>
        /// <param name="inputStream">          input stream from which .lzma data is read;
        ///                          it might be a good idea to wrap it in
        ///                          <code>BufferedInputStream</code>, see the
        ///                          note at the top of this page
        /// </param>
        /// <param name="memoryLimit"> memory usage limit in kibibytes (KiB)
        ///                          or <code>-1</code> to impose no
        ///                          memory usage limit
        /// </param>
        /// <exception cref="CorruptedInputException">
        ///                          file is corrupt or perhaps not in
        ///                          the .lzma format at all
        /// </exception>
        /// <exception cref="UnsupportedOptionsException">
        ///                          dictionary size or uncompressed size is too
        ///                          big for this implementation
        /// </exception>
        /// <exception cref="MemoryLimitException">
        ///                          memory usage limit was exceeded
        /// </exception>
        /// <exception cref="EOFException">
        ///                          file is truncated or perhaps not in
        ///                          the .lzma format at all
        /// </exception>
        /// <exception cref="IOException"> may be thrown by <code>in</code> </exception>
        public LZMAInputStream(Stream inputStream, int memoryLimit) : this(inputStream, memoryLimit, ArrayCache.DefaultCache)
        {
        }

        /// <summary>
        /// Creates a new .lzma file format decompressor with an optional
        /// memory usage limit.
        /// <para>
        /// This is identical to <code>LZMAInputStream(InputStream, int)</code>
        /// except that this also takes the <code>arrayCache</code> argument.
        /// 
        /// </para>
        /// </summary>
        /// <param name="inputStream">          input stream from which .lzma data is read;
        ///                          it might be a good idea to wrap it in
        ///                          <code>BufferedInputStream</code>, see the
        ///                          note at the top of this page
        /// </param>
        /// <param name="memoryLimit"> memory usage limit in kibibytes (KiB)
        ///                          or <code>-1</code> to impose no
        ///                          memory usage limit
        /// </param>
        /// <param name="arrayCache">  cache to be used for allocating large arrays
        /// </param>
        /// <exception cref="CorruptedInputException">
        ///                          file is corrupt or perhaps not in
        ///                          the .lzma format at all
        /// </exception>
        /// <exception cref="UnsupportedOptionsException">
        ///                          dictionary size or uncompressed size is too
        ///                          big for this implementation
        /// </exception>
        /// <exception cref="MemoryLimitException">
        ///                          memory usage limit was exceeded
        /// </exception>
        /// <exception cref="EOFException">
        ///                          file is truncated or perhaps not in
        ///                          the .lzma format at all
        /// </exception>
        /// <exception cref="IOException"> may be thrown by <code>in</code>
        /// 
        /// @since 1.7 </exception>
        public LZMAInputStream(Stream inputStream, int memoryLimit, ArrayCache arrayCache)
        {

            // Properties byte (lc, lp, and pb)
            byte propsByte = (byte)inputStream.ReadByte();

            // Dictionary size is an unsigned 32-bit little endian integer.
            int dictSize = 0;
            for (int i = 0; i < 4; ++i)
            {
                dictSize |= inputStream.ReadByte() << (8 * i);
            }

            // Uncompressed size is an unsigned 64-bit little endian integer.
            // The maximum 64-bit value is a special case (becomes -1 here)
            // which indicates that the end marker is used instead of knowing
            // the uncompressed size beforehand.
            long uncompSize = 0;
            for (int i = 0; i < 8; ++i)
            {
                uncompSize |= (long)inputStream.ReadByte() << (8 * i);
            }

            // Check the memory usage limit.
            int memoryNeeded = GetMemoryUsage(dictSize, propsByte);
            if (memoryLimit != -1 && memoryNeeded > memoryLimit)
            {
                throw new MemoryLimitException(memoryNeeded, memoryLimit);
            }

            Initialize(inputStream, uncompSize, propsByte, dictSize, null, arrayCache);
        }

        /// <summary>
        /// Creates a new input stream that decompresses raw LZMA data (no .lzma
        /// header) from <code>in</code>.
        /// <para>
        /// The caller needs to know if the "end of payload marker (EOPM)" alias
        /// "end of stream marker (EOS marker)" alias "end marker" is present.
        /// If the end marker isn't used, the caller must know the exact
        /// uncompressed size of the stream.
        /// </para>
        /// <para>
        /// The caller also needs to provide the LZMA properties byte that encodes
        /// the number of literal context bits (lc), literal position bits (lp),
        /// and position bits (pb).
        /// </para>
        /// <para>
        /// The dictionary size used when compressing is also needed. Specifying
        /// a too small dictionary size will prevent decompressing the stream.
        /// Specifying a too big dictionary is waste of memory but decompression
        /// will work.
        /// </para>
        /// <para>
        /// There is no need to specify a dictionary bigger than
        /// the uncompressed size of the data even if a bigger dictionary
        /// was used when compressing. If you know the uncompressed size
        /// of the data, this might allow saving some memory.
        /// 
        /// </para>
        /// </summary>
        /// <param name="inputStream">          input stream from which compressed
        ///                          data is read
        /// </param>
        /// <param name="uncompSize">  uncompressed size of the LZMA stream or -1
        ///                          if the end marker is used in the LZMA stream
        /// </param>
        /// <param name="propsByte">   LZMA properties byte that has the encoded
        ///                          values for literal context bits (lc), literal
        ///                          position bits (lp), and position bits (pb)
        /// </param>
        /// <param name="dictSize">    dictionary size as bytes, must be in the range
        ///                          [<code>0</code>, <code>DICT_SIZE_MAX</code>]
        /// </param>
        /// <exception cref="CorruptedInputException">
        ///                          if <code>propsByte</code> is invalid or
        ///                          the first input byte is not 0x00
        /// </exception>
        /// <exception cref="UnsupportedOptionsException">
        ///                          dictionary size or uncompressed size is too
        ///                          big for this implementation
        /// 
        ///  </exception>
        public LZMAInputStream(Stream inputStream, long uncompSize, byte propsByte, int dictSize)
        {
            Initialize(inputStream, uncompSize, propsByte, dictSize, null, ArrayCache.DefaultCache);
        }

        /// <summary>
        /// Creates a new input stream that decompresses raw LZMA data (no .lzma
        /// header) from <code>in</code> optionally with a preset dictionary.
        /// </summary>
        /// <param name="inputStream">          input stream from which LZMA-compressed
        ///                          data is read
        /// </param>
        /// <param name="uncompSize">  uncompressed size of the LZMA stream or -1
        ///                          if the end marker is used in the LZMA stream
        /// </param>
        /// <param name="propsByte">   LZMA properties byte that has the encoded
        ///                          values for literal context bits (lc), literal
        ///                          position bits (lp), and position bits (pb)
        /// </param>
        /// <param name="dictSize">    dictionary size as bytes, must be in the range
        ///                          [<code>0</code>, <code>DICT_SIZE_MAX</code>]
        /// </param>
        /// <param name="presetDict">  preset dictionary or <code>null</code>
        ///                          to use no preset dictionary
        /// </param>
        /// <exception cref="CorruptedInputException">
        ///                          if <code>propsByte</code> is invalid or
        ///                          the first input byte is not 0x00
        /// </exception>
        /// <exception cref="UnsupportedOptionsException">
        ///                          dictionary size or uncompressed size is too
        ///                          big for this implementation
        /// </exception>
        /// <exception cref="EOFException"> file is truncated or corrupt
        /// </exception>
        /// <exception cref="IOException"> may be thrown by <code>in</code> </exception>
        public LZMAInputStream(Stream inputStream, long uncompSize, byte propsByte, int dictSize, byte[] presetDict)
        {
            Initialize(inputStream, uncompSize, propsByte, dictSize, presetDict, ArrayCache.DefaultCache);
        }

        /// <summary>
        /// Creates a new input stream that decompresses raw LZMA data (no .lzma
        /// header) from <code>in</code> optionally with a preset dictionary.
        /// <para>
        /// This is identical to <code>LZMAInputStream(InputStream, long, byte, int,
        /// byte[])</code> except that this also takes the <code>arrayCache</code>
        /// argument.
        /// 
        /// </para>
        /// </summary>
        /// <param name="inputStream">          input stream from which LZMA-compressed
        ///                          data is read
        /// </param>
        /// <param name="uncompSize">  uncompressed size of the LZMA stream or -1
        ///                          if the end marker is used in the LZMA stream
        /// </param>
        /// <param name="propsByte">   LZMA properties byte that has the encoded
        ///                          values for literal context bits (lc), literal
        ///                          position bits (lp), and position bits (pb)
        /// </param>
        /// <param name="dictSize">    dictionary size as bytes, must be in the range
        ///                          [<code>0</code>, <code>DICT_SIZE_MAX</code>]
        /// </param>
        /// <param name="presetDict">  preset dictionary or <code>null</code>
        ///                          to use no preset dictionary
        /// </param>
        /// <param name="arrayCache">  cache to be used for allocating large arrays
        /// </param>
        /// <exception cref="CorruptedInputException">
        ///                          if <code>propsByte</code> is invalid or
        ///                          the first input byte is not 0x00
        /// </exception>
        /// <exception cref="UnsupportedOptionsException">
        ///                          dictionary size or uncompressed size is too
        ///                          big for this implementation
        /// </exception>
        /// <exception cref="EOFException"> file is truncated or corrupt
        /// </exception>
        /// <exception cref="IOException"> may be thrown by <code>in</code>
        /// 
        /// @since 1.7 </exception>
        public LZMAInputStream(Stream inputStream, long uncompSize, byte propsByte, int dictSize, byte[] presetDict, ArrayCache arrayCache)
        {
            Initialize(inputStream, uncompSize, propsByte, dictSize, presetDict, arrayCache);
        }

        /// <summary>
        /// Creates a new input stream that decompresses raw LZMA data (no .lzma
        /// header) from <code>in</code> optionally with a preset dictionary.
        /// </summary>
        /// <param name="inputStream">          input stream from which LZMA-compressed
        ///                          data is read
        /// </param>
        /// <param name="uncompSize">  uncompressed size of the LZMA stream or -1
        ///                          if the end marker is used in the LZMA stream
        /// </param>
        /// <param name="lc">          number of literal context bits, must be
        ///                          in the range [0, 8]
        /// </param>
        /// <param name="lp">          number of literal position bits, must be
        ///                          in the range [0, 4]
        /// </param>
        /// <param name="pb">          number position bits, must be
        ///                          in the range [0, 4]
        /// </param>
        /// <param name="dictSize">    dictionary size as bytes, must be in the range
        ///                          [<code>0</code>, <code>DICT_SIZE_MAX</code>]
        /// </param>
        /// <param name="presetDict">  preset dictionary or <code>null</code>
        ///                          to use no preset dictionary
        /// </param>
        /// <exception cref="CorruptedInputException">
        ///                          if the first input byte is not 0x00
        /// </exception>
        /// <exception cref="EOFException"> file is truncated or corrupt
        /// </exception>
        /// <exception cref="IOException"> may be thrown by <code>in</code> </exception>
        public LZMAInputStream(Stream inputStream, long uncompSize, int lc, int lp, int pb, int dictSize, byte[] presetDict)
        {
            Initialize(inputStream, uncompSize, lc, lp, pb, dictSize, presetDict, ArrayCache.DefaultCache);
        }

        /// <summary>
        /// Creates a new input stream that decompresses raw LZMA data (no .lzma
        /// header) from <code>in</code> optionally with a preset dictionary.
        /// <para>
        /// This is identical to <code>LZMAInputStream(InputStream, long, int, int,
        /// int, int, byte[])</code> except that this also takes the
        /// <code>arrayCache</code> argument.
        /// 
        /// </para>
        /// </summary>
        /// <param name="inputStream">          input stream from which LZMA-compressed
        ///                          data is read
        /// </param>
        /// <param name="uncompSize">  uncompressed size of the LZMA stream or -1
        ///                          if the end marker is used in the LZMA stream
        /// </param>
        /// <param name="lc">          number of literal context bits, must be
        ///                          in the range [0, 8]
        /// </param>
        /// <param name="lp">          number of literal position bits, must be
        ///                          in the range [0, 4]
        /// </param>
        /// <param name="pb">          number position bits, must be
        ///                          in the range [0, 4]
        /// </param>
        /// <param name="dictSize">    dictionary size as bytes, must be in the range
        ///                          [<code>0</code>, <code>DICT_SIZE_MAX</code>]
        /// </param>
        /// <param name="presetDict">  preset dictionary or <code>null</code>
        ///                          to use no preset dictionary
        /// </param>
        /// <param name="arrayCache">  cache to be used for allocating large arrays
        /// </param>
        /// <exception cref="CorruptedInputException">
        ///                          if the first input byte is not 0x00
        /// </exception>
        /// <exception cref="EOFException"> file is truncated or corrupt
        /// </exception>
        /// <exception cref="IOException"> may be thrown by <code>in</code>
        /// 
        /// @since 1.7 </exception>
        public LZMAInputStream(Stream inputStream, long uncompSize, int lc, int lp, int pb, int dictSize, byte[] presetDict, ArrayCache arrayCache)
        {
            Initialize(inputStream, uncompSize, lc, lp, pb, dictSize, presetDict, arrayCache);
        }

        private void Initialize(Stream inputStream, long uncompSize, byte propsByte, int dictSize, byte[] presetDict, ArrayCache arrayCache)
        {
            // Validate the uncompressed size since the other "initialize" throws
            // IllegalArgumentException if uncompSize < -1.
            if (uncompSize < -1)
            {
                throw new UnsupportedOptionsException("Uncompressed size is too big");
            }

            // Decode the properties byte. In contrast to LZMA2, there is no
            // limit of lc + lp <= 4.
            int props = propsByte & 0xFF;
            if (props > (4 * 5 + 4) * 9 + 8)
            {
                throw new CorruptedInputException("Invalid LZMA properties byte");
            }

            int pb = props / (9 * 5);
            props -= pb * 9 * 5;
            int lp = props / 9;
            int lc = props - lp * 9;

            // Validate the dictionary size since the other "initialize" throws
            // IllegalArgumentException if dictSize is not supported.
            if (dictSize < 0 || dictSize > DICT_SIZE_MAX)
            {
                throw new UnsupportedOptionsException("LZMA dictionary is too big for this implementation");
            }

            Initialize(inputStream, uncompSize, lc, lp, pb, dictSize, presetDict, arrayCache);
        }

        private void Initialize(Stream inputStream, long uncompSize, int lc, int lp, int pb, int dictSize, byte[] presetDict, ArrayCache arrayCache)
        {
            // getDictSize validates dictSize and gives a message in
            // the exception too, so skip validating dictSize here.
            if (uncompSize < -1 || lc < 0 || lc > 8 || lp < 0 || lp > 4 || pb < 0 || pb > 4)
            {
                throw new System.ArgumentException();
            }

            this.inputStream = inputStream;
            this.arrayCache = arrayCache;

            // If uncompressed size is known, use it to avoid wasting memory for
            // a uselessly large dictionary buffer.
            dictSize = GetDictSize(dictSize);
            if (uncompSize >= 0 && dictSize > uncompSize)
            {
                dictSize = GetDictSize((int)uncompSize);
            }

            lz = new LZDecoder(GetDictSize(dictSize), presetDict, arrayCache);
            rc = new RangeDecoderFromStream(inputStream);
            lzma = new LZMADecoder(lz, rc, lc, lp, pb);

            remainingSize = uncompSize;
        }

        /// <summary>
        /// Enables relaxed end-of-stream condition when uncompressed size is known.
        /// This is useful if uncompressed size is known but it is unknown if
        /// the end of stream (EOS) marker is present. After calling this function,
        /// both are allowed.
        /// <para>
        /// Note that this doesn't actually check if the EOS marker is present.
        /// This introduces a few minor downsides:
        /// <ul>
        ///   <li>Some (not all!) streams that would have more data than
        ///   the specified uncompressed size, for example due to data corruption,
        ///   will be accepted as valid.</li>
        ///   <li>After <code>read</code> has returned <code>-1</code> the
        ///   input position might not be at the end of the stream (too little
        ///   input may have been read).</li>
        /// </ul>
        /// </para>
        /// <para>
        /// This should be called after the constructor before reading any data
        /// from the stream. This is a separate function because adding even more
        /// constructors to this class didn't look like a good alternative.
        /// </para>
        /// </summary>
        public virtual void EnableRelaxedEndCondition()
        {
            relaxedEndCondition = true;
        }

        /// <summary>
        /// Decompresses the next byte from this input stream.
        /// <para>
        /// Reading lots of data with <code>read()</code> from this input stream
        /// may be inefficient. Wrap it in <code>java.io.BufferedInputStream</code>
        /// if you need to read lots of data one byte at a time.
        /// 
        /// </para>
        /// </summary>
        /// <returns>      the next decompressed byte, or <code>-1</code>
        ///              to indicate the end of the compressed stream
        /// </returns>
        /// <exception cref="CorruptedInputException">
        /// </exception>
        /// <exception cref="EOFException">
        ///                          compressed input is truncated or corrupt
        /// </exception>
        /// <exception cref="IOException"> may be thrown by <code>in</code> </exception>
        public virtual int Read()
        {
            return Read(tempBuf, 0, 1) == -1 ? -1 : (tempBuf[0] & 0xFF);
        }

        /// <summary>
        /// Decompresses into an array of bytes.
        /// <para>
        /// If <code>len</code> is zero, no bytes are read and <code>0</code>
        /// is returned. Otherwise this will block until <code>len</code>
        /// bytes have been decompressed, the end of the LZMA stream is reached,
        /// or an exception is thrown.
        /// 
        /// </para>
        /// </summary>
        /// <param name="buf">         target buffer for uncompressed data </param>
        /// <param name="off">         start offset in <code>buf</code> </param>
        /// <param name="len">         maximum number of uncompressed bytes to read
        /// </param>
        /// <returns>      number of bytes read, or <code>-1</code> to indicate
        ///              the end of the compressed stream
        /// </returns>
        /// <exception cref="CorruptedInputException">
        /// </exception>
        /// <exception cref="EOFException"> compressed input is truncated or corrupt
        /// </exception>
        /// <exception cref="IOException"> may be thrown by <code>in</code> </exception>
        public override int Read(byte[] buf, int off, int len)
        {
            if (off < 0 || len < 0 || off + len < 0 || off + len > buf.Length)
            {
                throw new System.IndexOutOfRangeException();
            }

            if (len == 0)
            {
                return 0;
            }

            if (inputStream == null)
            {
                throw new IOException("Stream closed");
            }

            if (exception != null)
                throw exception;

            if (endReached)
                return -1;

            try
            {
                int size = 0;

                while (len > 0)
                {
                    // If uncompressed size is known and thus no end marker will
                    // be present, set the limit so that the uncompressed size
                    // won't be exceeded.
                    int copySizeMax = len;
                    if (remainingSize >= 0 && remainingSize < len)
                        copySizeMax = (int)remainingSize;

                    lz.Limit = copySizeMax;

                    // Decode into the dictionary buffer.
                    try
                    {
                        lzma.Decode();
                    }
                    catch (CorruptedInputException e)
                    {
                        // The end marker is encoded with a LZMA symbol that
                        // indicates maximum match distance. This is larger
                        // than any supported dictionary and thus causes
                        // CorruptedInputException from LZDecoder.repeat.
                        if (remainingSize != -1 || !lzma.EndMarkerDetected())
                            throw;

                        endReached = true;

                        // The exception makes lzma.decode() miss the last range
                        // decoder normalization, so do it here. This might
                        // cause an IOException if it needs to read a byte
                        // from the input stream.
                        rc.Normalize();
                    }

                    // Copy from the dictionary to buf.
                    int copiedSize = lz.Flush(buf, off);
                    off += copiedSize;
                    len -= copiedSize;
                    size += copiedSize;

                    if (remainingSize >= 0)
                    {
                        // Update the number of bytes left to be decompressed.
                        remainingSize -= copiedSize;
                        Debug.Assert(remainingSize >= 0);

                        if (remainingSize == 0)
                            endReached = true;
                    }

                    if (endReached)
                    {
                        // Checking these helps a lot when catching corrupt
                        // or truncated .lzma files. LZMA Utils doesn't do
                        // the second check and thus it accepts many invalid
                        // files that this implementation and XZ Utils don't.
                        if (lz.HasPending() || (!relaxedEndCondition
                                                && !rc.Finished))
                            throw new CorruptedInputException();

                        PutArraysToCache();
                        return size == 0 ? -1 : size;
                    }
                }

                return size;

            }
            catch (IOException e)
            {
                exception = e;
                throw;
            }
        }
        private void PutArraysToCache()
        {
            if (lz != null)
            {
                lz.PutArraysToCache(arrayCache);
                lz = null;
            }
        }

        /// <summary>
        /// Closes the stream and calls <code>inputStream.Close()</code>.
        /// If the stream was already closed, this does nothing.
        /// </summary>
        public void Close()
        {
            if (inputStream != null)
            {
                PutArraysToCache();

                try
                {
                    inputStream.Close();
                }
                finally
                {
                    inputStream = null;
                }
            }
        }

        public override void Flush()
        {
            throw new NotSupportedException();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return inputStream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            throw new NotSupportedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotSupportedException();
        }
    }
}
