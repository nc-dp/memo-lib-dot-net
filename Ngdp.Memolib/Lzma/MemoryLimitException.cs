﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma
{
    /// <summary>
    /// Thrown when the memory usage limit given to the XZ decompressor
    /// would be exceeded.
    /// <para>
    /// The amount of memory required and the memory usage limit are
    /// included in the error detail message in human readable format.
    /// </para>
    /// </summary>
    public class MemoryLimitException : IOException
    {
        private const long serialVersionUID = 3L;

        private readonly int memoryNeeded;
        private readonly int memoryLimit;

        /// <summary>
        /// Creates a new MemoryLimitException.
        /// <para>
        /// The amount of memory needed and the memory usage limit are
        /// included in the error detail message.
        /// 
        /// </para>
        /// </summary>
        /// <param name="memoryNeeded">    amount of memory needed as kibibytes (KiB) </param>
        /// <param name="memoryLimit">     specified memory usage limit as kibibytes (KiB) </param>
        public MemoryLimitException(int memoryNeeded, int memoryLimit) : base($"{memoryNeeded} KiB of memory would be needed; limit was {memoryLimit} KiB")
        {

            this.memoryNeeded = memoryNeeded;
            this.memoryLimit = memoryLimit;
        }

        /// <summary>
        /// Gets how much memory is required to decompress the data.
        /// </summary>
        /// <returns>      amount of memory needed as kibibytes (KiB) </returns>
        public virtual int MemoryNeeded
        {
            get
            {
                return memoryNeeded;
            }
        }

        /// <summary>
        /// Gets what the memory usage limit was at the time the exception
        /// was created.
        /// </summary>
        /// <returns>      memory usage limit as kibibytes (KiB) </returns>
        public virtual int MemoryLimit
        {
            get
            {
                return memoryLimit;
            }
        }
    }
}
