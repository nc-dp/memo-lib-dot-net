﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma.Lzma
{
    internal sealed class Optimum
    {
        private static readonly int INFINITY_PRICE = 1 << 30;

        internal readonly State state = new State();
        internal readonly int[] reps = new int[LZMACoder.REPS];

        /// <summary>
        /// Cumulative price of arriving to this byte.
        /// </summary>
        internal int price;

        internal int optPrev;
        internal int backPrev;
        internal bool prev1IsLiteral;

        internal bool hasPrev2;
        internal int optPrev2;
        internal int backPrev2;

        /// <summary>
        /// Resets the price.
        /// </summary>
        internal void Reset()
        {
            price = INFINITY_PRICE;
        }

        /// <summary>
        /// Sets to indicate one LZMA symbol (literal, rep, or match).
        /// </summary>
        internal void Set1(int newPrice, int optCur, int back)
        {
            price = newPrice;
            optPrev = optCur;
            backPrev = back;
            prev1IsLiteral = false;
        }

        /// <summary>
        /// Sets to indicate two LZMA symbols of which the first one is a literal.
        /// </summary>
        internal void Set2(int newPrice, int optCur, int back)
        {
            price = newPrice;
            optPrev = optCur + 1;
            backPrev = back;
            prev1IsLiteral = true;
            hasPrev2 = false;
        }

        /// <summary>
        /// Sets to indicate three LZMA symbols of which the second one
        /// is a literal.
        /// </summary>
        internal void Set3(int newPrice, int optCur, int back2, int len2, int back)
        {
            price = newPrice;
            optPrev = optCur + len2 + 1;
            backPrev = back;
            prev1IsLiteral = true;
            hasPrev2 = true;
            optPrev2 = optCur;
            backPrev2 = back2;
        }
    }
}
