﻿using System;
using System.Collections.Generic;
using System.Text;
using Dk.Digst.Digital.Post.Memolib.Lzma.RangeCoder;

namespace Dk.Digst.Digital.Post.Memolib.Lzma.Lzma
{
    public abstract class LZMACoder
    {
        internal static readonly int POS_STATES_MAX = 1 << 4;

        internal const int MATCH_LEN_MIN = 2;
        internal static readonly int MATCH_LEN_MAX = MATCH_LEN_MIN + LengthCoder.LOW_SYMBOLS + LengthCoder.MID_SYMBOLS + LengthCoder.HIGH_SYMBOLS - 1;

        internal const int DIST_STATES = 4;
        internal static readonly int DIST_SLOTS = 1 << 6;
        internal const int DIST_MODEL_START = 4;
        internal const int DIST_MODEL_END = 14;
        internal static readonly int FULL_DISTANCES = 1 << (DIST_MODEL_END / 2);

        internal const int ALIGN_BITS = 4;
        internal static readonly int ALIGN_SIZE = 1 << ALIGN_BITS;
        internal static readonly int ALIGN_MASK = ALIGN_SIZE - 1;

        internal const int REPS = 4;

        internal readonly int posMask;

        internal readonly int[] reps = new int[REPS];
        internal readonly State state = new State();

        internal readonly short[][] isMatch = RectangularArrays.RectangularShortArray(State.STATES, POS_STATES_MAX);
        internal readonly short[] isRep = new short[State.STATES];
        internal readonly short[] isRep0 = new short[State.STATES];
        internal readonly short[] isRep1 = new short[State.STATES];
        internal readonly short[] isRep2 = new short[State.STATES];
        internal readonly short[][] isRep0Long = RectangularArrays.RectangularShortArray(State.STATES, POS_STATES_MAX);
        internal readonly short[][] distSlots = RectangularArrays.RectangularShortArray(DIST_STATES, DIST_SLOTS);
        internal readonly short[][] distSpecial = new short[][]
        {
            new short[2],
            new short[2],
            new short[4],
            new short[4],
            new short[8],
            new short[8],
            new short[16],
            new short[16],
            new short[32],
            new short[32]
        };
        internal readonly short[] distAlign = new short[ALIGN_SIZE];

        internal static int GetDistState(int len)
        {
            return len < DIST_STATES + MATCH_LEN_MIN ? len - MATCH_LEN_MIN : DIST_STATES - 1;
        }

        internal LZMACoder(int pb)
        {
            posMask = (1 << pb) - 1;
        }

        public virtual void Reset()
        {
            reps[0] = 0;
            reps[1] = 0;
            reps[2] = 0;
            reps[3] = 0;
            state.Reset();

            for (int i = 0; i < isMatch.Length; ++i)
            {
                RangeCoder.RangeCoder.InitProbs(isMatch[i]);
            }

            RangeCoder.RangeCoder.InitProbs(isRep);
            RangeCoder.RangeCoder.InitProbs(isRep0);
            RangeCoder.RangeCoder.InitProbs(isRep1);
            RangeCoder.RangeCoder.InitProbs(isRep2);

            for (int i = 0; i < isRep0Long.Length; ++i)
            {
                RangeCoder.RangeCoder.InitProbs(isRep0Long[i]);
            }

            for (int i = 0; i < distSlots.Length; ++i)
            {
                RangeCoder.RangeCoder.InitProbs(distSlots[i]);
            }

            for (int i = 0; i < distSpecial.Length; ++i)
            {
                RangeCoder.RangeCoder.InitProbs(distSpecial[i]);
            }

            RangeCoder.RangeCoder.InitProbs(distAlign);
        }


        internal abstract class LiteralCoder
        {
            protected readonly LZMACoder outerInstance;

            internal readonly int lc;
            internal readonly int literalPosMask;

            internal LiteralCoder(LZMACoder outerInstance, int lc, int lp)
            {
                this.outerInstance = outerInstance;
                this.lc = lc;
                this.literalPosMask = (1 << lp) - 1;
            }

            internal int GetSubcoderIndex(int prevByte, int pos)
            {
                int low = prevByte >> (8 - lc);
                int high = (pos & literalPosMask) << lc;
                return low + high;
            }


            internal abstract class LiteralSubcoder
            {
                internal readonly short[] probs = new short[0x300];

                internal virtual void Reset()
                {
                    RangeCoder.RangeCoder.InitProbs(probs);
                }
            }
        }


        internal abstract class LengthCoder
        {
            internal static readonly int LOW_SYMBOLS = 1 << 3;
            internal static readonly int MID_SYMBOLS = 1 << 3;
            internal static readonly int HIGH_SYMBOLS = 1 << 8;

            internal readonly short[] choice = new short[2];
            internal readonly short[][] low = RectangularArrays.RectangularShortArray(POS_STATES_MAX, LOW_SYMBOLS);
            internal readonly short[][] mid = RectangularArrays.RectangularShortArray(POS_STATES_MAX, MID_SYMBOLS);
            internal readonly short[] high = new short[HIGH_SYMBOLS];

            internal virtual void Reset()
            {
                RangeCoder.RangeCoder.InitProbs(choice);

                for (int i = 0; i < low.Length; ++i)
                {
                    RangeCoder.RangeCoder.InitProbs(low[i]);
                }

                for (int i = 0; i < low.Length; ++i)
                {
                    RangeCoder.RangeCoder.InitProbs(mid[i]);
                }

                RangeCoder.RangeCoder.InitProbs(high);
            }
        }
    }
}
