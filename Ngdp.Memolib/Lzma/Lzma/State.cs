﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma.Lzma
{
    internal sealed class State
    {
        internal const int STATES = 12;

        private const int LIT_STATES = 7;

        private const int LIT_LIT = 0;
        private const int MATCH_LIT_LIT = 1;
        private const int REP_LIT_LIT = 2;
        private const int SHORTREP_LIT_LIT = 3;
        private const int MATCH_LIT = 4;
        private const int REP_LIT = 5;
        private const int SHORTREP_LIT = 6;
        private const int LIT_MATCH = 7;
        private const int LIT_LONGREP = 8;
        private const int LIT_SHORTREP = 9;
        private const int NONLIT_MATCH = 10;
        private const int NONLIT_REP = 11;

        private int state;

        internal State()
        {
        }

        internal State(State other)
        {
            state = other.state;
        }

        internal void Reset()
        {
            state = LIT_LIT;
        }

        internal int Get()
        {
            return state;
        }

        internal void Set(State other)
        {
            state = other.state;
        }

        internal void UpdateLiteral()
        {
            if (state <= SHORTREP_LIT_LIT)
            {
                state = LIT_LIT;
            }
            else if (state <= LIT_SHORTREP)
            {
                state -= 3;
            }
            else
            {
                state -= 6;
            }
        }

        internal void UpdateMatch()
        {
            state = state < LIT_STATES ? LIT_MATCH : NONLIT_MATCH;
        }

        internal void UpdateLongRep()
        {
            state = state < LIT_STATES ? LIT_LONGREP : NONLIT_REP;
        }

        internal void UpdateShortRep()
        {
            state = state < LIT_STATES ? LIT_SHORTREP : NONLIT_REP;
        }

        internal bool Literal
        {
            get
            {
                return state < LIT_STATES;
            }
        }
    }
}
