﻿using Dk.Digst.Digital.Post.Memolib.Lzma.Lz;
using Dk.Digst.Digital.Post.Memolib.Lzma.RangeCoder;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma.Lzma
{
    public abstract class LZMAEncoder : LZMACoder
    {
        public const int MODE_FAST = 1;
        public const int MODE_NORMAL = 2;

        private static readonly int DIST_PRICE_UPDATE_INTERVAL = FULL_DISTANCES;
        private static readonly int ALIGN_PRICE_UPDATE_INTERVAL = ALIGN_SIZE;

        private readonly RangeEncoder rc;
        internal readonly LZEncoder lz;
        internal readonly LiteralEncoder literalEncoder;
        internal readonly LengthEncoder matchLenEncoder;
        internal readonly LengthEncoder repLenEncoder;
        internal readonly int niceLen;

        private int distPriceCount = 0;
        private int alignPriceCount = 0;

        private readonly int distSlotPricesSize;
        private readonly int[][] distSlotPrices;
        private readonly int[][] fullDistPrices = RectangularArrays.RectangularIntArray(DIST_STATES, FULL_DISTANCES);
        private readonly int[] alignPrices = new int[ALIGN_SIZE];

        internal int back = 0;
        internal int readAhead = -1;
        private int uncompressedSize = 0;

        public static int GetMemoryUsage(int mode, int dictSize, int extraSizeBefore, int mf)
        {
            int m = 80;

            switch (mode)
            {
                case MODE_FAST:
                    m += LZMAEncoderFast.GetMemoryUsage(dictSize, extraSizeBefore, mf);
                    break;

                case MODE_NORMAL:
                    m += LZMAEncoderNormal.GetMemoryUsage(dictSize, extraSizeBefore, mf);
                    break;

                default:
                    throw new System.ArgumentException();
            }

            return m;
        }

        public static LZMAEncoder GetInstance(RangeEncoder rc, int lc, int lp, int pb, int mode, int dictSize, int extraSizeBefore, int niceLen, int mf, int depthLimit, ArrayCache arrayCache)
        {
            switch (mode)
            {
                case MODE_FAST:
                    return new LZMAEncoderFast(rc, lc, lp, pb, dictSize, extraSizeBefore, niceLen, mf, depthLimit, arrayCache);

                case MODE_NORMAL:
                    return new LZMAEncoderNormal(rc, lc, lp, pb, dictSize, extraSizeBefore, niceLen, mf, depthLimit, arrayCache);
            }

            throw new System.ArgumentException();
        }

        public virtual void PutArraysToCache(ArrayCache arrayCache)
        {
            lz.PutArraysToCache(arrayCache);
        }

        /// <summary>
        /// Gets an integer [0, 63] matching the highest two bits of an integer.
        /// This is like bit scan reverse (BSR) on x86 except that this also
        /// cares about the second highest bit.
        /// </summary>
        public static int GetDistSlot(int dist)
        {
            if (dist <= DIST_MODEL_START && dist >= 0)
            {
                return dist;
            }

            int n = dist;
            int i = 31;

            if ((n & 0xFFFF0000) == 0)
            {
                n <<= 16;
                i = 15;
            }

            if ((n & 0xFF000000) == 0)
            {
                n <<= 8;
                i -= 8;
            }

            if ((n & 0xF0000000) == 0)
            {
                n <<= 4;
                i -= 4;
            }

            if ((n & 0xC0000000) == 0)
            {
                n <<= 2;
                i -= 2;
            }

            if ((n & 0x80000000) == 0)
            {
                --i;
            }
            uint shiftedDist = (uint)dist >> (i - 1);
            return ((i << 1) + (((int)shiftedDist) & 1));
        }

        /// <summary>
        /// Gets the next LZMA symbol.
        /// <para>
        /// There are three types of symbols: literal (a single byte),
        /// repeated match, and normal match. The symbol is indicated
        /// by the return value and by the variable <code>back</code>.
        /// </para>
        /// <para>
        /// Literal: <code>back == -1</code> and return value is <code>1</code>.
        /// The literal itself needs to be read from <code>lz</code> separately.
        /// </para>
        /// <para>
        /// Repeated match: <code>back</code> is in the range [0, 3] and
        /// the return value is the length of the repeated match.
        /// </para>
        /// <para>
        /// Normal match: <code>back - REPS<code> (<code>back - 4</code>)
        /// is the distance of the match and the return value is the length
        /// of the match.
        /// </para>
        /// </summary>
        internal abstract int NextSymbol { get; }

        internal LZMAEncoder(RangeEncoder rc, LZEncoder lz, int lc, int lp, int pb, int dictSize, int niceLen) : base(pb)
        {
            this.rc = rc;
            this.lz = lz;
            this.niceLen = niceLen;

            literalEncoder = new LiteralEncoder(this, lc, lp);
            matchLenEncoder = new LengthEncoder(this, pb, niceLen);
            repLenEncoder = new LengthEncoder(this, pb, niceLen);

            distSlotPricesSize = GetDistSlot(dictSize - 1) + 1;
            distSlotPrices = RectangularArrays.RectangularIntArray(DIST_STATES, distSlotPricesSize);

            Reset();
        }

        public virtual LZEncoder LZEncoder
        {
            get
            {
                return lz;
            }
        }

        public override void Reset()
        {
            base.Reset();
            literalEncoder.Reset();
            matchLenEncoder.Reset();
            repLenEncoder.Reset();
            distPriceCount = 0;
            alignPriceCount = 0;

            uncompressedSize += readAhead + 1;
            readAhead = -1;
        }

        public virtual int UncompressedSize
        {
            get
            {
                return uncompressedSize;
            }
        }

        public virtual void ResetUncompressedSize()
        {
            uncompressedSize = 0;
        }

        /// <summary>
        /// Compress for LZMA1
        /// </summary>
        public void EncodeForLZMA1()
        {
            if (!lz.IsStarted() && !EncodeInit())
                return;

            while (EncodeSymbol()) { }
        }

        public void EncodeLZMA1EndMarker()
        {
            // End of stream marker is encoded as a match with the maximum
            // possible distance. The length is ignored by the decoder,
            // but the minimum length has been used by the LZMA SDK.
            //
            // Distance is a 32-bit unsigned integer in LZMA.
            // With Java's signed int, UINT32_MAX becomes -1.
            int posState = (lz.GetPos() - readAhead) & posMask;
            rc.EncodeBit(isMatch[state.Get()], posState, 1);
            rc.EncodeBit(isRep, state.Get(), 0);
            EncodeMatch(-1, MATCH_LEN_MIN, posState);
        }

        private bool EncodeInit()
        {
            Debug.Assert(readAhead == -1);
            if (!lz.HasEnoughData(0))
                return false;

            // The first symbol must be a literal unless using
            // a preset dictionary. This code isn't run if using
            // a preset dictionary.
            Skip(1);
            rc.EncodeBit(isMatch[state.Get()], 0, 0);
            literalEncoder.EncodeInit();

            --readAhead;
            Debug.Assert(readAhead == -1);

            ++uncompressedSize;
            Debug.Assert(uncompressedSize == 1);

            return true;
        }

        private bool EncodeSymbol()
        {
            if (!lz.HasEnoughData(readAhead + 1))
                return false;

            int len = NextSymbol;

            Debug.Assert(readAhead >= 0);
            int posState = (lz.GetPos() - readAhead) & posMask;

            if (back == -1)
            {
                // Literal i.e. eight-bit byte
                Debug.Assert(len == 1);
                rc.EncodeBit(isMatch[state.Get()], posState, 0);
                literalEncoder.Encode();
            }
            else
            {
                // Some type of match
                rc.EncodeBit(isMatch[state.Get()], posState, 1);
                if (back < REPS)
                {
                    // Repeated match i.e. the same distance
                    // has been used earlier.
                    Debug.Assert(lz.GetMatchLen(-readAhead, reps[back], len) == len);
                    rc.EncodeBit(isRep, state.Get(), 1);
                    EncodeRepMatch(back, len, posState);
                }
                else
                {
                    // Normal match
                    Debug.Assert(lz.GetMatchLen(-readAhead, back - REPS, len) == len);
                    rc.EncodeBit(isRep, state.Get(), 0);
                    EncodeMatch(back - REPS, len, posState);
                }
            }

            readAhead -= len;
            uncompressedSize += len;

            return true;
        }

        private void EncodeMatch(int dist, int len, int posState)
        {
            state.UpdateMatch();
            matchLenEncoder.Encode(len, posState);

            int distSlot = GetDistSlot(dist);
            rc.EncodeBitTree(distSlots[GetDistState(len)], distSlot);

            if (distSlot >= DIST_MODEL_START)
            {
                int footerBits = (int)((uint)distSlot >> 1) - 1;
                int _base = (2 | ((int)distSlot & 1)) << footerBits;
                int distReduced = dist - _base;

                if (distSlot < DIST_MODEL_END)
                {
                    rc.EncodeReverseBitTree(distSpecial[distSlot - DIST_MODEL_START], distReduced);
                }
                else
                {
                    rc.EncodeDirectBits((int)((uint)distReduced >> ALIGN_BITS), footerBits - ALIGN_BITS);
                    rc.EncodeReverseBitTree(distAlign, distReduced & ALIGN_MASK);
                    --alignPriceCount;
                }
            }

            reps[3] = reps[2];
            reps[2] = reps[1];
            reps[1] = reps[0];
            reps[0] = dist;

            --distPriceCount;
        }

        private void EncodeRepMatch(int rep, int len, int posState)
        {
            if (rep == 0)
            {
                rc.EncodeBit(isRep0, state.Get(), 0);
                rc.EncodeBit(isRep0Long[state.Get()], posState, len == 1 ? 0 : 1);
            }
            else
            {
                int dist = reps[rep];
                rc.EncodeBit(isRep0, state.Get(), 1);

                if (rep == 1)
                {
                    rc.EncodeBit(isRep1, state.Get(), 0);
                }
                else
                {
                    rc.EncodeBit(isRep1, state.Get(), 1);
                    rc.EncodeBit(isRep2, state.Get(), rep - 2);

                    if (rep == 3)
                        reps[3] = reps[2];

                    reps[2] = reps[1];
                }

                reps[1] = reps[0];
                reps[0] = dist;
            }

            if (len == 1)
            {
                state.UpdateShortRep();
            }
            else
            {
                repLenEncoder.Encode(len, posState);
                state.UpdateLongRep();
            }
        }

        internal Matches GetMatches()
        {
            ++readAhead;
            Matches matches = lz.GetMatches();
            Debug.Assert(lz.VerifyMatches(matches));
            return matches;
        }

        internal void Skip(int len)
        {
            readAhead += len;
            lz.Skip(len);
        }

        internal int GetAnyMatchPrice(State state, int posState)
        {
            return RangeEncoder.GetBitPrice(isMatch[state.Get()][posState], 1);
        }

        internal int GetNormalMatchPrice(int anyMatchPrice, State state)
        {
            return anyMatchPrice
                   + RangeEncoder.GetBitPrice(isRep[state.Get()], 0);
        }

        internal int GetAnyRepPrice(int anyMatchPrice, State state)
        {
            return anyMatchPrice
                   + RangeEncoder.GetBitPrice(isRep[state.Get()], 1);
        }

        internal int GetShortRepPrice(int anyRepPrice, State state, int posState)
        {
            return anyRepPrice
                   + RangeEncoder.GetBitPrice(isRep0[state.Get()], 0)
                   + RangeEncoder.GetBitPrice(isRep0Long[state.Get()][posState], 0);
        }

        internal int GetLongRepPrice(int anyRepPrice, int rep, State state, int posState)
        {
            int price = anyRepPrice;

            if (rep == 0)
            {
                price += RangeEncoder.GetBitPrice(isRep0[state.Get()], 0)
                         + RangeEncoder.GetBitPrice(
                           isRep0Long[state.Get()][posState], 1);
            }
            else
            {
                price += RangeEncoder.GetBitPrice(isRep0[state.Get()], 1);

                if (rep == 1)
                    price += RangeEncoder.GetBitPrice(isRep1[state.Get()], 0);
                else
                    price += RangeEncoder.GetBitPrice(isRep1[state.Get()], 1)
                             + RangeEncoder.GetBitPrice(isRep2[state.Get()],
                                                        rep - 2);
            }

            return price;
        }

        internal int GetLongRepAndLenPrice(int rep, int len, State state, int posState)
        {
            int anyMatchPrice = GetAnyMatchPrice(state, posState);
            int anyRepPrice = GetAnyRepPrice(anyMatchPrice, state);
            int longRepPrice = GetLongRepPrice(anyRepPrice, rep, state, posState);
            return longRepPrice + repLenEncoder.GetPrice(len, posState);
        }

        internal int GetMatchAndLenPrice(int normalMatchPrice, int dist, int len, int posState)
        {
            int price = normalMatchPrice
                        + matchLenEncoder.GetPrice(len, posState);
            int distState = GetDistState(len);

            if (dist < FULL_DISTANCES)
            {
                price += fullDistPrices[distState][dist];
            }
            else
            {
                // Note that distSlotPrices includes also
                // the price of direct bits.
                int distSlot = GetDistSlot(dist);
                price += distSlotPrices[distState][distSlot]
                         + alignPrices[dist & ALIGN_MASK];
            }

            return price;
        }

        private void UpdateDistPrices()
        {
            distPriceCount = DIST_PRICE_UPDATE_INTERVAL;

            for (int distState = 0; distState < DIST_STATES; ++distState)
            {
                for (int distSlot = 0; distSlot < distSlotPricesSize; ++distSlot)
                    distSlotPrices[distState][distSlot] = RangeEncoder.GetBitTreePrice(distSlots[distState], distSlot);

                for (int distSlot = DIST_MODEL_END; distSlot < distSlotPricesSize; ++distSlot)
                {
                    int count = (int)((uint)distSlot >> 1) - 1 - ALIGN_BITS;
                    distSlotPrices[distState][distSlot] += RangeEncoder.GetDirectBitsPrice(count);
                }

                for (int d = 0; d < DIST_MODEL_START; ++d)
                    fullDistPrices[distState][d] = distSlotPrices[distState][d];
            }

            int dist = DIST_MODEL_START;
            for (int distSlot = DIST_MODEL_START; distSlot < DIST_MODEL_END; ++distSlot)
            {
                int footerBits = (int)((uint)distSlot >> 1) - 1;
                int _base = (2 | (distSlot & 1)) << footerBits;

                int limit = distSpecial[distSlot - DIST_MODEL_START].Length;
                for (int i = 0; i < limit; ++i)
                {
                    int distReduced = dist - _base;
                    int price = RangeEncoder.GetReverseBitTreePrice(distSpecial[distSlot - DIST_MODEL_START], distReduced);

                    for (int distState = 0; distState < DIST_STATES; ++distState)
                        fullDistPrices[distState][dist] = distSlotPrices[distState][distSlot] + price;

                    ++dist;
                }
            }

            Debug.Assert(dist == FULL_DISTANCES);
        }

        private void UpdateAlignPrices()
        {
            alignPriceCount = ALIGN_PRICE_UPDATE_INTERVAL;

            for (int i = 0; i < ALIGN_SIZE; ++i)
                alignPrices[i] = RangeEncoder.GetReverseBitTreePrice(distAlign, i);
        }

        /// <summary>
        /// Updates the lookup tables used for calculating match distance
        /// and length prices. The updating is skipped for performance reasons
        /// if the tables haven't changed much since the previous update.
        /// </summary>
        internal void UpdatePrices()
        {
            if (distPriceCount <= 0)
                UpdateDistPrices();

            if (alignPriceCount <= 0)
                UpdateAlignPrices();

            matchLenEncoder.UpdatePrices();
            repLenEncoder.UpdatePrices();
        }


        internal class LiteralEncoder : LiteralCoder
        {
            private readonly LiteralSubencoder[] subencoders;

            internal LiteralEncoder(LZMAEncoder encoder, int lc, int lp)
                    : base(encoder, lc, lp)
            {
                subencoders = new LiteralSubencoder[1 << (lc + lp)];
                for (int i = 0; i < subencoders.Length; ++i)
                    subencoders[i] = new LiteralSubencoder(encoder);
            }

            internal void Reset()
            {
                for (int i = 0; i < subencoders.Length; ++i)
                    subencoders[i].Reset();
            }

            internal void EncodeInit()
            {
                // When encoding the first byte of the stream, there is
                // no previous byte in the dictionary so the encode function
                // wouldn't work.
                Debug.Assert(((LZMAEncoder)outerInstance).readAhead >= 0);
                subencoders[0].Encode();
            }

            internal void Encode()
            {
                Debug.Assert(((LZMAEncoder)outerInstance).readAhead >= 0);
                int i = GetSubcoderIndex(((LZMAEncoder)outerInstance).lz.GetByte(1 + ((LZMAEncoder)outerInstance).readAhead),
                                         ((LZMAEncoder)outerInstance).lz.GetPos() - ((LZMAEncoder)outerInstance).readAhead);
                subencoders[i].Encode();
            }

            internal int GetPrice(int curByte, int matchByte,
                         int prevByte, int pos, State state)
            {
                var p = pos & (((LZMAEncoder)outerInstance).posMask);
                int price = RangeEncoder.GetBitPrice(
                        ((LZMAEncoder)outerInstance).isMatch[state.Get()][p], 0);

                int i = GetSubcoderIndex(prevByte, pos);
                price += state.Literal
                       ? subencoders[i].GetNormalPrice(curByte)
                       : subencoders[i].GetMatchedPrice(curByte, matchByte);

                return price;
            }

            internal class LiteralSubencoder : LiteralSubcoder
            {
                protected readonly LZMAEncoder encoder;
                public LiteralSubencoder(LZMAEncoder encoder)
                {
                    this.encoder = encoder;
                }
                internal void Encode()
                {
                    int symbol = encoder.lz.GetByte(encoder.readAhead) | 0x100;

                    if (encoder.state.Literal)
                    {
                        int subencoderIndex;
                        int bit;

                        do
                        {
                            subencoderIndex = (int)((uint)symbol >> 8);
                            bit = (int)((uint)symbol >> 7) & 1;
                            encoder.rc.EncodeBit(probs, subencoderIndex, bit);
                            symbol <<= 1;
                        } while (symbol < 0x10000);

                    }
                    else
                    {
                        int matchByte = encoder.lz.GetByte(encoder.reps[0] + 1 + encoder.readAhead);
                        int offset = 0x100;
                        int subencoderIndex;
                        int matchBit;
                        int bit;

                        do
                        {
                            matchByte <<= 1;
                            matchBit = matchByte & offset;
                            subencoderIndex = (offset + matchBit + (int)((uint)symbol >> 8));
                            bit = (int)((uint)symbol >> 7) & 1;
                            encoder.rc.EncodeBit(probs, subencoderIndex, bit);
                            symbol <<= 1;
                            offset &= ~(matchByte ^ symbol);
                        } while (symbol < 0x10000);
                    }

                    encoder.state.UpdateLiteral();
                }

                internal int GetNormalPrice(int symbol)
                {
                    int price = 0;
                    int subencoderIndex;
                    int bit;

                    symbol |= 0x100;

                    do
                    {
                        subencoderIndex = (int)((uint)symbol >> 8);
                        bit = (int)((uint)symbol >> 7) & 1;
                        price += RangeEncoder.GetBitPrice(probs[subencoderIndex], (int)bit);
                        symbol <<= 1;
                    } while (symbol < (0x100 << 8));

                    return price;
                }

                internal int GetMatchedPrice(int symbol, int matchByte)
                {
                    int price = 0;
                    int offset = 0x100;
                    int subencoderIndex;
                    int matchBit;
                    int bit;

                    symbol |= 0x100;

                    do
                    {
                        matchByte <<= 1;
                        matchBit = matchByte & offset;
                        subencoderIndex = (offset + matchBit + (int)((uint)symbol >> 8));
                        bit = (int)((uint)symbol >> 7) & 1;
                        price += RangeEncoder.GetBitPrice(probs[subencoderIndex], (int)bit);
                        symbol <<= 1;
                        offset &= ~(matchByte ^ symbol);
                    } while (symbol < (0x100 << 8));

                    return price;
                }
            }
        }


        internal class LengthEncoder : LengthCoder
        {
            // The prices are updated after at least
            // <code>PRICE_UPDATE_INTERVAL</code> many lengths
            // have been encoded with the same posState.
            private static readonly int PRICE_UPDATE_INTERVAL = 32;

            private readonly int[] counters;
            private readonly int[][] prices;

            private LZMAEncoder encoder;

            internal LengthEncoder(LZMAEncoder encoder, int pb, int niceLen)
            {
                this.encoder = encoder;
                int posStates = 1 << pb;
                counters = new int[posStates];

                // Always allocate at least LOW_SYMBOLS + MID_SYMBOLS because
                // it makes updatePrices slightly simpler. The prices aren't
                // usually needed anyway if niceLen < 18.
                int lenSymbols = Math.Max(niceLen - MATCH_LEN_MIN + 1,
                                          LOW_SYMBOLS + MID_SYMBOLS);
                prices = RectangularArrays.RectangularIntArray(posStates, lenSymbols);
            }

            internal override void Reset()
            {
                base.Reset();

                // Reset counters to zero to force price update before
                // the prices are needed.
                for (int i = 0; i < counters.Length; ++i)
                    counters[i] = 0;
            }

            internal void Encode(int len, int posState)
            {
                len -= MATCH_LEN_MIN;

                if (len < LOW_SYMBOLS)
                {
                    encoder.rc.EncodeBit(choice, 0, 0);
                    encoder.rc.EncodeBitTree(low[posState], len);
                }
                else
                {
                    encoder.rc.EncodeBit(choice, 0, 1);
                    len -= LOW_SYMBOLS;

                    if (len < MID_SYMBOLS)
                    {
                        encoder.rc.EncodeBit(choice, 1, 0);
                        encoder.rc.EncodeBitTree(mid[posState], len);
                    }
                    else
                    {
                        encoder.rc.EncodeBit(choice, 1, 1);
                        encoder.rc.EncodeBitTree(high, len - MID_SYMBOLS);
                    }
                }

                --counters[posState];
            }

            internal int GetPrice(int len, int posState)
            {
                return prices[posState][len - MATCH_LEN_MIN];
            }

            internal void UpdatePrices()
            {
                for (int posState = 0; posState < counters.Length; ++posState)
                {
                    if (counters[posState] <= 0)
                    {
                        counters[posState] = PRICE_UPDATE_INTERVAL;
                        UpdatePrices(posState);
                    }
                }
            }

            private void UpdatePrices(int posState)
            {
                int choice0Price = RangeEncoder.GetBitPrice(choice[0], 0);

                int i = 0;
                for (; i < LOW_SYMBOLS; ++i)
                    prices[posState][i] = choice0Price
                            + RangeEncoder.GetBitTreePrice(low[posState], i);

                choice0Price = RangeEncoder.GetBitPrice(choice[0], 1);
                int choice1Price = RangeEncoder.GetBitPrice(choice[1], 0);

                for (; i < LOW_SYMBOLS + MID_SYMBOLS; ++i)
                    prices[posState][i] = choice0Price + choice1Price
                             + RangeEncoder.GetBitTreePrice(mid[posState],
                                                            i - LOW_SYMBOLS);

                choice1Price = RangeEncoder.GetBitPrice(choice[1], 1);

                for (; i < prices[posState].Length; ++i)
                    prices[posState][i] = choice0Price + choice1Price
                             + RangeEncoder.GetBitTreePrice(high, i - LOW_SYMBOLS
                                                                  - MID_SYMBOLS);
            }
        }
    }
}
