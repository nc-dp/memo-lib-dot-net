﻿using Dk.Digst.Digital.Post.Memolib.Lzma.Lz;
using Dk.Digst.Digital.Post.Memolib.Lzma.RangeCoder;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma.Lzma
{
    public sealed class LZMADecoder : LZMACoder
    {
        private bool instanceFieldsInitialized = false;

        private void InitializeInstanceFields()
        {
            matchLenDecoder = new LengthDecoder(this);
            repLenDecoder = new LengthDecoder(this);
        }

        internal readonly LZDecoder lz;
        internal readonly RangeDecoder rc;
        private readonly LiteralDecoder literalDecoder;
        private LengthDecoder matchLenDecoder;
        private LengthDecoder repLenDecoder;

        public LZMADecoder(LZDecoder lz, RangeDecoder rc, int lc, int lp, int pb) : base(pb)
        {
            if (!instanceFieldsInitialized)
            {
                InitializeInstanceFields();
                instanceFieldsInitialized = true;
            }
            this.lz = lz;
            this.rc = rc;
            this.literalDecoder = new LiteralDecoder(this, lc, lp);
            Reset();
        }

        public override void Reset()
        {
            base.Reset();
            literalDecoder.Reset();
            matchLenDecoder.Reset();
            repLenDecoder.Reset();
        }

        /// <summary>
        /// Returns true if LZMA end marker was detected. It is encoded as
        /// the maximum match distance which with signed ints becomes -1. This
        /// function is needed only for LZMA1.
        /// </summary>
        public bool EndMarkerDetected()
        {
            return reps[0] == -1;
        }

        public void Decode()
        {
            lz.RepeatPending();

            while (lz.HasSpace())
            {
                int posState = lz.Pos & posMask;

                if (rc.DecodeBit(isMatch[state.Get()], posState) == 0)
                {
                    literalDecoder.Decode();
                }
                else
                {
                    int len = rc.DecodeBit(isRep, state.Get()) == 0 ? DecodeMatch(posState) : DecodeRepMatch(posState);

                    // NOTE: With LZMA1 streams that have the end marker,
                    // this will throw CorruptedInputException. LZMAInputStream
                    // handles it specially.
                    lz.Repeat(reps[0], len);
                }
            }

            rc.Normalize();
        }

        private int DecodeMatch(int posState)
        {
            state.UpdateMatch();

            reps[3] = reps[2];
            reps[2] = reps[1];
            reps[1] = reps[0];

            int len = matchLenDecoder.Decode(posState);
            int distSlot = rc.DecodeBitTree(distSlots[GetDistState(len)]);

            if (distSlot < DIST_MODEL_START)
            {
                reps[0] = distSlot;
            }
            else
            {
                int limit = (distSlot >> 1) - 1;
                reps[0] = (2 | (distSlot & 1)) << limit;

                if (distSlot < DIST_MODEL_END)
                {
                    reps[0] |= rc.DecodeReverseBitTree(distSpecial[distSlot - DIST_MODEL_START]);
                }
                else
                {
                    reps[0] |= rc.DecodeDirectBits(limit - ALIGN_BITS) << ALIGN_BITS;
                    reps[0] |= rc.DecodeReverseBitTree(distAlign);
                }
            }

            return len;
        }

        private int DecodeRepMatch(int posState)
        {
            if (rc.DecodeBit(isRep0, state.Get()) == 0)
            {
                if (rc.DecodeBit(isRep0Long[state.Get()], posState) == 0)
                {
                    state.UpdateShortRep();
                    return 1;
                }
            }
            else
            {
                int tmp;

                if (rc.DecodeBit(isRep1, state.Get()) == 0)
                {
                    tmp = reps[1];
                }
                else
                {
                    if (rc.DecodeBit(isRep2, state.Get()) == 0)
                    {
                        tmp = reps[2];
                    }
                    else
                    {
                        tmp = reps[3];
                        reps[3] = reps[2];
                    }

                    reps[2] = reps[1];
                }

                reps[1] = reps[0];
                reps[0] = tmp;
            }

            state.UpdateLongRep();

            return repLenDecoder.Decode(posState);
        }

        private class LiteralDecoder : LiteralCoder
        {
            private readonly LZMADecoder outerInstance;

            private readonly LiteralSubdecoder[] subdecoders;

            internal LiteralDecoder(LZMADecoder outerInstance, int lc, int lp) : base(outerInstance, lc, lp)
            {
                this.outerInstance = outerInstance;

                subdecoders = new LiteralSubdecoder[1 << (lc + lp)];
                for (int i = 0; i < subdecoders.Length; ++i)
                {
                    subdecoders[i] = new LiteralSubdecoder(outerInstance);
                }
            }

            internal virtual void Reset()
            {
                for (int i = 0; i < subdecoders.Length; ++i)
                {
                    subdecoders[i].Reset();
                }
            }

            internal virtual void Decode()
            {
                int i = GetSubcoderIndex(outerInstance.lz.GetByte(0), outerInstance.lz.Pos);
                subdecoders[i].Decode();
            }


            private class LiteralSubdecoder : LiteralSubcoder
            {
                private readonly LZMADecoder outerInstance;

                public LiteralSubdecoder(LZMADecoder outerInstance)
                {
                    this.outerInstance = outerInstance;
                }

                internal virtual void Decode()
                {
                    int symbol = 1;

                    if (outerInstance.state.Literal)
                    {
                        do
                        {
                            symbol = (symbol << 1) | outerInstance.rc.DecodeBit(probs, symbol);
                        } while (symbol < 0x100);

                    }
                    else
                    {
                        int matchByte = outerInstance.lz.GetByte(outerInstance.reps[0]);
                        int offset = 0x100;
                        int matchBit;
                        int bit;

                        do
                        {
                            matchByte <<= 1;
                            matchBit = matchByte & offset;
                            bit = outerInstance.rc.DecodeBit(probs, offset + matchBit + symbol);
                            symbol = (symbol << 1) | bit;
                            offset &= (0 - bit) ^ ~matchBit;
                        } while (symbol < 0x100);
                    }

                    outerInstance.lz.PutByte((byte)symbol);
                    outerInstance.state.UpdateLiteral();
                }
            }
        }

        private class LengthDecoder : LengthCoder
        {
            private readonly LZMADecoder outerInstance;

            public LengthDecoder(LZMADecoder outerInstance)
            {
                this.outerInstance = outerInstance;
            }

            internal int Decode(int posState)
            {
                if (outerInstance.rc.DecodeBit(choice, 0) == 0)
                    return outerInstance.rc.DecodeBitTree(low[posState]) + MATCH_LEN_MIN;

                if (outerInstance.rc.DecodeBit(choice, 1) == 0)
                    return outerInstance.rc.DecodeBitTree(mid[posState])
                           + MATCH_LEN_MIN + LOW_SYMBOLS;

                return outerInstance.rc.DecodeBitTree(high)
                       + MATCH_LEN_MIN + LOW_SYMBOLS + MID_SYMBOLS;
            }
        }
    }
}
