﻿using Dk.Digst.Digital.Post.Memolib.Lzma.Lz;
using Dk.Digst.Digital.Post.Memolib.Lzma.RangeCoder;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma.Lzma
{
    internal sealed class LZMAEncoderNormal : LZMAEncoder
    {
        private const int OPTS = 4096;

        private const int EXTRA_SIZE_BEFORE = OPTS;
        private const int EXTRA_SIZE_AFTER = OPTS;

        private readonly Optimum[] opts = new Optimum[OPTS];
        private int optCur = 0;
        private int optEnd = 0;

        private Matches matches;

        // These are fields solely to avoid allocating the objects again and
        // again on each function call.
        private readonly int[] repLens = new int[REPS];
        private readonly State nextState = new State();

        internal static int GetMemoryUsage(int dictSize, int extraSizeBefore, int mf)
        {
            return LZEncoder.GetMemoryUsage(dictSize, Math.Max(extraSizeBefore, EXTRA_SIZE_BEFORE), EXTRA_SIZE_AFTER, MATCH_LEN_MAX, mf) + OPTS * 64 / 1024;
        }

        internal LZMAEncoderNormal(RangeEncoder rc, int lc, int lp, int pb, int dictSize, int extraSizeBefore, int niceLen, int mf, int depthLimit, ArrayCache arrayCache) : base(rc, LZEncoder.GetInstance(dictSize, Math.Max(extraSizeBefore, EXTRA_SIZE_BEFORE), EXTRA_SIZE_AFTER, niceLen, MATCH_LEN_MAX, mf, depthLimit, arrayCache), lc, lp, pb, dictSize, niceLen)
        {

            for (int i = 0; i < OPTS; ++i)
            {
                opts[i] = new Optimum();
            }
        }

        public override void Reset()
        {
            optCur = 0;
            optEnd = 0;
            base.Reset();
        }

        /// <summary>
        /// Converts the opts array from backward indexes to forward indexes.
        /// Then it will be simple to get the next symbol from the array
        /// in later calls to <code>getNextSymbol()</code>.
        /// </summary>
        private int ConvertOpts()
        {
            optEnd = optCur;

            int optPrev = opts[optCur].optPrev;

            do
            {
                Optimum opt = opts[optCur];

                if (opt.prev1IsLiteral)
                {
                    opts[optPrev].optPrev = optCur;
                    opts[optPrev].backPrev = -1;
                    optCur = optPrev--;

                    if (opt.hasPrev2)
                    {
                        opts[optPrev].optPrev = optPrev + 1;
                        opts[optPrev].backPrev = opt.backPrev2;
                        optCur = optPrev;
                        optPrev = opt.optPrev2;
                    }
                }

                int temp = opts[optPrev].optPrev;
                opts[optPrev].optPrev = optCur;
                optCur = optPrev;
                optPrev = temp;
            } while (optCur > 0);

            optCur = opts[0].optPrev;
            back = opts[optCur].backPrev;
            return optCur;
        }

        internal override int NextSymbol
        {
            get
            {
                // If there are pending symbols from an earlier call to this
                // function, return those symbols first.
                if (optCur < optEnd)
                {
                    int len = opts[optCur].optPrev - optCur;
                    optCur = opts[optCur].optPrev;
                    back = opts[optCur].backPrev;
                    return len;
                }

                Debug.Assert(optCur == optEnd);
                optCur = 0;
                optEnd = 0;
                back = -1;

                if (readAhead == -1)
                {
                    matches = GetMatches();
                }

                // Get the number of bytes available in the dictionary, but
                // not more than the maximum match length. If there aren't
                // enough bytes remaining to encode a match at all, return
                // immediately to encode this byte as a literal.
                int avail = Math.Min(lz.GetAvail(), MATCH_LEN_MAX);
                if (avail < MATCH_LEN_MIN)
                {
                    return 1;
                }

                // Get the lengths of repeated matches.
                int repBest = 0;
                for (int rep = 0; rep < REPS; ++rep)
                {
                    repLens[rep] = lz.GetMatchLen(reps[rep], avail);

                    if (repLens[rep] < MATCH_LEN_MIN)
                    {
                        repLens[rep] = 0;
                        continue;
                    }

                    if (repLens[rep] > repLens[repBest])
                    {
                        repBest = rep;
                    }
                }

                // Return if the best repeated match is at least niceLen bytes long.
                if (repLens[repBest] >= niceLen)
                {
                    back = repBest;
                    Skip(repLens[repBest] - 1);
                    return repLens[repBest];
                }

                // Initialize mainLen and mainDist to the longest match found
                // by the match finder.
                int mainLen = 0;
                int mainDist = 0;
                if (matches.count > 0)
                {
                    mainLen = matches.len[matches.count - 1];
                    mainDist = matches.dist[matches.count - 1];

                    // Return if it is at least niceLen bytes long.
                    if (mainLen >= niceLen)
                    {
                        back = mainDist + REPS;
                        Skip(mainLen - 1);
                        return mainLen;
                    }
                }

                int curByte = lz.GetByte(0);
                int matchByte = lz.GetByte(reps[0] + 1);

                // If the match finder found no matches and this byte cannot be
                // encoded as a repeated match (short or long), we must be return
                // to have the byte encoded as a literal.
                if (mainLen < MATCH_LEN_MIN && curByte != matchByte && repLens[repBest] < MATCH_LEN_MIN)
                {
                    return 1;
                }


                int pos = lz.GetPos();
                int posState = pos & posMask;

                {
                    // Calculate the price of encoding the current byte as a literal.
                    int prevByte = lz.GetByte(1);
                    int literalPrice = literalEncoder.GetPrice(curByte, matchByte, prevByte, pos, state);
                    opts[1].Set1(literalPrice, 0, -1);
                }

                int anyMatchPrice = GetAnyMatchPrice(state, posState);
                int anyRepPrice = GetAnyRepPrice(anyMatchPrice, state);

                // If it is possible to encode this byte as a short rep, see if
                // it is cheaper than encoding it as a literal.
                if (matchByte == curByte)
                {
                    int shortRepPrice = GetShortRepPrice(anyRepPrice, state, posState);
                    if (shortRepPrice < opts[1].price)
                    {
                        opts[1].Set1(shortRepPrice, 0, 0);
                    }
                }

                // Return if there is neither normal nor long repeated match. Use
                // a short match instead of a literal if is is possible and cheaper.
                optEnd = Math.Max(mainLen, repLens[repBest]);
                if (optEnd < MATCH_LEN_MIN)
                {
                    Debug.Assert(optEnd == 0);
                    back = opts[1].backPrev;
                    return 1;
                }


                // Update the lookup tables for distances and lengths before using
                // those price calculation functions. (The price function above
                // don't need these tables.)
                UpdatePrices();

                // Initialize the state and reps of this position in opts[].
                // updateOptStateAndReps() will need these to get the new
                // state and reps for the next byte.
                opts[0].state.Set(state);
                Array.Copy(reps, 0, opts[0].reps, 0, REPS);

                // Initialize the prices for latter opts that will be used below.
                for (int i = optEnd; i >= MATCH_LEN_MIN; --i)
                    opts[i].Reset();

                // Calculate the prices of repeated matches of all lengths.
                for (int rep = 0; rep < REPS; ++rep)
                {
                    int repLen = repLens[rep];
                    if (repLen < MATCH_LEN_MIN)
                        continue;

                    int longRepPrice = GetLongRepPrice(anyRepPrice, rep,
                                                       state, posState);
                    do
                    {
                        int price = longRepPrice + repLenEncoder.GetPrice(repLen,
                                                                          posState);
                        if (price < opts[repLen].price)
                            opts[repLen].Set1(price, 0, rep);
                    } while (--repLen >= MATCH_LEN_MIN);
                }

                // Calculate the prices of normal matches that are longer than rep0.
                {
                    int len = Math.Max(repLens[0] + 1, MATCH_LEN_MIN);
                    if (len <= mainLen)
                    {
                        int normalMatchPrice = GetNormalMatchPrice(anyMatchPrice,
                                                                   state);

                        // Set i to the index of the shortest match that is
                        // at least len bytes long.
                        int i = 0;
                        while (len > matches.len[i])
                            ++i;

                        while (true)
                        {
                            int dist = matches.dist[i];
                            int price = GetMatchAndLenPrice(normalMatchPrice,
                                                            dist, len, posState);
                            if (price < opts[len].price)
                                opts[len].Set1(price, 0, dist + REPS);

                            if (len == matches.len[i])
                                if (++i == matches.count)
                                    break;

                            ++len;
                        }
                    }
                }


                avail = Math.Min(lz.GetAvail(), OPTS - 1);

                // Get matches for later bytes and optimize the use of LZMA symbols
                // by calculating the prices and picking the cheapest symbol
                // combinations.
                while (++optCur < optEnd)
                {
                    matches = GetMatches();
                    if (matches.count > 0
                            && matches.len[matches.count - 1] >= niceLen)
                        break;

                    --avail;
                    ++pos;
                    posState = pos & posMask;

                    UpdateOptStateAndReps();
                    anyMatchPrice = opts[optCur].price
                                    + GetAnyMatchPrice(opts[optCur].state, posState);
                    anyRepPrice = GetAnyRepPrice(anyMatchPrice, opts[optCur].state);

                    calc1BytePrices(pos, posState, avail, anyRepPrice);

                    if (avail >= MATCH_LEN_MIN)
                    {
                        int startLen = CalcLongRepPrices(pos, posState,
                                                         avail, anyRepPrice);
                        if (matches.count > 0)
                            CalcNormalMatchPrices(pos, posState, avail,
                                                  anyMatchPrice, startLen);
                    }
                }

                return ConvertOpts();
            }
        }

        /// <summary>
        /// Updates the state and reps for the current byte in the opts array.
        /// </summary>
        private void UpdateOptStateAndReps()
        {
            int optPrev = opts[optCur].optPrev;
            Debug.Assert(optPrev < optCur);

            if (opts[optCur].prev1IsLiteral)
            {
                --optPrev;

                if (opts[optCur].hasPrev2)
                {
                    opts[optCur].state.Set(opts[opts[optCur].optPrev2].state);
                    if (opts[optCur].backPrev2 < REPS)
                        opts[optCur].state.UpdateLongRep();
                    else
                        opts[optCur].state.UpdateMatch();
                }
                else
                {
                    opts[optCur].state.Set(opts[optPrev].state);
                }

                opts[optCur].state.UpdateLiteral();
            }
            else
            {
                opts[optCur].state.Set(opts[optPrev].state);
            }

            if (optPrev == optCur - 1)
            {
                // Must be either a short rep or a literal.
                Debug.Assert(opts[optCur].backPrev == 0 || opts[optCur].backPrev == -1);

                if (opts[optCur].backPrev == 0)
                    opts[optCur].state.UpdateShortRep();
                else
                    opts[optCur].state.UpdateLiteral();

                Array.Copy(opts[optPrev].reps, 0,
                                 opts[optCur].reps, 0, REPS);
            }
            else
            {
                int back;
                if (opts[optCur].prev1IsLiteral && opts[optCur].hasPrev2)
                {
                    optPrev = opts[optCur].optPrev2;
                    back = opts[optCur].backPrev2;
                    opts[optCur].state.UpdateLongRep();
                }
                else
                {
                    back = opts[optCur].backPrev;
                    if (back < REPS)
                        opts[optCur].state.UpdateLongRep();
                    else
                        opts[optCur].state.UpdateMatch();
                }

                if (back < REPS)
                {
                    opts[optCur].reps[0] = opts[optPrev].reps[back];

                    int rep;
                    for (rep = 1; rep <= back; ++rep)
                        opts[optCur].reps[rep] = opts[optPrev].reps[rep - 1];

                    for (; rep < REPS; ++rep)
                        opts[optCur].reps[rep] = opts[optPrev].reps[rep];
                }
                else
                {
                    opts[optCur].reps[0] = back - REPS;
                    Array.Copy(opts[optPrev].reps, 0,
                                     opts[optCur].reps, 1, REPS - 1);
                }
            }
        }

        /// <summary>
        /// Calculates prices of a literal, a short rep, and literal + rep0.
        /// </summary>
        private void calc1BytePrices(int pos, int posState,
                                     int avail, int anyRepPrice)
        {
            // This will be set to true if using a literal or a short rep.
            bool nextIsByte = false;

            int curByte = lz.GetByte(0);
            int matchByte = lz.GetByte(opts[optCur].reps[0] + 1);

            // Try a literal.
            int literalPrice = opts[optCur].price
                    + literalEncoder.GetPrice(curByte, matchByte, lz.GetByte(1),
                                              pos, opts[optCur].state);
            if (literalPrice < opts[optCur + 1].price)
            {
                opts[optCur + 1].Set1(literalPrice, optCur, -1);
                nextIsByte = true;
            }

            // Try a short rep.
            if (matchByte == curByte && (opts[optCur + 1].optPrev == optCur
                                          || opts[optCur + 1].backPrev != 0))
            {
                int shortRepPrice = GetShortRepPrice(anyRepPrice,
                                                     opts[optCur].state,
                                                     posState);
                if (shortRepPrice <= opts[optCur + 1].price)
                {
                    opts[optCur + 1].Set1(shortRepPrice, optCur, 0);
                    nextIsByte = true;
                }
            }

            // If neither a literal nor a short rep was the cheapest choice,
            // try literal + long rep0.
            if (!nextIsByte && matchByte != curByte && avail > MATCH_LEN_MIN)
            {
                int lenLimit = Math.Min(niceLen, avail - 1);
                int len = lz.GetMatchLen(1, opts[optCur].reps[0], lenLimit);

                if (len >= MATCH_LEN_MIN)
                {
                    nextState.Set(opts[optCur].state);
                    nextState.UpdateLiteral();
                    int nextPosState = (pos + 1) & posMask;
                    int price = literalPrice
                                + GetLongRepAndLenPrice(0, len,
                                                        nextState, nextPosState);

                    int i = optCur + 1 + len;
                    while (optEnd < i)
                        opts[++optEnd].Reset();

                    if (price < opts[i].price)
                        opts[i].Set2(price, optCur, 0);
                }
            }
        }

        /// <summary>
        /// Calculates prices of long rep and long rep + literal + rep0.
        /// </summary>
        private int CalcLongRepPrices(int pos, int posState, int avail, int anyRepPrice)
        {
            int startLen = MATCH_LEN_MIN;
            int lenLimit = Math.Min(avail, niceLen);

            for (int rep = 0; rep < REPS; ++rep)
            {
                int len = lz.GetMatchLen(opts[optCur].reps[rep], lenLimit);
                if (len < MATCH_LEN_MIN)
                    continue;

                while (optEnd < optCur + len)
                    opts[++optEnd].Reset();

                int longRepPrice = GetLongRepPrice(anyRepPrice, rep,
                                                   opts[optCur].state, posState);

                for (int i = len; i >= MATCH_LEN_MIN; --i)
                {
                    int price = longRepPrice
                                + repLenEncoder.GetPrice(i, posState);
                    if (price < opts[optCur + i].price)
                        opts[optCur + i].Set1(price, optCur, rep);
                }

                if (rep == 0)
                    startLen = len + 1;

                int len2Limit = Math.Min(niceLen, avail - len - 1);
                int len2 = lz.GetMatchLen(len + 1, opts[optCur].reps[rep],
                                          len2Limit);

                if (len2 >= MATCH_LEN_MIN)
                {
                    // Rep
                    int price = longRepPrice
                                + repLenEncoder.GetPrice(len, posState);
                    nextState.Set(opts[optCur].state);
                    nextState.UpdateLongRep();

                    // Literal
                    int curByte = lz.GetByte(len, 0);
                    int matchByte = lz.GetByte(0); // lz.getByte(len, len)
                    int prevByte = lz.GetByte(len, 1);
                    price += literalEncoder.GetPrice(curByte, matchByte, prevByte,
                                                     pos + len, nextState);
                    nextState.UpdateLiteral();

                    // Rep0
                    int nextPosState = (pos + len + 1) & posMask;
                    price += GetLongRepAndLenPrice(0, len2,
                                                   nextState, nextPosState);

                    int i = optCur + len + 1 + len2;
                    while (optEnd < i)
                        opts[++optEnd].Reset();

                    if (price < opts[i].price)
                        opts[i].Set3(price, optCur, rep, len, 0);
                }
            }

            return startLen;
        }

        /// <summary>
        /// Calculates prices of a normal match and normal match + literal + rep0.
        /// </summary>
        private void CalcNormalMatchPrices(int pos, int posState, int avail,
                                           int anyMatchPrice, int startLen)
        {
            // If the longest match is so long that it would not fit into
            // the opts array, shorten the matches.
            if (matches.len[matches.count - 1] > avail)
            {
                matches.count = 0;
                while (matches.len[matches.count] < avail)
                    ++matches.count;

                matches.len[matches.count++] = avail;
            }

            if (matches.len[matches.count - 1] < startLen)
                return;

            while (optEnd < optCur + matches.len[matches.count - 1])
                opts[++optEnd].Reset();

            int normalMatchPrice = GetNormalMatchPrice(anyMatchPrice,
                                                       opts[optCur].state);

            int match = 0;
            while (startLen > matches.len[match])
                ++match;

            for (int len = startLen; ; ++len)
            {
                int dist = matches.dist[match];

                // Calculate the price of a match of len bytes from the nearest
                // possible distance.
                int matchAndLenPrice = GetMatchAndLenPrice(normalMatchPrice,
                                                           dist, len, posState);
                if (matchAndLenPrice < opts[optCur + len].price)
                    opts[optCur + len].Set1(matchAndLenPrice,
                                            optCur, dist + REPS);

                if (len != matches.len[match])
                    continue;

                // Try match + literal + rep0. First get the length of the rep0.
                int len2Limit = Math.Min(niceLen, avail - len - 1);
                int len2 = lz.GetMatchLen(len + 1, dist, len2Limit);

                if (len2 >= MATCH_LEN_MIN)
                {
                    nextState.Set(opts[optCur].state);
                    nextState.UpdateMatch();

                    // Literal
                    int curByte = lz.GetByte(len, 0);
                    int matchByte = lz.GetByte(0); // lz.getByte(len, len)
                    int prevByte = lz.GetByte(len, 1);
                    int price = matchAndLenPrice
                            + literalEncoder.GetPrice(curByte, matchByte,
                                                      prevByte, pos + len,
                                                      nextState);
                    nextState.UpdateLiteral();

                    // Rep0
                    int nextPosState = (pos + len + 1) & posMask;
                    price += GetLongRepAndLenPrice(0, len2,
                                                   nextState, nextPosState);

                    int i = optCur + len + 1 + len2;
                    while (optEnd < i)
                        opts[++optEnd].Reset();

                    if (price < opts[i].price)
                        opts[i].Set3(price, optCur, dist + REPS, len, 0);
                }

                if (++match == matches.count)
                    break;
            }
        }
    }
}
