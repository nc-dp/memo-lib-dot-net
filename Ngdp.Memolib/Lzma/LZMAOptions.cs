﻿using Dk.Digst.Digital.Post.Memolib.Lzma.Lz;
using Dk.Digst.Digital.Post.Memolib.Lzma.Lzma;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma
{
    /// <summary>
    /// LZMA compression options.
    /// <para>
    /// While this allows setting the LZMA compression options in detail,
    /// often you only need <code>LZMAOptions()</code> or
    /// <code>LZMAOptions(int)</code>.
    /// </para>
    /// </summary>
    public class LZMAOptions
    {
        /// <summary>
        /// Minimum valid compression preset level is 0.
        /// </summary>
        public const int PRESET_MIN = 0;

        /// <summary>
        /// Maximum valid compression preset level is 9.
        /// </summary>
        public const int PRESET_MAX = 9;

        /// <summary>
        /// Default compression preset level is 6.
        /// </summary>
        public const int PRESET_DEFAULT = 6;

        /// <summary>
        /// Minimum dictionary size is 4 KiB.
        /// </summary>
        public const int DICT_SIZE_MIN = 4096;

        /// <summary>
        /// Maximum dictionary size for compression is 768 MiB.
        /// <para>
        /// The decompressor supports bigger dictionaries, up to almost 2 GiB.
        /// With HC4 the encoder would support dictionaries bigger than 768 MiB.
        /// The 768 MiB limit comes from the current implementation of BT4 where
        /// we would otherwise hit the limits of signed ints in array indexing.
        /// </para>
        /// <para>
        /// If you really need bigger dictionary for decompression,
        /// use <seealso cref="LZMA2InputStream"/> directly.
        /// </para>
        /// </summary>
        public static readonly int DICT_SIZE_MAX = 768 << 20;

        /// <summary>
        /// The default dictionary size is 8 MiB.
        /// </summary>
        public static readonly int DICT_SIZE_DEFAULT = 8 << 20;

        /// <summary>
        /// Maximum value for lc + lp is 4.
        /// </summary>
        public const int LC_LP_MAX = 4;

        /// <summary>
        /// The default number of literal context bits is 3.
        /// </summary>
        public const int LC_DEFAULT = 3;

        /// <summary>
        /// The default number of literal position bits is 0.
        /// </summary>
        public const int LP_DEFAULT = 0;

        /// <summary>
        /// Maximum value for pb is 4.
        /// </summary>
        public const int PB_MAX = 4;

        /// <summary>
        /// The default number of position bits is 2.
        /// </summary>
        public const int PB_DEFAULT = 2;

        /// <summary>
        /// Compression mode: uncompressed.
        /// The data is wrapped into a LZMA stream without compression.
        /// </summary>
        public const int MODE_UNCOMPRESSED = 0;

        /// <summary>
        /// Compression mode: fast.
        /// This is usually combined with a hash chain match finder.
        /// </summary>
        public const int MODE_FAST = LZMAEncoder.MODE_FAST;

        /// <summary>
        /// Compression mode: normal.
        /// This is usually combined with a binary tree match finder.
        /// </summary>
        public const int MODE_NORMAL = LZMAEncoder.MODE_NORMAL;

        /// <summary>
        /// Minimum value for <code>niceLen</code> is 8.
        /// </summary>
        public const int NICE_LEN_MIN = 8;

        /// <summary>
        /// Maximum value for <code>niceLen</code> is 273.
        /// </summary>
        public const int NICE_LEN_MAX = 273;

        /// <summary>
        /// Match finder: Hash Chain 2-3-4
        /// </summary>
        public const int MF_HC4 = LZEncoder.MF_HC4;

        /// <summary>
        /// Match finder: Binary tree 2-3-4
        /// </summary>
        public const int MF_BT4 = LZEncoder.MF_BT4;

        private static readonly int[] presetToDictSize = new int[] { 1 << 18, 1 << 20, 1 << 21, 1 << 22, 1 << 22, 1 << 23, 1 << 23, 1 << 24, 1 << 25, 1 << 26 };

        private static readonly int[] presetToDepthLimit = new int[] { 4, 8, 24, 48 };

        private int dictSize;
        private sbyte[] presetDict = null;
        private int lc;
        private int lp;
        private int pb;
        private int mode;
        private int niceLen;
        private int mf;
        private int depthLimit;

        /// <summary>
        /// Creates new LZMA options and sets them to the default values.
        /// This is equivalent to <code>LZMAOptions(PRESET_DEFAULT)</code>.
        /// </summary>
        public LZMAOptions()
        {
            try
            {
                Preset = PRESET_DEFAULT;
            }
            catch (UnsupportedOptionsException)
            {
                Debug.Assert(false);
                throw new System.Exception();
            }
        }

        /// <summary>
        /// Creates new LZMA options and sets them to the given preset.
        /// </summary>
        /// <exception cref="UnsupportedOptionsException">
        /// <code>preset</code> is not supported </exception>
        public LZMAOptions(int preset)
        {
            Preset = preset;
        }

        /// <summary>
        /// Creates new LZMA options and sets them to the given custom values.
        /// </summary>
        /// <exception cref="UnsupportedOptionsException">
        /// unsupported options were specified </exception>
        public LZMAOptions(int dictSize, int lc, int lp, int pb, int mode, int niceLen, int mf, int depthLimit)
        {
            DictSize = dictSize;
            SetLcLp(lc, lp);
            Pb = pb;
            Mode = mode;
            NiceLen = niceLen;
            MatchFinder = mf;
            DepthLimit = depthLimit;
        }

        /// <summary>
        /// Sets the compression options to the given preset.
        /// <para>
        /// The presets 0-3 are fast presets with medium compression.
        /// The presets 4-6 are fairly slow presets with high compression.
        /// The default preset (<code>PRESET_DEFAULT</code>) is 6.
        /// </para>
        /// <para>
        /// The presets 7-9 are like the preset 6 but use bigger dictionaries
        /// and have higher compressor and decompressor memory requirements.
        /// Unless the uncompressed size of the file exceeds 8&nbsp;MiB,
        /// 16&nbsp;MiB, or 32&nbsp;MiB, it is waste of memory to use the
        /// presets 7, 8, or 9, respectively.
        /// 
        /// </para>
        /// </summary>
        /// <exception cref="UnsupportedOptionsException">
        /// <code>preset</code> is not supported </exception>
        public virtual int Preset
        {
            set
            {
                if (value < 0 || value > 9)
                {
                    throw new UnsupportedOptionsException("Unsupported preset: " + value);
                }

                lc = LC_DEFAULT;
                lp = LP_DEFAULT;
                pb = PB_DEFAULT;
                dictSize = presetToDictSize[value];

                if (value <= 3)
                {
                    mode = MODE_FAST;
                    mf = MF_HC4;
                    niceLen = value <= 1 ? 128 : NICE_LEN_MAX;
                    depthLimit = presetToDepthLimit[value];
                }
                else
                {
                    mode = MODE_NORMAL;
                    mf = MF_BT4;
                    niceLen = (value == 4) ? 16 : (value == 5) ? 32 : 64;
                    depthLimit = 0;
                }
            }
        }

        /// <summary>
        /// Sets the dictionary size in bytes.
        /// <para>
        /// The dictionary (or history buffer) holds the most recently seen
        /// uncompressed data. Bigger dictionary usually means better compression.
        /// However, using a dictioanary bigger than the size of the uncompressed
        /// data is waste of memory.
        /// </para>
        /// <para>
        /// Any value in the range [DICT_SIZE_MIN, DICT_SIZE_MAX] is valid,
        /// but sizes of 2^n and 2^n + 2^(n-1) bytes are somewhat
        /// recommended.
        /// 
        /// </para>
        /// </summary>
        /// <exception cref="UnsupportedOptionsException">
        /// <code>dictSize</code> is not supported </exception>
        public virtual int DictSize
        {
            set
            {
                if (value < DICT_SIZE_MIN)
                {
                    throw new UnsupportedOptionsException("LZMA2 dictionary size must be at least 4 KiB: " + value + " B");
                }

                if (value > DICT_SIZE_MAX)
                {
                    throw new UnsupportedOptionsException("LZMA2 dictionary size must not exceed " + (DICT_SIZE_MAX >> 20) + " MiB: " + value + " B");
                }

                this.dictSize = value;
            }
            get
            {
                return dictSize;
            }
        }


        /// <summary>
        /// Sets a preset dictionary. Use null to disable the use of
        /// a preset dictionary. By default there is no preset dictionary.
        /// <para>
        /// <b>The format doesn't support a preset dictionary for now.
        /// Do not set a preset dictionary unless you use raw LZMA.</b>
        /// </para>
        /// <para>
        /// Preset dictionary can be useful when compressing many similar,
        /// relatively small chunks of data independently from each other.
        /// A preset dictionary should contain typical strings that occur in
        /// the files being compressed. The most probable strings should be
        /// near the end of the preset dictionary. The preset dictionary used
        /// for compression is also needed for decompression.
        /// </para>
        /// </summary>
        public virtual sbyte[] PresetDict
        {
            set
            {
                this.presetDict = value;
            }
            get
            {
                return presetDict;
            }
        }


        /// <summary>
        /// Sets the number of literal context bits and literal position bits.
        /// <para>
        /// The sum of <code>lc</code> and <code>lp</code> is limited to 4.
        /// Trying to exceed it will throw an exception. This function lets
        /// you change both at the same time.
        /// 
        /// </para>
        /// </summary>
        /// <exception cref="UnsupportedOptionsException">
        /// <code>lc</code> and <code>lp</code>
        /// are invalid </exception>
        public virtual void SetLcLp(int lc, int lp)
        {
            if (lc < 0 || lp < 0 || lc > LC_LP_MAX || lp > LC_LP_MAX || lc + lp > LC_LP_MAX)
            {
                throw new UnsupportedOptionsException("lc + lp must not exceed " + LC_LP_MAX + ": " + lc + " + " + lp);
            }

            this.lc = lc;
            this.lp = lp;
        }

        /// <summary>
        /// Sets the number of literal context bits.
        /// <para>
        /// All bytes that cannot be encoded as matches are encoded as literals.
        /// That is, literals are simply 8-bit bytes that are encoded one at
        /// a time.
        /// </para>
        /// <para>
        /// The literal coding makes an assumption that the highest <code>lc</code>
        /// bits of the previous uncompressed byte correlate with the next byte.
        /// For example, in typical English text, an upper-case letter is often
        /// followed by a lower-case letter, and a lower-case letter is usually
        /// followed by another lower-case letter. In the US-ASCII character set,
        /// the highest three bits are 010 for upper-case letters and 011 for
        /// lower-case letters. When <code>lc</code> is at least 3, the literal
        /// coding can take advantage of this property in the  uncompressed data.
        /// </para>
        /// <para>
        /// The default value (3) is usually good. If you want maximum compression,
        /// try <code>setLc(4)</code>. Sometimes it helps a little, and sometimes it
        /// makes compression worse. If it makes it worse, test for example
        /// <code>setLc(2)</code> too.
        /// 
        /// </para>
        /// </summary>
        /// <exception cref="UnsupportedOptionsException">
        ///                          <code>lc</code> is invalid, or the sum
        ///                          of <code>lc</code> and <code>lp</code>
        ///                          exceed LC_LP_MAX </exception>
        public virtual int Lc
        {
            set
            {
                SetLcLp(value, lp);
            }
            get
            {
                return lc;
            }
        }

        /// <summary>
        /// Sets the number of literal position bits.
        /// <para>
        /// This affets what kind of alignment in the uncompressed data is
        /// assumed when encoding literals. See <seealso cref="setPb(int) setPb"/> for
        /// more information about alignment.
        /// 
        /// </para>
        /// </summary>
        /// <exception cref="UnsupportedOptionsException">
        ///                          <code>lp</code> is invalid, or the sum
        ///                          of <code>lc</code> and <code>lp</code>
        ///                          exceed LC_LP_MAX </exception>
        public virtual int Lp
        {
            set
            {
                SetLcLp(Lc, value);
            }
            get
            {
                return lp;
            }
        }

        /// <summary>
        /// Sets the number of position bits.
        /// This affects what kind of alignment in the uncompressed data is
        /// assumed in general. The default (2) means four-byte alignment
        /// (2^<code>pb</code> = 2^2 = 4), which is often a good choice when
        /// there's no better guess.
        /// When the alignment is known, setting the number of position bits
        /// accordingly may reduce the file size a little. For example with text
        /// files having one-byte alignment (US-ASCII, ISO-8859-*, UTF-8), using
        /// <code>Pb(0)</code> can improve compression slightly. For UTF-16
        /// text, <code>Pb(1)</code> is a good choice. If the alignment is
        /// an odd number like 3 bytes, <code>setPb(0)</code> might be the best
        /// choice.
        /// </summary>
        public int Pb
        {
            set
            {
                if (value < 0 || value > PB_MAX)
                    throw new UnsupportedOptionsException(
                            "pb must not exceed " + PB_MAX + ": " + value);

                this.pb = value;
            }

            get
            {
                return pb;
            }
        }

        /// <summary>
        /// Sets the compression mode.
        /// This specifies the method to analyze the data produced by
        /// a match finder. The default is <code>MODE_FAST</code> for presets
        /// 0-3 and <code>MODE_NORMAL</code> for presets 4-9.
        /// Usually <code>MODE_FAST</code> is used with Hash Chain match finders
        /// and <code>MODE_NORMAL</code> with Binary Tree match finders. This is
        /// also what the presets do.
        /// </summary
        public int Mode
        {
            set
            {
                if (value < MODE_UNCOMPRESSED || value > MODE_NORMAL)
                    throw new UnsupportedOptionsException(
                            "Unsupported compression mode: " + value);

                this.mode = value;
            }

            get
            {
                return mode;
            }
        }

        /// <summary>
        /// Sets the nice length of matches.
        /// Once a match of at least <code>niceLen</code> bytes is found,
        /// the algorithm stops looking for better matches. Higher values tend
        /// to give better compression at the expense of speed. The default
        /// depends on the preset.
        /// </summary>
        public int NiceLen
        {
            set
            {
                if (value < NICE_LEN_MIN)
                    throw new UnsupportedOptionsException(
                            "Minimum nice length of matches is "
                            + NICE_LEN_MIN + " bytes: " + value);

                if (value > NICE_LEN_MAX)
                    throw new UnsupportedOptionsException(
                            "Maximum nice length of matches is " + NICE_LEN_MAX
                            + ": " + value);

                this.niceLen = value;
            }

            get
            {
                return niceLen;
            }
        }

        /// <summary>
        /// Sets the match finder type.
        /// Match finder has a major effect on compression speed, memory usage,
        /// and compression ratio. Usually Hash Chain match finders are faster
        /// than Binary Tree match finders. The default depends on the preset:
        /// 0-3 use <code>MF_HC4</code> and 4-9 use <code>MF_BT4</code>.
        /// </summary>
        public int MatchFinder
        {
            set
            {
                if (value != MF_HC4 && value != MF_BT4)
                    throw new UnsupportedOptionsException(
                            "Unsupported match finder: " + value);

                this.mf = value;
            }

            get
            {
                return mf;
            }
        }

        /// <summary>
        /// Sets the match finder search depth limit.
        /// The default is a special value of <code>0</code> which indicates that
        /// the depth limit should be automatically calculated by the selected
        /// match finder from the nice length of matches.
        /// Reasonable depth limit for Hash Chain match finders is 4-100 and
        /// 16-1000 for Binary Tree match finders. Using very high values can
        /// make the compressor extremely slow with some files. Avoid settings
        /// higher than 1000 unless you are prepared to interrupt the compression
        /// in case it is taking far too long.
        /// </summary>
        public int DepthLimit
        {
            get
            {
                return depthLimit;
            }
            set
            {
                if (value < 0)
                    throw new UnsupportedOptionsException(
                            "Depth limit cannot be negative: " + value);

                this.depthLimit = value;
            }
        }
    }
}
