﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma
{
	/// <summary>
	/// Output stream that supports finishing without closing
	/// the underlying stream.
	/// </summary>
	public abstract class FinishableOutputStream : Stream
	{
		/// <summary>
		/// Finish the stream without closing the underlying stream.
		/// No more data may be written to the stream after finishing.
		/// <para>
		/// The <code>finish</code> method of <code>FinishableOutputStream</code>
		/// does nothing. Subclasses should override it if they need finishing
		/// support, which is the case, for example, with compressors.
		/// 
		/// </para>
		/// </summary>
		public virtual void Finish()
		{
		}
	}
}
