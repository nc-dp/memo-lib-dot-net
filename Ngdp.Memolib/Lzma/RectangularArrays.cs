﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma
{
    internal static class RectangularArrays
    {
        public static long[][] RectangularLongArray(int size1, int size2)
        {
            long[][] newArray = new long[size1][];
            for (int array1 = 0; array1 < size1; array1++)
            {
                newArray[array1] = new long[size2];
            }

            return newArray;
        }

        public static short[][] RectangularShortArray(int size1, int size2)
        {
            short[][] newArray = new short[size1][];
            for (int array1 = 0; array1 < size1; array1++)
            {
                newArray[array1] = new short[size2];
            }

            return newArray;
        }

        public static int[][] RectangularIntArray(int size1, int size2)
        {
            int[][] newArray = new int[size1][];
            for (int array1 = 0; array1 < size1; array1++)
            {
                newArray[array1] = new int[size2];
            }

            return newArray;
        }
    }
}
