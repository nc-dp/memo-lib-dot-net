﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma.RangeCoder
{
    public abstract class RangeCoder
    {
        internal const int SHIFT_BITS = 8;
        internal const int TOP_MASK = unchecked((int)0xFF000000);
        internal const int BIT_MODEL_TOTAL_BITS = 11;
        internal static readonly int BIT_MODEL_TOTAL = 1 << BIT_MODEL_TOTAL_BITS;
        internal static readonly short PROB_INIT = (short)(BIT_MODEL_TOTAL / 2);
        internal const int MOVE_BITS = 5;

        public static void InitProbs(short[] probs)
        {
            Arrays.Fill(probs, PROB_INIT);
        }
    }
}
