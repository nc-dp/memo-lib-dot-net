﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma.RangeCoder
{
    public abstract class RangeDecoder : RangeCoder
    {
        internal uint range = 0;
        internal uint code = 0;

        public abstract void Normalize();

        public virtual int DecodeBit(short[] probs, int index)
        {
            Normalize();

            int prob = probs[index];
            uint bound = (range >> BIT_MODEL_TOTAL_BITS) * (uint)prob;
            int bit;

            // Compare code and bound as if they were unsigned 32-bit integers.
            if ((code) < (bound))
            {
                range = bound;
                probs[index] = (short)(prob + ((int)((uint)(BIT_MODEL_TOTAL - prob) >> MOVE_BITS)));
                bit = 0;
            }
            else
            {
                range -= bound;
                code -= bound;
                probs[index] = (short)(prob - ((int)((uint)prob >> MOVE_BITS)));
                bit = 1;
            }

            return bit;
        }

        public virtual int DecodeBitTree(short[] probs)
        {
            int symbol = 1;

            do
            {
                symbol = (symbol << 1) | DecodeBit(probs, symbol);
            } while (symbol < probs.Length);

            return symbol - probs.Length;
        }

        public virtual int DecodeReverseBitTree(short[] probs)
        {
            int symbol = 1;
            int i = 0;
            int result = 0;

            do
            {
                int bit = DecodeBit(probs, symbol);
                symbol = (symbol << 1) | bit;
                result |= bit << i++;
            } while (symbol < probs.Length);

            return result;
        }

        public virtual int DecodeDirectBits(int count)
        {
            uint result = 0;

            do
            {
                Normalize();

                range = range >> 1;
                uint t = (code - range) >> 31;
                code -= range & (t - 1);
                result = (result << 1) | (1 - t);
            } while (--count != 0);

            return (int)result;
        }
    }
}
