﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma.RangeCoder
{
    public abstract class RangeEncoder : RangeCoder
    {
        private const int MOVE_REDUCING_BITS = 4;
        private const int BIT_PRICE_SHIFT_BITS = 4;

        private static readonly int[] prices = new int[(int)((uint)BIT_MODEL_TOTAL >> MOVE_REDUCING_BITS)];

        private long low;
        private int range;

        internal long cacheSize;
        private sbyte cache;

        static RangeEncoder()
        {
            for (int i = (1 << MOVE_REDUCING_BITS) / 2; i < BIT_MODEL_TOTAL; i += (1 << MOVE_REDUCING_BITS))
            {
                int w = i;
                int bitCount = 0;

                for (int j = 0; j < BIT_PRICE_SHIFT_BITS; ++j)
                {
                    w *= w;
                    bitCount <<= 1;

                    while ((w & 0xFFFF0000) != 0)
                    {
                        w = (int)((uint)w >> 1);
                        ++bitCount;
                    }
                }

                prices[i >> MOVE_REDUCING_BITS] = (BIT_MODEL_TOTAL_BITS << BIT_PRICE_SHIFT_BITS) - 15 - bitCount;
            }
        }

        public virtual void Reset()
        {
            low = 0;
            range = unchecked((int)0xFFFFFFFF);
            cache = 0x00;
            cacheSize = 1;
        }

        public virtual int PendingSize
        {
            get
            {
                // This function is only needed by users of RangeEncoderToBuffer,
                // but providing a must-be-never-called version here makes
                // LZMAEncoder simpler.
                throw new System.Exception();
            }
        }

        public virtual int Finish()
        {
            for (int i = 0; i < 5; ++i)
            {
                ShiftLow();
            }

            // RangeEncoderToBuffer.Finish() needs a return value to tell
            // how big the finished buffer is. RangeEncoderToStream has no
            // buffer and thus no return value is needed. Here we use a dummy
            // value which can be overriden in RangeEncoderToBuffer.Finish().
            return -1;
        }

        internal abstract void WriteByte(int b);

        private void ShiftLow()
        {
            int lowHi = (int)((long)((ulong)low >> 32));

            if (lowHi != 0 || low < 0xFF000000L)
            {
                int temp = cache;

                do
                {
                    WriteByte(temp + lowHi);
                    temp = 0xFF;
                } while (--cacheSize != 0);

                cache = (sbyte)((long)((ulong)low >> 24));
            }

            ++cacheSize;
            low = (low & 0x00FFFFFF) << 8;
        }

        public virtual void EncodeBit(short[] probs, int index, int bit)
        {
            int prob = probs[index];
            int bound = ((int)((uint)range >> BIT_MODEL_TOTAL_BITS)) * prob;

            // NOTE: Any non-zero value for bit is taken as 1.
            if (bit == 0)
            {
                range = bound;
                probs[index] = (short)(prob + ((int)((uint)(BIT_MODEL_TOTAL - prob) >> MOVE_BITS)));
            }
            else
            {
                low += bound & 0xFFFFFFFFL;
                range -= bound;
                probs[index] = (short)(prob - ((int)((uint)prob >> MOVE_BITS)));
            }

            if ((range & TOP_MASK) == 0)
            {
                range <<= SHIFT_BITS;
                ShiftLow();
            }
        }

        public static int GetBitPrice(int prob, int bit)
        {
            // NOTE: Unlike in encodeBit(), here bit must be 0 or 1.
            Debug.Assert(bit == 0 || bit == 1);
            return prices[(int)((uint)(prob ^ ((-bit) & (BIT_MODEL_TOTAL - 1))) >> MOVE_REDUCING_BITS)];
        }

        public virtual void EncodeBitTree(short[] probs, int symbol)
        {
            int index = 1;
            int mask = probs.Length;

            do
            {
                mask = (int)((uint)mask >> 1);
                int bit = symbol & mask;
                EncodeBit(probs, index, bit);

                index <<= 1;
                if (bit != 0)
                {
                    index |= 1;
                }

            } while (mask != 1);
        }

        public static int GetBitTreePrice(short[] probs, int symbol)
        {
            int price = 0;
            symbol |= probs.Length;

            do
            {
                int bit = symbol & 1;
                symbol = (int)((uint)symbol >> 1);
                price += GetBitPrice(probs[symbol], bit);
            } while (symbol != 1);

            return price;
        }

        public virtual void EncodeReverseBitTree(short[] probs, int symbol)
        {
            int index = 1;
            symbol |= probs.Length;

            do
            {
                int bit = symbol & 1;
                symbol = (int)((uint)symbol >> 1);
                EncodeBit(probs, index, bit);
                index = (index << 1) | bit;
            } while (symbol != 1);
        }

        public static int GetReverseBitTreePrice(short[] probs, int symbol)
        {
            int price = 0;
            int index = 1;
            symbol |= probs.Length;

            do
            {
                int bit = symbol & 1;
                symbol = (int)((uint)symbol >> 1);
                price += GetBitPrice(probs[index], bit);
                index = (index << 1) | bit;
            } while (symbol != 1);

            return price;
        }

        public void EncodeDirectBits(int value, int count)
        {
            do
            {
                range = (int)((uint)range >> 1);
                low += range & (0 - ((int)((uint)value >> --count) & 1));

                if ((range & TOP_MASK) == 0)
                {
                    range <<= SHIFT_BITS;
                    ShiftLow();
                }
            } while (count != 0);
        }

        public static int GetDirectBitsPrice(int count)
        {
            return count << BIT_PRICE_SHIFT_BITS;
        }
    }
}
