﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma.RangeCoder
{
	public sealed class RangeEncoderToStream : RangeEncoder
	{
		private readonly Stream outputStream;

		public RangeEncoderToStream(Stream outputStream)
		{
			this.outputStream = outputStream;
			Reset();
		}

		internal override void WriteByte(int b)
		{
			outputStream.WriteByte((byte)b);
		}
	}
}
