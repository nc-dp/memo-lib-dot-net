﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma.RangeCoder
{
    public sealed class RangeDecoderFromStream : RangeDecoder
    {
        private readonly Stream inputStream;

        public RangeDecoderFromStream(Stream inputStream)
        {
            this.inputStream = inputStream;
            if (inputStream.ReadByte() != 0x00)
            {
                throw new CorruptedInputException();
            }

            byte[] buff = new byte[4];
            for (int i = 3; i >= 0; i--)
            {
                buff[i] = (byte)inputStream.ReadByte();
            }

            code = BitConverter.ToUInt32(buff, 0);
            range = 0xFFFFFFFF;
        }

        public bool Finished
        {
            get
            {
                return code == 0;
            }
        }

        public override void Normalize()
        {
            if ((range & TOP_MASK) == 0)
            {
                code = (code << SHIFT_BITS) | (byte)inputStream.ReadByte();
                range <<= SHIFT_BITS;
            }
        }
    }
}
