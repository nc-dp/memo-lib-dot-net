﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma.Lz
{
    /// <summary>
    /// Provides a CRC32 table using the polynomial from IEEE 802.3.
    /// </summary>
    internal class CRC32Hash
    {
        private const uint CRC32_POLY = 0xEDB88320;

        internal static readonly uint[] crcTable = new uint[256];

        static CRC32Hash()
        {
            for (uint i = 0; i < 256; ++i)
            {
                uint r = i;

                for (int j = 0; j < 8; ++j)
                {
                    if ((r & 1) != 0)
                    {
                        r = (r >> 1) ^ CRC32_POLY;
                    }
                    else
                    {
                        r = (r >> 1);
                    }
                }

                crcTable[i] = r;
            }
        }
    }
}
