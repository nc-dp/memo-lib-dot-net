﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma.Lz
{
    public abstract class LZEncoder
    {
        public const int MF_HC4 = 0x04;
        public const int MF_BT4 = 0x14;

        /// <summary>
        /// Number of bytes to keep available before the current byte
        /// when moving the LZ window.
        /// </summary>
        private readonly int keepSizeBefore;

        /// <summary>
        /// Number of bytes that must be available, the current byte included,
        /// to make hasEnoughData return true. Flushing and finishing are
        /// naturally exceptions to this since there cannot be any data after
        /// the end of the uncompressed input.
        /// </summary>
        private readonly int keepSizeAfter;

        internal readonly int matchLenMax;
        internal readonly int niceLen;

        internal readonly byte[] buf;
        internal readonly int bufSize; // To avoid buf.length with an array-cached buf.

        internal int readPos = -1;
        private int readLimit = -1;
        private bool finishing = false;
        private int writePos = 0;
        private int pendingSize = 0;

        internal static void Normalize(int[] positions, int positionsCount, int normalizationOffset)
        {
            for (int i = 0; i < positionsCount; ++i)
            {
                if (positions[i] <= normalizationOffset)
                {
                    positions[i] = 0;
                }
                else
                {
                    positions[i] -= normalizationOffset;
                }
            }
        }

        /// <summary>
        /// Gets the size of the LZ window buffer that needs to be allocated.
        /// </summary>
        private static int GetBufSize(int dictSize, int extraSizeBefore, int extraSizeAfter, int matchLenMax)
        {
            int keepSizeBefore = extraSizeBefore + dictSize;
            int keepSizeAfter = extraSizeAfter + matchLenMax;
            int reserveSize = Math.Min(dictSize / 2 + (256 << 10), 512 << 20);
            return keepSizeBefore + keepSizeAfter + reserveSize;
        }

        /// <summary>
        /// Gets approximate memory usage of the LZEncoder base structure and
        /// the match finder as kibibytes.
        /// </summary>
        public static int GetMemoryUsage(int dictSize, int extraSizeBefore, int extraSizeAfter, int matchLenMax, int mf)
        {
            // Buffer size + a little extra
            int m = GetBufSize(dictSize, extraSizeBefore, extraSizeAfter, matchLenMax) / 1024 + 10;

            switch (mf)
            {
                case MF_HC4:
                    m += HC4.GetMemoryUsage(dictSize);
                    break;

                case MF_BT4:
                    m += BT4.GetMemoryUsage(dictSize);
                    break;

                default:
                    throw new System.ArgumentException();
            }

            return m;
        }

        /// <summary>
        /// Creates a new LZEncoder.
        /// <para>
        /// </para>
        /// </summary>
        /// <param name="dictSize">dictionary size
        /// </param>
        /// <param name="extraSizeBefore">
        /// number of bytes to keep available in the history in addition to dictSize
        /// </param>
        /// <param name="extraSizeAfter">
        /// number of bytes that must be available after current position + matchLenMax
        /// </param>
        /// <param name="niceLen">
        /// if a match of at least <code>niceLen</code> bytes is found, be happy with it and don't stop looking for longer matches
        /// </param>
        /// <param name="matchLenMax">
        /// don't test for matches longer than <code>matchLenMax</code> bytes
        /// </param>
        /// <param name="mf">
        /// match finder ID
        /// </param>
        /// <param name="depthLimit">
        /// match finder search depth limit 
        /// </param>
        public static LZEncoder GetInstance(int dictSize, int extraSizeBefore, int extraSizeAfter, int niceLen, int matchLenMax, int mf, int depthLimit, ArrayCache arrayCache)
        {
            switch (mf)
            {
                case MF_HC4:
                    return new HC4(dictSize, extraSizeBefore, extraSizeAfter, niceLen, matchLenMax, depthLimit, arrayCache);

                case MF_BT4:
                    return new BT4(dictSize, extraSizeBefore, extraSizeAfter, niceLen, matchLenMax, depthLimit, arrayCache);
            }

            throw new System.ArgumentException();
        }

        /// <summary>
        /// Creates a new LZEncoder. See <code>getInstance</code>.
        /// </summary>
        internal LZEncoder(int dictSize, int extraSizeBefore, int extraSizeAfter, int niceLen, int matchLenMax, ArrayCache arrayCache)
        {
            bufSize = GetBufSize(dictSize, extraSizeBefore, extraSizeAfter, matchLenMax);
            buf = arrayCache.GetByteArray(bufSize, false);

            keepSizeBefore = extraSizeBefore + dictSize;
            keepSizeAfter = extraSizeAfter + matchLenMax;

            this.matchLenMax = matchLenMax;
            this.niceLen = niceLen;
        }

        public virtual void PutArraysToCache(ArrayCache arrayCache)
        {
            arrayCache.PutArray(buf);
        }

        /// <summary>
        /// Sets a preset dictionary. If a preset dictionary is wanted, this
        /// function must be called immediately after creating the LZEncoder
        /// before any data has been encoded.
        /// </summary>
        public virtual void SetPresetDict(int dictSize, sbyte[] presetDict)
        {
            Debug.Assert(!IsStarted());
            Debug.Assert(writePos == 0);

            if (presetDict != null)
            {
                // If the preset dictionary buffer is bigger than the dictionary
                // size, copy only the tail of the preset dictionary.
                int copySize = Math.Min(presetDict.Length, dictSize);
                int offset = presetDict.Length - copySize;
                Array.Copy(presetDict, offset, buf, 0, copySize);
                writePos += copySize;
                Skip(copySize);
            }
        }

        /// <summary>
        /// Moves data from the end of the buffer to the beginning, discarding
        /// old data and making space for new input.
        /// </summary>
        private void MoveWindow()
        {
            // Align the move to a multiple of 16 bytes. LZMA2 needs this
            // because it uses the lowest bits from readPos to get the
            // alignment of the uncompressed data.
            int moveOffset = (readPos + 1 - keepSizeBefore) & ~15;
            int moveSize = writePos - moveOffset;
            Array.Copy(buf, moveOffset, buf, 0, moveSize);

            readPos -= moveOffset;
            readLimit -= moveOffset;
            writePos -= moveOffset;
        }

        /// <summary>
        /// Copies new data into the LZEncoder's buffer.
        /// </summary>
        public virtual int FillWindow(byte[] @in, int off, int len)
        {
            Debug.Assert(!finishing);

            // Move the sliding window if needed.
            if (readPos >= bufSize - keepSizeAfter)
            {
                MoveWindow();
            }

            // Try to fill the dictionary buffer. If it becomes full,
            // some of the input bytes may be left unused.
            if (len > bufSize - writePos)
            {
                len = bufSize - writePos;
            }

            Array.Copy(@in, off, buf, writePos, len);
            writePos += len;

            // Set the new readLimit but only if there's enough data to allow
            // encoding of at least one more byte.
            if (writePos >= keepSizeAfter)
            {
                readLimit = writePos - keepSizeAfter;
            }

            ProcessPendingBytes();

            // Tell the caller how much input we actually copied into
            // the dictionary.
            return len;
        }

        /// <summary>
        /// Process pending bytes remaining from preset dictionary initialization
        /// or encoder flush operation.
        /// </summary>
        private void ProcessPendingBytes()
        {
            // After flushing or setting a preset dictionary there will be
            // pending data that hasn't been ran through the match finder yet.
            // Run it through the match finder now if there is enough new data
            // available (readPos < readLimit) that the encoder may encode at
            // least one more input byte. This way we don't waste any time
            // looping in the match finder (and marking the same bytes as
            // pending again) if the application provides very little new data
            // per write call.
            if (pendingSize > 0 && readPos < readLimit)
            {
                readPos -= pendingSize;
                int oldPendingSize = pendingSize;
                pendingSize = 0;
                Skip(oldPendingSize);
                Debug.Assert(pendingSize < oldPendingSize);
            }
        }

        /// <summary>
        /// Returns true if at least one byte has already been run through
        /// the match finder.
        /// </summary>
        public virtual bool IsStarted()
        {
            return readPos != -1;
        }

        /// <summary>
        /// Marks that all the input needs to be made available in
        /// the encoded output.
        /// </summary>
        public virtual void SetFlushing()
        {
            readLimit = writePos - 1;
            ProcessPendingBytes();
        }

        /// <summary>
        /// Marks that there is no more input remaining. The read position
        /// can be advanced until the end of the data.
        /// </summary>
        public void SetFinishing()
        {
            readLimit = writePos - 1;
            finishing = true;
            ProcessPendingBytes();
        }

        /// <summary>
        /// Tests if there is enough input available to let the caller encode
        /// at least one more byte.
        /// </summary>
        public bool HasEnoughData(int alreadyReadLen)
        {
            return readPos - alreadyReadLen < readLimit;
        }

        public void CopyUncompressed(Stream outStream, int backward, int len)
        {
            outStream.Write((byte[])(Array)buf, readPos + 1 - backward, len);
        }

        /// <summary>
        /// Get the number of bytes available, including the current byte.
        /// <p>
        /// Note that the result is undefined if <code>getMatches</code> or
        /// <code>skip</code> hasn't been called yet and no preset dictionary
        /// is being used.
        /// </summary>
        public int GetAvail()
        {
            Debug.Assert(IsStarted());
            return writePos - readPos;
        }

        /// <summary>
        /// Gets the lowest four bits of the absolute offset of the current byte.
        /// Bits other than the lowest four are undefined.
        /// </summary>
        public int GetPos()
        {
            return readPos;
        }

        /// <summary>
        /// Gets the byte from the given backward offset.
        /// <p>
        /// The current byte is at <code>0</code>, the previous byte
        /// at <code>1</code> etc. To get a byte at zero-based distance,
        /// use <code>getByte(dist + 1)<code>.
        /// <p>
        /// This function is equivalent to <code>getByte(0, backward)</code>.
        /// </summary>
        public int GetByte(int backward)
        {
            return buf[readPos - backward] & 0xFF;
        }

        /// <summary>
        /// Gets the byte from the given forward minus backward offset.
        /// The forward offset is added to the current position. This lets
        /// one read bytes ahead of the current byte.
        /// </summary>
        public int GetByte(int forward, int backward)
        {
            return buf[readPos + forward - backward] & 0xFF;
        }

        /// <summary>
        /// Get the length of a match at the given distance.
        /// </summary>
        /// <param name="dist">
        /// zero-based distance of the match to test 
        /// </param>
        /// <param name="lenLimit">
        /// don't test for a match longer than this 
        /// </param>
        /// <returns>length of the match; it is in the range [0, lenLimit]</returns>
        public int GetMatchLen(int dist, int lenLimit)
        {
            int backPos = readPos - dist - 1;
            int len = 0;

            while (len < lenLimit && buf[readPos + len] == buf[backPos + len])
                ++len;

            return len;
        }

        /// <summary>
        /// Get the length of a match at the given distance and forward offset.
        /// </summary>
        /// <param name="forward">
        /// forward offset 
        /// </param>
        /// <param name="dist">
        /// zero-based distance of the match to test 
        /// </param>
        /// <param name="lenLimit">
        /// don't test for a match longer than this 
        /// </param>
        /// <returns>length of the match; it is in the range [0, lenLimit]</returns>
        public int GetMatchLen(int forward, int dist, int lenLimit)
        {
            int curPos = readPos + forward;
            int backPos = curPos - dist - 1;
            int len = 0;

            while (len < lenLimit && buf[curPos + len] == buf[backPos + len])
                ++len;

            return len;
        }

        /// <summary>
        /// Verifies that the matches returned by the match finder are valid.
        /// This is meant to be used in an assert statement. This is totally
        /// useless for actual encoding since match finder's results should
        /// naturally always be valid if it isn't broken.
        /// </summary>
        /// <param name="matches">
        /// return value from <code>getMatches</code> 
        /// </param>
        /// <returns>true if matches are valid, false if match finder is broken</returns>
        public bool VerifyMatches(Matches matches)
        {
            int lenLimit = Math.Min(GetAvail(), matchLenMax);

            for (int i = 0; i < matches.count; ++i)
                if (GetMatchLen(matches.dist[i], lenLimit) != matches.len[i])
                    return false;

            return true;
        }

        /// <summary>
        /// Moves to the next byte, checks if there is enough input available,
        /// and returns the amount of input available.
        /// </summary>
        /// <param name="requiredForFlushing">
        /// minimum number of available bytes when flushing; encoding may be continued with new input after flushing
        /// </param>
        /// <param name="requiredForFinishing">
        /// minimum number of available bytes when finishing; encoding must not be continued after finishing or the match finder state may be corrupt
        /// </param>
        /// <returns>the number of bytes available or zero if there is not enough input available</returns>
        protected int MovePos(int requiredForFlushing, int requiredForFinishing)
        {
            Debug.Assert(requiredForFlushing >= requiredForFinishing);

            ++readPos;
            int avail = writePos - readPos;

            if (avail < requiredForFlushing)
            {
                if (avail < requiredForFinishing || !finishing)
                {
                    ++pendingSize;
                    avail = 0;
                }
            }

            return avail;
        }

        /// <summary>
        /// Runs match finder for the next byte and returns the matches found.
        /// </summary>
        public abstract Matches GetMatches();

        /// <summary>
        /// Skips the given number of bytes in the match finder.
        /// </summary>
        public abstract void Skip(int len);
    }
}
