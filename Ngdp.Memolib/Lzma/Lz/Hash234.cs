﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma.Lz
{
    internal sealed class Hash234 : CRC32Hash
    {
        private static readonly int HASH_2_SIZE = 1 << 10;
        private static readonly int HASH_2_MASK = HASH_2_SIZE - 1;

        private static readonly int HASH_3_SIZE = 1 << 16;
        private static readonly int HASH_3_MASK = HASH_3_SIZE - 1;

        private readonly int hash4Mask;

        private readonly int[] hash2Table;
        private readonly int[] hash3Table;
        private readonly int[] hash4Table;
        private readonly int hash4Size;

        private int hash2Value = 0;
        private int hash3Value = 0;
        private int hash4Value = 0;

        internal static int GetHash4Size(int dictSize)
        {
            uint h = (uint)(dictSize - 1);
            h |= (h >> 1);
            h |= (h >> 2);
            h |= (h >> 4);
            h |= (h >> 8);
            h = (h >> 1);
            h |= 0xFFFF;
            if (h > (1 << 24))
            {
                h = h >> 1;
            }

            return (int)(h + 1);
        }

        internal static int GetMemoryUsage(int dictSize)
        {
            // Sizes of the hash arrays + a little extra
            return (HASH_2_SIZE + HASH_3_SIZE + GetHash4Size(dictSize)) / (1024 / 4) + 4;
        }

        internal Hash234(int dictSize, ArrayCache arrayCache)
        {
            hash2Table = arrayCache.GetIntArray(HASH_2_SIZE, true);
            hash3Table = arrayCache.GetIntArray(HASH_3_SIZE, true);

            hash4Size = GetHash4Size(dictSize);
            hash4Table = arrayCache.GetIntArray(hash4Size, true);
            hash4Mask = hash4Size - 1;
        }

        internal void PutArraysToCache(ArrayCache arrayCache)
        {
            arrayCache.PutArray(hash4Table);
            arrayCache.PutArray(hash3Table);
            arrayCache.PutArray(hash2Table);
        }

        internal void CalcHashes(byte[] buf, int off)
        {
            int temp = (int)(crcTable[buf[off] & 0xFF] ^ (buf[off + 1] & 0xFF));
            hash2Value = temp & HASH_2_MASK;

            temp ^= ((buf[off + 2] & 0xFF) << 8);
            hash3Value = temp & HASH_3_MASK;

            temp ^= (int)crcTable[buf[off + 3] & 0xFF] << 5;
            hash4Value = temp & hash4Mask;
        }

        internal int Hash2Pos
        {
            get
            {
                return hash2Table[hash2Value];
            }
        }

        internal int Hash3Pos
        {
            get
            {
                return hash3Table[hash3Value];
            }
        }

        internal int Hash4Pos
        {
            get
            {
                return hash4Table[hash4Value];
            }
        }

        internal void UpdateTables(int pos)
        {
            hash2Table[hash2Value] = pos;
            hash3Table[hash3Value] = pos;
            hash4Table[hash4Value] = pos;
        }

        internal void Normalize(int normalizationOffset)
        {
            LZEncoder.Normalize(hash2Table, HASH_2_SIZE, normalizationOffset);
            LZEncoder.Normalize(hash3Table, HASH_3_SIZE, normalizationOffset);
            LZEncoder.Normalize(hash4Table, hash4Size, normalizationOffset);
        }
    }
}
