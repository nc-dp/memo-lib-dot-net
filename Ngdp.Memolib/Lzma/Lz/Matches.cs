﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma.Lz
{
    public sealed class Matches
    {
        public readonly int[] len;
        public readonly int[] dist;
        public int count = 0;

        internal Matches(int countMax)
        {
            len = new int[countMax];
            dist = new int[countMax];
        }
    }
}
