﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma.Lz
{
    public sealed class LZDecoder
    {
        private readonly byte[] buf;
        private readonly int bufSize; // To avoid buf.length with an array-cached buf.
        private int start = 0;
        private int pos = 0;
        private int full = 0;
        private int limit = 0;
        private int pendingLen = 0;
        private int pendingDist = 0;

        public LZDecoder(int dictSize, byte[] presetDict, ArrayCache arrayCache)
        {
            bufSize = dictSize;
            buf = arrayCache.GetByteArray(bufSize, false);

            if (presetDict != null)
            {
                pos = Math.Min(presetDict.Length, dictSize);
                full = pos;
                start = pos;
                Array.Copy(presetDict, presetDict.Length - pos, buf, 0, pos);
            }
        }

        public void PutArraysToCache(ArrayCache arrayCache)
        {
            arrayCache.PutArray(buf);
        }

        public void Reset()
        {
            start = 0;
            pos = 0;
            full = 0;
            limit = 0;
            buf[bufSize - 1] = 0x00;
        }

        public int Limit
        {
            set
            {
                if (bufSize - pos <= value)
                {
                    limit = bufSize;
                }
                else
                {
                    limit = pos + value;
                }
            }
        }

        public bool HasSpace()
        {
            return pos < limit;
        }

        public bool HasPending()
        {
            return pendingLen > 0;
        }

        public int Pos
        {
            get
            {
                return pos;
            }
        }

        public int GetByte(int dist)
        {
            int offset = pos - dist - 1;
            if (dist >= pos)
            {
                offset += bufSize;
            }

            return buf[offset] & 0xFF;
        }

        public void PutByte(byte b)
        {
            buf[pos++] = b;

            if (full < pos)
            {
                full = pos;
            }
        }

        public void Repeat(int dist, int len)
        {
            if (dist < 0 || dist >= full)
            {
                throw new CorruptedInputException();
            }

            int left = Math.Min(limit - pos, len);
            pendingLen = len - left;
            pendingDist = dist;

            int back = pos - dist - 1;
            if (back < 0)
            {
                // The distance wraps around to the end of the cyclic dictionary
                // buffer. We cannot get here if the dictionary isn't full.
                Debug.Assert(full == bufSize);
                back += bufSize;

                // Here we will never copy more than dist + 1 bytes and
                // so the copying won't repeat from its own output.
                // Thus, we can always use arraycopy safely.
                int copySize = Math.Min(bufSize - back, left);
                Debug.Assert(copySize <= dist + 1);

                Array.Copy(buf, back, buf, pos, copySize);
                pos += copySize;
                back = 0;
                left -= copySize;

                if (left == 0)
                {
                    return;
                }
            }

            Debug.Assert(back < pos);
            Debug.Assert(left > 0);

            do
            {
                // Determine the number of bytes to copy on this loop iteration:
                // copySize is set so that the source and destination ranges
                // don't overlap. If "left" is large enough, the destination
                // range will start right after the last byte of the source
                // range. This way we don't need to advance "back" which
                // allows the next iteration of this loop to copy (up to)
                // twice the number of bytes.
                int copySize = Math.Min(left, pos - back);
                Array.Copy(buf, back, buf, pos, copySize);
                pos += copySize;
                left -= copySize;
            } while (left > 0);

            if (full < pos)
            {
                full = pos;
            }
        }

        public void RepeatPending()
        {
            if (pendingLen > 0)
            {
                Repeat(pendingDist, pendingLen);
            }
        }

        public void CopyUncompressed(BinaryReader inData, int len)
        {
            int copySize = Math.Min(bufSize - pos, len);
            inData.Read(buf, pos, copySize);
            pos += copySize;

            if (full < pos)
            {
                full = pos;
            }
        }

        public int Flush(byte[] @out, int outOff)
        {
            int copySize = pos - start;
            if (pos == bufSize)
            {
                pos = 0;
            }

            Array.Copy(buf, start, @out, outOff, copySize);
            start = pos;

            return copySize;
        }
    }
}
