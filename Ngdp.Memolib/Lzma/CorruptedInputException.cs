﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Dk.Digst.Digital.Post.Memolib.Lzma
{
    public class CorruptedInputException : IOException
    {
        /// <summary>
        /// Creates a new CorruptedInputException with
        /// the default error detail message.
        /// </summary>
        public CorruptedInputException() : base("Compressed data is corrupt")
        {
        }

        /// <summary>
        /// Creates a new CorruptedInputException with
        /// the specified error detail message.
        /// </summary>
        /// <param name="s">           error detail message </param>
        public CorruptedInputException(string s) : base(s)
        {
        }
    }
}
