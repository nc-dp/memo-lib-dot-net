﻿namespace Dk.Digst.Digital.Post.Memolib.Util
{
    public delegate void XmlStreamConsumer<in T>(T t);
}