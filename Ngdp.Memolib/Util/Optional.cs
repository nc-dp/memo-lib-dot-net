﻿using System;

namespace Dk.Digst.Digital.Post.Memolib.Util
{
    public class Optional<T>
    {
        private readonly T _data;

        private readonly bool _isPresent;

        private Optional(T data)
        {
            _data = data;
            _isPresent = true;
        }

        private Optional()
        {
        }

        public static Optional<T> Of(T value)
        {
            return new Optional<T>(value);
        }

        public static Optional<T> Empty()
        {
            return new Optional<T>();
        }

        public bool IsPresent()
        {
            return _isPresent;
        }

        public bool IsEmpty()
        {
            return !_isPresent;
        }

        public T Get()
        {
            if (!_isPresent) throw new InvalidOperationException("No data is present");
            return _data;
        }
    }
}