﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Dk.Digst.Digital.Post.Memolib.Util
{
    public static class ReflectionUtil
    {
        public static IEnumerable<T> GetFieldValues<T>(Type type)
        {
            return type.GetFields(BindingFlags.Public | BindingFlags.Static)
                .Select(field => (T) field.GetValue(null));
        }
    }
}