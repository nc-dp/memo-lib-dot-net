﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using Dk.Digst.Digital.Post.Memolib.Exception;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Properties;

namespace Dk.Digst.Digital.Post.Memolib.Util
{
    public static class MemoVersionUtil
    {
        /// <summary>
        /// Takes a seekable stream and returns the MeMo version.
        /// </summary>
        /// <remarks>Only reads the first X bytes, configurable in <c>MemoLibConfiguration.ReadWindowSize</c>.</remarks>
        /// <param name="stream"></param>
        /// <returns>MeMo Version</returns>
        /// <exception cref="MeMoParseException"></exception>
        public static MemoVersion ReadMemoVersion(Stream stream)
        {
            if (!stream.CanSeek)
            {
                throw new MeMoParseException("Unable to determine MeMo Version without seekable stream.");
            }

            var buffer = PeekBytes(stream, MemoLibConfiguration.ReadWindowSize);

            string bufferString = System.Text.Encoding.Default.GetString(buffer);
            var match = MEMO_VERSION_REGEX.Match(bufferString);

            if (match.Success)
            {
                return MemoVersionMetadata.FromString(match.Groups[1].Value);
            }

            return MemoVersion.MeMoVersionUnknown;
        }

        public static readonly Regex MEMO_VERSION_REGEX = new Regex(@"memoVersion=""(\d+\.\d+)""");

        private static byte[] PeekBytes(Stream stream, int count)
        {
            byte[] buffer = new byte[count];
            long originalPosition = stream.Position;

            for (int i = 0; i < count; i++)
            {
                int peekedByte = stream.ReadByte();
                if (peekedByte == -1)
                    break;
                buffer[i] = (byte)peekedByte;
            }

            stream.Position = originalPosition;
            return buffer;
        }
    }
}