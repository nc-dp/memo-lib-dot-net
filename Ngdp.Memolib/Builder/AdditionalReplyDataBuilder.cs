﻿using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class AdditionalReplyDataBuilder
    {
        private string _label;

        private string _value;

        public static AdditionalReplyDataBuilder NewBuilder()
        {
            return new AdditionalReplyDataBuilder();
        }

        public AdditionalReplyDataBuilder Label(string label)
        {
            _label = label;
            return this;
        }

        public AdditionalReplyDataBuilder Value(string value)
        {
            _value = value;
            return this;
        }

        public AdditionalReplyData Build()
        {
            return new AdditionalReplyData
            {
                label = _label,
                value = _value
            };
        }
    }
}
