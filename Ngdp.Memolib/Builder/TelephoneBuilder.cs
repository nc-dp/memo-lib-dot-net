using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class TelephoneBuilder
    {
        private string _relatedAgent;

        private string _telephoneNumber;

        public static TelephoneBuilder NewBuilder()
        {
            return new TelephoneBuilder();
        }

        public TelephoneBuilder TelephoneNumber(string telephoneNumber)
        {
            _telephoneNumber = telephoneNumber;
            return this;
        }

        public TelephoneBuilder RelatedAgent(string relatedAgent)
        {
            _relatedAgent = relatedAgent;
            return this;
        }

        public Telephone Build()
        {
            return new Telephone
            {
                telephoneNumber = _telephoneNumber,
                relatedAgent = _relatedAgent
            };
        }
    }
}