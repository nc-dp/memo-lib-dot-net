using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class GeneratingSystemBuilder
    {
        private string _generatingSystemID;

        private string _label;

        public static GeneratingSystemBuilder NewBuilder()
        {
            return new GeneratingSystemBuilder();
        }

        public GeneratingSystemBuilder GeneratingSystemID(string generatingSystemID)
        {
            _generatingSystemID = generatingSystemID;
            return this;
        }

        public GeneratingSystemBuilder Label(string label)
        {
            _label = label;
            return this;
        }

        public GeneratingSystem Build()
        {
            return new GeneratingSystem
            {
                generatingSystemID = _generatingSystemID,
                label = _label
            };
        }
    }
}