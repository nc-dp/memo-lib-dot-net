using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class SenderBuilder
    {
        private AttentionData _attentionData;

        private Representative _representative;

        private ContactPoint _contactPoint;

        private string _idType;

        private string _idTypeLabel;

        private string _label;

        private string _senderId;

        public static SenderBuilder NewBuilder()
        {
            return new SenderBuilder();
        }

        public SenderBuilder SenderId(string senderId)
        {
            _senderId = senderId;
            return this;
        }

        public SenderBuilder IdType(string idType)
        {
            _idType = idType;
            return this;
        }

        public SenderBuilder IdTypeLabel(string idTypeLabel)
        {
            _idTypeLabel = idTypeLabel;
            return this;
        }

        public SenderBuilder Label(string label)
        {
            _label = label;
            return this;
        }

        public SenderBuilder AttentionData(AttentionData attentionData)
        {
            _attentionData = attentionData;
            return this;
        }

        public SenderBuilder Representative(Representative representative)
        {
            _representative = representative;
            return this;
        }

        public SenderBuilder ContactPoint(ContactPoint contactPoint)
        {
            _contactPoint = contactPoint;
            return this;
        }

        public Sender Build()
        {
            return new Sender
            {
                senderID = _senderId,
                idType = _idType,
                idTypeLabel = _idTypeLabel,
                label = _label,
                AttentionData = _attentionData,
                Representative = _representative,
                ContactPoint = _contactPoint
            };
        }
    }
}