using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class ContactInfoBuilder
    {
        private string _label;

        private string _value;

        public static ContactInfoBuilder NewBuilder()
        {
            return new ContactInfoBuilder();
        }

        public ContactInfoBuilder Label(string label)
        {
            _label = label;
            return this;
        }

        public ContactInfoBuilder Value(string value)
        {
            _value = value;
            return this;
        }

        public ContactInfo Build()
        {
            return new ContactInfo
            {
                label = _label,
                value = _value
            };
        }
    }
}