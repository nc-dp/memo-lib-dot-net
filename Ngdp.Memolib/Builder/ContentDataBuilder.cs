using System.Collections.Generic;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class ContentDataBuilder
    {
        private List<AdditionalContentData> _additionalContentData = new List<AdditionalContentData>();

        private Address _address;

        private CaseID _caseID;

        private CPRdata _cprData;

        private CVRdata _cvrData;

        private Education _education;

        private FORMdata _formData;

        private KLEdata _kleData;

        private MotorVehicle _motorVehicle;

        private ProductionUnit _productionUnit;

        private PropertyNumber _propertyNumber;

        private UnstructuredAddress _unstructuredAddress;

        public static ContentDataBuilder NewBuilder()
        {
            return new ContentDataBuilder();
        }

        public ContentDataBuilder CprData(CPRdata cprData)
        {
            _cprData = cprData;
            return this;
        }

        public ContentDataBuilder CvrData(CVRdata cvrData)
        {
            _cvrData = cvrData;
            return this;
        }

        public ContentDataBuilder MotorVehicle(MotorVehicle motorVehicle)
        {
            _motorVehicle = motorVehicle;
            return this;
        }

        public ContentDataBuilder PropertyNumber(PropertyNumber propertyNumber)
        {
            _propertyNumber = propertyNumber;
            return this;
        }

        public ContentDataBuilder CaseID(CaseID caseId)
        {
            _caseID = caseId;
            return this;
        }

        public ContentDataBuilder KleData(KLEdata kleData)
        {
            _kleData = kleData;
            return this;
        }

        public ContentDataBuilder FormData(FORMdata formData)
        {
            _formData = formData;
            return this;
        }

        public ContentDataBuilder ProductionUnit(ProductionUnit productionUnit)
        {
            _productionUnit = productionUnit;
            return this;
        }

        public ContentDataBuilder Education(Education education)
        {
            _education = education;
            return this;
        }

        public ContentDataBuilder Address(Address address)
        {
            _address = address;
            return this;
        }

        public ContentDataBuilder UnstructuredAddress(UnstructuredAddress unstructuredAddress)
        {
            _unstructuredAddress = unstructuredAddress;
            return this;
        }

        public ContentDataBuilder AdditionalContentData(
            List<AdditionalContentData> additionalContentData)
        {
            _additionalContentData = additionalContentData;
            return this;
        }

        public ContentDataBuilder AddAdditionalContentData(AdditionalContentData additionalContentData)
        {
            _additionalContentData.Add(additionalContentData);
            return this;
        }

        public ContentData Build()
        {
            return new ContentData
            {
                CPRdata = _cprData,
                CVRdata = _cvrData,
                MotorVehicle = _motorVehicle,
                PropertyNumber = _propertyNumber,
                CaseID = _caseID,
                KLEdata = _kleData,
                FORMdata = _formData,
                ProductionUnit = _productionUnit,
                Education = _education,
                Address = _address,
                UnstructuredAddress = _unstructuredAddress,
                AdditionalContentData = _additionalContentData.ToArray()
            };
        }
    }
}