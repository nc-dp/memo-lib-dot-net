using System.Collections.Generic;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class TechnicalDocumentBuilder
    {
        private string _documentId;

        private List<File> _file = new List<File>();

        private string _label;

        public static TechnicalDocumentBuilder NewBuilder()
        {
            return new TechnicalDocumentBuilder();
        }

        public TechnicalDocumentBuilder DocumentId(string documentId)
        {
            _documentId = documentId;
            return this;
        }

        public TechnicalDocumentBuilder Label(string label)
        {
            _label = label;
            return this;
        }

        public TechnicalDocumentBuilder File(List<File> file)
        {
            _file = file;
            return this;
        }

        public TechnicalDocumentBuilder AddFile(File file)
        {
            _file.Add(file);
            return this;
        }

        public TechnicalDocument Build()
        {
            return new TechnicalDocument
            {
                technicalDocumentID = _documentId,
                label = _label,
                File = _file.ToArray()
            };
        }
    }
}