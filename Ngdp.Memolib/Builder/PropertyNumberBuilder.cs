using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class PropertyNumberBuilder
    {
        private string _propertyNumber;

        public static PropertyNumberBuilder NewBuilder()
        {
            return new PropertyNumberBuilder();
        }

        public PropertyNumberBuilder PropertyNumber(string propertyNumber)
        {
            _propertyNumber = propertyNumber;
            return this;
        }

        public PropertyNumber Build()
        {
            return new PropertyNumber {propertyNumber = _propertyNumber};
        }
    }
}