using System;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class FileContentBuilder
    {
        private bool _base64Encoded;

        private string _content;

        private string _location;

        public static FileContentBuilder NewBuilder()
        {
            return new FileContentBuilder();
        }

        public FileContentBuilder Base64Content(string content)
        {
            _location = null;
            _content = content;
            _base64Encoded = true;
            return this;
        }

        public FileContentBuilder Location(string location)
        {
            _content = null;
            _location = location;
            _base64Encoded = false;
            return this;
        }

        public IFileContent Build()
        {
            if (_content != null)
                return new IncludedFileContent(_content, _base64Encoded);
            if (_location != null)
                return new ExternalFileContent(
                    _location, _base64Encoded);
            throw new InvalidOperationException("Either content or file path must be provided.");
        }
    }
}