using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class SeNumberBuilder
    {
        private string _companyName;

        private string _seNumber;

        public static SeNumberBuilder NewBuilder()
        {
            return new SeNumberBuilder();
        }

        public SeNumberBuilder SeNumber(string seNumber)
        {
            _seNumber = seNumber;
            return this;
        }

        public SeNumberBuilder CompanyName(string companyName)
        {
            _companyName = companyName;
            return this;
        }

        public SEnumber Build()
        {
            return new SEnumber
            {
                seNumber = _seNumber,
                companyName = _companyName
            };
        }
    }
}