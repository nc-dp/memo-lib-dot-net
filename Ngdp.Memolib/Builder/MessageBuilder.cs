using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class MessageBuilder
    {
        private MessageBody _messageBody;

        private MessageHeader _messageHeader;
        private Model.MemoVersion _memoVersion = Model.MemoVersion.MemoVersion11;
        private string _memoSchVersion = Model.MemoSchVersion.Name;

        public static MessageBuilder NewBuilder()
        {
            return new MessageBuilder();
        }

        public MessageBuilder MessageHeader(MessageHeader messageHeader)
        {
            _messageHeader = messageHeader;
            return this;
        }

        public MessageBuilder MessageBody(MessageBody messageBody)
        {
            _messageBody = messageBody;
            return this;
        }

        public MessageBuilder MemoVersion(MemoVersion memoVersion)
        {
            _memoVersion = memoVersion;
            return this;
        }

        public MessageBuilder MemoSchVersion(string memoSchVersion)
        {
            _memoSchVersion = memoSchVersion;
            return this;
        }

        public Message Build()
        {
            return new Message
            {
                memoVersion = MemoVersionMetadata.GetDecimalValue(_memoVersion),
                memoSchVersion = _memoVersion == Model.MemoVersion.MemoVersion11 ? _memoSchVersion : null,
                MessageHeader = _messageHeader,
                MessageBody = _messageBody
            };
        }
    }
}