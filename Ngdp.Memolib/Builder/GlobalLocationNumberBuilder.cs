using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class GlobalLocationNumberBuilder
    {
        private long _globalLocationNumber;

        private string _location;

        public static GlobalLocationNumberBuilder NewBuilder()
        {
            return new GlobalLocationNumberBuilder();
        }

        public GlobalLocationNumberBuilder GlobalLocationNumber(long globalLocationNumber)
        {
            _globalLocationNumber = globalLocationNumber;
            return this;
        }

        public GlobalLocationNumberBuilder Location(string location)
        {
            _location = location;
            return this;
        }

        public GlobalLocationNumber Build()
        {
            return new GlobalLocationNumber
            {
                globalLocationNumber = _globalLocationNumber,
                location = _location
            };
        }
    }
}