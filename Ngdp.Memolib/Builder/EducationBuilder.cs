﻿using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class EducationBuilder
    {
        private string _educationCode;

        private string _educationName;

        public static EducationBuilder NewBuilder()
        {
            return new EducationBuilder();
        }

        public EducationBuilder EducationCode(string educationCode)
        {
            _educationCode = educationCode;
            return this;
        }

        public EducationBuilder EducationName(string educationName)
        {
            _educationName = educationName;
            return this;
        }

        public Education Build()
        {
            return new Education
            {
                educationCode = _educationCode,
                educationName = _educationName
            };
        }
    }
}