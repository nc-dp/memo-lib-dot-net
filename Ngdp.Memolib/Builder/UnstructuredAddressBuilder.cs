using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class UnstructuredAddressBuilder
    {
        private string _unstructured;

        public static UnstructuredAddressBuilder NewBuilder()
        {
            return new UnstructuredAddressBuilder();
        }

        public UnstructuredAddressBuilder Unstructured(string unstructured)
        {
            _unstructured = unstructured;
            return this;
        }

        public UnstructuredAddress Build()
        {
            return new UnstructuredAddress {unstructured = _unstructured};
        }
    }
}