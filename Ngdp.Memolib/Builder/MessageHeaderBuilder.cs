using System;
using System.Collections.Generic;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class MessageHeaderBuilder
    {
        private string _additionalNotification;

        private ContentData _contentData;

        private DateTime _doNotDeliverUntilDate;

        private bool _doNotDeliverUntilDateSpecified;

        private ForwardData _forwardData;

        private string _label;

        private bool _legalNotification;

        private bool _mandatory;

        private string _messageCode;

        private string _messageID;

        private memoMessageType _messageType;

        private Guid _messageUUID;

        private string _notification;

        private Recipient _recipient;

        private bool _reply;

        private DateTime _replyByDateTime;

        private bool _replyByDateTimeSpecified;

        private List<ReplyData> _replyData = new List<ReplyData>();

        private Sender _sender;

        private string _postType;

        public static MessageHeaderBuilder NewBuilder()
        {
            return new MessageHeaderBuilder();
        }

        public MessageHeaderBuilder MessageType(memoMessageType messageType)
        {
            _messageType = messageType;
            return this;
        }

        public MessageHeaderBuilder MessageUUID(Guid messageUUID)
        {
            _messageUUID = messageUUID;
            return this;
        }

        public MessageHeaderBuilder MessageID(string messageID)
        {
            _messageID = messageID;
            return this;
        }

        public MessageHeaderBuilder MessageCode(string messageCode)
        {
            _messageCode = messageCode;
            return this;
        }

        public MessageHeaderBuilder Label(string label)
        {
            _label = label;
            return this;
        }

        public MessageHeaderBuilder Notification(string notification)
        {
            _notification = notification;
            return this;
        }

        public MessageHeaderBuilder AdditionalNotification(string additionalNotification)
        {
            _additionalNotification = additionalNotification;
            return this;
        }

        public MessageHeaderBuilder Reply(bool reply)
        {
            _reply = reply;
            return this;
        }

        public MessageHeaderBuilder ReplyByDateTime(DateTime replyByDateTime)
        {
            _replyByDateTime = replyByDateTime;
            _replyByDateTimeSpecified = true;
            return this;
        }

        public MessageHeaderBuilder DoNotDeliverUntilDate(DateTime doNotDeliverUntilDate)
        {
            _doNotDeliverUntilDate = doNotDeliverUntilDate;
            _doNotDeliverUntilDateSpecified = true;
            return this;
        }

        public MessageHeaderBuilder Mandatory(bool mandatory)
        {
            _mandatory = mandatory;
            return this;
        }

        public MessageHeaderBuilder LegalNotification(bool legalNotification)
        {
            _legalNotification = legalNotification;
            return this;
        }

        public MessageHeaderBuilder PostType(string postType)
        {
            _postType = postType;
            return this;
        }


        public MessageHeaderBuilder Sender(Sender sender)
        {
            _sender = sender;
            return this;
        }

        public MessageHeaderBuilder Recipient(Recipient recipient)
        {
            _recipient = recipient;
            return this;
        }

        public MessageHeaderBuilder ContentData(ContentData contentData)
        {
            _contentData = contentData;
            return this;
        }

        public MessageHeaderBuilder ForwardData(ForwardData forwardData)
        {
            _forwardData = forwardData;
            return this;
        }

        public MessageHeaderBuilder ReplyData(List<ReplyData> replyData)
        {
            _replyData = replyData;
            return this;
        }

        public MessageHeaderBuilder AddReplyData(ReplyData replyData)
        {
            _replyData.Add(replyData);
            return this;
        }

        public MessageHeader Build()
        {
            return new MessageHeader
            {
                messageType = _messageType,
                messageUUID = _messageUUID.ToString(),
                messageID = _messageID,
                messageCode = _messageCode,
                label = _label,
                notification = _notification,
                additionalNotification = _additionalNotification,
                reply = _reply,
                replyByDateTime = _replyByDateTime.ToUniversalTime(),
                replyByDateTimeSpecified = _replyByDateTimeSpecified,
                doNotDeliverUntilDate = _doNotDeliverUntilDate,
                doNotDeliverUntilDateSpecified = _doNotDeliverUntilDateSpecified,
                mandatory = _mandatory,
                legalNotification = _legalNotification,
                postType = _postType,
                Sender = _sender,
                Recipient = _recipient,
                ContentData = _contentData,
                ForwardData = _forwardData,
                ReplyData = _replyData.ToArray()
            };
        }
    }
}