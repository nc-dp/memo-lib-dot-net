using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class SorDataBuilder
    {
        private string _sorIdentifier;

        private string _entryName;

        public static SorDataBuilder NewBuilder()
        {
            return new SorDataBuilder();
        }

        public SorDataBuilder SorIdentifier(string sorIdentifier)
        {
            _sorIdentifier = sorIdentifier;
            return this;
        }

        public SorDataBuilder EntryName(string entryName)
        {
            _entryName = entryName;
            return this;
        }

        public SORdata Build()
        {
            return new SORdata
            {
                sorIdentifier = _sorIdentifier,
                entryName = _entryName
            };
        }
    }
}