using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class ProductionUnitBuilder
    {
        private string _productionUnitName;

        private int _productionUnitNumber;

        public static ProductionUnitBuilder NewBuilder()
        {
            return new ProductionUnitBuilder();
        }

        public ProductionUnitBuilder ProductionUnitNumber(int productionUnitNumber)
        {
            _productionUnitNumber = productionUnitNumber;
            return this;
        }

        public ProductionUnitBuilder ProductionUnitName(string productionUnitName)
        {
            _productionUnitName = productionUnitName;
            return this;
        }

        public ProductionUnit Build()
        {
            return new ProductionUnit
            {
                productionUnitNumber = _productionUnitNumber,
                productionUnitName = _productionUnitName
            };
        }
    }
}