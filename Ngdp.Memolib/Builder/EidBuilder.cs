using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class EidBuilder
    {
        private string _label;

        private string _eid;

        public static EidBuilder NewBuilder()
        {
            return new EidBuilder();
        }

        public EidBuilder Eid(string eid)
        {
            _eid = eid;
            return this;
        }

        public EidBuilder Label(string label)
        {
            _label = label;
            return this;
        }

        public EID Build()
        {
            return new EID{eID = _eid, label = _label};
        }
    }
}