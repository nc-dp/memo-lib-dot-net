using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class AttentionPersonBuilder
    {
        private string _label;

        private string _personID;

        public static AttentionPersonBuilder NewBuilder()
        {
            return new AttentionPersonBuilder();
        }

        public AttentionPersonBuilder PersonID(string personID)
        {
            _personID = personID;
            return this;
        }

        public AttentionPersonBuilder Label(string label)
        {
            _label = label;
            return this;
        }

        public AttentionPerson Build()
        {
            return new AttentionPerson
            {
                personID = _personID,
                label = _label
            };
        }
    }
}