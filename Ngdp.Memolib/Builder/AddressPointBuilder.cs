using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class AddressPointBuilder
    {
        private string _geographicHeightMeasure;
        private string _geographicEastingMeasure;
        private string _geographicNorthingMeasure;

        public static AddressPointBuilder NewBuilder()
        {
            return new AddressPointBuilder();
        }

        public AddressPointBuilder GeographicHeightMeasure(string geographicHeightMeasure)
        {
            _geographicHeightMeasure = geographicHeightMeasure;
            return this;
        }

        public AddressPointBuilder GeographicNorthingMeasure(string geographicNorthingMeasure)
        {
            _geographicNorthingMeasure = geographicNorthingMeasure;
            return this;
        }

        public AddressPointBuilder GeographicEastingMeasure(string geographicEastingMeasure)
        {
            _geographicEastingMeasure = geographicEastingMeasure;
            return this;
        }

        public AddressPoint Build()
        {
            return new AddressPoint
            {
                geographicHeightMeasure = _geographicHeightMeasure,
                geographicEastingMeasure = _geographicEastingMeasure,
                geographicNorthingMeasure = _geographicNorthingMeasure
            };
        }
    }
}