using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class AddressBuilder
    {
        private string _addressLabel;

        private AddressPoint _addressPoint;

        private string _city;

        private string _co;

        private string _country;

        private string _door;

        private string _floor;

        private string _houseNumber;

        private string _id;

        private string _zipCode;

        public static AddressBuilder NewBuilder()
        {
            return new AddressBuilder();
        }

        public AddressBuilder Id(string id)
        {
            _id = id;
            return this;
        }

        public AddressBuilder AddressLabel(string addressLabel)
        {
            _addressLabel = addressLabel;
            return this;
        }

        public AddressBuilder HouseNumber(string houseNumber)
        {
            _houseNumber = houseNumber;
            return this;
        }

        public AddressBuilder Door(string door)
        {
            _door = door;
            return this;
        }

        public AddressBuilder Floor(string floor)
        {
            _floor = floor;
            return this;
        }

        public AddressBuilder Co(string co)
        {
            _co = co;
            return this;
        }

        public AddressBuilder ZipCode(string zipCode)
        {
            _zipCode = zipCode;
            return this;
        }

        public AddressBuilder City(string city)
        {
            _city = city;
            return this;
        }

        public AddressBuilder Country(string country)
        {
            _country = country;
            return this;
        }

        public AddressBuilder AddressPoint(AddressPoint addressPoint)
        {
            _addressPoint = addressPoint;
            return this;
        }

        public Address Build()
        {
            return new Address
            {
                id = _id,
                addressLabel = _addressLabel,
                houseNumber = _houseNumber,
                door = _door,
                floor = _floor,
                co = _co,
                zipCode = _zipCode,
                city = _city,
                country = _country,
                AddressPoint = _addressPoint
            };
        }
    }
}