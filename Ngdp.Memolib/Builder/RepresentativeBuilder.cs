﻿using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class RepresentativeBuilder
    {
        private string _idType;

        private string _idTypeLabel;

        private string _label;

        private string _representativeID;

        public static RepresentativeBuilder NewBuilder()
        {
            return new RepresentativeBuilder();
        }

        public RepresentativeBuilder RepresentativeID(string representativeID)
        {
            _representativeID = _representativeID;
            return this;
        }

        public RepresentativeBuilder IdType(string idType)
        {
            _idType = idType;
            return this;
        }

        public RepresentativeBuilder IdTypeLabel(string idTypeLabel)
        {
            _idTypeLabel = idTypeLabel;
            return this;
        }

        public RepresentativeBuilder Label(string label)
        {
            _label = label;
            return this;
        }

        public Representative Build()
        {
            return new Representative
            {
                representativeID = _representativeID,
                idType = _idType,
                idTypeLabel = _idTypeLabel,
                label = _label,
            };
        }
    }
}