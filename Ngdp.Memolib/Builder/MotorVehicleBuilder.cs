using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class MotorVehicleBuilder
    {
        private string _chassisNumber;

        private string _licenseNumber;

        public static MotorVehicleBuilder NewBuilder()
        {
            return new MotorVehicleBuilder();
        }

        public MotorVehicleBuilder LicenseNumber(string licenseNumber)
        {
            _licenseNumber = licenseNumber;
            return this;
        }

        public MotorVehicleBuilder ChassisNumber(string chassisNumber)
        {
            _chassisNumber = chassisNumber;
            return this;
        }

        public MotorVehicle Build()
        {
            return new MotorVehicle
            {
                licenseNumber = _licenseNumber,
                chassisNumber = _chassisNumber
            };
        }
    }
}