using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class EmailBuilder
    {
        private string _emailAddress;

        private string _relatedAgent;

        public static EmailBuilder NewBuilder()
        {
            return new EmailBuilder();
        }

        public EmailBuilder EmailAddress(string emailAddress)
        {
            _emailAddress = emailAddress;
            return this;
        }

        public EmailBuilder RelatedAgent(string relatedAgent)
        {
            _relatedAgent = relatedAgent;
            return this;
        }

        public EMail Build()
        {
            return new EMail {emailAddress = _emailAddress, relatedAgent = _relatedAgent};
        }
    }
}