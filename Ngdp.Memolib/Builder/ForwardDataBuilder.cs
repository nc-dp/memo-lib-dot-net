using System;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class ForwardDataBuilder
    {
        private string _comment;

        private string _contactPointID;

        private Guid _messageUUID;

        private DateTime _originalMessageDateTime;

        private string _originalContentResponsible;

        private string _originalRepresentative;

        private string _originalSender;


        public static ForwardDataBuilder NewBuilder()
        {
            return new ForwardDataBuilder();
        }

        public ForwardDataBuilder MessageUUID(Guid messageUUID)
        {
            _messageUUID = messageUUID;
            return this;
        }

        public ForwardDataBuilder OriginalSender(string originalSender)
        {
            _originalSender = originalSender;
            return this;
        }

        public ForwardDataBuilder OriginalMessageDateTime(DateTime originalMessageDateTime)
        {
            _originalMessageDateTime = originalMessageDateTime;
            return this;
        }

        public ForwardDataBuilder OriginalContentResponsible(string originalContentResponsible)
        {
            _originalContentResponsible = originalContentResponsible;
            return this;
        }

        public ForwardDataBuilder OriginalOriginalRepresentative(string originalRepresentative)
        {
            _originalRepresentative = originalRepresentative;
            return this;
        }

        public ForwardDataBuilder Comment(string comment)
        {
            _comment = comment;
            return this;
        }

        public ForwardDataBuilder ContactPointID(string contactPointID)
        {
            _contactPointID = contactPointID;
            return this;
        }

        public ForwardData Build()
        {
            return new ForwardData
            {
                messageUUID = _messageUUID.ToString(),
                originalSender = _originalSender,
                originalContentResponsible = _originalContentResponsible,
                originalRepresentative = _originalRepresentative,
                originalMessageDateTime = _originalMessageDateTime,
                contactPointID = _contactPointID,
                comment = _comment
            };
        }
    }
}