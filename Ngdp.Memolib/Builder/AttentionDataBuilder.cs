using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class AttentionDataBuilder
    {
        private Address _address;

        private AttentionPerson _attentionPerson;

        private ContentResponsible _contentResponsible;

        private EMail _email;

        private GeneratingSystem _generatingSystem;

        private GlobalLocationNumber _globalLocationNumber;

        private ProductionUnit _productionUnit;

        private EID _eid;

        private SEnumber _senumber;

        private SORdata _sorData;

        private Telephone _telephone;

        private UnstructuredAddress _unstructuredAddress;

        public static AttentionDataBuilder NewBuilder()
        {
            return new AttentionDataBuilder();
        }

        public AttentionDataBuilder AttentionPerson(AttentionPerson attentionPerson)
        {
            _attentionPerson = attentionPerson;
            return this;
        }

        public AttentionDataBuilder ProductionUnit(ProductionUnit productionUnit)
        {
            _productionUnit = productionUnit;
            return this;
        }

        public AttentionDataBuilder GlobalLocationNumber(GlobalLocationNumber globalLocationNumber)
        {
            _globalLocationNumber = globalLocationNumber;
            return this;
        }

        public AttentionDataBuilder EMail(EMail email)
        {
            _email = email;
            return this;
        }

        public AttentionDataBuilder SEnumber(SEnumber senumber)
        {
            _senumber = senumber;
            return this;
        }

        public AttentionDataBuilder Telephone(Telephone telephone)
        {
            _telephone = telephone;
            return this;
        }

        public AttentionDataBuilder Eid(EID eid)
        {
            _eid = eid;
            return this;
        }

        public AttentionDataBuilder ContentResponsible(ContentResponsible contentResponsible)
        {
            _contentResponsible = contentResponsible;
            return this;
        }

        public AttentionDataBuilder GeneratingSystem(GeneratingSystem generatingSystem)
        {
            _generatingSystem = generatingSystem;
            return this;
        }

        public AttentionDataBuilder Address(Address address)
        {
            _address = address;
            return this;
        }

        public AttentionDataBuilder SorData(SORdata sorData)
        {
            _sorData = sorData;
            return this;
        }

        public AttentionDataBuilder UnstructuredAddress(UnstructuredAddress unstructuredAddress)
        {
            _unstructuredAddress = unstructuredAddress;
            return this;
        }

        public AttentionData Build()
        {
            return new AttentionData
            {
                AttentionPerson = _attentionPerson,
                ProductionUnit = _productionUnit,
                GlobalLocationNumber = _globalLocationNumber,
                EMail = _email,
                SEnumber = _senumber,
                Telephone = _telephone,
                EID = _eid,
                ContentResponsible = _contentResponsible,
                GeneratingSystem = _generatingSystem,
                SORdata = _sorData,
                Address = _address,
                UnstructuredAddress = _unstructuredAddress
            };
        }
    }
}