﻿using System;
using System.Collections.Generic;
using Dk.Digst.Digital.Post.Memolib.Model;
using Action = Dk.Digst.Digital.Post.Memolib.Model.Action;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class ActionBuilder
    {
        private readonly List<ItemsChoiceType> _itemsElementNameField = new List<ItemsChoiceType>();

        private readonly List<object> _itemsField = new List<object>();

        private string _actionCode;

        private string _label;

        public static ActionBuilder NewBuilder()
        {
            return new ActionBuilder();
        }

        public ActionBuilder Label(string label)
        {
            _label = label;
            return this;
        }

        public ActionBuilder ActionCode(string actionCode)
        {
            _actionCode = actionCode;
            return this;
        }

        public ActionBuilder StartDateTime(DateTime startDateTime)
        {
            _itemsField.Add(startDateTime.ToUniversalTime());
            _itemsElementNameField.Add(ItemsChoiceType.startDateTime);
            return this;
        }

        public ActionBuilder EndDateTime(DateTime endDateTime)
        {
            _itemsField.Add(endDateTime.ToUniversalTime());
            _itemsElementNameField.Add(ItemsChoiceType.endDateTime);
            return this;
        }

        public ActionBuilder EntryPoint(EntryPoint entryPoint)
        {
            _itemsField.Add(entryPoint);
            _itemsElementNameField.Add(ItemsChoiceType.EntryPoint);
            return this;
        }

        public ActionBuilder Reservation(Reservation reservation)
        {
            _itemsField.Add(reservation);
            _itemsElementNameField.Add(ItemsChoiceType.Reservation);
            return this;
        }

        public Action Build()
        {
            return new Action
            {
                label = _label,
                actionCode = _actionCode,
                Items = _itemsField.ToArray(),
                ItemsElementName = _itemsElementNameField.ToArray()
            };
        }
    }
}