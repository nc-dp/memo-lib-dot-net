using System;
using System.Collections.Generic;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class ReplyDataBuilder
    {
        private string _caseID;

        private string _comment;

        private string _contactPointID;

        private string _generatingSystemID;

        private string _messageID;

        private Guid _messageUUID;

        private string _recipientID;

        private List<AdditionalReplyData> _additionalReplyData = new List<AdditionalReplyData>();

        private Nullable<Guid> _replyUUID;

        private string _senderID;

        public static ReplyDataBuilder NewBuilder()
        {
            return new ReplyDataBuilder();
        }

        public ReplyDataBuilder MessageID(string messageID)
        {
            _messageID = messageID;
            return this;
        }

        public ReplyDataBuilder MessageUUID(Guid messageUUID)
        {
            _messageUUID = messageUUID;
            return this;
        }

        public ReplyDataBuilder ReplyUUID(Guid replyUUID)
        {
            _replyUUID = replyUUID;
            return this;
        }

        public ReplyDataBuilder SenderID(string senderID)
        {
            _senderID = senderID;
            return this;
        }

        public ReplyDataBuilder RecipientID(string recipientID)
        {
            _recipientID = recipientID;
            return this;
        }

        public ReplyDataBuilder CaseID(string caseID)
        {
            _caseID = caseID;
            return this;
        }

        public ReplyDataBuilder ContactPointID(string contactPointID)
        {
            _contactPointID = contactPointID;
            return this;
        }

        public ReplyDataBuilder GeneratingSystemID(string generatingSystemID)
        {
            _generatingSystemID = generatingSystemID;
            return this;
        }

        public ReplyDataBuilder Comment(string comment)
        {
            _comment = comment;
            return this;
        }

        public ReplyDataBuilder AdditionalReplyData(List<AdditionalReplyData> additionalReplyData)
        {
            _additionalReplyData = additionalReplyData;
            return this;
        }

        public ReplyDataBuilder AddAdditionalReplyData(AdditionalReplyData additionalReplyData)
        {
            _additionalReplyData.Add(additionalReplyData);
            return this;
        }

        public ReplyData Build()
        {
            return new ReplyData
            {
                messageID = _messageID,
                messageUUID = _messageUUID.ToString(),
                replyUUID = _replyUUID == null ? null : _replyUUID.ToString(),
                senderID = _senderID,
                recipientID = _recipientID,
                caseID = _caseID,
                contactPointID = _contactPointID,
                generatingSystemID = _generatingSystemID,
                comment = _comment,
                AdditionalReplyData = _additionalReplyData.ToArray()
            };
        }
    } 
}