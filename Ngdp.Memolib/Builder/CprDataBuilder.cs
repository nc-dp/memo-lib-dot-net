using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class CprDataBuilder
    {
        private string _cprNumber;

        private string _name;

        public static CprDataBuilder NewBuilder()
        {
            return new CprDataBuilder();
        }

        public CprDataBuilder CprNumber(string cprNumber)
        {
            _cprNumber = cprNumber;
            return this;
        }

        public CprDataBuilder Name(string name)
        {
            _name = name;
            return this;
        }

        public CPRdata Build()
        {
            return new CPRdata
            {
                cprNumber = _cprNumber,
                name = _name
            };
        }
    }
}