﻿using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class FormDataBuilder
    {
        private string _activityFacet;

        private string _label;

        private string _taskKey;

        private string _version;

        public static FormDataBuilder NewBuilder()
        {
            return new FormDataBuilder();
        }

        public FormDataBuilder TaskKey(string taskKey)
        {
            _taskKey = taskKey;
            return this;
        }

        public FormDataBuilder Version(string version)
        {
            _version = version;
            return this;
        }

        public FormDataBuilder ActivityFacet(string activityFacet)
        {
            _activityFacet = activityFacet;
            return this;
        }

        public FormDataBuilder Label(string label)
        {
            _label = label;
            return this;
        }

        public FORMdata Build()
        {
            return new FORMdata
            {
                activityFacet = _activityFacet,
                label = _label,
                taskKey = _taskKey,
                version = _version
            };
        }
    }
}