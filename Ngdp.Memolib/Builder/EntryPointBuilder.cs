using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class EntryPointBuilder
    {
        private string _url;

        public static EntryPointBuilder NewBuilder()
        {
            return new EntryPointBuilder();
        }

        public EntryPointBuilder Url(string url)
        {
            _url = url;
            return this;
        }

        public EntryPoint Build()
        {
            return new EntryPoint {url = _url};
        }
    }
}