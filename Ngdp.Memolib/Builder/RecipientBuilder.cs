using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class RecipientBuilder
    {
        private AttentionData _attentionData;

        private ContactPoint _contactPoint;

        private string _idType;

        private string _idTypeLabel;

        private string _label;

        private string _recipientID;

        public static RecipientBuilder NewBuilder()
        {
            return new RecipientBuilder();
        }

        public RecipientBuilder RecipientID(string recipientID)
        {
            _recipientID = recipientID;
            return this;
        }

        public RecipientBuilder IdType(string idType)
        {
            _idType = idType;
            return this;
        }

        public RecipientBuilder IdTypeLabel(string idTypeLabel)
        {
            _idTypeLabel = idTypeLabel;
            return this;
        }

        public RecipientBuilder Label(string label)
        {
            _label = label;
            return this;
        }

        public RecipientBuilder AttentionData(AttentionData attentionData)
        {
            _attentionData = attentionData;
            return this;
        }

        public RecipientBuilder ContactPoint(ContactPoint contactPoint)
        {
            _contactPoint = contactPoint;
            return this;
        }

        public Recipient Build()
        {
            return new Recipient
            {
                recipientID = _recipientID,
                idType = _idType,
                idTypeLabel = _idTypeLabel,
                label = _label,
                AttentionData = _attentionData,
                ContactPoint = _contactPoint
            };
        }
    }
}