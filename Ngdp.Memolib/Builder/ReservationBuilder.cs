using System;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class ReservationBuilder
    {
        private string _description;

        private DateTime _endDateTime;

        private string _location;

        private string _organizerMail;

        private string _organizerName;

        private string _reservationAbstract;

        private Guid _reservationUuid;

        private DateTime _startDateTime;

        public static ReservationBuilder NewBuilder()
        {
            return new ReservationBuilder();
        }

        public ReservationBuilder Description(string description)
        {
            _description = description;
            return this;
        }

        public ReservationBuilder ReservationUuid(Guid reservationUuid)
        {
            _reservationUuid = reservationUuid;
            return this;
        }

        public ReservationBuilder ReservationAbstract(string reservationAbstract)
        {
            _reservationAbstract = reservationAbstract;
            return this;
        }

        public ReservationBuilder Location(string location)
        {
            _location = location;
            return this;
        }

        public ReservationBuilder StartDateTime(DateTime startDateTime)
        {
            _startDateTime = startDateTime;
            return this;
        }

        public ReservationBuilder EndDateTime(DateTime endDateTime)
        {
            _endDateTime = endDateTime;
            return this;
        }

        public ReservationBuilder OrganizerMail(string organizerMail)
        {
            _organizerMail = organizerMail;
            return this;
        }

        public ReservationBuilder OrganizerName(string organizerName)
        {
            _organizerName = organizerName;
            return this;
        }

        public Reservation Build()
        {
            return new Reservation
            {
                description = _description,
                reservationUUID = _reservationUuid.ToString(),
                @abstract = _reservationAbstract,
                location = _location,
                startDateTime = _startDateTime.ToUniversalTime(),
                endDateTime = _endDateTime.ToUniversalTime(),
                organizerMail = _organizerMail,
                organizerName = _organizerName
            };
        }
    }
}