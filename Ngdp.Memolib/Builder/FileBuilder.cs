using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class FileBuilder
    {
        private string _encodingFormat;

        private IFileContent _fileContent;

        private string _filename;

        private string _language;

        public static FileBuilder NewBuilder()
        {
            return new FileBuilder();
        }

        public FileBuilder EncodingFormat(string encodingFormat)
        {
            _encodingFormat = encodingFormat;
            return this;
        }

        public FileBuilder Filename(string filename)
        {
            _filename = filename;
            return this;
        }

        public FileBuilder Language(string language)
        {
            _language = language;
            return this;
        }

        public FileBuilder Content(IFileContent content)
        {
            _fileContent = content;
            return this;
        }

        public File Build()
        {
            File file = new File
            {
                encodingFormat = _encodingFormat,
                filename = _filename,
                language = _language
            };
            file.SetContent(_fileContent);
            return file;
        }
    }
}