using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class ContentResponsibleBuilder
    {
        private string _contentResponsibleID;

        private string _label;

        public static ContentResponsibleBuilder NewBuilder()
        {
            return new ContentResponsibleBuilder();
        }

        public ContentResponsibleBuilder ContentResponsibleID(string contentResponsibleID)
        {
            _contentResponsibleID = contentResponsibleID;
            return this;
        }

        public ContentResponsibleBuilder Label(string label)
        {
            _label = label;
            return this;
        }

        public ContentResponsible Build()
        {
            return new ContentResponsible
            {
                contentResponsibleID = _contentResponsibleID,
                label = _label
            };
        }
    }
}