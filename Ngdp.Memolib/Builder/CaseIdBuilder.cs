using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class CaseIDBuilder
    {
        private string _caseID;

        private string _caseSystem;

        public static CaseIDBuilder NewBuilder()
        {
            return new CaseIDBuilder();
        }

        public CaseIDBuilder CaseID(string caseID)
        {
            _caseID = caseID;
            return this;
        }

        public CaseIDBuilder CaseSystem(string caseSystem)
        {
            _caseSystem = caseSystem;
            return this;
        }

        public CaseID Build()
        {
            return new CaseID
            {
                caseID = _caseID,
                caseSystem = _caseSystem
            };
        }
    }
}