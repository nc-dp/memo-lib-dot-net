﻿using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class AdditionalContentDataBuilder
    {
        private string _contentDataName;

        private string _contentDataType;

        private string _contentDataValue;

        public static AdditionalContentDataBuilder NewBuilder()
        {
            return new AdditionalContentDataBuilder();
        }

        public AdditionalContentDataBuilder ContentDataType(string contentDataType)
        {
            _contentDataType = contentDataType;
            return this;
        }

        public AdditionalContentDataBuilder ContentDataName(string contentDataName)
        {
            _contentDataName = contentDataName;
            return this;
        }

        public AdditionalContentDataBuilder ContentDataValue(string contentDataValue)
        {
            _contentDataValue = contentDataValue;
            return this;
        }


        public AdditionalContentData Build()
        {
            return new AdditionalContentData
            {
                contentDataType = _contentDataType,
                contentDataName = _contentDataName,
                contentDataValue = _contentDataValue
            };
        }
    }
}