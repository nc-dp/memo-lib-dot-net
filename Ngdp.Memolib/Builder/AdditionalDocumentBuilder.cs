using System.Collections.Generic;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class AdditionalDocumentBuilder
    {
        private List<Action> _action = new List<Action>();

        private string _documentId;

        private List<File> _file = new List<File>();

        private string _label;

        public static AdditionalDocumentBuilder NewBuilder()
        {
            return new AdditionalDocumentBuilder();
        }

        public AdditionalDocumentBuilder DocumentId(string documentId)
        {
            _documentId = documentId;
            return this;
        }

        public AdditionalDocumentBuilder Label(string label)
        {
            _label = label;
            return this;
        }

        public AdditionalDocumentBuilder File(List<File> file)
        {
            _file = file;
            return this;
        }

        public AdditionalDocumentBuilder AddFile(File file)
        {
            _file.Add(file);
            return this;
        }

        public AdditionalDocumentBuilder Action(List<Action> action)
        {
            _action = action;
            return this;
        }

        public AdditionalDocumentBuilder AddAction(Action action)
        {
            _action.Add(action);
            return this;
        }

        public AdditionalDocument Build()
        {
            return new AdditionalDocument
            {
                Action = _action.ToArray(),
                additionalDocumentID = _documentId,
                File = _file.ToArray(),
                label = _label
            };
        }
    }
}