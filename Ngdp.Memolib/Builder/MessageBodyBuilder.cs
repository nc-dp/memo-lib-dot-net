using System;
using System.Collections.Generic;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class MessageBodyBuilder
    {
        private List<AdditionalDocument> _additionalDocuments = new List<AdditionalDocument>();

        private DateTime _createdDateTime;

        private MainDocument _mainDocument;

        private List<TechnicalDocument> _technicalDocuments = new List<TechnicalDocument>();

        public static MessageBodyBuilder NewBuilder()
        {
            return new MessageBodyBuilder();
        }

        public MessageBodyBuilder CreatedDateTime(DateTime createdDateTime)
        {
            _createdDateTime = createdDateTime;
            return this;
        }

        public MessageBodyBuilder MainDocument(MainDocument mainDocument)
        {
            _mainDocument = mainDocument;
            return this;
        }

        public MessageBodyBuilder AdditionalDocuments(List<AdditionalDocument> additionalDocuments)
        {
            _additionalDocuments = additionalDocuments;
            return this;
        }

        public MessageBodyBuilder AddAdditionalDocument(AdditionalDocument additionalDocument)
        {
            _additionalDocuments.Add(additionalDocument);
            return this;
        }

        public MessageBodyBuilder TechnicalDocuments(List<TechnicalDocument> technicalDocuments)
        {
            _technicalDocuments = technicalDocuments;
            return this;
        }

        public MessageBodyBuilder AddTechnicalDocument(TechnicalDocument technicalDocument)
        {
            _technicalDocuments.Add(technicalDocument);
            return this;
        }

        public MessageBody Build()
        {
            return new MessageBody
            {
                createdDateTime = _createdDateTime.ToUniversalTime(),
                MainDocument = _mainDocument,
                AdditionalDocument = _additionalDocuments.ToArray(),
                TechnicalDocument = _technicalDocuments.ToArray()
            };
        }
    }
}