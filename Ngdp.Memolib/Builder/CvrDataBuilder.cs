using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class CvrDataBuilder
    {
        private string _companyName;

        private string _cvrNumber;

        public static CvrDataBuilder NewBuilder()
        {
            return new CvrDataBuilder();
        }

        public CvrDataBuilder CvrNumber(string cvrNumber)
        {
            _cvrNumber = cvrNumber;
            return this;
        }

        public CvrDataBuilder CompanyName(string companyName)
        {
            _companyName = companyName;
            return this;
        }

        public CVRdata Build()
        {
            return new CVRdata
            {
                cvrNumber = _cvrNumber,
                companyName = _companyName
            };
        }
    }
}