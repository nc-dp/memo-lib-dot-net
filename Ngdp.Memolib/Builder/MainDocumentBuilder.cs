using System.Collections.Generic;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class MainDocumentBuilder
    {
        private List<Action> _action = new List<Action>();

        private string _documentID;

        private List<File> _file = new List<File>();

        private string _label;

        public static MainDocumentBuilder NewBuilder()
        {
            return new MainDocumentBuilder();
        }

        public MainDocumentBuilder DocumentID(string documentID)
        {
            _documentID = documentID;
            return this;
        }

        public MainDocumentBuilder Label(string label)
        {
            _label = label;
            return this;
        }

        public MainDocumentBuilder File(List<File> file)
        {
            _file = file;
            return this;
        }

        public MainDocumentBuilder AddFile(File file)
        {
            _file.Add(file);
            return this;
        }

        public MainDocumentBuilder Action(List<Action> action)
        {
            _action = action;
            return this;
        }

        public MainDocumentBuilder AddAction(Action action)
        {
            _action.Add(action);
            return this;
        }

        public MainDocument Build()
        {
            return new MainDocument
            {
                mainDocumentID = _documentID,
                label = _label,
                File = _file.ToArray(),
                Action = _action.ToArray()
            };
        }
    }
}