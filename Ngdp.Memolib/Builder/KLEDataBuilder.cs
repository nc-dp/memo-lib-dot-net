using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class KleDataBuilder
    {
        private string _activityFacet;

        private string _label;

        private string _subjectKey;

        private string _version;

        public static KleDataBuilder NewBuilder()
        {
            return new KleDataBuilder();
        }

        public KleDataBuilder Label(string label)
        {
            _label = label;
            return this;
        }

        public KleDataBuilder Version(string version)
        {
            _version = version;
            return this;
        }

        public KleDataBuilder ActivityFacet(string activityFacet)
        {
            _activityFacet = activityFacet;
            return this;
        }

        public KleDataBuilder SubjectKey(string subjectKey)
        {
            _subjectKey = subjectKey;
            return this;
        }

        public KLEdata Build()
        {
            return new KLEdata
            {
                label = _label,
                version = _version,
                activityFacet = _activityFacet,
                subjectKey = _subjectKey
            };
        }
    }
}