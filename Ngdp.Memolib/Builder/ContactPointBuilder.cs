using System.Collections.Generic;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Builder
{
    public class ContactPointBuilder
    {
        private string _contactGroup;

        private List<ContactInfo> _contactInfo = new List<ContactInfo>();

        private string _contactPointID;

        private string _label;

        public static ContactPointBuilder NewBuilder()
        {
            return new ContactPointBuilder();
        }

        public ContactPointBuilder ContactGroup(string contactGroup)
        {
            _contactGroup = contactGroup;
            return this;
        }

        public ContactPointBuilder ContactPointID(string contactPointID)
        {
            _contactPointID = contactPointID;
            return this;
        }

        public ContactPointBuilder Label(string label)
        {
            _label = label;
            return this;
        }

        public ContactPointBuilder ContactInfo(List<ContactInfo> contactInfo)
        {
            _contactInfo = contactInfo;
            return this;
        }

        public ContactPoint Build()
        {
            return new ContactPoint
            {
                contactGroup = _contactGroup,
                contactPointID = _contactPointID,
                label = _label,
                ContactInfo = _contactInfo.ToArray()
            };
        }
    }
}