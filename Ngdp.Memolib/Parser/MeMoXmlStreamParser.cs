﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using Dk.Digst.Digital.Post.Memolib.Exception;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Validation;
using Dk.Digst.Digital.Post.Memolib.Visitor;

namespace Dk.Digst.Digital.Post.Memolib.Parser
{
    /// <summary>
    ///     The MeMoXmlStreamParser can be used to parse a MeMo.
    /// </summary>
    public class MeMoXmlStreamParser : IMeMoParser
    {
        private readonly XmlReader _reader;

        private bool _xmlStreamOpen;

        public MeMoXmlStreamParser(Stream stream, bool enableValidation, MemoVersion memoVersion)
        {
            if (enableValidation)
            {
                XmlReaderSettings settings = CreateReaderValidatingSettings(memoVersion);
                _reader = XmlReader.Create(stream, settings);
            }
            else
            {
                _reader = XmlReader.Create(stream);
            }
            _xmlStreamOpen = true;
        }

        /// <inheritdoc />
        public Message Parse()
        {
            try
            {
                _reader.MoveToContent();
                if (!_reader.LocalName.Equals(nameof(Message), StringComparison.InvariantCultureIgnoreCase))
                    throw new MeMoParseException("Unexpected element: " + _reader.LocalName);

                XmlSerializer xmlSerializer = new XmlSerializer(typeof(Message));
                return (Message) xmlSerializer.Deserialize(_reader);
            }
            catch (XmlException e)
            {
                throw new MeMoParseException("Unable to parse memo stream", e);
            }
            finally
            {
                DisposeReader();
            }
        }

        /// <inheritdoc />
        public void Traverse(List<IMeMoStreamVisitor> visitors)
        {
            MeMoXmlStreamLocation location = new MeMoXmlStreamLocation();

            try
            {
                while (NextStartElement())
                {
                    bool cursorMoved = HandleElement(location, visitors);
                    NextElementIf(!cursorMoved);
                }
            }
            catch (XmlException e)
            {
                throw new MeMoParseException("Traversing the xml stream failed. " + e);
            }
            finally
            {
                DisposeReader();
            }
        }

        /// <inheritdoc />
        public bool IsDisposed()
        {
            return !_xmlStreamOpen;
        }

        public void Dispose()
        {
            DisposeReader();
        }

        private XmlReaderSettings CreateReaderValidatingSettings(MemoVersion memoVersion)
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.Schemas.Add(MeMoSchemaValidatorProvider.CreateMemoXmlSchemaSet(memoVersion));
            settings.ValidationType = ValidationType.Schema;
            return settings;
        }

        /// <summary>
        ///     Disposes the reader. Does not close the underlying stream.
        /// </summary>
        public void DisposeReader()
        {
            try
            {
                _reader.Dispose();
                _xmlStreamOpen = false;
            }
            catch (XmlException e)
            {
                throw new IOException("Unable to dispose reader", e);
            }
        }

        public void TraverseChildren(MeMoXmlStreamLocation location, int parentDepth, List<IMeMoStreamVisitor> visitors)
        {
            // NextElement();
            while (NextStartElement())
            {
                if (_reader.Depth + 1 <= parentDepth)
                    break;

                bool cursorMoved = HandleElement(location, visitors);
                NextElementIf(!cursorMoved);
            }
        }

        public T ParseClass<T>()
        {
            if (!_reader.IsStartElement())
                throw new XmlException("Parser must be on START_ELEMENT to read next text");

            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                return (T) xmlSerializer.Deserialize(_reader);
            }
            catch (IOException e)
            {
                throw new XmlException("Deserializing element failed", e);
            }
        }

        public MeMoClassElements ParseElements()
        {
            MeMoClassElements meMoClassElements = new MeMoClassElements();

            string elementName = _reader.LocalName;
            if (!MeMoMessageClassDictionary.IsMeMoClass(_reader.LocalName))
                throw new InvalidOperationException(
                    "Not able to parse elements for " + elementName);

            NextElement();

            while (NextStartElement())
            {
                string currentElementName = _reader.LocalName;

                if (MeMoMessageClassDictionary.IsMeMoClass(currentElementName))
                    break;
                string currentElementValue = _reader.ReadElementContentAsString();
                meMoClassElements.Add(currentElementName, currentElementValue);
            }
            return meMoClassElements;
        }

        private bool NextStartElement()
        {
            while (!_reader.EOF && !_reader.IsStartElement())
                _reader.Read();

            return !_reader.EOF;
        }


        private bool HandleElement(MeMoXmlStreamLocation location, IEnumerable<IMeMoStreamVisitor> visitors)
        {
            string elementName = _reader.LocalName;
            MeMoXmlStreamCursor cursor = new MeMoXmlStreamCursor(elementName, location, this);

            if (!MeMoMessageClassDictionary.IsMeMoClass(elementName))
                return cursor.GetState() != MeMoXmlStreamCursor.CursorState.InitialPosition;

            UpdateLocation(location, elementName);

            foreach (IMeMoStreamVisitor visitor in visitors)
                cursor.Accept(visitor);

            return cursor.GetState() != MeMoXmlStreamCursor.CursorState.InitialPosition;
        }

        private void UpdateLocation(MeMoXmlStreamLocation location, string elementName)
        {
            Type meMoModelType = MeMoMessageClassDictionary.GetMemoType(elementName);
            location.UpdateStack(_reader.Depth + 1, meMoModelType);
        }

        private void NextElement()
        {
            if (!_reader.EOF)
                _reader.Read();
        }

        private void NextElementIf(bool isTrue)
        {
            if (isTrue && !_reader.EOF)
                _reader.Read();
        }
    }
}