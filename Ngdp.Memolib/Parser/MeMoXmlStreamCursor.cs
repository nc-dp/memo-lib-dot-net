﻿using System;
using System.Collections.Generic;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Util;
using Dk.Digst.Digital.Post.Memolib.Visitor;

namespace Dk.Digst.Digital.Post.Memolib.Parser
{
    /// <summary>
    ///     This class exposes a limited view of the state of
    /// </summary>
    public class MeMoXmlStreamCursor
    {
        public enum CursorState
        {
            InitialPosition,

            ElementsParsed,

            Parsed
        }

        private readonly MeMoXmlStreamLocation _location;

        private readonly MeMoXmlStreamParser _parser;

        private readonly string _xmlElementName;

        private MeMoClassElements _parsedElements;

        private object _parsedObject;

        private CursorState _state;

        public MeMoXmlStreamCursor(string elementName, MeMoXmlStreamLocation location, MeMoXmlStreamParser parser)
        {
            _xmlElementName = elementName;
            _location = location;
            _parser = parser;
            _state = CursorState.InitialPosition;
        }

        /// <summary>
        ///     Returns the local name of the xml element
        /// </summary>
        /// <returns>the local name of the xml element</returns>
        public string GetXmlElementName()
        {
            return _xmlElementName;
        }

        /// <summary>
        ///     Returns the MeMo class which is currently in context. The actual xml stream cursor
        ///     might point to a simple element/field within the context of this MeMo class
        /// </summary>
        /// <returns>MeMo class</returns>
        public Type GetCurrentPosition()
        {
            return _location.GetCurrentPosition();
        }

        /// <summary>
        ///     Returns the parent MeMo class of the current one if any
        /// </summary>
        /// <returns>MeMo class</returns>
        public Optional<Type> GetParentPosition()
        {
            return _location.GetParentPosition();
        }

        /// <summary>
        ///     Returns the state of the MeMoXmlStreamCursor which is used to
        ///     determine if the state of the underlying xml stream has been changed
        ///     by invoking methods in this class.
        /// </summary>
        /// <returns>the state</returns>
        public CursorState GetState()
        {
            return _state;
        }

        /// <summary>
        ///     Parses the MeMo class and returns a complete object. The CursorState is changed
        ///     due to this, preventing later invocations of the methods TraverseChildren or
        ///     ParseMeMoClassElements which is no longer possible
        /// </summary>
        /// <typeparam name="T">the type to which the xml should be parsed</typeparam>
        /// <returns>a MeMo class object</returns>
        public T ParseMeMoClass<T>() where T : IMeMoClass
        {
            if (_parsedObject != null)
                return (T) _parsedObject;

            if (_state != CursorState.InitialPosition)
                throw new InvalidOperationException(
                    "MeMo Class can't be parsed. ParseElements has already been called");

            if (typeof(T) != _location.GetCurrentPosition())
                throw new InvalidOperationException("Type " + typeof(T) + " does not match current position of " +
                                                    _location.GetCurrentPosition());
            _parsedObject = _parser.ParseClass<T>();
            _state = CursorState.Parsed;

            return (T) _parsedObject;
        }

        /// <summary>
        ///     Parses the elements of the MeMo class. Child MeMo classes are not parsed
        ///     and can be handled by another visitor or by invoking the method TraverseChrildren
        /// </summary>
        /// <returns>the returned object wraps the elements</returns>
        public MeMoClassElements ParseMeMoClassElements()
        {
            if (_parsedElements != null)
                return _parsedElements;

            if (_state != CursorState.InitialPosition)
                throw new InvalidOperationException(
                    "MeMo Class elements can't be parsed. Another method affecting the cursor has already been called");

            _parsedElements = _parser.ParseElements();
            _state = CursorState.ElementsParsed;

            return _parsedElements;
        }

        /// <summary>
        ///     Traverses the child MeMo classes with a list of visitors.
        /// </summary>
        /// <param name="visitors">a list of memo stream visistors</param>
        public void TraverseChildren(List<IMeMoStreamVisitor> visitors)
        {
            if (_state == CursorState.Parsed)
                throw new InvalidOperationException(
                    "Children can't be traversed, since the ParseMeMoClass method has already been called");

            _parser.TraverseChildren(new MeMoXmlStreamLocation(_location), _location.GetDepth(), visitors);
        }

        /// <summary>
        ///     Dispatches to the visitor
        /// </summary>
        /// <param name="visitor">the visistor</param>
        public void Accept(IMeMoStreamVisitor visitor)
        {
            visitor.VisitMeMoClass(this);
        }
    }
}