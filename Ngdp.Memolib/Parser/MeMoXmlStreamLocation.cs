﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Util;

namespace Dk.Digst.Digital.Post.Memolib.Parser
{
    /// <summary>
    ///     Represents the locaiton of the xml stream cursor in the MeMo message structure
    /// </summary>
    public class MeMoXmlStreamLocation
    {
        private readonly List<Type> _elementStack = new List<Type>();

        public MeMoXmlStreamLocation()
        {
        }

        public MeMoXmlStreamLocation(List<Type> meMoTypes)
        {
            if (meMoTypes == null) throw new ArgumentNullException(nameof(meMoTypes));
            if (!meMoTypes.All(IsMeMoType)) throw new ArgumentException("Argument types must implement IMeMoClass");
            _elementStack.AddRange(meMoTypes);
        }

        /// <summary>
        ///     This method creates a copy of another
        /// </summary>
        /// <param name="other"></param>
        public MeMoXmlStreamLocation(MeMoXmlStreamLocation other)
        {
            _elementStack.AddRange(other._elementStack);
        }


        /// <summary>
        ///     This method updates the internal stack and should be invoked when the cursor position has been moved
        ///     to a new MeMo class
        /// </summary>
        /// <param name="xmlDepth">the current depth in the xml document structure</param>
        /// <param name="currentType">the current MeMo class</param>
        public void UpdateStack(int xmlDepth, Type currentType)
        {
            if (!IsMeMoType(currentType)) throw new ArgumentException("Argument type must implement IMeMoClass");
            if (xmlDepth < 1)
                throw new ArgumentException("xmlDepth must be 1 or greater");

            if (GetDepth() >= xmlDepth)
                UpdateStack(xmlDepth);

            _elementStack.Add(currentType);
        }

        /// <summary>
        ///     Returns the MeMo class at the current position.
        /// </summary>
        /// <returns>a MeMo class</returns>
        public Type GetCurrentPosition()
        {
            if (_elementStack.Count == 0)
                throw new InvalidOperationException("The element stack is empty");
            return _elementStack[GetDepth() - 1];
        }

        /// <summary>
        ///     Returns the parent MeMo class is available
        /// </summary>
        /// <returns>the parent MeMo class or null</returns>
        public Optional<Type> GetParentPosition()
        {
            return GetDepth() >= 2
                ? Optional<Type>.Of(_elementStack[GetDepth() - 2])
                : Optional<Type>.Empty();
        }

        /// <summary>
        ///     Returns the current depth in the xml document at the current position
        /// </summary>
        /// <returns>a number representing the depth</returns>
        public int GetDepth()
        {
            return _elementStack.Count;
        }

        private void UpdateStack(int xmlDepth)
        {
            while (GetDepth() >= xmlDepth)
                _elementStack.RemoveAt(GetDepth() - 1);
        }

        private static bool IsMeMoType(Type t)
        {
            return t.GetInterfaces().Any(i => i == typeof(IMeMoClass));
        }
    }
}