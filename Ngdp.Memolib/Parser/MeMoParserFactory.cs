﻿using System.IO;
using Dk.Digst.Digital.Post.Memolib.Exception;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Util;

namespace Dk.Digst.Digital.Post.Memolib.Parser
{
    /// <summary>
    ///     The MeMoParserFactory is used to create a parser which can parse an XML representation of
    ///     a MeMo message. It handles the configuration of the XmlWriter.
    /// </summary>
    public static class MeMoParserFactory
    {
        /// <summary>
        ///     A method to create a basic non-validating parser
        /// </summary>
        /// <remarks>Requires seekable stream to auto-determine MeMo version.</remarks>
        /// <param name="stream">the XML-stream to parse</param>
        /// <returns>A streaming MeMo message parser</returns>
        public static IMeMoParser CreateParser(Stream stream)
        {
            return CreateParser(stream, false);
        }

        /// <summary>
        ///     A method which can be used to create a validating parser.
        /// </summary>
        /// <remarks>Requires seekable stream to auto-determine MeMo version.</remarks>
        /// <param name="stream">the XML-stream to parse</param>
        /// <param name="enableValidation">Enables XSD validation</param>
        /// <returns>A streaming MeMo message parser</returns>
        public static IMeMoParser CreateParser(Stream stream, bool enableValidation)
        {
            var memoVersion = MemoVersionUtil.ReadMemoVersion(stream);

            return CreateParser(stream, enableValidation, memoVersion);
        }


        /// <summary>
        ///     A method which can be used to create a validating parser.
        /// </summary>
        /// <param name="stream">the XML-stream to parse</param>
        /// <param name="enableValidation">Enables XSD validation</param>
        /// <param name="memoVersion">Define parser version</param>
        /// <returns></returns>
        public static IMeMoParser CreateParser(
            Stream stream, bool enableValidation, MemoVersion memoVersion)
        {
            if (memoVersion == MemoVersion.MeMoVersionUnknown)
            {
                throw new MeMoParseException("Unable to parse a supported MeMo version");
            }

            return new MeMoXmlStreamParser(stream, enableValidation, memoVersion);
        }
    }
}