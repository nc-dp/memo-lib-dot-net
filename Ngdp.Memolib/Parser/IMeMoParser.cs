﻿using System;
using System.Collections.Generic;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Visitor;

namespace Dk.Digst.Digital.Post.Memolib.Parser
{
    public interface IMeMoParser : IDisposable
    {
        /// <summary>
        ///     Parses the stream to a Message
        /// </summary>
        /// <returns>a MeMo Message</returns>
        Message Parse();

        /// <summary>
        ///     This method traverses the entire xml stream and applies the provided MeMoStreamVisitor
        ///     visitors for each element which is of type MeMo class. The visitor gains access to the
        ///     MeMoXmlStreamCursor which can be used to parse either the child elements or the whole MeMo
        ///     class and as a side-effect move the underlying cursor. When using this method, this should
        ///     be taken into consideration if multiple visitors are applied to the same stream.
        /// </summary>
        /// <param name="visitors"></param>
        void Traverse(List<IMeMoStreamVisitor> visitors);

        bool IsDisposed();

        new void Dispose();
    }
}