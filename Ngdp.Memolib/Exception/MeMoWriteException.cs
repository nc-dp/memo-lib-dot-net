﻿namespace Dk.Digst.Digital.Post.Memolib.Exception
{
    public class MeMoWriteException : System.Exception
    {
        public MeMoWriteException(string message) : base(message)
        {
        }

        public MeMoWriteException(string message, System.Exception e) : base(message, e)
        {
        }
    }
}