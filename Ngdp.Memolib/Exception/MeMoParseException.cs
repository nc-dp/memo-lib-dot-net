﻿namespace Dk.Digst.Digital.Post.Memolib.Exception
{
    public class MeMoParseException : System.Exception
    {
        public MeMoParseException(string message) : base(message)
        {
        }

        public MeMoParseException(string message, System.Exception e) : base(message, e)
        {
        }
    }
}