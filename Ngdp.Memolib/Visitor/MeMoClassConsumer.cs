﻿using System;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Parser;
using Dk.Digst.Digital.Post.Memolib.Util;

namespace Dk.Digst.Digital.Post.Memolib.Visitor
{
    /// <inheritdoc />
    /// <summary>
    ///     This class is an implementation of IMeMoStreamVisitor which delegates the handling
    ///     of the xml stream to a XmlStreamConsumer
    /// </summary>
    public class MeMoClassConsumer<T> : AbstractMeMoStreamVisitor<T> where T : IMeMoClass
    {
        private readonly XmlStreamConsumer<MeMoXmlStreamCursor> _consumer;

        internal MeMoClassConsumer(Type parentType, XmlStreamConsumer<MeMoXmlStreamCursor> consumer) : base(parentType)
        {
            _consumer = consumer ?? throw new ArgumentNullException(nameof(consumer));
        }

        /// <summary>
        ///     This method invokes the consumer if the MeMoXmlStreamCursor position at the
        ///     expected MeMo class
        /// </summary>
        /// <param name="cursor">a cursor which represents the current position</param>
        public override void VisitMeMoClass(MeMoXmlStreamCursor cursor)
        {
            if (IsCorrectMeMoClass(cursor) && (ParentType == null || IsCorrectParentMeMoClass(cursor)))
                _consumer.Invoke(cursor);
        }
    }
}