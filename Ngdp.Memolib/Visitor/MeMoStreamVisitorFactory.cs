﻿using System;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Parser;
using Dk.Digst.Digital.Post.Memolib.Util;

namespace Dk.Digst.Digital.Post.Memolib.Visitor
{
    /// <summary>
    ///     A factory to create a IMeMoStreamVisitor. Two types of visitors can be created, one that
    ///     can consume the MeMoXmlStreamCursor by a XmlStreamConsumer, and one that can parse
    ///     the MeMoClass and collect the object(s) internally
    /// </summary>
    public static class MeMoStreamVisitorFactory
    {
        /// <summary>
        ///     Creates a IMeMoStreamVisitor
        /// </summary>
        /// <typeparam name="T">the MeMo class to visit</typeparam>
        /// <param name="consumer">the Consumer which should be invoked</param>
        /// <returns>a IMeMoStreamVisitor</returns>
        public static IMeMoStreamVisitor CreateConsumer<T>(
            XmlStreamConsumer<MeMoXmlStreamCursor> consumer) where T : IMeMoClass
        {
            return new MeMoClassConsumer<T>(null, consumer);
        }

        /// <summary>
        ///     Creates a IMeMoStreamVisitor. A parent class must be provided to further
        ///     filter the MeMo class consumed
        /// </summary>
        /// <typeparam name="T">the MeMo class to visit</typeparam>
        /// <param name="parentType">the parent of the MeMo class to visit</param>
        /// <param name="consumer">the Consumer which should be invoked</param>
        /// <returns>a IMeMoStreamVisitor</returns>
        public static IMeMoStreamVisitor CreateConsumer<T>(
            Type parentType,
            XmlStreamConsumer<MeMoXmlStreamCursor> consumer) where T : IMeMoClass
        {
            return new MeMoClassConsumer<T>(parentType, consumer);
        }

        /// <summary>
        ///     Creates a IMeMoStreamProcessorVisitor which can be used to parse and collect
        ///     MeMo classes from the xml stream
        /// </summary>
        /// <typeparam name="T">the MeMo class to visit</typeparam>
        /// <returns>a MeMoStreamVisitor</returns>
        public static IMeMoStreamProcessorVisitor<T> CreateProcessor<T>() where T : IMeMoClass
        {
            return new MeMoClassProcessor<T>(null);
        }

        /// <summary>
        ///     Creates a IMeMoStreamProcessorVisitor which can be used to parse and collect
        ///     MeMo classes from the xml stream. A parent class must be provided to further
        ///     filter the MeMo class to be collected
        /// </summary>
        /// <typeparam name="T">the MeMo class to visit</typeparam>
        /// <param name="parentType">the parent of the MeMo class to visit</param>
        /// <returns>a MeMoStreamVisitor</returns>
        public static IMeMoStreamProcessorVisitor<T> CreateProcessor<T>(Type parentType) where T : IMeMoClass
        {
            return new MeMoClassProcessor<T>(parentType);
        }
    }
}