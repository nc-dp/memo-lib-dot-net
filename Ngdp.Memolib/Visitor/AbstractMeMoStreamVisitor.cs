﻿using System;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Parser;
using Dk.Digst.Digital.Post.Memolib.Util;

namespace Dk.Digst.Digital.Post.Memolib.Visitor
{
    /// <summary>
    ///     An abstract implementation of IMeMoStreamVisitor which contains common
    ///     methods used in subclasses.
    /// </summary>
    public abstract class AbstractMeMoStreamVisitor<T> : IMeMoStreamVisitor where T : IMeMoClass
    {
        protected readonly Type ParentType;

        protected readonly Type Type;

        protected AbstractMeMoStreamVisitor(Type parentType)
        {
            Type = typeof(T);
            ParentType = parentType;
        }

        public abstract void VisitMeMoClass(MeMoXmlStreamCursor cursor);

        protected bool IsCorrectParentMeMoClass(MeMoXmlStreamCursor meMoXmlStreamCursor)
        {
            Optional<Type> parent = meMoXmlStreamCursor.GetParentPosition();
            return parent.IsPresent() && ParentType.IsAssignableFrom(parent.Get());
        }

        protected bool IsCorrectMeMoClass(MeMoXmlStreamCursor meMoXmlStreamCursor)
        {
            return Type.IsAssignableFrom(meMoXmlStreamCursor.GetCurrentPosition());
        }
    }
}