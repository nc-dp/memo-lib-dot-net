﻿using System.Collections.Generic;
using Dk.Digst.Digital.Post.Memolib.Util;

namespace Dk.Digst.Digital.Post.Memolib.Visitor
{
    /// <summary>
    ///     Represents a visitor which collects values/objects while visiting the xml stream
    /// </summary>
    public interface IMeMoStreamProcessorVisitor<T> : IMeMoStreamVisitor
    {
        /// <summary>
        ///     Returns the collected objects
        /// </summary>
        /// <returns>a list of the collected objects</returns>
        List<T> GetResult();

        /// <summary>
        ///     Returns a single result. If more objects have been collected, the method should
        ///     throw an exception
        /// </summary>
        /// <returns>an Optional of the collected object</returns>
        Optional<T> GetSingleResult();
    }
}