﻿using Dk.Digst.Digital.Post.Memolib.Parser;

namespace Dk.Digst.Digital.Post.Memolib.Visitor
{
    /// <summary>
    ///     Represents a visitor of a streamed MeMo message during dezerialization. The visitor can access
    ///     the underlying stream erader by a cursor and apply logic to influence how the MeMo message
    ///     is handled
    /// </summary>
    public interface IMeMoStreamVisitor
    {
        /// <summary>
        ///     This method is invoked when the xml stream reaches an xml element which
        ///     represents a MeMo class
        /// </summary>
        /// <param name="cursor">a cursor which represents the current position of the underlying stream</param>
        void VisitMeMoClass(MeMoXmlStreamCursor cursor);
    }
}