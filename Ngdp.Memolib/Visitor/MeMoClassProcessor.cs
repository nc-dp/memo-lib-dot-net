﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Parser;
using Dk.Digst.Digital.Post.Memolib.Util;

namespace Dk.Digst.Digital.Post.Memolib.Visitor
{
    /// <summary>
    ///     This class is an implementation of MeMoStreamVisitor which parses and collects MeMo
    ///     class objects. It implements MeMoStreamProcessorVisitor which exposes methods to get
    ///     the result.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MeMoClassProcessor<T> : AbstractMeMoStreamVisitor<T>, IMeMoStreamProcessorVisitor<T>
        where T : IMeMoClass
    {
        private readonly List<T> _collectedObjects = new List<T>();

        public MeMoClassProcessor(Type parentType) : base(parentType)
        {
        }

        /// <summary>
        ///     This method invokes the consumer if the cursor position is at the expected
        ///     memo class
        /// </summary>
        /// <param name="cursor">a cursor which represents the </param>
        public override void VisitMeMoClass(MeMoXmlStreamCursor cursor)
        {
            if (IsCorrectMeMoClass(cursor) && (ParentType == null || IsCorrectParentMeMoClass(cursor)))
                _collectedObjects.Add(cursor.ParseMeMoClass<T>());
        }

        /// <inheritdoc />
        public List<T> GetResult()
        {
            return _collectedObjects;
        }

        /// <inheritdoc />
        public Optional<T> GetSingleResult()
        {
            if (_collectedObjects.Count > 1)
                throw new InvalidOperationException(
                    "Size of the collected objects is " + _collectedObjects.Count);
            return !_collectedObjects.Any() ? Optional<T>.Empty() : Optional<T>.Of(_collectedObjects.Single());
        }
    }
}