﻿using System.IO;
using System.Text;
using Dk.Digst.Digital.Post.Memolib.Lzma;
using ICSharpCode.SharpZipLib.Tar;
using SharpCompress.Common;
using SharpCompress.Compressors.LZMA;
using SharpCompress.Writers;
using SharpCompress.Writers.Tar;

namespace Dk.Digst.Digital.Post.Memolib.Container
{
    public class MeMoContainerOutputStream : IMeMoContainerOutputStream
    {
        private readonly TarOutputStream _archiveStream;
        private MemoryStream entryStream;
        private TarEntry entry;

        public MeMoContainerOutputStream(Stream stream)
        {
            LZMAOptions options = new LZMAOptions(3);
            options.DictSize = 128 * 1024 * 1024;

            LZMAOutputStream lzmaStream = new LZMAOutputStream(stream, options, -1);

            _archiveStream = new TarOutputStream(lzmaStream, Encoding.UTF8);
        }

        /// <inheritdoc />
        public void Dispose()
        {
            _archiveStream.Dispose();
        }

        /// <inheritdoc />
        public Stream WriteNextEntry(string key)
        {
            entry = TarEntry.CreateTarEntry(key);
            entryStream = new MemoryStream();
            return entryStream;
        }

        public void CloseEntry()
        {
            entry.Size = entryStream.Length;
            _archiveStream.PutNextEntry(entry);
            entryStream.Position = 0;

            byte[] buffer = entryStream.ToArray();
            _archiveStream.Write(buffer, 0, buffer.Length);
            _archiveStream.CloseEntry();
        }
    }
}