﻿using System;

namespace Dk.Digst.Digital.Post.Memolib.Container
{
    /// <inheritdoc />
    /// <summary>
    ///     An iterator of MeMoContainerEntry object instances
    /// </summary>
    public interface IIterableMeMoContainer : IDisposable
    {
        /// <summary>
        ///     Returns true if there is an entry at the current cursor position, which can be read from the container
        /// </summary>
        /// <returns>true if there is an entry</returns>
        bool HasEntry();

        /// <summary>
        ///     Returns the entry at the current cursor position
        /// </summary>
        /// <returns>the entry at the current cursor position</returns>
        IMeMoContainerEntry GetEntry();

        /// <summary>
        ///     Moves the cursor to the next entry
        /// </summary>
        void NextEntry();
    }
}