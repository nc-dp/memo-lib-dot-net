﻿using System.IO;

namespace Dk.Digst.Digital.Post.Memolib.Container
{
    /// <summary>
    ///     An Entry in a MeMo Container. An entry is a MeMo xml message
    /// </summary>
    public interface IMeMoContainerEntry
    {
        /// <summary>
        ///     Returns the key of the entry in the MeMo container
        /// </summary>
        /// <returns>the key of the entry</returns>
        string GetKey();

        /// <summary>
        ///     Returns the underlying stream
        /// </summary>
        /// <returns>the stream</returns>
        Stream Stream();

        /// <summary>
        ///     Returns the size of the entry in the MeMo container
        /// </summary>
        /// <returns>the size of the entry</returns>
        long GetSize();
    }
}