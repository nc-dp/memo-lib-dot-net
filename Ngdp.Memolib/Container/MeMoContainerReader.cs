﻿using System;
using System.IO;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Parser;
using Dk.Digst.Digital.Post.Memolib.Util;

namespace Dk.Digst.Digital.Post.Memolib.Container
{
    /// <inheritdoc />
    /// <summary>
    ///     A MeMoContainerReader can be used to read the entries of a MeMo container which holds one
    ///     or more MeMo message files that can be extracted.
    /// </summary>
    public class MeMoContainerReader : IDisposable
    {
        private readonly IIterableMeMoContainer _iterator;

        public MeMoContainerReader(IIterableMeMoContainer iterator)
        {
            _iterator = iterator;
        }

        public void Dispose()
        {
            _iterator.Dispose();
        }

        /// <summary>
        ///     A method which parses the current entry and returns a Message. It moves the
        ///     MeMoContainerEntryCursor to the next entry when the current entry has been marshalled.
        /// </summary>
        /// <param name="validate">marks if entry should be validated when read</param>
        /// <param name="memoVersion"></param>
        /// <returns>
        ///     and optional Message object. If there are no entries left in the file, and
        ///     empty optional is returned.
        /// </returns>
        public Optional<Message> ReadEntry(bool validate, MemoVersion memoVersion)
        {
            if (!_iterator.HasEntry())
                return Optional<Message>.Empty();

            IMeMoContainerEntry entry = _iterator.GetEntry();
            Optional<Message> message;

            using (IMeMoParser parser = MeMoParserFactory.CreateParser(
                       entry.Stream(), validate, memoVersion))
            {
                message = Optional<Message>.Of(parser.Parse());
            }

            _iterator.NextEntry();
            return message;
        }

        /// <summary>
        ///     Returns true if there is an entry at the current cursor position.
        /// </summary>
        /// <returns>true if there is an entry</returns>
        public bool HasEntry()
        {
            return _iterator.HasEntry();
        }
    }
}