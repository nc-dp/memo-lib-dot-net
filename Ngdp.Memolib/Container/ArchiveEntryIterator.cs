﻿
using ICSharpCode.SharpZipLib.Tar;

namespace Dk.Digst.Digital.Post.Memolib.Container
{
    /// <inheritdoc />
    /// <summary>
    ///     The ArchiveEntryIterator implements IIterableMeMoContainer and can be used to process the
    ///     entries of a IReader.
    /// </summary>
    public class ArchiveEntryIterator
        : IIterableMeMoContainer
    {
        private readonly TarInputStream _archiveReader;

        private TarEntry _entry;

        public ArchiveEntryIterator(TarInputStream archiveReader)
        {
            _archiveReader = archiveReader;

            _entry = _archiveReader.GetNextEntry();

        }


        public void Dispose()
        {
            _archiveReader.Dispose();
        }

        public bool HasEntry()
        {
            return _entry != null;
        }

        public IMeMoContainerEntry GetEntry()
        {
            return new MeMoArchiveEntry(_entry, _archiveReader);
        }

        public void NextEntry()
        {
            _entry = _archiveReader.GetNextEntry();
        }
    }
}