using System;
using System.IO;
using Dk.Digst.Digital.Post.Memolib.Factory;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Writer;

namespace Dk.Digst.Digital.Post.Memolib.Container
{
    /// <inheritdoc />
    /// <summary>
    ///     MeMoContainerWriter writes entries to a MeMoContainerStream
    /// </summary>
    public class MeMoContainerWriter : IDisposable
    {
        private readonly IMeMoContainerOutputStream _containerOutputStream;

        private IMeMoStreamWriter _writer;

        public MeMoContainerWriter(IMeMoContainerOutputStream containerOutputStream)
        {
            _containerOutputStream = containerOutputStream;
        }

        public void Dispose()
        {
            if (IsWritingMeMoMessage())
                CloseCurrentlyOpenEntry();

            _containerOutputStream.Dispose();
        }

        /// <summary>
        ///     This method writes a single entry to a MeMoContainerStream
        /// </summary>
        /// <param name="name">
        ///     name should be a unique name or identifier for this entry (
        ///     within the context of the container)
        /// </param>
        /// <param name="message">the message to be added as an entry</param>
        /// <param name="validate">marks if the message should be schema validated</param>
        /// <returns>this</returns>
        public MeMoContainerWriter WriteEntry(string name, Message message, bool validate = false)
        {
            IMeMoStreamWriter writer = GetWriterForNextEntry(name, validate);

            writer.Write(message);

            CloseCurrentlyOpenEntry();

            return this;
        }


        /// <summary>
        ///     This method adds a new entry to the container, but does not write the actual content of
        ///     the entry. Instead a writer is returned which must be used to write the actual MeMo
        ///     message.
        /// </summary>
        /// <param name="name">
        ///     should be a unique name or identifier for this entry (within the context of the
        ///     container)
        /// </param>
        /// <param name="validate">marks if the message should be schema validated</param>
        /// <returns>MeMoStreamWriter which must be used to write the MeMo message entry</returns>
        public IMeMoStreamWriter GetWriterForNextEntry(string name, bool validate = false)
        {
            if (IsWritingMeMoMessage())
                CloseCurrentlyOpenEntry();

            Stream outputStream = _containerOutputStream.WriteNextEntry(name);

            _writer = MeMoWriterFactory.CreateWriter(outputStream, new FileContentLoader());

            return _writer;
        }

        private bool IsWritingMeMoMessage()
        {
            return _writer != null;
        }

        private void CloseCurrentlyOpenEntry()
        {
            _containerOutputStream.CloseEntry();
            _writer.Dispose();
            _writer = null;
        }
    }
}