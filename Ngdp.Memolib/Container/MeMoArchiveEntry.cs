﻿using System.IO;
using ICSharpCode.SharpZipLib.Tar;

namespace Dk.Digst.Digital.Post.Memolib.Container
{
    /// <inheritdoc />
    /// <summary>
    ///     A MeMoArchiveEntry is a single _entry in a MeMo container.
    /// </summary>
    public class MeMoArchiveEntry : IMeMoContainerEntry
    {
        private readonly TarEntry _entry;

        private readonly TarInputStream _archiveReader;

        internal MeMoArchiveEntry
            (TarEntry entry, TarInputStream archiveReader)
        {
            _entry = entry;
            _archiveReader = archiveReader;
        }

        /// <inheritdoc />
        public string GetKey()
        {
            return _entry?.Name;
        }

        /// <inheritdoc />
        public Stream Stream()
        {            
            return _archiveReader;
        }

        /// <inheritdoc />
        public long GetSize()
        {
            return _entry.Size;
        }
    }
}