﻿using System.IO;
using SharpCompress.Writers;

namespace Dk.Digst.Digital.Post.Memolib.Container
{
    public class ArchiveStream : MemoryStream
    {
        private readonly string _key;
        private readonly IWriter _archiveWriter;

        public ArchiveStream(string key, IWriter archiveWriter)
        {
            _key = key;
            _archiveWriter = archiveWriter;
        }

        protected override void Dispose(bool disposing)
        {
            Position = 0;
            _archiveWriter.Write(_key, this);

            base.Dispose(disposing);
        }
    }
}