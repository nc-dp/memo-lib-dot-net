﻿using System;
using System.IO;

namespace Dk.Digst.Digital.Post.Memolib.Container
{
    /// <inheritdoc />
    /// <summary>
    ///     A MeMoContainerStream allows entries to be written to a MeMo container
    /// </summary>
    public interface IMeMoContainerOutputStream : IDisposable
    {
        /// <summary>
        ///     This method must be used when adding an entry to the container. This method only adds entries,
        ///     and does not add content to the entry.
        /// </summary>
        /// <param name="key">the identifier of the entry. It could be a file name or another unique identifier</param>
        /// <returns>a Stream which must be used to write the content of the entry</returns>
        Stream WriteNextEntry(string key);

        /// <summary>
        /// This method must be used when the processing of the entry is complete and the content has been
        /// written.The OutputStream must not be closed directly.
        /// </summary>
        void CloseEntry();
    }
}