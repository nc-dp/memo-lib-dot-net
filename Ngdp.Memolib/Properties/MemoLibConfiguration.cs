﻿namespace Dk.Digst.Digital.Post.Memolib.Properties
{
    public static class MemoLibConfiguration
    {
        public static int ReadWindowSize { get; set; } = 2500;
    }
}