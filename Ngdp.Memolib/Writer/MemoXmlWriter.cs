﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Writer
{
    public class MeMoXmlWriter : IDisposable
    {
        private const string XmlNamespace = "xmlns";

        private readonly XmlWriter _xmlWriter;

        public FileContentLoader FileContentLoader { get; }

        public Stream Stream { get; }

        public MeMoXmlWriter(Stream stream, FileContentLoader fileContentLoader)
        {
            _xmlWriter = XmlWriter.Create(stream);
            Stream = stream;
            FileContentLoader = fileContentLoader;
        }

        public void Dispose()
        {
            _xmlWriter?.Dispose();
        }

        /// <inheritdoc cref="XmlWriter.Flush()" />
        public void Flush()
        {
            _xmlWriter.Flush();
        }

        /// <summary>
        ///     Writes an element of type DateTime to a date in the Memo namespace.
        /// </summary>
        /// <param name="elementName">the local name of the element</param>
        /// <param name="value">the value of the element</param>
        public void WriteDateElement(string elementName, DateTime value)
        {
            WriteElement(elementName, value.ToUniversalTime().ToString("yyyy-MM-dd"), Namespaces.Memo);
        }

        /// <summary>
        ///     Writes and element of the type Datetime to a datetime in the Memo namespace
        /// </summary>
        /// <param name="elementName">the local name of the element</param>
        /// <param name="value">the value of the element</param>
        public void WriteDateTimeElement(string elementName, DateTime value)
        {
            WriteElement(elementName, value.ToUniversalTime().ToString("yyyy'-'MM'-'ddTHH':'mm':'ssZ"), Namespaces.Memo);
        }

        /// <summary>
        ///     Writes an element in the Memo namespace
        /// </summary>
        /// <param name="elementName">the local name of the element</param>
        /// <param name="value">the value of the element</param>
        public void WriteElement(string elementName, string value)
        {
            WriteElement(elementName, value, Namespaces.Memo);
        }

        /// <summary>
        ///     Writes an element to the xml stream
        /// </summary>
        /// <param name="elementName">the local name of the element</param>
        /// <param name="value">the value of the element</param>
        /// <param name="namespaceValue">the namespace of the element</param>
        public void WriteElement(string elementName, string value, string namespaceValue)
        {
            if (elementName == null) throw new ArgumentNullException(nameof(elementName));
            if (value == null) return;
            _xmlWriter.WriteElementString(elementName, namespaceValue, value);
        }

        /// <summary>
        ///     Writes the start of an xml document
        /// </summary>
        public void WriteStartDocument()
        {
            _xmlWriter.WriteStartDocument();
        }

        /// <summary>
        ///     Writes the start of an element in the Memo namespace
        /// </summary>
        /// <param name="elementName">the local name of the element</param>
        public void WriteStartElement(string elementName)
        {
            if (elementName == null) throw new ArgumentNullException(nameof(elementName));
            _xmlWriter.WriteStartElement(elementName, Namespaces.Memo);
        }

        /// <summary>
        ///     Writes raw markup manually from a string
        /// </summary>
        /// <param name="data"></param>
        public void WriteRaw(string data)
        {
            _xmlWriter.WriteRaw(data);
        }

        /// <summary>
        ///     Writes the end of one element
        /// </summary>
        public void WriteEndElement()
        {
            _xmlWriter.WriteEndElement();
        }

        /// <summary>
        ///     Closes any open elements or attributes
        /// </summary>
        public void WriteEndDocument()
        {
            _xmlWriter.WriteEndDocument();
        }

        /// <summary>
        ///     Writes the full end of one element
        /// </summary>
        public void WriteFullEndElement()
        {
            _xmlWriter.WriteFullEndElement();
        }

        /// <summary>
        ///     Adds a namespace
        /// </summary>
        /// <param name="nsShort">abbreviation of namespace in xml</param>
        /// <param name="nsFull">full namespace in xml</param>
        public void WriteNamespace(string nsShort, string nsFull)
        {
            _xmlWriter.WriteAttributeString(XmlNamespace, nsShort, "", nsFull);
        }

        /// <summary>
        ///     Adds an attribute
        /// </summary>
        /// <param name="name">the name of the attribute</param>
        /// <param name="value">the value of the attribute</param>
        public void WriteAttribute(string name, string value)
        {
            _xmlWriter.WriteAttributeString(name, value);
        }

        /// <summary>
        ///     Writes an Xml Type without namespaces
        /// </summary>
        /// <typeparam name="T">XmlTypeAttribute type</typeparam>
        /// <param name="elementName">the name of the element</param>
        /// <param name="value">the value of the element</param>
        public void WriteXmlType<T>(string elementName, T value)
        {
            if (elementName == null) throw new ArgumentNullException(nameof(elementName));

            XmlSerializer xmlSerializer = new XmlSerializer(value.GetType());
            XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces(new[] {XmlQualifiedName.Empty});

            xmlSerializer.Serialize(_xmlWriter, value, namespaces);
        }
    }
}