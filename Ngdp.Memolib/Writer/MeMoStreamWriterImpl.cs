﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using Dk.Digst.Digital.Post.Memolib.Exception;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Writer
{
    public class MeMoStreamWriterImpl : IMeMoStreamWriter
    {
        private readonly MeMoXmlWriter _meMoXmlWriter;

        private InternalState _state;

        public MeMoStreamWriterImpl(
            Stream meMoStream,
            FileContentLoader fileContentLoader)
        {
            _meMoXmlWriter = new MeMoXmlWriter(meMoStream, fileContentLoader);
            _state = InternalState.Initialized;
            FileContentLoader = fileContentLoader;
        }

        public FileContentLoader FileContentLoader { get; }

        /// <inheritdoc />
        public bool IsClosed()
        {
            return _state == InternalState.Closed;
        }

        /// <inheritdoc />
        public void Dispose()
        {
            if (!IsClosed())
                _meMoXmlWriter.Dispose();

            _state = InternalState.Closed;
        }

        /// <inheritdoc />
        public void Write(Message message)
        {
            try
            {
                WriteStartOfMessage(message.memoVersion).WriteHeader(message.MessageHeader);

                if (message.MessageBody != null)
                    WriteBody(message.MessageBody);

                _meMoXmlWriter.WriteEndDocument();
                _meMoXmlWriter.Flush();
            }
            catch (XmlException e)
            {
                throw new MeMoWriteException("Writing memo to stream failed.", e);
            }
        }

        /// <summary>
        ///     Writes the MessageHeader to the xml stream
        /// </summary>
        /// <param name="messageHeader">the header to be written</param>
        /// <returns>this</returns>
        public MeMoStreamWriterImpl WriteHeader(MessageHeader messageHeader)
        {
            _meMoXmlWriter.WriteXmlType(nameof(MessageHeader), messageHeader);
            return this;
        }

        /// <summary>
        ///     Writes the MessageBody to the xml stream
        /// </summary>
        /// <param name="messageBody"></param>
        /// <returns></returns>
        public MeMoStreamWriterImpl WriteBody(MessageBody messageBody)
        {
            MeMoBodyWriter bodyWriter = new MeMoBodyWriter(_meMoXmlWriter);
            bodyWriter.Write(messageBody);
            return this;
        }

        /// <summary>
        ///     Writes the start of the XML document to the stream and adds the required namespace definitions
        /// </summary>
        /// <param name="memoVersion"></param>
        /// <returns>this</returns>
        public MeMoStreamWriterImpl WriteStartOfMessage(decimal memoVersion)
        {
            _meMoXmlWriter.WriteStartDocument();

            _meMoXmlWriter.WriteStartElement("Message");
            _meMoXmlWriter.WriteAttribute("memoVersion", memoVersion.ToString(CultureInfo.InvariantCulture));
            if (MemoVersionMetadata.FromDecimal(memoVersion) == MemoVersion.MemoVersion11)
            {
                _meMoXmlWriter.WriteAttribute("memoSchVersion", MemoSchVersion.Name);
            }

            WriteNamespaces();

            _state = InternalState.InProgress;

            return this;
        }

        private void WriteNamespaces()
        {
            foreach (KeyValuePair<string, string> kvp in Namespaces.GetNamespaces())
                _meMoXmlWriter.WriteNamespace(kvp.Key, kvp.Value);
        }

        private enum InternalState
        {
            Initialized,

            InProgress,

            Closed
        }
    }
}