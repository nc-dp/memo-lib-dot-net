﻿using System;
using System.Collections.Generic;
using System.IO;
using Dk.Digst.Digital.Post.Memolib.Model;
using Dk.Digst.Digital.Post.Memolib.Util;

namespace Dk.Digst.Digital.Post.Memolib.Writer
{
    public class FileContentLoader
    {
        public List<IFileContentResolver> Resolvers { get; }

        public FileContentLoader()
        {
            Resolvers = new List<IFileContentResolver> {new DefaultFileContentResolver()};
        }

        public FileContentLoader(List<IFileContentResolver> resolvers)
        {
            Resolvers = resolvers;
        }

        public Stream ResolveContent(IFileContent fileContent)
        {
            if (fileContent == null) throw new ArgumentNullException(nameof(fileContent));
            if (fileContent.CanBeStreamed)
                return fileContent.StreamContent();
            foreach (IFileContentResolver resolver in Resolvers)
            {
                Optional<Stream> optionalStream = resolver.Resolve(fileContent.Location);
                if (optionalStream.IsPresent())
                    return optionalStream.Get();
            }
            throw new InvalidOperationException("Content could not be resolved");
        }
    }
}