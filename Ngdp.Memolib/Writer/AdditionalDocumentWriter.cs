﻿using System;
using Dk.Digst.Digital.Post.Memolib.Model;
using Action = Dk.Digst.Digital.Post.Memolib.Model.Action;

namespace Dk.Digst.Digital.Post.Memolib.Writer
{
    public class AdditionalDocumentWriter
    {
        private readonly FileWriter _fileWriter;

        private readonly MeMoXmlWriter _xmlWriter;

        public AdditionalDocumentWriter(MeMoXmlWriter xmlWriter)
        {
            _xmlWriter = xmlWriter;
            _fileWriter = new FileWriter(_xmlWriter);
        }

        /// <summary>
        ///     Writes an additional document
        /// </summary>
        /// <param name="document">the additional document</param>
        public void Write(AdditionalDocument document)
        {
            _xmlWriter.WriteStartElement(nameof(AdditionalDocument));
            _xmlWriter.WriteElement(nameof(document.additionalDocumentID), document.additionalDocumentID);
            _xmlWriter.WriteElement(nameof(document.label), document.label);

            WriteFiles(document.File);
            WriteActions(document.Action);

            _xmlWriter.WriteEndElement();
        }

        private void WriteActions(Action[] actions)
        {
            if (actions == null) return;
            foreach (Action action in actions)
                _xmlWriter.WriteXmlType(nameof(AdditionalDocument.Action), action);
        }

        private void WriteFiles(File[] files)
        {
            if (files == null) throw new ArgumentNullException(nameof(files));
            foreach (File file in files)
                _fileWriter.WriteFile(file);
        }
    }
}