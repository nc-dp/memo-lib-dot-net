﻿using System.IO;
using Dk.Digst.Digital.Post.Memolib.Util;

namespace Dk.Digst.Digital.Post.Memolib.Writer
{
    public interface IFileContentResolver
    {
        Optional<Stream> Resolve(string location);
    }
}