﻿using System.IO;
using Dk.Digst.Digital.Post.Memolib.Util;

namespace Dk.Digst.Digital.Post.Memolib.Writer
{
    public class DefaultFileContentResolver : IFileContentResolver
    {
        public Optional<Stream> Resolve(string location)
        {
            return File.Exists(location)
                ? Optional<Stream>.Of(File.OpenRead(location))
                : Optional<Stream>.Empty();
        }
    }
}