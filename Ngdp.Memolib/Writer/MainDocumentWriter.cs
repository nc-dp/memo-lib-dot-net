﻿using System;
using Dk.Digst.Digital.Post.Memolib.Model;
using Action = Dk.Digst.Digital.Post.Memolib.Model.Action;

namespace Dk.Digst.Digital.Post.Memolib.Writer
{
    public class MainDocumentWriter
    {
        private readonly FileWriter _fileWriter;

        private readonly MeMoXmlWriter _xmlWriter;

        public MainDocumentWriter(MeMoXmlWriter xmlWriter)
        {
            _xmlWriter = xmlWriter;
            _fileWriter = new FileWriter(_xmlWriter);
        }

        /// <summary>
        ///     Writes the main document
        /// </summary>
        /// <param name="document">the main document</param>
        public void Write(MainDocument document)
        {
            _xmlWriter.WriteStartElement(nameof(MainDocument));
            _xmlWriter.WriteElement(nameof(document.mainDocumentID), document.mainDocumentID);
            _xmlWriter.WriteElement(nameof(document.label), document.label);

            WriteFiles(document.File);
            WriteActions(document.Action);

            _xmlWriter.WriteEndElement();
        }

        private void WriteActions(Action[] actions)
        {
            if (actions == null) return;
            foreach (Action action in actions)
                _xmlWriter.WriteXmlType(nameof(MainDocument.Action), action);
        }

        private void WriteFiles(File[] files)
        {
            if (files == null) throw new ArgumentNullException(nameof(files));
            foreach (File file in files)
                _fileWriter.WriteFile(file);
        }
    }
}