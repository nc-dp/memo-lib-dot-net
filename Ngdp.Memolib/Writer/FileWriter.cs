﻿using System.IO;
using System.Security.Cryptography;
using File = Dk.Digst.Digital.Post.Memolib.Model.File;

namespace Dk.Digst.Digital.Post.Memolib.Writer
{
    public class FileWriter
    {
        protected readonly MeMoXmlWriter Writer;

        public FileWriter(MeMoXmlWriter writer)
        {
            Writer = writer;
        }

        /// <summary>
        ///     Writes a file to the writer
        /// </summary>
        /// <param name="file">the file</param>
        public void WriteFile(File file)
        {
            using (Stream stream = Writer.FileContentLoader.ResolveContent(file.GetContent()))
            {
                Writer.WriteStartElement(nameof(File));
                Writer.WriteElement(nameof(file.encodingFormat), file.encodingFormat);
                Writer.WriteElement(nameof(file.filename), file.filename);
                Writer.WriteElement(nameof(file.language), file.language);
                WriteContent(file, stream);
                Writer.WriteEndElement();
            }
        }

        private void WriteContent(File file, Stream stream)
        {
            Writer.WriteStartElement(nameof(File.content));
            Writer.WriteRaw("");
            Writer.Flush();

            WriteFileContentToStreamAsBase64(file, stream);

            Writer.WriteFullEndElement();
        }

        private void WriteFileContentToStreamAsBase64(File file, Stream stream)
        {
            if (file.GetContent().IsBase64Encoded)
                stream.CopyTo(Writer.Stream);
            else
                new CryptoStream(stream, new ToBase64Transform(), CryptoStreamMode.Read).CopyTo(Writer.Stream);
        }
    }
}