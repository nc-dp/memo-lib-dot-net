﻿using System;
using Dk.Digst.Digital.Post.Memolib.Exception;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Writer
{
    /// <inheritdoc />
    /// <summary>MeMoStreamWriter supports writing a MeMo message as a stream</summary>
    public interface IMeMoStreamWriter : IDisposable
    {
        FileContentLoader FileContentLoader { get; }

        /// <summary>Writes the <paramref name="message" /> element to the stream</summary>
        /// <param name="message">The message to be written</param>
        /// <exception cref="MeMoWriteException">If an error occurs while writing the message</exception>
        void Write(Message message);

        /// <summary>Used to determine if the MeMoStreamWriter is closed</summary>
        /// <returns>A bool indicating if the writer is closed</returns>
        bool IsClosed();
    }
}