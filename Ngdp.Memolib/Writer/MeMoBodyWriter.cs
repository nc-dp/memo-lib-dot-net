﻿using System;
using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Writer
{
    public class MeMoBodyWriter
    {
        private readonly MeMoXmlWriter _xmlWriter;

        public MeMoBodyWriter(MeMoXmlWriter xmlWriter)
        {
            _xmlWriter = xmlWriter;
        }

        /// <summary>
        ///     Writes the messagebody
        /// </summary>
        /// <param name="messageBody">the messagebody</param>
        public void Write(MessageBody messageBody)
        {
            _xmlWriter.WriteStartElement(nameof(MessageBody));
            _xmlWriter.WriteDateTimeElement(nameof(messageBody.createdDateTime),
                messageBody.createdDateTime);

            WriteMainDocument(messageBody.MainDocument);
            WriteAdditionalDocuments(messageBody.AdditionalDocument);
            WriteTechnicalDocuments(messageBody.TechnicalDocument);

            _xmlWriter.WriteEndElement();
        }

        private void WriteAdditionalDocuments(AdditionalDocument[] additionalDocuments)
        {
            if (additionalDocuments == null) return;
            AdditionalDocumentWriter additionalDocumentWriter = new AdditionalDocumentWriter(_xmlWriter);
            foreach (AdditionalDocument document in additionalDocuments)
                additionalDocumentWriter.Write(document);
        }

        private void WriteTechnicalDocuments(TechnicalDocument[] technicalDocuments)
        {
            if (technicalDocuments == null) return;
            TechnicalDocumentWriter technicalDocumentWriter = new TechnicalDocumentWriter(_xmlWriter);
            foreach (TechnicalDocument document in technicalDocuments)
                technicalDocumentWriter.Write(document);
        }

        private void WriteMainDocument(MainDocument mainDocument)
        {
            if (mainDocument == null) throw new ArgumentNullException(nameof(mainDocument));
            MainDocumentWriter mainDocumentWriter = new MainDocumentWriter(_xmlWriter);
            mainDocumentWriter.Write(mainDocument);
        }
    }
}