﻿using Dk.Digst.Digital.Post.Memolib.Model;

namespace Dk.Digst.Digital.Post.Memolib.Writer
{
    public class TechnicalDocumentWriter
    {
        private readonly FileWriter _fileWriter;

        private readonly MeMoXmlWriter _xmlWriter;

        public TechnicalDocumentWriter(MeMoXmlWriter xmlWriter)
        {
            _xmlWriter = xmlWriter;
            _fileWriter = new FileWriter(_xmlWriter);
        }

        /// <summary>
        ///     Writes a technical document
        /// </summary>
        /// <param name="document">the technical document</param>
        public void Write(TechnicalDocument document)
        {
            _xmlWriter.WriteStartElement(nameof(TechnicalDocument));
            _xmlWriter.WriteElement(nameof(document.technicalDocumentID), document.technicalDocumentID);
            _xmlWriter.WriteElement(nameof(document.label), document.label);

            foreach (File file in document.File)
                _fileWriter.WriteFile(file);

            _xmlWriter.WriteEndElement();
        }
    }
}