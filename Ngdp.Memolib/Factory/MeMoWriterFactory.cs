﻿using System.IO;
using Dk.Digst.Digital.Post.Memolib.Writer;

namespace Dk.Digst.Digital.Post.Memolib.Factory
{
    /// <summary>
    ///     MeMoWriterFactory can be used to create an instance of an IMeMoStreamWriter
    /// </summary>
    public static class MeMoWriterFactory
    {
        /// <summary>
        ///     This method creates a IMeMoStreamWriter which will use the provided OutputStream
        ///     to write a MeMo message.
        /// </summary>
        /// <param name="stream">the stream to be written to</param>
        /// <returns>IMeMoStreamWriter</returns>
        public static IMeMoStreamWriter CreateWriter(Stream stream)
        {
            return new MeMoStreamWriterImpl(stream, new FileContentLoader());
        }

        /// <summary>
        ///     This method creates a IMeMoStreamWriter which will use the provided OutputStream
        ///     to write a MeMo message.
        /// </summary>
        /// <param name="stream">the stream to be written to</param>
        /// <param name="fileContentLoader">a loader which can be used to load external file content resources</param>
        /// <returns>IMeMoStreamWriter</returns>
        public static IMeMoStreamWriter CreateWriter(Stream stream, FileContentLoader fileContentLoader)
        {
            return new MeMoStreamWriterImpl(stream, fileContentLoader);
        }
    }
}